//
//  Global.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import UIKit

class Global{
    static let shared = Global()
    static let selectedPage = HomeViewController()
    
    var accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoiamVyc2Fta3Jpc0BnbWFpbC5jb20iLCJmaXJzdG5hbWUiOiJTYW11ZWwiLCJsYXN0bmFtZSI6IktyaXNuYSIsInRva2VuIjoiSFJOWXFwQkM0TWp5N2tqWHlvNnllV1VaQTh3Unhzenpra3l1azkxc0JxaDdaOFhEeU5RZGxlWVFTaUlRRzBxemxSdnFIeTV4eUl4QUViYzlZZXNHSWZGU0k4SmdmZUlHZ042eUh5VG9WaGN0TGxscHNmeWZ6RkVQT2dlM3JRSENZZkdNVzlsUXNYRzFOTmVXdkVkMWZxazR6NUR2c2xhUmxaeTZpRjZNUnZpYzA5SWdSdTA2clBiNktuZ1R6RkFqRjJBSm9aUmtwdnF3RUtqTldURXowRjZDNmNCb2xqcmI0SUVzbG40S2pLWmMySjVRQldwWXAwamZIR2V5dE9HIiwibG9naW5kYXRldGltZSI6IjIwMjAtMDUtMTUgMDA6NTk6MDIiLCJpYXQiOjE1ODk0NzkxNDIsImlzcyI6IlBULiBJbXBlcml1bSBIYXBweSBQdXBweSJ9.aG4PYarPOmDy30mekYRE49pgPechfG-1iMS0xCHUA5ve5vhwpKMEJbvMRTIS-mqzTbW7l-XUWak3vPxjmY25JA"
    
    func getEnvironment() -> String! {
        var environment:String = Environment.Release
        if (Bundle.main.object(forInfoDictionaryKey: "Environment") != nil) {
            environment = Bundle.main.object(forInfoDictionaryKey: "Environment") as! String
        }
        return environment
    }
    
    func generateQRCode(from string:String, completion:@escaping(UIImage)->Void){
        let data = string.data(using: .ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 5, y: 5)
            if let output = filter.outputImage?.transformed(by: transform){
                completion(UIImage(ciImage: output))
            }
        }
    }
    
    func getCurrentTimeStampWOMiliseconds() -> String {
        let dateToConvert = Date()
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd"
        objDateformat.locale = Locale(identifier: "id_ID")
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
    
    func fromDateTimeToDate(_ dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let toDate = dateFormatter.date(from: dateTime)
        dateFormatter.dateFormat = "dd LLLL yyyy"
        let date = dateFormatter.string(from: toDate!)
        return date
    }
    
    func fromDateTimeToTime(_ dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let toTime = dateFormatter.date(from: dateTime)
        dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.string(from: toTime!)
        return time
    }
    
    func fromddLLLLyyyyToyyyyMMdd(_ dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd LLLL yyyy"
        let toDate = dateFormatter.date(from: dateTime)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: toDate!)
        return date
    }
    
    //From time format HH:mm:ss to HH:mm
    func fromHHmmssToHHmm(_ HHmmss: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let toHHmm = dateFormatter.date(from: HHmmss)
        dateFormatter.dateFormat = "HH:mm"
        let HHmm = dateFormatter.string(from: toHHmm!)
        return HHmm
    }
    
    func getCurrentDateAndTime() -> Date {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let stringdate = dateFormatter.string(from: date)
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let dateType = dateFormatter.date(from: stringdate)
        return dateType!
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    func getCurrentTime() -> String {
        let time = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let stringTime = dateFormatter.string(from: time)
        return stringTime
    }
    
    func getCurrentTimeOnlyHour() -> Int {
        let time = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        let stringTime = dateFormatter.string(from: time)
        return Int(stringTime)!
    }
    
    func getTimeIntervalFromNow(_ date: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let nextTime = dateFormatter.date(from: date)
        let timeInterval = nextTime!.timeIntervalSince(getCurrentDateAndTime())
        return Int(timeInterval)
    }
    
    func addOneHourFromNow(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: date){
            let calendar = Calendar.current
            if let newData = calendar.date(byAdding: .hour, value: 1, to: date){
                return dateFormatter.string(from: newData)
            }
        }
        
        return ""
    }
    
    func incrementDate(_ date: String, _ value:Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: date){
            let calendar = Calendar.current
            if let newData = calendar.date(byAdding: .hour, value: value, to: date){
                return dateFormatter.string(from: newData)
            }
        }
        
        return ""
    }
    
    func withUTC(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let tempDate = dateFormatter.date(from: date)
        let finalDate = dateFormatter.string(from: tempDate!)
        return finalDate
    }
    
    func willCloseTime(date: String, time: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"

        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let dateAndTime = dateFormatter.date(from: "\(date) \(time)")
        
        print(self.getCurrentDateAndTime())
        
        //* TESTING
//        let df = DateFormatter()
//        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date = df.date(from: "2021-05-28 16:36:00")
        
        var interval = self.getCurrentDateAndTime().timeIntervalSince(dateAndTime!)
        print("INTERVAL: \(interval)")
        if (interval > 0){
            interval = 86400 - interval
        }else{
            interval = interval * -1
        }
        
        return Int(interval)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
}
