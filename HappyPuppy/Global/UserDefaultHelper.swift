//
//  UserDefaultHelper.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

class UserDefaultHelper {
    static let shared = UserDefaultHelper()
    
    func setupUser(user:User){
        UserDefaults.standard.set(user.accessToken, forKey: "accessToken")
        UserDefaults.standard.set(user.bod, forKey: "bod")
        UserDefaults.standard.set(user.firstName, forKey: "firstName")
        UserDefaults.standard.set(user.lastName, forKey: "lastName")
        UserDefaults.standard.set(user.gender, forKey: "gender")
        UserDefaults.standard.set(user.memberId, forKey: "memberCode")
        UserDefaults.standard.set(user.phone, forKey: "phone")
        UserDefaults.standard.set(user.email, forKey: "email")
        UserDefaults.standard.set(user.point, forKey: "point")
        UserDefaults.standard.set(user.memberType, forKey: "memberType")
        UserDefaults.standard.set(user.photoUrl, forKey: "photoUrl")
        UserDefaults.standard.set(user.currentMember.name, forKey: "memberType")
        UserDefaults.standard.set(user.currentMember.color, forKey: "memberColor")
        UserDefaults.standard.set(user.isVerified, forKey: "emailStatus")
        UserDefaults.standard.set(user.isPhoneVerified, forKey: "phoneStatus")
        UserDefaults.standard.set(user.idCardStatus, forKey: "idCardStatus")
        UserDefaults.standard.set(user.idCardNote, forKey: "id_card_note")
        
        UserDefaults.standard.synchronize()
    }
    
    func updateUser(user:User){
        UserDefaults.standard.set(user.bod, forKey: "bod")
        UserDefaults.standard.set(user.firstName, forKey: "firstName")
        UserDefaults.standard.set(user.lastName, forKey: "lastName")
        UserDefaults.standard.set(user.gender, forKey: "gender")
        UserDefaults.standard.set(user.memberId, forKey: "memberCode")
        UserDefaults.standard.set(user.phone, forKey: "phone")
        UserDefaults.standard.set(user.email, forKey: "email")
        UserDefaults.standard.set(user.point, forKey: "point")
        UserDefaults.standard.set(user.memberType, forKey: "memberType")
        UserDefaults.standard.set(user.photoUrl, forKey: "photoUrl")
        UserDefaults.standard.set(user.currentMember.name, forKey: "memberType")
        UserDefaults.standard.set(user.currentMember.color, forKey: "memberColor")
        UserDefaults.standard.set(user.isVerified, forKey: "emailStatus")
        UserDefaults.standard.set(user.isPhoneVerified, forKey: "phoneStatus")
        UserDefaults.standard.set(user.idCardStatus, forKey: "idCardStatus")        
        UserDefaults.standard.set(user.idCardNote, forKey: "id_card_note")
        
        UserDefaults.standard.synchronize()
    }
    
    func setCurrentUser(data: Data){
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: "current_user")
        userDefaults.synchronize()
    }
    
    func getCurrentUser() -> User?{
        if let data = UserDefaults.standard.data(forKey: "current_user"){
            do {
                let userDetail = try JSONDecoder().decode(User.self, from: data)
                return userDetail
            }catch(let error){
                print(error)
                return nil
            }
        }
        return nil
    }
    
    
    func setRequireIDCard(isRequireIdCard:String){
        UserDefaults.standard.set(isRequireIdCard, forKey: "require_idCard")
    }
    
    func setToken(accessToken:String){
        UserDefaults.standard.set(accessToken, forKey: "accessToken")
    }
    
    func setFCMToken(fcmToken:String){
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
    }
    
    func setQrImage(qrData:Data?){
        UserDefaults.standard.set(qrData, forKey: "qrData")
    }
    
    func setSMUserInfo(firstName:String, lastName:String, email:String){
        UserDefaults.standard.set(firstName, forKey: "SMfirstName")
        UserDefaults.standard.set(lastName, forKey: "SMlastName")
        UserDefaults.standard.set(email, forKey: "SMemail")
        UserDefaults.standard.synchronize()
    }
    
    func setAppleIDUserInfo(firstName:String, lastName:String, email:String){
        UserDefaults.standard.set(firstName, forKey: "appleID_firstName")
        UserDefaults.standard.set(lastName, forKey: "appleID_lastName")
        UserDefaults.standard.set(email, forKey: "appleID_email")
    }
    
    func setUnreadCount(count:Int){
        UserDefaults.standard.set(count, forKey: "unread_count")
    }
    
    func updateUnreadCount(){
        var unreadCount = self.getUnreadCount()
        unreadCount -= 1
        self.setUnreadCount(count: unreadCount)
    }
    
    func getUnreadCount()->Int{
        return UserDefaults.standard.integer(forKey: "unread_count")
    }
    
    func getAppleIDInfo()->[String:String?]{
        let firstName = UserDefaults.standard.string(forKey: "appleID_firstName")
        let lastName = UserDefaults.standard.string(forKey: "appleID_lastName")
        let email = UserDefaults.standard.string(forKey: "appleID_email")
        let userInfo = [
            "firstName":firstName,
            "lastName":lastName,
            "email":email
        ]
        return userInfo
    }
    
    func getSMUserInfo()->[String:String?]{
        let firstName = UserDefaults.standard.string(forKey: "SMfirstName")
        let lastName = UserDefaults.standard.string(forKey: "SMlastName")
        let email = UserDefaults.standard.string(forKey: "SMemail")
        let userInfo = [
            "firstName":firstName,
            "lastName":lastName,
            "email":email
        ]
        return userInfo
    }
    
    func clearSMUserInfo(){
        UserDefaults.standard.set(nil, forKey: "SMfirstName")
        UserDefaults.standard.set(nil, forKey: "SMlastName")
        UserDefaults.standard.set(nil, forKey: "SMemail")
        UserDefaults.standard.synchronize()
    }
    
    func getEmailStatus()->Bool{
        return UserDefaults.standard.bool(forKey: "emailStatus")
    }
    
    func getPhoneStatus()->Bool{
        return UserDefaults.standard.bool(forKey: "phoneStatus")
    }
    
    func getQrImage()->Data?{
        UserDefaults.standard.data(forKey: "qrData")
    }
    
    func getMemberColor()->String{
        if let memberColor = UserDefaults.standard.string(forKey: "memberColor"){
            return memberColor
        }
        return ""
    }
    
    func getPhotoUrl()->String{
        if let photoUrl = UserDefaults.standard.string(forKey: "photoUrl"){
            return photoUrl
        }
        return ""
    }
    
    func getToken()->String{
        if let token = UserDefaults.standard.string(forKey: "accessToken"){
            return token
        }
        return ""
    }
    
    func getFCMToken(fcmToken:String)->String{
        if let fcmToken = UserDefaults.standard.string(forKey: "fcmToken"){
            return fcmToken
        }
        return ""
    }
    
    func getFirstName()->String{
        if let token = UserDefaults.standard.string(forKey: "firstName"){
            return token
        }
        return ""
    }
    
    func getLastName()->String{
        if let token = UserDefaults.standard.string(forKey: "lastName"){
            return token
        }
        return ""
    }
    
    func getGender()->String{
        if let token = UserDefaults.standard.string(forKey: "gender"){
            return token
        }
        return ""
    }
    
    func getBirthdate()->String{
        if let token = UserDefaults.standard.string(forKey: "bod"){
            return token
        }
        return ""
    }
    
    func getMemberCode()->String{
        if let token = UserDefaults.standard.string(forKey: "memberCode"){
            return token
        }
        return ""
    }
    
    func getPhone()->String{
        if let token = UserDefaults.standard.string(forKey: "phone"){
            return token
        }
        return ""
    }
    
    func getEmail()->String{
        if let token = UserDefaults.standard.string(forKey: "email"){
            return token
        }
        return ""
    }
    
    func getPoint()->String{
        if let token = UserDefaults.standard.string(forKey: "point"){
            return token
        }
        return "0"
    }
    
    func getMemberType()->String{
        if let token = UserDefaults.standard.string(forKey: "memberType"){
            return token
        }
        return ""
    }
    
    func getIdCardStatus()->String{
        if let idCardStatus = UserDefaults.standard.string(forKey: "idCardStatus"){
            return idCardStatus
        }
        return ""
    }
    
    func getIsRequireIdCard()->Bool{
        if let isRequireIdCard = UserDefaults.standard.string(forKey: "require_idCard"){
            return Bool(truncating: NSNumber(value: Int(isRequireIdCard)!))
        }
        return false
    }
    
    func getidCardNote()->String{
        if let idCardNote = UserDefaults.standard.string(forKey: "id_card_note"){
            return idCardNote
        }
        return ""
    }
    
    func clearUser(){
            
        UserDefaults.standard.set(nil, forKey: "accessToken")
        UserDefaults.standard.set(nil, forKey: "bod")
        UserDefaults.standard.set(nil, forKey: "firstName")
        UserDefaults.standard.set(nil, forKey: "lastName")
        UserDefaults.standard.set(nil, forKey: "gender")
        UserDefaults.standard.set(nil, forKey: "memberCode")
        UserDefaults.standard.set(nil, forKey: "phone")
        UserDefaults.standard.set(nil, forKey: "email")
        UserDefaults.standard.set(nil, forKey: "point")
        UserDefaults.standard.set(nil, forKey: "memberType")
        UserDefaults.standard.set(nil, forKey: "qrData")
        UserDefaults.standard.removeObject(forKey: "qrData")
        UserDefaults.standard.set(nil, forKey: "photoUrl")
        UserDefaults.standard.set(nil, forKey: "memberColor")
        
        self.setUnreadCount(count: 0)
        
        UserDefaults.standard.synchronize()
    }
}
