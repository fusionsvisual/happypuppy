//
//  AutomaticHeightTableView.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 26/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class AutomaticHeightTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
