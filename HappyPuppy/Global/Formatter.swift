//
//  Formatter.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 28/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

class Formatter{
    static let shared = Formatter()
    func ISOFormatter(dateStr:String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy HH:mm:ss"

        let date: Date = dateFormatterGet.date(from: dateStr)!
        return dateFormatterPrint.string(from:date)
    }
    
    func birthDateFormatter(getDateFormat:String, printDateFormat:String, dateStr:String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "id_ID")
        dateFormatterGet.dateFormat = getDateFormat

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "id_ID")
        dateFormatterPrint.dateFormat = printDateFormat

        let date: Date = dateFormatterGet.date(from: dateStr)!
        return dateFormatterPrint.string(from:date)
    }
    
    
    func priceFormatter(_ number:Int)->String{
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.numberStyle = .currency
        if let str = formatter.string(for: number){
            return str.replacingOccurrences(of: "Rp", with: "")
        }else{
            return ""
        }
    }
}
