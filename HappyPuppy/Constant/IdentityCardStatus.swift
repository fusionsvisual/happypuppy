//
//  IdentityCardStatus.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

enum IdentityCardStatus {
    static let NOT_VERIFIED = "0"
    static let WAITING = "1"
    static let APPROVED = "2"
    static let REJECTED = "3"
}
