//
//  ObserverName.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

struct ObserverName {
    static let SEE_ALL_OUTLET = NSNotification.Name(rawValue: "SEE_ALL_OUTLET")
    static let SET_OUTLET_CITY = NSNotification.Name(rawValue: "SET_OUTLET_CITY")
}
