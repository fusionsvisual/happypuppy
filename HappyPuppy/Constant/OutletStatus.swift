//
//  OutletStatus.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 29/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

struct OutletStatus {
    static let OPEN = 1
    static let CLOSE = 0
}
