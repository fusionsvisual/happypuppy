//
//  Boolean.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 02/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

enum Boolean {
    static let FALSE = 0
    static let TRUE = 1
}
