//
//  Color.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/08/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    static var TEXT_SECONDARY = UIColor.init(hexString: "#9E9E9E")
    static var BUTTON_PRIMARY = UIColor(red: 202/255, green: 76/255, blue: 121/255, alpha: 1)
    static var PRIMARY = UIColor(red: 68/255, green: 90/255, blue: 166/255, alpha: 1)
    static var DISABLED = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
    
    struct VERIFICATION {
        static var DEFAULT = UIColor(red: 72/255, green: 68/255, blue: 67/255, alpha: 1)
        static var EMAIL = UIColor(red: 178/255, green: 222/255, blue: 250/255, alpha: 1)
        static var PHONE_NUMBER = UIColor(red: 94/255, green: 194/255, blue: 245/255, alpha: 1)
    }
    
    struct reservation{
        static var REJECTED = UIColor(red: 206/255, green: 84/255, blue: 67/255, alpha: 1)
    }
}
