//
//  String+Ext.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

extension String
{
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}
