//
//  DateFormatter+Ext.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 12/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

extension DateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}
