//
//  DelayConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct DelayConstant {
    static let LONG:TimeInterval = 2
    static let SHORT:TimeInterval = 1
}
