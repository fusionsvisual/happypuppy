//
//  Environment.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Environment {
    static let Develop = "Debug"
    static let Release = "Release"
}
