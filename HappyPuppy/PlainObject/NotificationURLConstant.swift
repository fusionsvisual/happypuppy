//
//  NotificationURLConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct NotificationURLConstant {
    static let ACCOUNT_VERIFICATION = "Verifikasi Nomer HP"
    static let BOTTLE_OTP = "Save OTP"
    static let PAYMENT = "Bayar"
}
