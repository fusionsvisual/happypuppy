//
//  ErrorMessage.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct ErrorMessage {
    static var SOMETHING_WRONG = "There is something wrong"
    static var UNAUTHORIZED = "Unauthorized"
    static var GOOGLE_NOT_REGISTERED = "GOOGLE NOT REGISTERED"
    
    static var EMAIL_AND_PHONE_NOT_VERIFIED = "Tolong verifikasi email/ HP terlebih dahulu sebelum mengakses fitur ini"
    static var PHONE_NOT_VERIFIED = "Tolong verifikasi HP terlebih dahulu sebelum mengakses fitur ini"
}
