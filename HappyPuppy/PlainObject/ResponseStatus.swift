//
//  ResponseStatus.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct ResponseStatus {
    static let SUCCESS = "success"
    static let FAILED = "failed"
}
