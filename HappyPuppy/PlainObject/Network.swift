//
//  Network.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

class Network {
    static let shared = Network()
    
    static let ASSET_URL = "https://adm.happypuppy.id"
    
    func getBaseURL() -> String {
//        if (Global.shared.getEnvironment() == Environment.Release) {
//            return "https://club.happypuppy.id"
//        } else {
//            return "http://happup.suspis.com"
//        }
        return "https://club.happypuppy.id"
    }
    
    func getBaseURLAPI() -> String {
//        return "https://club.happypuppy.id/api"
//        if (Global.shared.getEnvironment() == Environment.Release) {
            return "https://club.happypuppy.id/api"
//        } else {
//            return "http://happup.suspis.com/api"
//        }
    }
}
