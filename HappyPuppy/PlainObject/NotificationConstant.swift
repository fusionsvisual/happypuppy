//
//  NotificationConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 05/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct NotificationConstant {
    static var GOOGLE_LOGIN:NSNotification.Name = NSNotification.Name(rawValue: "GOOGLE_LOGIN")
    static let REFRESH_PROFILE = NSNotification.Name(rawValue: "REFRESH_PROFILE")
}
