//
//  PointTypeConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct PointTypeConstant {
    static let CLAIM = "claim"
    static let ADD = "add"
}
