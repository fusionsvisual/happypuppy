//
//  SocialMediaConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 02/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct SocialMediaConstant {
    static var accessToken:String?
    static var provider:String?
    
    // For AppleId
    static var userString:String?
    
    static func clear(){
        self.accessToken = nil
        self.provider = nil
        self.userString = nil
    }
}
