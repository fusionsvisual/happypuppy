//
//  CountryCode.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct CountryCode{
    var flag:String = ""
    var name:String = ""
    var codeId:String = ""
    var codeNumber:String = ""
    
    init(flag:String, name:String, codeId:String, codeNumber:String) {
        self.flag = flag
        self.name = name
        self.codeId = codeId
        self.codeNumber = codeNumber
    }
}
