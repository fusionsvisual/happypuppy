//
//  GenderConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
struct GenderConstant {
    static let PRIA = "Pria"
    static let WANITA = "Wanita"
}
