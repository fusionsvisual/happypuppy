//
//  MemberType.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 22/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct MemberType {
    static let BLUE = "BLUE"
    static let SILVER = "SILVER"
    static let GOLD = "GOLD"
}
