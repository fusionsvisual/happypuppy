//
//  ShimmerConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 06/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct ShimmerConstant {
    static let TOTAL_CELL = 5
}
