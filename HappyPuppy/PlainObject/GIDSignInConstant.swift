//
//  GIDSignInConstant.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 05/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct GIDSignInConstant {
    static let CLIENT_ID = "770776259770-qmo4oavuj41qdo8trhi43hgdo5qk1hve.apps.googleusercontent.com"
    static let CLIENT_ID_REVERSED = "com.googleusercontent.apps.770776259770-9fp8ghahpbfdb6od5s3pagdjpfcb6g21"
}
