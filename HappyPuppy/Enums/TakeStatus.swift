//
//  TakeStatus.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/02/23.
//  Copyright © 2023 FusionsVisual. All rights reserved.
//

import Foundation

enum TakeStatus: Int {
    case myself = 0
    case otherPerson = 1
}

extension TakeStatus {
    var displayTitles: [TakeStatus: String] {[
        .myself: "Sendiri",
        .otherPerson: "Orang Lain"
    ]}

    var displayTitle: String {
        displayTitles[self] ?? ""
    }
}
