//
//  Reservation.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Reservation: Decodable {
    var outlet: String? = ""
    var reservationId: String? = ""
    var date: String? = ""
    var shift: String? = ""
    var member: String = ""
    var name:String? = ""
    var phoneNumber: String? = ""
    var reservationDate: String? = ""
    var hourDuration: Int?
    var minuteDuration: Int?
    var checkoutDate: String? = ""
    var roomType: Int?
    var roomTypeName: String? = ""
    var room: String? = ""
    var downPayment: Int?
    var chTime: String? = ""
    var chUsr: Int?
    var posted: String? = ""
    var reconfirmationNote: String? = ""
    var reconfirmationHour: String? = ""
    var export: String? = ""
    var idPayment: Int?
    var transactionDate: String? = ""
    var status: String?
    var type: String?
    var note: String? = ""
    var photo: String? = ""
    var bankName: String? = ""
    var bankNumber: String? = ""
    var bankAccountName: String? = ""
    var price: Int?
    var payDate: String? = ""
    var rejectInfo: String? = ""
    var outletName: String? = ""
    var outletImage: String? = ""
    var outletAddress: String? = ""
    var outletPhone: String? = ""
    var roomDetail: RoomDetail?
    var outletData: SingleOutlet?
    
    var priceDetails: [PriceDetail] = []
    
    /*
     Payment Variables
     */
    var paymentProofUrl:String?
    var paymentDate:String?
    
    enum CodingKeys: String, CodingKey {
        case outlet = "Outlet"
        case reservation = "Reservation"
        case date = "Date"
        case shift = "Shift"
        case member = "Member"
        case name = "Name"
        case phoneNumber = "Telepon"
        case reservationDate = "ReservationDate"
        case hourDuration = "HourDuration"
        case minuteDuration = "MinuteDuration"
        case checkoutDate = "CheckoutDate"
        case roomType = "RoomType"
        case roomTypeName = "RoomTypeName"
        case room = "Room"
        case downPayment = "DownPayment"
        case chTime = "CHTime"
        case chUsr = "CHusr"
        case posted = "Posted"
        case reconfirmationNote = "ReconfirmationNote"
        case reconfirmationHour = "ReconfirmationHour"
        case export = "Export"
        case idPayment = "IDPayment"
        case transactionDate = "TransactionDate"
        case status = "Status_"
        case type = "Type_"
        case note = "Note"
        case bankName = "BankName"
        case bankNumber = "BankNumber"
        case bankAccountName = "BankAccountName"
        case price = "Price"
        case payDate = "PayDate"
        case rejectInfo = "RejectInfo"
        case outletName = "outlet_name"
        case outletImage = "outlet_image"
        case outletAddress = "outlet_address"
        case outletPhone = "outlet_phone"
        case roomDetail = "room_details"
        case roomDetail02 = "room_detail"
        case outletData = "outlet_data"
        case priceDetails = "price_detail"
        
        /*
         Payment Coding Keys
         */
        case paymentProofImageUrl = "Photo"
        case paymentProofUploadDate = "PhotoUploadDate"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        outlet = try container.decode(String?.self, forKey: .outlet)
        reservationId = try container.decode(String?.self, forKey: .reservation)
        if let date = try container.decodeIfPresent(String.self, forKey: .date) {
            self.date = date
        }
        if let shift = try container.decodeIfPresent(String?.self, forKey: .shift) {
            self.shift = shift
        }
        member = try container.decode(String.self, forKey: .member)
        name = try container.decode(String?.self, forKey: .name)
        phoneNumber = try container.decode(String?.self, forKey: .phoneNumber)
        reservationDate = try container.decode(String?.self, forKey: .reservationDate)
        hourDuration = try container.decode(Int?.self, forKey: .hourDuration)
        minuteDuration = try container.decode(Int?.self, forKey: .minuteDuration)
        checkoutDate = try container.decode(String?.self, forKey: .checkoutDate)
        roomType = try container.decode(Int?.self, forKey: .roomType)
        roomTypeName = try container.decode(String?.self, forKey: .roomTypeName)
        if let room = try container.decodeIfPresent(String?.self, forKey: .room) {
            self.room = room
        }
        downPayment = try container.decode(Int?.self, forKey: .downPayment)
        if let chTime = try container.decodeIfPresent(String?.self, forKey: .chTime) {
            self.chTime = chTime
        }
        if let chUsr = try container.decodeIfPresent(Int?.self, forKey: .chUsr) {
            self.chUsr = chUsr
        }else{
            if let chUsr = try container.decodeIfPresent(String?.self, forKey: .chUsr) {
                self.chUsr = Int(chUsr!)
            }
        }
        if let posted = try container.decodeIfPresent(String?.self, forKey: .posted) {
            self.posted = posted
        }
        if let reconfirmationNote = try container.decodeIfPresent(String?.self, forKey: .reconfirmationNote) {
            self.reconfirmationNote = reconfirmationNote
        }
        if let reconfirmationHour = try container.decodeIfPresent(String?.self, forKey: .reconfirmationHour) {
            self.reconfirmationHour = reconfirmationHour
        }
        if let export = try container.decodeIfPresent(String?.self, forKey: .export) {
            self.export = export
        }
        if let idPayment = try container.decodeIfPresent(Int?.self, forKey: .idPayment) {
            self.idPayment = idPayment
        }
        transactionDate = try container.decode(String?.self, forKey: .transactionDate)
        
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
        
        if let type = try container.decodeIfPresent(Int.self, forKey: .type){
            self.type = String(type)
        }
        
        note = try container.decode(String?.self, forKey: .note)
        bankName = try container.decode(String?.self, forKey: .bankName)
        if let bankNumber = try container.decodeIfPresent(String?.self, forKey: .bankNumber) {
            self.bankNumber = bankNumber
        }
        
        if let bankAccountName = try container.decodeIfPresent(String?.self, forKey: .bankAccountName) {
            self.bankAccountName = bankAccountName
        }
        
        if let price = try container.decodeIfPresent(Int?.self, forKey: .price) {
            self.price = price
        }
        
        if let payDate = try container.decodeIfPresent(String?.self, forKey: .payDate) {
            self.payDate = payDate
        }
        
        if let rejectInfo = try container.decodeIfPresent(String?.self, forKey: .rejectInfo) {
            self.rejectInfo = rejectInfo
        }
        
        if let outletName = try container.decodeIfPresent(String?.self, forKey: .outletName){
            self.outletName = outletName
        }
        
        if let outletImage = try container.decodeIfPresent(String?.self, forKey: .outletImage){
            self.outletImage = outletImage
        }
        
        if let outletAddress = try container.decodeIfPresent(String?.self, forKey: .outletAddress){
            self.outletAddress = outletAddress
        }
        
        if let outletPhone = try container.decodeIfPresent(String?.self, forKey: .outletPhone){
            self.outletPhone = outletPhone
        }
        
        if let roomDetail = try container.decodeIfPresent(RoomDetail.self, forKey: .roomDetail){
            self.roomDetail = roomDetail
        }else{
            if let roomDetail = try container.decodeIfPresent(RoomDetail.self, forKey: .roomDetail02){
                self.roomDetail = roomDetail
            }
        }
        
        if let outletData = try container.decodeIfPresent(SingleOutlet?.self, forKey: .outletData) {
            self.outletData = outletData
        }
        
        if let priceDetails = try container.decodeIfPresent([PriceDetail].self, forKey: .priceDetails){
            self.priceDetails = priceDetails
        }
        
        /*
         Payment Decode Init
         */
        
        if let paymentProofUrl = try container.decodeIfPresent(String.self, forKey: .paymentProofImageUrl){
            self.paymentProofUrl = paymentProofUrl
        }
        
        if let paymentDate = try container.decodeIfPresent(String.self, forKey: .paymentProofUploadDate){
            self.paymentDate = paymentDate
        }
    }
}

struct SingleOutlet:Decodable{
    var id: Int?
    var code: String? = ""
    var brand: Int? = 0
    var name: String? = ""
    var address: String? = ""
    var telp: String? = ""
    var city: String? = ""
    var bigCity: Int? = 0
    var email: String? = ""
    var facebook: String? = ""
    var instagram: String? = ""
    var outletLat: String? = ""
    var outletLong: String? = ""
    var outletMapDesc: String? = ""
    var entryUser: Int? = 0
    var entryDate: String? = ""
    var createUser: Int?
    var createDate: String? = ""
    var photo: String? = ""
    var status: String? = ""
    var tax: Int? = 0
    var service: Int?
    var bank: String? = ""
    var bankNumber: String? = ""
    var bankAccountName: String? = ""
    var timeZoneName: String? = ""
    var timeZone: String? = ""
    var qrisImageUrl: String?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case code = "Code"
        case brand = "Brand"
        case name = "Name"
        case address = "Address"
        case telp = "Telp"
        case city = "City"
        case bigCity = "BigCity"
        case email = "Email"
        case facebook = "Facebook"
        case instagram = "Instagram"
        case outletLat = "OutletLat"
        case outletLong = "OutletLong"
        case outletMapDesc = "OutletMapDesc"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createUser = "CreateUser"
        case createDate = "CreateDate"
        case photo = "Photo"
        case status = "Status_"
        case tax = "Tax"
        case service = "Service"
        case bank = "Bank"
        case bankNumber = "BankNumber"
        case bankName = "BankName"
        case timeZoneName = "TimeZoneName"
        case timeZone = "TimeZone"
        case qrisImageUrl = "PhotoQRIS"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(Int.self, forKey: .id)
        code = try container.decode(String.self, forKey: .code)
        brand = try container.decode(Int.self, forKey: .brand)
        name = try container.decode(String.self, forKey: .name)
        address = try container.decode(String.self, forKey: .address)
        telp = try container.decode(String.self, forKey: .telp)
        city = try container.decode(String.self, forKey: .city)
        bigCity = try container.decode(Int.self, forKey: .bigCity)
        email = try container.decode(String.self, forKey: .email)
        facebook = try container.decode(String?.self, forKey: .facebook)
        instagram = try container.decode(String?.self, forKey: .instagram)
        outletLat = try container.decode(String.self, forKey: .outletLat)
        outletLong = try container.decode(String.self, forKey: .outletLong)
        outletMapDesc = try container.decode(String.self, forKey: .outletMapDesc)
        if let entryUser = try container.decodeIfPresent(Int.self, forKey: .entryUser){
            self.entryUser = entryUser
        }
        entryDate = try container.decode(String.self, forKey: .entryDate)
        createUser = (try container.decode(Int?.self, forKey: .createUser))
        createDate = try container.decode(String?.self, forKey: .createDate)
        photo = try container.decode(String.self, forKey: .photo)
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
        if let tax = try container.decodeIfPresent(Int.self, forKey: .tax){
            self.tax = tax
        }
        if let service = try container.decodeIfPresent(Int.self, forKey: .service){
            self.service = service
        }
        bank = try container.decode(String.self, forKey: .bank)
        bankNumber = try container.decode(String.self, forKey: .bankNumber)
        bankAccountName = try container.decode(String.self, forKey: .bankName)
        timeZoneName = try container.decode(String.self, forKey: .timeZoneName)
        timeZone = try container.decode(String.self, forKey: .timeZone)
        
        if let qrisImageUrl = try container.decodeIfPresent(String?.self, forKey: .qrisImageUrl){
            self.qrisImageUrl = qrisImageUrl
        }

    }
}

struct RoomDetail: Decodable {
    let code: String?
    let id: Int?
    let roomType: String?
    let photo: String?
    let pax: Int?
    let status: Int?
    let description: String?
    let available: Int?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case id = "ID"
        case roomType = "RoomType"
        case photo = "Photo"
        case pax = "Pax"
        case status = "Status"
        case description = "Description"
        case available = "Available"
    }
}

struct ReservationList: Decodable {
    let reservationList: [Reservation]
    
    enum CodingKeys: String, CodingKey {
        case reservationList = "data"
    }
}

struct ReservationResponse: Decodable {
    let data: ReservationList
}

struct ReservationDetailResp: Decodable {
    let data: Reservation
}

struct ReservationDetailResponse: Decodable {
    let data: ReservationDetailResp
}


struct SingleReservationDetail: Decodable {
    var outlet: String? = ""
    var reservation: String? = ""
    var date: String? = ""
    var shift: String? = ""
    var member: String = ""
    var name:String? = ""
    var phoneNumber: String? = ""
    var reservationDate: String? = ""
    var hourDuration: Int?
    var minuteDuration: Int?
    var checkoutDate: String? = ""
    var roomType: Int?
    var roomTypeName: String? = ""
    var room: String? = ""
    var downPayment: Int?
    var chTime: String? = ""
    var chUsr: Int?
    var posted: String? = ""
    var reconfirmationNote: String? = ""
    var reconfirmationHour: String? = ""
    var export: String? = ""
    var idPayment: Int?
    var transactionDate: String? = ""
    var status: String? = ""
    var type: String?
    var note: String? = ""
    
    var paymentProofImageUrl: String? = ""
    var paymentProofUploadDate: String? = ""
    
    var bankName: String? = ""
    var bankNumber: String? = ""
    var bankAccountName: String? = ""
    var price: Int?
    var payDate: String? = ""
    var rejectInfo: String? = ""
    var outletData: SingleOutlet?
    var roomDetail: RoomDetail?
    var priceDetail: [PriceDetail]?
    var totalPrice: Int?
    var reservation_date: String? = ""
    var checkInTime: String? = ""
    var checkOutTime: String? = ""
    
    enum CodingKeys: String, CodingKey {
        case outlet = "Outlet"
        case reservation = "Reservation"
        case date = "Date"
        case shift = "Shift"
        case member = "Member"
        case name = "Name"
        case phoneNumber = "Telepon"
        case reservationDate = "ReservationDate"
        case hourDuration = "HourDuration"
        case minuteDuration = "MinuteDuration"
        case checkoutDate = "CheckoutDate"
        case roomType = "RoomType"
        case roomTypeName = "RoomTypeName"
        case room = "Room"
        case downPayment = "DownPayment"
        case chTime = "CHTime"
        case chUsr = "CHusr"
        case posted = "Posted"
        case reconfirmationNote = "ReconfirmationNote"
        case reconfirmationHour = "ReconfirmationHour"
        case export = "Export"
        case idPayment = "IDPayment"
        case transactionDate = "TransactionDate"
        case status = "Status_"
        case type = "Type_"
        case note = "Note"
        case paymentProofImageUrl = "Photo"
        case paymentProofUploadDate = "PhotoUploadDate"
        case bankName = "BankName"
        case bankNumber = "BankNumber"
        case bankAccountName = "BankAccountName"
        case price = "Price"
        case payDate = "PayDate"
        case rejectInfo = "RejectInfo"
        case outletData = "outlet_data"
        case roomDetail = "room_detail"
        case priceDetail = "price_detail"
        case totalPrice = "total_price"
        case reservation_date = "reservation_date"
        case checkInTime = "check_in_time"
        case checkOutTime = "check_out_time"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        outlet = try container.decode(String?.self, forKey: .outlet)
        reservation = try container.decode(String?.self, forKey: .reservation)
        if let date = try container.decodeIfPresent(String.self, forKey: .date) {
            self.date = date
        }
        if let shift = try container.decodeIfPresent(String?.self, forKey: .shift) {
            self.shift = shift
        }
        member = try container.decode(String.self, forKey: .member)
        name = try container.decode(String?.self, forKey: .name)
        phoneNumber = try container.decode(String?.self, forKey: .phoneNumber)
        reservationDate = try container.decode(String?.self, forKey: .reservationDate)
        hourDuration = try container.decode(Int?.self, forKey: .hourDuration)
        minuteDuration = try container.decode(Int?.self, forKey: .minuteDuration)
        checkoutDate = try container.decode(String?.self, forKey: .checkoutDate)
        roomType = try container.decode(Int?.self, forKey: .roomType)
        roomTypeName = try container.decode(String?.self, forKey: .roomTypeName)
        if let room = try container.decodeIfPresent(String?.self, forKey: .room) {
            self.room = room
        }
        downPayment = try container.decode(Int?.self, forKey: .downPayment)
        if let chTime = try container.decodeIfPresent(String?.self, forKey: .chTime) {
            self.chTime = chTime
        }
        if let chUsr = try container.decodeIfPresent(Int?.self, forKey: .chUsr) {
            self.chUsr = chUsr
        }
        if let posted = try container.decodeIfPresent(String?.self, forKey: .posted) {
            self.posted = posted
        }
        if let reconfirmationNote = try container.decodeIfPresent(String?.self, forKey: .reconfirmationNote) {
            self.reconfirmationNote = reconfirmationNote
        }
        if let reconfirmationHour = try container.decodeIfPresent(String?.self, forKey: .reconfirmationHour) {
            self.reconfirmationHour = reconfirmationHour
        }
        if let export = try container.decodeIfPresent(String?.self, forKey: .export) {
            self.export = export
        }
        if let idPayment = try container.decodeIfPresent(Int?.self, forKey: .idPayment) {
            self.idPayment = idPayment
        }
        transactionDate = try container.decode(String?.self, forKey: .transactionDate)
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
        
        if let type = try container.decodeIfPresent(Int.self, forKey: .type){
            self.type = String(type)
        }
        
        note = try container.decode(String?.self, forKey: .note)
        self.paymentProofImageUrl = try container.decode(String?.self, forKey: .paymentProofImageUrl)
        self.paymentProofUploadDate = try container.decode(String?.self, forKey: .paymentProofUploadDate)
        bankName = try container.decode(String?.self, forKey: .bankName)
        if let bankNumber = try container.decodeIfPresent(String?.self, forKey: .bankNumber) {
            self.bankNumber = bankNumber
        }
        if let bankAccountName = try container.decodeIfPresent(String?.self, forKey: .bankAccountName) {
            self.bankAccountName = bankAccountName
        }
        if let price = try container.decodeIfPresent(Int?.self, forKey: .price) {
            self.price = price
        }
        if let payDate = try container.decodeIfPresent(String?.self, forKey: .payDate) {
            self.payDate = payDate
        }
        if let rejectInfo = try container.decodeIfPresent(String?.self, forKey: .rejectInfo) {
            self.rejectInfo = rejectInfo
        }
        outletData = try container.decode(SingleOutlet?.self, forKey: .outletData)
        roomDetail = try container.decode(RoomDetail?.self, forKey: .roomDetail)
        priceDetail = try container.decode([PriceDetail]?.self, forKey: .priceDetail)
        if let totalPrice = try container.decodeIfPresent(Int?.self, forKey: .totalPrice){
            self.totalPrice = totalPrice
        }
        reservation_date = try container.decode(String?.self, forKey: .reservation_date)
        checkInTime = try container.decode(String?.self, forKey: .checkInTime)
        checkoutDate = try container.decode(String?.self, forKey: .checkoutDate)
    }
}

struct PriceDetail: Decodable{
    let outlet: String?
    let reservation: String?
    let dateTimeStart: String?
    let dateTimeFinish: String?
    let price: Int?
    let totalPrice: Int?
    let minutes: Int?
    let point: Int?
    let service: Int?
    let tax: Int?
    let timeDisplay: String?
    
    enum CodingKeys:String, CodingKey {
        case outlet = "Outlet"
        case reservation = "Title"
        case dateTimeStart = "DateTimeStart"
        case dateTimeFinish = "DateTimeFinish"
        case price = "Price"
        case totalPrice = "TotalPrice"
        case minutes = "Minutes"
        case point = "Point"
        case service = "Service"
        case tax = "Tax"
        case timeDisplay = "time_display"
    }
}

struct SingleRes: Decodable {
    var data: SingleReservationDetail
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct SingleReservationDetailResponse: Decodable {
    var data:SingleRes
}
