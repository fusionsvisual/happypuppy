//
//  Reward.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import UIKit

struct Reward:Decodable{
    var id:String? = ""
    var code:String? = ""
    var name:String? = ""
    var point:String? = ""
    var photoUrl:String? = ""
    var image:UIImage?
    var type:String? = ""
    
    var outlet:String? = ""
    var price:Int? = 0
    var status:Int? = -1
    var group:Int? = -1
    var stock:Int? = 0
    var description:String? = ""
    var entryUser:String? = ""
    var entryDate:String? = ""
    var createUser:Int?
    var createdDate:String? = ""
    
    /* Redeem List */
    var transactionDate:String? = ""
    var quantity:String? = ""
    var invoiceCode:String? = ""
    var outletData:Outlet?
//    var rewardData:Reward?
    
    enum CodingKeys:String, CodingKey {
        case code = "Code"
        case name = "Name"
        case point = "Point"
        case photoUrl = "Photo"
        case image
        case type = "Type_"
        
        /* Reward Detail */
        case id = "ID"
        case outlet = "Outlet"
        case price = "Price"
        case status = "Status"
        case group = "Group_"
        case stock = "Stock"
        case description = "Description"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createUser = "createUser"
        case createdDate = "createdDate"
        
        /* Redeem List */
        case transactionDate = "Date_"
        case nameMyRewardList = "Name_"
        case quantity = "Qty"
        case invoiceCode = "Invoice"
        case outletData = "outletData"
        case rewardData = "rewardData"
        
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let code = try container.decodeIfPresent(String.self, forKey: .code){
            self.code = code
        }
        
        if let name = try container.decodeIfPresent(String.self, forKey: .name){
            self.name = name
        }else{
            self.name = try container.decode(String.self, forKey: .nameMyRewardList)
        }
        
        do {
            if let point = try container.decodeIfPresent(String.self, forKey: .point){
                self.point = point
            }
        } catch {
            if let point = try container.decodeIfPresent(Int.self, forKey: .point){
                self.point = String(point)
            }
        }
        
        
        if let photoUrl = try container.decodeIfPresent(String.self, forKey: .photoUrl){
            self.photoUrl = photoUrl
        }
        
        
        do {
            if let type = try container.decodeIfPresent(String.self, forKey: .type){
                self.type = type
            }
        } catch {
            if let type = try container.decodeIfPresent(Int.self, forKey: .type){
                self.type = String(type)
            }
        }
        
        
        do {
            self.id = try container.decode(String.self, forKey: .id)
        } catch  {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        if let outlet = try container.decodeIfPresent(String.self, forKey: .outlet) {
            self.outlet = outlet
        }
        
        if let price = try container.decodeIfPresent(Int.self, forKey: .price) {
            self.price = price
        }
        
        if let status = try container.decodeIfPresent(Int.self, forKey: .status) {
            self.status = status
        }
        
        if let group = try container.decodeIfPresent(Int.self, forKey: .group) {
            self.group = group
        }
        
        if let stock = try container.decodeIfPresent(Int.self, forKey: .stock) {
            self.stock = stock
        }
        
        if let description = try container.decodeIfPresent(String.self, forKey: .description) {
            self.description = description
        }
        
        if let entryUser = try container.decodeIfPresent(String.self, forKey: .entryUser) {
            self.entryUser = entryUser
        }
        
        if let entryDate = try container.decodeIfPresent(String.self, forKey: .entryDate) {
            self.entryDate = entryDate
        }
        
        if let createUser = try container.decodeIfPresent(Int?.self, forKey: .createUser) {
            self.createUser = createUser
        }
        
        if let createdDate = try container.decodeIfPresent(String.self, forKey: .createdDate) {
            self.createdDate = createdDate
        }
        
        if let quantity = try container.decodeIfPresent(String.self, forKey: .quantity) {
            self.quantity = quantity
        }
        
        if let invoiceCode = try container.decodeIfPresent(String.self, forKey: .invoiceCode) {
            self.invoiceCode = invoiceCode
        }
        
        if let outletData = try container.decodeIfPresent(Outlet.self, forKey: .outletData) {
            self.outletData = outletData
        }
        
        if let transactionDate = try container.decodeIfPresent(String.self, forKey: .transactionDate){
            self.transactionDate = transactionDate
        }
        
//        if let rewardData = try container.decodeIfPresent(Reward.self, forKey: .rewardData){
//            self.rewardData = rewardData
//        }
    }
}

struct MyReward:Decodable{
    let invoiceCode:String?
    let quantity:Int?
    let transactionDate:String?
    let rewardData:Reward?
    let outletData:Outlet?
    
    enum CodingKeys:String, CodingKey {
        case invoiceCode = "invoice"
        case quantity = "Qty"
        case transactionDate = "Date_"
        case rewardData = "rewardData"
        case outletData = "outletData"
    }
}

struct MyRewardList:Decodable{
    let data:[MyReward]
}

struct MyRewardResponse:Decodable{
    let data:MyRewardList
}

struct RewardList:Decodable{
    let rewardList:[Reward]
    
    enum CodingKeys:String, CodingKey {
        case rewardList = "data"
    }
}

struct SingleReward:Decodable{
    let data:Reward
}

struct SingleRewardResponse:Decodable{
    let data:SingleReward
}

struct RewardListResponse:Decodable{
    let data:[Reward]
}

struct RewardResponse:Decodable{
    let data:RewardList
}
