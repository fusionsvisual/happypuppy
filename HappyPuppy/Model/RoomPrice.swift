//
//  RoomPrice.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 12/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

struct RoomPrice: Codable {
    
    var startTime:String = ""
    var endTime:String = ""
    var totalPrice:Int = 0
    
    enum CodingKeys:String, CodingKey {
        case startTime = "start"
        case endTime = "finish"
        case totalPrice = "totalPrice"
    }
}

struct RoomPriceResp: Codable{
    
    var roomPrices:[RoomPrice] = []
    
    enum CodingKeys:String, CodingKey {
        case roomPrices = "data"
    }
}
