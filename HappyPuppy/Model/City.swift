//
//  City.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 22/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct City: Decodable{
    let id: Int?
    let name: String?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case name = "Name"
    }
}

struct CityList: Decodable{
    var data:[City]
    
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}

struct CityResponse: Decodable {
    var data:CityList
}
