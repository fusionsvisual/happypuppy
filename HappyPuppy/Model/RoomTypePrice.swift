//
//  RoomTypePrice.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 08/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct RoomTypePrice:Decodable{
    let room:RoomType?
    let facility:[Facility]?
    let price:Int?

    enum CodingKeys:String, CodingKey {
        case room = "room"
        case facility = "facilities"
        case price = "price"
    }
}

struct RoomTypePriceResponse:Decodable{
    let data:[RoomTypePrice]
    
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}
