//
//  DetailOutlet.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 05/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct OutletDetail: Decodable{
    var id: String? = ""
    var code: String? = ""
    var brand: Int? = 0
    var name: String? = ""
    var address: String? = ""
    var telp: String? = ""
    var city: String? = ""
    var bigCity: Int? = 0
    var email: String? = ""
    var facebook: String? = ""
    var instagram: String? = ""
    var outletLat: String? = ""
    var outletLong: String? = ""
    var outletMapDesc: String? = ""
    var entryUser: Int? = 0
    var entryDate: String? = ""
    var createUser: Int? = 0
    var createDate: String? = ""
    var photo: String? = ""
    var status: Int? = 0
    var tax: Int? = 0
    var service: Int? = 0
    var bank: String? = ""
    var bankNumber: String? = ""
    var bankName: String? = ""
    var timeZoneName: String? = ""
    var timeZone: String? = ""
    var roomType: [RoomType]?
    var facility: [SingleFacility]?
    var brandData: BrandData?
    var gallery: [Gallery]?
    var schedule: [Schedule]?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case code = "Code"
        case brand = "Brand"
        case name = "Name"
        case address = "Address"
        case telp = "Telp"
        case city = "City"
        case bigCity = "BigCity"
        case email = "Email"
        case facebook = "Facebook"
        case instagram = "Instagram"
        case outletLat = "OutletLat"
        case outletLong = "OutletLong"
        case outletMapDesc = "OutletMapDesc"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createUser = "CreateUser"
        case createDate = "CreateDate"
        case photo = "Photo"
        case status = "Status_"
        case tax = "Tax"
        case service = "Service"
        case bank = "Bank"
        case bankNumber = "BankNumber"
        case bankName = "BankName"
        case timeZoneName = "TimeZoneName"
        case timeZone = "TimeZone"
        case roomType = "roomTypes"
        case facility = "facilities"
        case brandData = "brand"
        case gallery = "galleries"
        case schedule = "schedule"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.id = try container.decode(String.self, forKey: .id)
        } catch {
            self.id = String(try container.decode(Int.self, forKey: .id))
        }
        
        code = try container.decode(String.self, forKey: .code)
        brand = try container.decode(Int.self, forKey: .brand)
        if let name = try container.decodeIfPresent(String.self, forKey: .name){
            self.name = name
        }
        address = try container.decode(String.self, forKey: .address)
        telp = try container.decode(String.self, forKey: .telp)
        city = try container.decode(String.self, forKey: .city)
        bigCity = try container.decode(Int.self, forKey: .bigCity)
        email = try container.decode(String.self, forKey: .email)
        facebook = try container.decode(String?.self, forKey: .facebook)
        instagram = try container.decode(String?.self, forKey: .instagram)
        outletLat = try container.decode(String.self, forKey: .outletLat)
        outletLong = try container.decode(String.self, forKey: .outletLong)
        outletMapDesc = try container.decode(String.self, forKey: .outletMapDesc)
        entryUser = try container.decode(Int.self, forKey: .entryUser)
        entryDate = try container.decode(String.self, forKey: .entryDate)
        createUser = (try container.decode(Int?.self, forKey: .createUser))
        createDate = try container.decode(String?.self, forKey: .createDate)
        photo = try container.decode(String.self, forKey: .photo)
        status = try container.decode(Int.self, forKey: .status)
        tax = try container.decode(Int.self, forKey: .tax)
        service = try container.decode(Int.self, forKey: .service)
        bank = try container.decode(String.self, forKey: .bank)
        bankNumber = try container.decode(String.self, forKey: .bankNumber)
        bankName = try container.decode(String.self, forKey: .bankName)
        timeZoneName = try container.decode(String.self, forKey: .timeZoneName)
        timeZone = try container.decode(String.self, forKey: .timeZone)
        photo = try container.decode(String.self, forKey: .photo)
        if let roomType = try container.decodeIfPresent([RoomType].self, forKey: .roomType){
            self.roomType = roomType
        }
        if let facility = try container.decodeIfPresent([SingleFacility].self, forKey: .facility){
            self.facility = facility
        }
        brandData = try container.decode(BrandData.self, forKey: .brandData)
        gallery = try container.decode([Gallery].self, forKey: .gallery)
        schedule = try container.decode([Schedule].self, forKey: .schedule)
    }
}

struct SingleFacility: Decodable {
    var id: String?
    var code: String?
    var facilityId: Int? = 0
    var description: String?
    var facilityIcon: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case code = "Code"
        case facilityId = "FacilitiesID"
        case description = "Description"
        case facilityIcon = "facilityIcon"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        do {
            self.id = try container.decode(String.self, forKey: .id)
        } catch {
            self.id = String(try container.decode(Int.self, forKey: .id))
        }
        code = try container.decode(String.self, forKey: .code)
        facilityId = try container.decode(Int.self, forKey: .facilityId)
        description = try container.decode(String.self, forKey: .description)
        facilityIcon = try container.decode(String.self, forKey: .facilityIcon)
    }
}

struct Gallery: Decodable {
    let id: String?
    let code: String?
    var photo: String? = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case code = "Code_"
        case photo = "Photo"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        do {
            self.id = try container.decode(String.self, forKey: .id)
        } catch {
            self.id = String(try container.decode(Int.self, forKey: .id))
        }
        code = try container.decode(String.self, forKey: .code)
        if let photo = try container.decodeIfPresent(String.self, forKey: .photo){
            self.photo = photo
        }
    }
}

struct OutletDetailSingle: Decodable {
    var data: OutletDetail
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct OutletDetailResponse: Decodable {
    var data: OutletDetailSingle
}
