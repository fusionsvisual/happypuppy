//
//  Promo.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Promo:Decodable{
    let id:String?
    let name:String?
    let outlet:String?
    let photoUrl:String?
    let publishDate:String?
    let status:String?
    let distance:String?
    let description:String?
    let entryDate:String?
    let entryUser:String?
    let createdDate:String?
    let createUser:Int?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case name = "Name"
        case description = "Description"
        case photoUrl = "Photo"
        case outlet = "Outlet"
        case status = "Status"
        case publishDate = "PublishDate"
        case createUser = "CreateUser"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createdDate = "CreateDate"
        case distance = "distance"
    }
}

struct SinglePromo:Decodable{
    var promo:Promo
    
    enum CodingKeys:String, CodingKey {
        case promo = "data"
    }
}

struct PromoList:Decodable{
    let promoList:[Promo]
    
    enum CodingKeys:String, CodingKey {
        case promoList = "data"
    }
}

struct PromoResponse:Decodable {
    var data:SinglePromo
}

struct PromoListResponse:Decodable {
    let data:PromoList
}
