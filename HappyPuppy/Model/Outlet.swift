//
//  Outlet.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Outlet:Decodable{
    var id: String? = ""
    var code: String? = ""
    var brand: Int? = 0
    var name: String = ""
    var address: String? = ""
    var telp: String? = ""
    var city: String? = ""
    var bigCity: Int? = 0
    var email: String? = ""
    var facebook: String? = ""
    var instagram: String? = ""
    var outletLat: String? = ""
    var outletLong: String? = ""
    var outletMapDesc: String? = ""
    var entryUser: Int? = 0
    var entryDate: String? = ""
    var createUser: Int?
    var createDate: String? = ""
    var photo: String? = ""
    var status: String? = ""
    var tax: Int? = 0
    var service: Int? = 0
    var bank: String? = ""
    var bankNumber: String? = ""
    var bankName: String? = ""
    var timeZoneName: String? = ""
    var timeZone: String? = ""
    var distance: String? = ""
    var facilities: [Facility] = []
    var galleries: [Gallery] = []

    var roomType: [RoomType]?
    var operational: Operational?
    var brandData: BrandData?
    var schedule: [Schedule]?
    var reservationPaymentType = false
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case code = "Code"
        case brand = "Brand"
        case name = "Name"
        case address = "Address"
        case telp = "Telp"
        case city = "City"
        case bigCity = "BigCity"
        case email = "Email"
        case facebook = "Facebook"
        case instagram = "Instagram"
        case outletLat = "OutletLat"
        case outletLong = "OutletLong"
        case outletMapDesc = "OutletMapDesc"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createUser = "CreateUser"
        case createDate = "CreateDate"
        case photo = "Photo"
        case status = "Status_"
        case tax = "Tax"
        case service = "Service"
        case bank = "Bank"
        case bankNumber = "BankNumber"
        case bankName = "BankName"
        case timeZoneName = "TimeZoneName"
        case timeZone = "TimeZone"
        case distance = "distance"
        case roomType = "roomTypes"
        case operational = "operational"
        case brandData = "brandData"
        case brandData2 = "brand_data"
        case brandData3 = "brand"
        case facilities = "facilities"
        case galleries = "galleries"

        case schedule = "schedule"
//        case brandForPoint = "brand"
        case outletName = "OutletName"
        case reservationPaymentType = "TypePaymentReservasi"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        do {
            if let id = try container.decodeIfPresent(String.self, forKey: .id){
                self.id = id
            }
        } catch {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        code = try container.decode(String.self, forKey: .code)
        do {
            if let brand = try container.decodeIfPresent(String.self, forKey: .brand){
                self.brand = Int(brand)
            }
        } catch {
            if let brand = try container.decodeIfPresent(Int.self, forKey: .brand){
                self.brand = brand
            }
        }
        
        if let name = try container.decodeIfPresent(String.self, forKey: .name){
            self.name = name
        }else{
            name = try container.decode(String.self, forKey: .outletName)
        }
        address = try container.decode(String.self, forKey: .address)
        if let telp = try container.decode(String?.self, forKey: .telp){
            self.telp = telp
        }
        city = try container.decode(String?.self, forKey: .city)
        
        do {
            if let bigCity = try container.decodeIfPresent(String.self, forKey: .bigCity){
                self.bigCity = Int(bigCity)
            }
        } catch  {
            if let bigCity = try container.decodeIfPresent(Int.self, forKey: .bigCity){
                self.bigCity = bigCity
            }
        }
       
        email = try container.decode(String.self, forKey: .email)
        facebook = try container.decode(String?.self, forKey: .facebook)
        instagram = try container.decode(String?.self, forKey: .instagram)
        outletLat = try container.decode(String.self, forKey: .outletLat)
        outletLong = try container.decode(String.self, forKey: .outletLong)
        if let outletMapDesc = try container.decode(String?.self, forKey: .outletMapDesc){
            self.outletMapDesc = outletMapDesc
        }
        do {
            if let entryUser = try container.decodeIfPresent(String.self, forKey: .entryUser){
                self.entryUser = Int(entryUser)
            }
        } catch {
            if let entryUser = try container.decodeIfPresent(Int.self, forKey: .entryUser){
                self.entryUser = entryUser
            }
        }
    
        entryDate = try container.decode(String?.self, forKey: .entryDate)
        
        if let createUser = try container.decodeIfPresent(Int?.self, forKey: .createUser){
            self.createUser = createUser
        }
        
        createDate = try container.decode(String?.self, forKey: .createDate)
        photo = try container.decode(String.self, forKey: .photo)

        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
        do {
            if let tax = try container.decodeIfPresent(String.self, forKey: .tax){
                self.tax = Int(tax)
            }
        } catch {
            if let tax = try container.decodeIfPresent(Int.self, forKey: .tax){
                self.tax = tax
            }
        }
        
        do {
            if let service = try container.decodeIfPresent(String.self, forKey: .service){
                self.service = Int(service)
            }
        } catch {
            if let service = try container.decodeIfPresent(Int.self, forKey: .service){
                self.service = service
            }
        }
        
        bank = try container.decode(String.self, forKey: .bank)
        bankNumber = try container.decode(String.self, forKey: .bankNumber)
        bankName = try container.decode(String.self, forKey: .bankName)
        timeZoneName = try container.decode(String.self, forKey: .timeZoneName)
        timeZone = try container.decode(String.self, forKey: .timeZone)
        
        do {
            if let distance = try container.decodeIfPresent(String.self, forKey: .distance){
                self.distance = distance
            }
        } catch {
            if let distance = try container.decodeIfPresent(Float.self, forKey: .distance){
                self.distance = String(distance)
            }
        }
        
        if let roomType = try container.decodeIfPresent([RoomType].self, forKey: .roomType){
            self.roomType = roomType
        }
        photo = try container.decode(String.self, forKey: .photo)
        if let schedule = try container.decodeIfPresent([Schedule].self, forKey: .schedule){
            self.schedule = schedule
        }
        if let operational = try container.decodeIfPresent(Operational.self, forKey: .operational){
            self.operational = operational
        }
        if let brandData = try container.decodeIfPresent(BrandData.self, forKey: .brandData){
            self.brandData = brandData
        }else{
            if let brandData2 = try container.decodeIfPresent(BrandData.self, forKey: .brandData2){
                self.brandData = brandData2
            }
            
            if let brandData = try container.decodeIfPresent(BrandData.self, forKey: .brandData3){
                self.brandData = brandData
            }
        }
        
        if let reservationPaymentType = try container.decodeIfPresent(Int.self, forKey: .reservationPaymentType){
            self.reservationPaymentType = Bool(truncating: reservationPaymentType as NSNumber)
        }
        
        if let facilities = try container.decodeIfPresent([Facility].self, forKey: .facilities){
            self.facilities = facilities
        }
        
        if let galleries = try container.decodeIfPresent([Gallery].self, forKey: .galleries){
            self.galleries = galleries
        }
    }
}

struct RoomType:Decodable {
    var code: String?
    var id: String?
    var roomType: String?
    var photo: String?
    var pax: String?
    var status: String?
    var description: String?
    var available: String?
    var minimumCharge: Int = 0
    var minimumHour: Int = 0
    
    enum CodingKeys:String, CodingKey {
        case code = "Code"
        case id = "ID"
        case roomType = "RoomType"
        case photo = "Photo"
        case pax = "Pax"
        case status = "Status"
        case description = "Description"
        case available = "Available"
        case minimumCharge = "MinimumCharge"
        case minimumHour = "MinimumChargeHour"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let code = try container.decodeIfPresent(String.self, forKey: .code){
            self.code = code
        }
        
        do {
            if let id = try container.decodeIfPresent(String.self, forKey: .id){
                self.id = id
            }
        } catch {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        if let roomType = try container.decodeIfPresent(String.self, forKey: .roomType){
            self.roomType = roomType
        }
        
        if let photo = try container.decodeIfPresent(String.self, forKey: .photo){
            self.photo = photo
        }
        
        do {
            if let pax = try container.decodeIfPresent(String.self, forKey: .pax){
                self.pax = pax
            }
        } catch {
            if let pax = try container.decodeIfPresent(Int.self, forKey: .pax){
                self.pax = String(pax)
            }
        }
        
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
        
        if let description = try container.decodeIfPresent(String.self, forKey: .description){
            self.description = description
        }
        do {
            if let available = try container.decodeIfPresent(String.self, forKey: .available){
                self.available = available
            }
        } catch {
            if let available = try container.decodeIfPresent(Int.self, forKey: .available){
                self.available = String(available)
            }
        }
        
        if let minimumCharge = try container.decodeIfPresent(Int.self, forKey: .minimumCharge){
            self.minimumCharge = minimumCharge
        }
        
        if let minimumHour = try container.decodeIfPresent(Int.self, forKey: .minimumHour){
            self.minimumHour = minimumHour
        }
    }
}

struct RoomTypeList:Decodable{
    let roomTypeList:[RoomType]

    enum CodingKeys:String, CodingKey {
        case roomTypeList = "roomTypes"
    }
}

struct OutletList:Decodable{
    let outletList:[Outlet]
    
    enum CodingKeys:String, CodingKey {
        case outletList = "data"
    }
}

struct OutletResponse:Decodable {
    let data:OutletList
}

struct Operational:Decodable{
    var timezone: String? = ""
    var date: String? = ""
    var dateYesterday: String? = ""
    var isOpen: Int? = 0
    var timeStart: String? = ""
    var timeEnd: String? = ""
    var isOpenYesterday: Int? = 0
    var timeStartYesterday: String? = ""
    var timeEndYestarday: String? = ""
    
    enum CodingKeys:String, CodingKey {
        case timezone = "time_zone"
        case date = "date"
        case dateYesterday = "date_yesterday"
        case isOpen = "is_open"
        case timeStart = "time_start"
        case timeEnd = "time_end"
        case isOpenYesterday = "is_open_yesterday"
        case timeStartYesterday = "time_start_yesterday"
        case timeEndYestarday = "time_end_yesterday"
    }
}

struct BrandData:Decodable{
    var id:String? = ""
    var name:String? = ""
    var photoUrl:String? = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case name = "Name_"
        case photoUrl = "Photo"
    }
    
    init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            if let id = try container.decodeIfPresent(String.self, forKey: .id){
                self.id = id
            }
        } catch {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        if let name = try container.decodeIfPresent(String.self, forKey: .name){
            self.name = name
        }
        
        if let photoURL = try container.decodeIfPresent(String.self, forKey: .photoUrl){
            self.photoUrl = photoURL
        }
    }
}

struct Schedule: Decodable {
    var id: String?
    var outlet: String?
    var day: String?
    var timeStart: String?
    var timeFinish: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case outlet = "Outlet"
        case day = "Day_"
        case timeStart = "TimeStart"
        case timeFinish = "TimeFinish"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let id = try container.decodeIfPresent(String.self, forKey: .id){
                self.id = id
            }
        } catch {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        
        if let outlet = try container.decodeIfPresent(String.self, forKey: .outlet){
            self.outlet = outlet
        }
        
        do {
            if let day = try container.decodeIfPresent(String.self, forKey: .day){
                self.day = day
            }
        } catch {
            if let day = try container.decodeIfPresent(Int.self, forKey: .day){
                self.day = String(day)
            }
        }
        
        if let timeStart = try container.decodeIfPresent(String.self, forKey: .timeStart){
            self.timeStart = timeStart
        }
        
        if let timeFinish = try container.decodeIfPresent(String.self, forKey: .timeFinish){
            self.timeFinish = timeFinish
        }
    }
}

struct AvailableOutlet{
    var name:String = ""
    var code:String = ""
    var outletCode:String = ""
}
