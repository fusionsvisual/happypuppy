//
//  Receipt.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Receipt:Decodable {
    let summaryCode:String?
    let date:String?
    let invoiceCode:String?
    let outletData:Outlet?
    
    enum CodingKeys:String, CodingKey {
        case summaryCode = "Summary"
        case date = "Date"
        case invoiceCode = "Invoice"
        case outletData = "outlet_data"
    }
    
    init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.summaryCode = try container.decode(String.self, forKey: .summaryCode)
        self.date = try container.decode(String.self, forKey: .date)
        self.invoiceCode = try container.decode(String.self, forKey: .invoiceCode)
        self.outletData = try container.decode(Outlet.self, forKey: .outletData)
    }
}

struct ReceiptList:Decodable{
    var totalData: Int = .zero
    let receiptList:[Receipt]?
    
    enum CodingKeys:String, CodingKey {
        case totalData = "total"
       case receiptList = "data"
   }
}

struct ReceiptListResponse:Decodable{
    let data:ReceiptList?
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}

struct ReceiptionInvoice {
    let invoice:String?
    let roomNumber:String?
    let customerName:String?
    let date:String?
    let roomPrice:Int?
    let discount:Int?
    let otherPrice:Int?
    let fnbServiceCharge:Int?
    let roomServiceCharge:Int?
    let fnbTax:Int?
    let roomServiceTax:Int?
    let totalPayment:Int?
    let paymentMethod:String?
    let receiptItem:[ReceiptItem]
}

struct ReceiptItem{
    var nama:String = ""
    var quantity:Int = 0
    var price:Int = 0
    
    init(nama: String, quantity:Int, price:Int) {
        self.nama = nama
        self.quantity = quantity
        self.price = price
    }
}
