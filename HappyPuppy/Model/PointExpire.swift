//
//  PointExpire.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct PointExpire:Decodable{
    let month:String?
    let year:String?
    let total:Int?
    
    enum CodingKeys:String, CodingKey {
        case month
        case year
        case total
    }
}

struct PointExpireList:Decodable{
    var data:[PointExpire]
}

struct PointExpireResponse:Decodable{
    var data:PointExpireList
}
