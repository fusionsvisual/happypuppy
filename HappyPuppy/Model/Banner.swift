//
//  Banner.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Banner: Decodable{
    let id: Int?
    let title: String?
    let photo_url: String?
    let order_: Int?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case title = "Title"
        case photo_url = "Photo"
        case order_ = "Order_"
    }
}

struct BannerList: Decodable{
    var data:[Banner]
    
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}

struct BannerResponse: Decodable {
    var data:BannerList
}
