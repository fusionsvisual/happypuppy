//
//  Voucher.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Voucher:Decodable {
    var id:String?
    var code = ""
    var memberType:String? = ""
    var name:String? = ""
    var category:String? = ""
    var expiredDate:String = ""
    var startDay:String? = ""
    var startHour:String? = ""
    var finishDay:String? = ""
    var finishHour:String? = ""
    var conditionDay:String? = ""
    var conditionTaxnService:String? = ""
    var hour:String? = ""
    var minute:String? = ""
    var conditionRoomType:String? = ""
    var conditionRoomTypeOver:Int?
    var conditionHour:String? = ""
    var roomPrice:Int? = 0
    var conditionRoomPrice:String? = ""
    var item:String? = ""
    var quantity:String? = ""
    var conditionItem:String? = ""
    var conditionItemQuantity:Int?
    var conditionItemPrice:Int?
    var fnbPrice:String? = ""
    var conditionFnbPrice:String? = ""
    var fnbDiscount:String? = ""
    var conditionFnBDiscount:String? = ""
    var price:Int? = 0
    var conditionPrice:Int? = 0
    var discount:Int? = 0
    var conditionDiscount:String? = ""
    var redeemPoint:String = ""
    var description:String? = ""
    var description1:String? = ""
    var photoUrl:String? = ""
    var voucherType:String? = ""
    var outlet:String? = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case code = "Code_"
        case memberType = "MemberType"
        case name = "Name_"
        case category = "Category"
        case expiredDate = "ExpiredDate"
        case startDay = "StartDay"
        case startHour = "StartHour"
        case finishDay = "FinishDay"
        case finishHour = "FinishHour"
        case conditionDay = "ConditionDay"
        case conditionTaxnService = "ConditionTaxNService"
        case hour = "Hour"
        case minute = "Minute"
        case conditionRoomType = "ConditionRoomType"
        case conditionRoomTypeOver = "ConditionRoomTypeOver"
        case conditionHour = "ConditionHour"
        case roomPrice = "RoomPrice"
        case conditionRoomPrice = "ConditionRoomPrice"
        case item = "Item"
        case quantity = "Qty"
        case conditionItem = "ConditionItem"
        case conditionItemQuantity = "ConditionItemQty"
        case conditionItemPrice = "ConditionItemPrice"
        case fnbPrice = "FnBPrice"
        case conditionFnbPrice = "ConditionFnBPrice"
        case fnbDiscount = "FnBDiscount"
        case conditionFnBDiscount = "ConditionFnBDiscount"
        case price = "Price"
        case conditionPrice = "ConditionPrice"
        case discount = "Discount"
        case conditionDiscount = "ConditionDiscount"
        case redeemPoint = "ReedemPoin"
        case description = "Description"
        case description1 = "Description1"
        case photoUrl = "Photo"
        case voucherType = "VoucherType"
        case outlet = "Outlet"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            if let id = try container.decodeIfPresent(String.self, forKey: .id){
                self.id = id
            }
        } catch  {
            if let id = try container.decodeIfPresent(Int.self, forKey: .id){
                self.id = String(id)
            }
        }
        
        do {
            if let memberType = try container.decodeIfPresent(String.self, forKey: .memberType){
                self.memberType = memberType
            }
        } catch {
            if let memberType = try container.decodeIfPresent(Int.self, forKey: .memberType){
                self.memberType = String(memberType)
            }
        }
        
        self.name = try container.decode(String.self, forKey: .name)
        
        do {
            if let category = try container.decodeIfPresent(String.self, forKey: .category){
                self.category = category
            }
        } catch {
            if let category = try container.decodeIfPresent(Int.self, forKey: .category){
                self.category = String(category)
            }
        }
        
        if let expiredDate = try container.decodeIfPresent(String.self, forKey: .expiredDate){
            self.expiredDate = expiredDate
        }
        
        do {
            if let startDay = try container.decodeIfPresent(String.self, forKey: .startDay){
                self.startDay = startDay
            }
        } catch {
            if let startDay = try container.decodeIfPresent(Int.self, forKey: .startDay){
                self.startDay = String(startDay)
            }
        }
        
        do {
            if let startHour = try container.decodeIfPresent(String.self, forKey: .startHour){
                self.startHour = startHour
            }
        } catch {
            if let startHour = try container.decodeIfPresent(Int.self, forKey: .startHour){
                self.startHour = String(startHour)
            }
        }
        
        do {
            if let finishDay = try container.decodeIfPresent(String.self, forKey: .finishDay){
                self.finishDay = finishDay
            }
        } catch {
            if let finishDay = try container.decodeIfPresent(Int.self, forKey: .finishDay){
                self.finishDay = String(finishDay)
            }
        }
        
        do {
            if let finishHour = try container.decodeIfPresent(String.self, forKey: .finishHour){
                self.finishHour = finishHour
            }
        } catch {
            if let finishHour = try container.decodeIfPresent(Int.self, forKey: .finishHour){
                self.finishHour = String(finishHour)
            }
        }
        
        do {
            if let conditionDay = try container.decodeIfPresent(String.self, forKey: .conditionDay){
                self.conditionDay = conditionDay
            }
        } catch {
            if let conditionDay = try container.decodeIfPresent(Int.self, forKey: .conditionDay){
                self.conditionDay = String(conditionDay)
            }
        }
        
        do {
            if let conditionTaxnService = try container.decodeIfPresent(String.self, forKey: .conditionTaxnService){
                self.conditionTaxnService = conditionTaxnService
            }
        } catch {
            if let conditionTaxnService = try container.decodeIfPresent(Int.self, forKey: .conditionTaxnService){
                self.conditionTaxnService = String(conditionTaxnService)
            }
        }
        
        do {
            if let hour = try container.decodeIfPresent(String.self, forKey: .hour){
                self.hour = hour
            }
        } catch {
            if let hour = try container.decodeIfPresent(Int.self, forKey: .hour){
                self.hour = String(hour)
            }
        }
        
        do {
            if let minute = try container.decodeIfPresent(String.self, forKey: .minute){
                self.minute = minute
            }
        } catch {
            if let minute = try container.decodeIfPresent(Int.self, forKey: .minute){
                self.minute = String(minute)
            }
        }
        
        self.conditionRoomType = try container.decode(String.self, forKey: .conditionRoomType)
        
        if let conditionRoomTypeOver = try container.decodeIfPresent(Int.self, forKey: .conditionRoomTypeOver){
            self.conditionRoomTypeOver = conditionRoomTypeOver
        }
        
        do {
            if let conditionHour = try container.decodeIfPresent(String.self, forKey: .conditionHour){
                self.conditionHour = conditionHour
            }
        } catch {
            if let conditionHour = try container.decodeIfPresent(Int.self, forKey: .conditionHour){
                self.conditionHour = String(conditionHour)
            }
        }
        
        if let roomPrice = try container.decodeIfPresent(Int.self, forKey: .roomPrice){
            self.roomPrice = roomPrice
        }
        if let conditionRoomPrice = try container.decodeIfPresent(String.self, forKey: .conditionRoomPrice){
            self.conditionRoomPrice = conditionRoomPrice
        }
        self.item = try container.decode(String.self, forKey: .item)
        
        do {
            if let quantity = try container.decodeIfPresent(String.self, forKey: .quantity){
                self.quantity = quantity
            }
        } catch {
            if let quantity = try container.decodeIfPresent(Int.self, forKey: .quantity){
                self.quantity = String(quantity)
            }
        }
        
        self.conditionItem = try container.decode(String.self, forKey: .conditionItem)
        if let conditionItemQuantity = try container.decodeIfPresent(Int.self, forKey: .conditionItemQuantity){
            self.conditionItemQuantity = conditionItemQuantity
        }
        if let conditionItemPrice = try container.decodeIfPresent(Int.self, forKey: .conditionItemPrice){
            self.conditionItemPrice = conditionItemPrice
        }
        if let fnbPrice = try container.decodeIfPresent(String.self, forKey: .fnbPrice){
            self.fnbPrice = fnbPrice
        }
        if let conditionFnBDiscount = try container.decodeIfPresent(String.self, forKey: .conditionFnBDiscount){
            self.conditionFnBDiscount = conditionFnBDiscount
        }
        
        do {
            if let price = try container.decodeIfPresent(Int.self, forKey: .price){
                self.price = price
            }
        }catch{
            if let priceStr = try container.decodeIfPresent(String.self, forKey: .price){
                self.price = Int(priceStr)
            }
        }
        
        do {
            if let conditionPrice = try container.decodeIfPresent(Int.self, forKey: .conditionPrice){
                self.conditionPrice = conditionPrice
            }
        }catch{
            if let conditionPriceStr = try container.decodeIfPresent(String.self, forKey: .conditionPrice){
                self.conditionPrice = Int(conditionPriceStr)
            }
        }
        
        if let discount = try container.decodeIfPresent(Int.self, forKey: .discount){
            self.discount = discount
        }
        if let conditionDiscount = try container.decodeIfPresent(String.self, forKey: .conditionDiscount){
            self.conditionDiscount = conditionDiscount
        }
        
        do {
            if let redeemPoint = try container.decodeIfPresent(String.self, forKey: .redeemPoint){
                self.redeemPoint = redeemPoint
            }
        }catch{
            if let redeemPoint = try container.decodeIfPresent(Int.self, forKey: .redeemPoint){
                self.redeemPoint = String(redeemPoint)
            }
        }
        
        self.description = try container.decode(String.self, forKey: .description)
        self.description1 = try container.decode(String.self, forKey: .description1)
        self.photoUrl = try container.decode(String.self, forKey: .photoUrl)
        do {
            if let voucherType = try container.decodeIfPresent(String.self, forKey: .voucherType){
                self.voucherType = voucherType
            }
        }catch{
            if let voucherType = try container.decodeIfPresent(Int.self, forKey: .voucherType){
                self.voucherType = String(voucherType)
            }
        }
        self.outlet = try container.decode(String.self, forKey: .outlet)
        
        if let code = try container.decodeIfPresent(String.self, forKey: .code){
            self.code = code
        }
    }
}
    
    struct VoucherList:Decodable{
        var data:[Voucher]
        var totalCount: Int = 0
        
        enum CodingKeys:String, CodingKey {
            case data = "data"
            case totalCount = "total"
        }
    }
    
    struct VoucherResponse:Decodable{
        var data:VoucherList
        var result:String
    }

