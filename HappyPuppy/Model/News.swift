//
//  News.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct News: Decodable {
    let id: Int?
    let title: String?
    let description: String?
    let eventDate: String?
    let like: Int?
    let status: Int?
    let photo: String?
    let publishDate: String?
    let createUser: String?
    let createDate: String?
    
    enum CodingKeys : String, CodingKey {
        case id = "ID"
        case title = "Title"
        case description = "Description"
        case eventDate = "EventDate"
        case like = "Like_"
        case status = "Status"
        case photo = "Photo"
        case publishDate = "PublishDate"
        case createUser = "CreateUser"
        case createDate = "CreateDate"
    }
}

struct NewsList: Decodable {
    var data: [News]
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct NewsResponse: Decodable {
    var data: NewsList
}
