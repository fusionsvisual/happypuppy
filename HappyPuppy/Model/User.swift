//
//  User.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct User:Decodable{
    var id:Int?
    var memberId:String?
    var photoUrl:String?
    var religion:String?
    var outlet:String?
    var memberType:String?
    var isVerified:Bool = false
    var point:String?
    var rememberToken:String?
    var status:String?
    var city:String?
    var registerDate:String?
    var activateDate:String?
    var expiredDate:String?
    var isPhoneVerified:Bool = false
    var phoneOTP:String?
    var nextOTPAttempt:String?
    var oldMemberId:String?
    var importDate:String?
    var idCardStatus:String?
    var idCardNote:String?
    
    var email:String?
    var firstName:String?
    var lastName:String?
    var bod:String?
    var gender:String?
    var phone:String?
    var accessToken:String?
    var memberCode:String?
    
    var currentMember:Member = Member()
    var nextMember:Member = Member()
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case memberId = "MemberID"
        case photoUrl = "Photo"
        case religion = "Religion"
        case outlet = "Outlet"
        case memberType = "MemberType"
        case isVerified = "IsVerified"
        case point = "Point"
        case rememberToken = "RememberToken"
        case status = "Status"
        case city = "City"
        case registerDate = "RegisterDate"
        case activateDate = "ActivateDate"
        case expiredDate = "ExpiredDate"
        case isPhoneVerified = "IsPhoneVerified"
        case phoneOTP = "PhoneOTP"
        case nextOTPAttempt = "NextOTPAttempt"
        case oldMemberId = "OldMemberID"
        case importDate = "ImportDAte"
        case idCardStatus = "IDCardStatus"
        case idCardNote = "IDCardNote"
        
        case email = "UserName"
        case firstName = "FirstName"
        case lastName = "LastName"
        case bod = "Birthday"
        case gender = "Gender"
        case phone = "HP"
        case accessToken = "accessToken"
        case memberCode = "member_code"
        
        case currentMember = "benefitName"
        case nextMember = "nextBenefits"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try container.decodeIfPresent(Int.self, forKey: .id) {
            self.id = id
        }
        if let memberId = try container.decodeIfPresent(String.self, forKey: .memberId) {
            self.memberId = memberId
        }
        if let photoUrl = try container.decodeIfPresent(String?.self, forKey: .photoUrl) {
            self.photoUrl = photoUrl
        }
        if let religion = try container.decodeIfPresent(String?.self, forKey: .religion) {
            self.religion = religion
        }
        if let outlet = try container.decodeIfPresent(String?.self, forKey: .outlet) {
            self.outlet = outlet
        }
        
        do{
            if let memberType = try container.decodeIfPresent(String.self, forKey: .memberType) {
                self.memberType = memberType
            }
        }catch{
            if let memberTypeInt = try container.decodeIfPresent(Int.self, forKey: .memberType){
                self.memberType = String(memberTypeInt)
            }
        }
        
        
        do{
            if let isVerified = try container.decodeIfPresent(String.self, forKey: .isVerified) {
                if isVerified == "0"{
                    self.isVerified = false
                }else{
                    self.isVerified = true
                }
            }
        }catch{
            if let isVerified = try container.decodeIfPresent(Int.self, forKey: .isVerified) {
                self.isVerified = Bool(truncating: isVerified as NSNumber)
            }
        }
        
        do{
            if let point = try container.decodeIfPresent(String?.self, forKey: .point) {
                self.point = point
            }
        }catch{
            if let point = try container.decodeIfPresent(Int.self, forKey: .point) {
                self.point = String(point)
            }
        }
        
        if let rememberToken = try container.decodeIfPresent(String?.self, forKey: .rememberToken) {
            self.rememberToken = rememberToken
        }
        
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status) {
                self.status = status
            }
        } catch {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status) {
                self.status = String(status)
            }
        }
        
        if let city = try container.decodeIfPresent(String?.self, forKey: .city) {
            self.city = city
        }
        if let registerDate = try container.decodeIfPresent(String.self, forKey: .registerDate) {
            self.registerDate = registerDate
        }
        if let activateDate = try container.decodeIfPresent(String?.self, forKey: .activateDate) {
            self.activateDate = activateDate
        }
        if let expiredDate = try container.decodeIfPresent(String.self, forKey: .expiredDate) {
            self.expiredDate = expiredDate
        }
        
        do {
           if let isPhoneVerified = try container.decodeIfPresent(String.self, forKey: .isPhoneVerified) {
                if isPhoneVerified == "0"{
                    self.isPhoneVerified = false
                }else{
                    self.isPhoneVerified = true
                }
            }
        } catch {
            if let isPhoneVerified = try container.decodeIfPresent(Int.self, forKey: .isPhoneVerified) {
                self.isPhoneVerified = Bool(isPhoneVerified as NSNumber)
            }
        }
        
        
        if let phoneOTP = try container.decodeIfPresent(String.self, forKey: .phoneOTP) {
            self.phoneOTP = phoneOTP
        }
        if let nextOTPAttempt = try container.decodeIfPresent(String?.self, forKey: .nextOTPAttempt) {
            self.nextOTPAttempt = nextOTPAttempt
        }
        if let oldMemberId = try container.decodeIfPresent(String?.self, forKey: .oldMemberId) {
            self.oldMemberId = oldMemberId
        }
        if let importDate = try container.decodeIfPresent(String?.self, forKey: .importDate) {
            self.importDate = importDate
        }
        
        do {
            if let idCardStatus = try container.decodeIfPresent(String?.self, forKey: .idCardStatus) {
                self.idCardStatus = idCardStatus
            }
        } catch  {
            if let idCardStatus = try container.decodeIfPresent(Int.self, forKey: .idCardStatus) {
                self.idCardStatus = String(idCardStatus)
            }
        }
        
        if let idCardNote = try container.decodeIfPresent(String?.self, forKey: .idCardNote) {
            self.idCardNote = idCardNote
        }
        
        if let email = try container.decodeIfPresent(String.self, forKey: .email) {
            self.email = email
        }
        if let firstName = try container.decodeIfPresent(String.self, forKey: .firstName) {
            self.firstName = firstName
        }
        if let lastName = try container.decodeIfPresent(String.self, forKey: .lastName) {
            self.lastName = lastName
        }
        if let bod = try container.decodeIfPresent(String.self, forKey: .bod) {
            self.bod = bod
        }
        if let gender = try container.decodeIfPresent(String.self, forKey: .gender) {
            self.gender = gender
        }
        if let phone = try container.decodeIfPresent(String.self, forKey: .phone) {
            self.phone = phone
        }
        if let accessToken = try container.decodeIfPresent(String.self, forKey: .accessToken) {
            self.accessToken = accessToken
        }
        if let memberCode = try container.decodeIfPresent(String.self, forKey: .memberCode) {
            self.memberCode = memberCode
        }
        if let currentMember = try container.decodeIfPresent(Member.self, forKey: .currentMember) {
            self.currentMember = currentMember
        }
        if let nextMember = try container.decodeIfPresent(Member.self, forKey: .nextMember) {
            self.nextMember = nextMember
        }
    }
}

struct UserSettings: Decodable {
    var requireKtp: String?
    
    enum CodingKeys: String, CodingKey {
        case requireKtp = "require_ktp"
    }
}

struct UserVariable:Decodable{
    var userDetail:User?
    var settings:UserSettings?
    
    enum CodingKeys:String, CodingKey {
        case userDetail = "user"
        case settings = "settings"
    }
}

struct UserDetailResponse:Decodable{
    var result:String?
    var userVariable:UserVariable
    
    enum CodingKeys:String, CodingKey {
        case result
        case userVariable = "data"
    }
}

struct UserResponse:Decodable{
    var result:String?
    var user:User?
    
    enum CodingKeys:String, CodingKey {
        case result
        case user = "data"
    }
}
