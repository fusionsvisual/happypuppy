//
//  Notification.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Notification:Decodable {
    var id:Int = 0
    var member:String = ""
    var title:String = ""
    var description:String = ""
    var createdDate:String = ""
    var photoUrl:String = ""
    var url:String? = ""
    var linkName:String?
    var type:String? = ""
    var status:String = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case member = "Member"
        case title = "Title"
        case description = "Description"
        case createdDate = "CreateDate"
        case photoUrl = "Photo"
        case url = "URL"
        case linkName = "LinkName"
        case type = "Type_"
        case status = "Status_"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let id = try container.decodeIfPresent(Int.self, forKey: .id){
            self.id = id
        }
        
        if let member = try container.decodeIfPresent(String.self, forKey: .member){
            self.member = member
        }
        
        if let title = try container.decodeIfPresent(String.self, forKey: .title){
            self.title = title
        }
        
        if let description = try container.decodeIfPresent(String.self, forKey: .description){
            self.description = description
        }
        
        if let createdDate = try container.decodeIfPresent(String.self, forKey: .createdDate){
            self.createdDate = createdDate
        }
        
        if let photoURL = try container.decodeIfPresent(String.self, forKey: .photoUrl){
            self.photoUrl = photoURL
        }
        
        if let url = try container.decodeIfPresent(String.self, forKey: .url){
            self.url = url
        }
        
        if let linkName = try container.decodeIfPresent(String.self, forKey: .linkName){
            self.linkName = linkName 
        }
        
        do {
            if let type = try container.decodeIfPresent(String.self, forKey: .type){
                self.type = type
            }
        } catch {
            if let typeInt = try container.decodeIfPresent(Int.self, forKey: .type){
                self.type = String(typeInt)
            }
        }
        
        
        do {
            if let status = try container.decodeIfPresent(String.self, forKey: .status){
                self.status = status
            }
        } catch  {
            if let status = try container.decodeIfPresent(Int.self, forKey: .status){
                self.status = String(status)
            }
        }
    }
}

struct NotificationList:Decodable {
    var data:[Notification]
}

struct NotificationResponse:Decodable {
    var data:NotificationList
}
