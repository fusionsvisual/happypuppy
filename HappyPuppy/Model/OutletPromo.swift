//
//  OutletPromo.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 07/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct OutletPromo:Decodable{
    let id:Int? = 0
    let name:String?
    let description:String?
    let photo:String?
    let outlet:String?
    let status:Int? = 0
    let publishDate:String?
    let createUser:Int?
    let entryUser:Int? = 0
    let entryDate:String?
    let createdDate:String?
    let outletName:String?

    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case name = "Name"
        case description = "Description"
        case photo = "Photo"
        case outlet = "Outlet"
        case status = "Status"
        case publishDate = "PublishDate"
        case createUser = "CreateUser"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
        case createdDate = "CreateDate"
        case outletName = "outlet_name"
    }
}

struct OutletPromoList:Decodable{
    let promoList:[OutletPromo]
    
    enum CodingKeys:String, CodingKey {
        case promoList = "data"
    }
}

struct OutletPromoListResponse:Decodable {
    let data: OutletPromoList
}
