//
//  Member.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Member:Decodable {
    var code:String = ""
    var name:String = ""
    var maxPoint:Int = 0
    var discRoom:String = ""
    var discFnB:String = ""
    var iconImageUrl:String?
    var color:String = ""
    var patternURL:String?
    var benefitList:[Benefit] = []
    
    enum CodingKeys:String, CodingKey {
        case code = "Code"
        case name = "Name"
        case maxPoint = "MaxPoint"
        case discRoom = "DiscRoom"
        case discFnB = "DiscFnB"
        case iconImageUrl = "IconPattern"
        case color = "Color"
        case patternURL = "Pattern"
        case benefitList = "benefits"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do{
            self.code = try container.decode(String.self, forKey: .code)
        }catch{
            if let code = try container.decodeIfPresent(Int.self, forKey: .code){
                self.code = String(code)
            }
        }
        self.name = try container.decode(String.self, forKey: .name)
        
        do{
            self.maxPoint = try container.decode(Int.self, forKey: .maxPoint)
        }catch{
            if let maxPoint = try container.decodeIfPresent(String.self, forKey: .maxPoint){
                self.maxPoint = Int(maxPoint)!
            }
        }
        
        self.discRoom = try container.decode(String.self, forKey: .discRoom)
        self.discFnB = try container.decode(String.self, forKey: .discFnB)
        self.iconImageUrl = try container.decode(String?.self, forKey: .iconImageUrl)
        self.color = try container.decode(String.self, forKey: .color)
        self.patternURL = try container.decode(String?.self, forKey: .patternURL)
        
        if let benefitList = try container.decodeIfPresent([Benefit].self, forKey: .benefitList){
            self.benefitList = benefitList
        }
    }
    
    init() {
        
    }
}

struct Benefit:Decodable {
    var description:String = ""
    enum CodingKeys:String, CodingKey {
        case description = "Description"
    }
}

struct MemberList:Decodable {
    var memberList:[Member]?
    enum CodingKeys:String, CodingKey {
        case memberList = "data"
    }
}

struct MemberResponse:Decodable {
    var data:MemberList?
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}
