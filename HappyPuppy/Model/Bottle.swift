//
//  Bottle.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Bottle:Decodable {
    var id: Int = -1
    var brand: String?
    var volume: String = DefaultValues.emptyString
    var size: String = DefaultValues.emptyString
    var photoUrl: String = DefaultValues.emptyString
    var saveDate: String = DefaultValues.emptyString
    var redeemerName: String?
    var takeDate: String = DefaultValues.emptyString
    var status: String = DefaultValues.emptyString
    var qrCode: String = DefaultValues.emptyString
    var takeStatus: Int?
    var takePhone: String?
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case brand = "Merk"
        case volume = "BottleVolume"
        case size = "BottleSize"
        case photoUrl = "Photo"
        case saveDate = "SaveDate"
        case redeemerName = "TakeBy"
        case takeDate = "TakeDate"
        case status = "StatusKeeping"
        case qrCode = "QrCode"
        case takeStatus = "TakeStatus"
        case takePhone = "TakePhone"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.brand = try container.decode(String?.self, forKey: .brand)
        self.volume = try container.decode(String.self, forKey: .volume)
        self.size = try container.decode(String.self, forKey: .size)
        self.photoUrl = try container.decode(String.self, forKey: .photoUrl)
        if let saveDate = try container.decodeIfPresent(String.self, forKey: .saveDate){
            self.saveDate = Formatter.shared.birthDateFormatter(getDateFormat: "yyyy-MM-dd HH:mm:ss", printDateFormat: "dd MMMM yyyy", dateStr: saveDate)
        }

        if let takeStatus = try container.decodeIfPresent(
            Int.self,
            forKey: .takeStatus
        ) {
            self.takeStatus = takeStatus
        }

        if let takePhone = try container.decodeIfPresent(
            String.self,
            forKey: .takePhone
        ) {
            self.takePhone = takePhone
        }

        do {
            if let statusInt = try container.decodeIfPresent(String.self, forKey: .status){
                if statusInt == "0"{
                    self.status = "Belum Diapprove"
                }else{
                    self.status = "Sudah Diapprove"
                }
            }
        } catch  {
            if let statusInt = try container.decodeIfPresent(Int.self, forKey: .status){
                if statusInt == 0{
                    self.status = "Belum Diapprove"
                }else{
                    self.status = "Sudah Diapprove"
                }
            }
        }
        
        if let qrCode = try container.decodeIfPresent(String.self, forKey: .qrCode){
            self.qrCode = qrCode
        }
        if let takeDate = try container.decodeIfPresent(String.self, forKey: .takeDate){
            self.takeDate = Formatter.shared.birthDateFormatter(getDateFormat: "yyyy-MM-dd HH:mm:ss", printDateFormat: "dd MMMM yyyy", dateStr: takeDate)
        }
        if let redeemerName = try container.decodeIfPresent(String?.self, forKey: .redeemerName){
           self.redeemerName = redeemerName
       }
    }
}

extension Bottle {
    var brandDisplayText: String {
        guard let brand = self.brand else {
            return Separators.hyphen
        }
        return brand.isEmpty
            ? Separators.hyphen
            : brand
    }
}

struct BottleList:Decodable{
    var bottleList:[Bottle]
    
    enum CodingKeys:String, CodingKey {
        case bottleList = "data"
    }
}

struct BottleListResponse:Decodable{
    var data:BottleList
}

struct BottleResponse:Decodable {
    var data:Bottle
}
