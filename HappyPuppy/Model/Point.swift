//
//  Point.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Point:Decodable{
    var type:String?
    var date:String?
    var outlet:Outlet
    var point:String?
    var brand:BrandData?
    var detail:[PointPrizeDetail]?
//    let name:String?
//    let outlet:String?
//    let photoUrl:String?
//    let publishDate:String?
//    let status:String?
//    let distance:String?
//    let description:String?
//    let entryDate:String?
//    let entryUser:String?
//    let createdDate:String?
//    let createUser:String?
    
    enum CodingKeys:String, CodingKey {
        case type = "type"
        case date = "date"
        case outlet = "outlet"
        case point = "points"
        case brand = "brand"
        case detail = "detail"
//        case date
//        case name = "Name"
//        case description = "Description"
//        case photoUrl = "Photo"
//        case outlet = "Outlet"
//        case status = "Status"
//        case publishDate = "PublishDate"
//        case createUser = "CreateUser"
//        case entryUser = "EntryUser"
//        case entryDate = "EntryDate"
//        case createdDate = "CreateDate"
//        case distance = "distance"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.type = try container.decode(String.self, forKey: .type)
        self.date = try container.decode(String.self, forKey: .date)
        self.outlet = try container.decode(Outlet.self, forKey: .outlet)
        if let brand = try container.decode(BrandData?.self, forKey: .brand){
            self.brand = brand
        }
        
        do {
            self.point = try container.decode(String?.self, forKey: .point)
        }catch{
            if let point = try container.decode(Int?.self, forKey: .point){
                self.point = String(point)
            }
        }
        
        if let detail = try container.decodeIfPresent([PointPrizeDetail].self, forKey: .detail){
            self.detail = detail
        }
    }
}

struct PointPrizeDetail:Decodable{
    var inventory:String?
    var name:String?
    var point:Int?
    var quantity:String?
    
    enum CodingKeys:String, CodingKey {
        case inventory = "inventory"
        case name = "name"
        case point = "point"
        case quantity = "quantity"
    }
}

struct PointList:Decodable{
    var data:[Point]
}

struct PointResponse:Decodable{
    var data:PointList
}
