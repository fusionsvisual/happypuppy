//
//  Faqs.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire

struct Faqs:Decodable {
    var id:Int? = 0
    var category:String? = ""
    var question:String? = ""
    var answer:String? = ""
    var entryUser:String? = ""
    var entryDate:String? = ""
    
    enum CodingKeys:String, CodingKey {
        case id = "ID"
        case category = "Category"
        case question = "Question"
        case answer = "Answer"
        case entryUser = "EntryUser"
        case entryDate = "EntryDate"
    }
}

struct FaqsList:Decodable {
    var faqsList:[Faqs]
    
    enum CodingKeys:String, CodingKey {
        case faqsList = "data"
    }
}

struct FaqsResponse:Decodable {
    var data:FaqsList
}
