//
//  RoomType.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 26/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct RoomTypeByOutlet: Decodable {
    let code: String?
    let id: Int?
    let roomType: String?
    let photo: String?
    let pax: Int?
    let status: Int?
    let description: String?
    let available: Int?
    var minimumCharge: Int = 0
    var minimumHour: Int = 0
    let facilities: [Facility]?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case id = "ID"
        case roomType = "RoomType"
        case photo = "Photo"
        case pax = "Pax"
        case status = "Status"
        case description = "Description"
        case available = "Available"
        case minimumCharge = "MinimumCharge"
        case minimumHour = "MinimumChargeHour"
        case facilities = "facilities"
    }
}

struct Facility: Decodable {
    let id: Int?
    let code: String?
    let roomType: Int?
    let facilityId: Int?
    let description: String?
    let quantity: Int?
    let type: FacilityType?
    let iconUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case code = "Code"
        case roomType = "RoomType"
        case facilityId = "FacilitiesID"
        case description = "Description"
        case quantity = "Qty"
        case type = "type"
        case iconUrl = "facilityIcon"
    }
}

struct FacilityType: Decodable {
    let id: Int?
    let name: String?
    let description: String?
    let icon: String?
    let status: Int?
    let unit: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case description = "Description"
        case icon = "Icon"
        case status = "Status_"
        case unit = "Unit"
    }
}

struct RoomTypeByOtletList: Decodable {
    var data:[RoomTypeByOutlet]
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

struct RoomTypeByOtletResponse: Decodable {
    var data:RoomTypeByOtletList
}
