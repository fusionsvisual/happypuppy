//
//  About.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation

struct Contact:Decodable {
    var companyName:String? = ""
    var address1:String? = ""
    var address2:String? = ""
    var telp:String? = ""
    var fax:String? = ""
    var merk:String? = ""
    var logo:String? = ""
    var email:String? = ""
    var facebook:String? = ""
    var twitter:String? = ""
    var instagram:String? = ""
    var website:String? = ""
    
    enum CodingKeys:String, CodingKey {
        case companyName = "CompanyName"
        case address1 = "Address1"
        case address2 = "Address2"
        case telp = "Telp"
        case fax = "Fax"
        case merk = "Merk"
        case logo = "Logo"
        case email = "Email"
        case facebook = "Facebook"
        case twitter = "Twitter"
        case instagram = "Instagram"
        case website = "Website"
    }
}

struct About:Decodable {
    var description:String? = ""
    
    enum CodingKeys:String, CodingKey {
        case description = "Description"
    }
}

struct AboutDataResponse:Decodable {
    var data: About
    var contact:Contact
    
    enum CodingKeys:String, CodingKey {
        case data = "about"
        case contact = "contact"
    }
}

struct AboutResponse:Decodable {
    var data: AboutDataResponse
    
    enum CodingKeys:String, CodingKey {
        case data = "data"
    }
}
