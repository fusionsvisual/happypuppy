//
//  BottleController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BottleController {
    static let shared = BottleController()

    func requestSaveItemOtp(id:String, completion:@escaping(_ status:String,_ bottle:Bottle?,_ error: String?)->Void){
        print("GET SAVE OTP")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "id":id
        ]
        AF.request("\(Network.shared.getBaseURLAPI())/keepingbottle/getSaveOTPDetail", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    do {
                        let response = try JSONDecoder().decode(BottleResponse.self, from: response.data!)
                        let bottle = response.data
                        completion(NetworkStatus.SUCCESS, bottle, nil)
                    } catch (let error) {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }

    func requestTakeItemOtp(
        requestId: Int,
        onSuccess: @escaping (_ response: BottleResponse) -> Void,
        onFailed: @escaping (_ message: String) -> Void
    ) {
        AF.request(
            String(
                format: "%@/keepingbottle/getTakeOTPDetail",
                Network.shared.getBaseURLAPI()
            ),
            method: .get,
            parameters: [
                "id": requestId
            ],
            headers: [
                "Authorization": String.init(
                    format: "Bearer %@", UserDefaultHelper.shared.getToken()
                )
            ]
        ).responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                if json["result"].stringValue != ResponseStatus.SUCCESS {
                    onFailed(
                        json["data"].string ?? ErrorMessage.SOMETHING_WRONG
                    )
                    return
                }
                do {
                    let response = try JSONDecoder().decode(
                        BottleResponse.self,
                        from: json.rawData()
                    )
                    onSuccess(response)
                } catch(let error) {
                    onFailed(error.localizedDescription)
                }
            case .failure(let error):
                onFailed(error.localizedDescription)
            }
        }
    }
    
    func getActiveBottle(length:Int?, start:Int?, completion:@escaping(_ status:String,_ bottleList:[Bottle]?,_ error: String?)->Void){
        print("GET ACTIVE BOTTLE")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/keeping_bottle/view", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    do {
                        let response = try JSONDecoder().decode(BottleListResponse.self, from: response.data!)
                        let bottle = response.data.bottleList
                        completion(NetworkStatus.SUCCESS, bottle, nil)
                    } catch (let error) {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }
    
    func getDoneBottle(length:Int?, start:Int?, completion:@escaping(_ status:String,_ bottleList:[Bottle]?,_ error: String?)->Void){
        print("GET DONE BOTTLE")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/keeping_bottle/viewFinished", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    do {
                        let response = try JSONDecoder().decode(BottleListResponse.self, from: response.data!)
                        let bottle = response.data.bottleList
                        completion(NetworkStatus.SUCCESS, bottle, nil)
                    } catch (let error) {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }
    
    func saveVerification(bottleId:Int, otpNumber:String, completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        print("Bottle OTP Verification")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "keeping_id":bottleId,
            "otp":otpNumber
        ]
        AF.request("\(Network.shared.getBaseURLAPI())/keepingbottle/saveOTP", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func verifyTakeOtp(
        bottleId: Int,
        otpNumber: String,
        onSuccess: @escaping (_ successMessage: String) -> Void,
        onFailed: @escaping (_ errorMessage: String) -> Void
    ) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "keeping_id": bottleId,
            "otp": otpNumber
        ]
        AF.request("\(Network.shared.getBaseURLAPI())/keepingbottle/takeOTP", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    onSuccess(message)
                default:
                    let error = json["data"].stringValue
                    onFailed(error)
                }
                
            case .failure(let error):
                print(error)
                onFailed(error.errorDescription ?? "")
            }
        }
    }
}
