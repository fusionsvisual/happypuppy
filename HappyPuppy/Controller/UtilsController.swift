//
//  UtilsController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 23/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire

class UtilsController {
    static let shared = UtilsController()
    
    func getCityList(completion:@escaping(_ status:String?, _ cityList:[City]?, _ error:String?)->Void){
        AF.request("\(Network.shared.getBaseURLAPI())/city/list", method: .get, parameters: [:], headers:nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let cityResponse = try JSONDecoder().decode(CityResponse.self, from: response.data!)
                    let cityList = cityResponse.data.data
                                        
                    completion(NetworkStatus.SUCCESS, cityList, nil)
                } catch  (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
}
