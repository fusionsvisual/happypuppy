//
//  ReceiptController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ReceiptController {
    static let shared = ReceiptController()
    
    func getReceipt(length: Int?, startIndex: Int?, completion:@escaping(_ status:String, _ receiptList: ReceiptListResponse?, _ error:String?)->Void){
        print("\(Network.shared.getBaseURLAPI())/receipts/list")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let startIndex = startIndex{
            parameters["start"] = startIndex
        }
        
        print(parameters)
        
        AF.request("\(Network.shared.getBaseURLAPI())/receipts/list", method: .get, parameters: parameters, headers: headers).validate().response { (response) in
            switch response.result {
            case .success:
                do {
                    let receiptResponse = try JSONDecoder().decode(
                        ReceiptListResponse.self,
                        from: response.data ?? Data()
                    )
                    completion(NetworkStatus.SUCCESS, receiptResponse, nil)
                } catch  (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getReceiptDetail(outletCode:String, invoiceCode:String, completion:@escaping(_ status:String, _ receiptDetail:ReceiptionInvoice?, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let urlString = "\(Network.shared.getBaseURLAPI())/receipts/detail/\(outletCode)/\(invoiceCode)"
        
            AF.request(urlString, method: .get, parameters: [:], headers: headers).validate().responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let data = json["data"]["Keterangan"].stringValue.data(using: .utf8)!
                
                let description = JSON(data)
                print(description)
                let invoiceCode = description[1]["summaryPayment"]["data"][0]["Invoice"].stringValue
                let roomNumber = description[0]["receptionInvoice"]["data"][0]["Kamar"].stringValue
                let customerName = description[0]["receptionInvoice"]["data"][0]["Nama"].stringValue
                
                let date = description[0]["receptionInvoice"]["data"][0]["Date"].stringValue
                let roomPrice = description[0]["receptionInvoice"]["data"][0]["Sewa_Kamar_Setelah_Diskon_Non_Member"].intValue
                let otherPrice = description[0]["receptionInvoice"]["data"][0]["Charge_Penjualan"].intValue
                let discount = description[0]["receptionInvoice"]["data"][0]["Total_Discount_Member"].intValue
                let fnbServiceCharge = Int((description[0]["receptionInvoice"]["data"][0]["Service_Penjualan"].doubleValue).rounded())
                let roomServiceCharge = Int((description[0]["receptionInvoice"]["data"][0]["Service_Kamar"].doubleValue).rounded())
                let fnbTax = Int((description[0]["receptionInvoice"]["data"][0]["Tax_Penjualan"].doubleValue).rounded())
                let roomTax = Int((description[0]["receptionInvoice"]["data"][0]["Tax_Kamar"].doubleValue).rounded())
                
                let totalPayment = (roomPrice+otherPrice-discount+fnbServiceCharge+roomServiceCharge+fnbTax+roomTax)
                
                let paymentMethod = description[1]["summaryPayment"]["data"][0]["Payment_Type"].stringValue
                
                var itemList:[ReceiptItem] = []
                let itemListResponse = description[2]["orderPenjualan"]["data"][0].arrayValue
                for itemResponse in itemListResponse{
                    let itemName  = itemResponse["Nama"].stringValue
                    let itemQuantity = itemResponse["Qty"].intValue
                    let itemPrice = itemResponse["Harga_Setelah_Diskon_Promo"].intValue
                    let item = ReceiptItem(nama: itemName, quantity: itemQuantity, price: itemPrice)
                    itemList.append(item)
                }
                
                let receiptDetail = ReceiptionInvoice(invoice: invoiceCode, roomNumber: roomNumber, customerName: customerName, date: date, roomPrice: roomPrice, discount: discount, otherPrice: otherPrice, fnbServiceCharge: fnbServiceCharge, roomServiceCharge: roomServiceCharge, fnbTax: fnbTax, roomServiceTax: roomTax, totalPayment: totalPayment, paymentMethod: paymentMethod, receiptItem: itemList)
                completion(NetworkStatus.FAILURE, receiptDetail, nil)
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func deleteReceipt(outletCode:String, receiptCode:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        parameters["outlet"] = outletCode
        parameters["receiptCode"] = receiptCode
        
        AF.request("\(Network.shared.getBaseURLAPI())/receipts/delete", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success (let value):
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    completion(NetworkStatus.SUCCESS, nil)
                }else{
                    completion(NetworkStatus.FAILURE, json["data"].stringValue)
                }
                
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
}
