//
//  LoginController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseMessaging

class AuthenticationContoller {
    static let shared = AuthenticationContoller()
    
    func login(email:String, password:String, completion:@escaping(_ status:String, _ user:User?, _ error:String?)->Void){
        var fcmToken = ""
        
        if let fcmTokenGet = Messaging.messaging().fcmToken{
            fcmToken = fcmTokenGet
            print("FCM TOKEN: \(fcmTokenGet)")
        }
        
        let parameters:[String:Any] = [
            "username":email,
            "password":password,
            "fcm_id":fcmToken,
            "device_name":DeviceNameConstant.IPHONE
        ]
        
        let urlString = String(format: "\(Network.shared.getBaseURLAPI())%@", "/user/login")
        print(urlString)
        AF.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result{
            case .success:
                print(response)
                do {
                    let userResponse = try JSONDecoder().decode(UserResponse.self, from: response.data!)
                    if userResponse.result == ResponseStatus.SUCCESS{
                        let user = userResponse.user
                        if let token = user?.accessToken{
                            UserDefaultHelper.shared.setToken(accessToken: token)
                            completion(NetworkStatus.SUCCESS, user, nil)
                        }else{
                            completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                        }
                        
                        //# UNUSED
//                        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", token!)]
//                        let urlString = String(format: "\(Network.shared.getBaseURLAPI())%@", "/user/getProfile")
//                        print(urlString)
                        
//                        user?.accessToken = token
//                        UserDefaultHelper.shared.setupUser(user: user!)
                        
//                        AF.request(urlString, method: .get, headers: headers).validate().responseJSON { (response) in
//                            print(response)
//                            switch response.result {
//                            case .success:
//                                do {
//                                    let userDetailResponse = try JSONDecoder().decode(UserDetailResponse.self, from: response.data!)
//                                    var user = userDetailResponse.userVariable.userDetail
//                                    user?.accessToken = token
//                                    switch user?.memberType {
//                                    case "1":
//                                        user?.memberType = MemberType.BLUE
//                                    case "2":
//                                        user?.memberType = MemberType.SILVER
//                                    default:
//                                        user?.memberType = MemberType.GOLD
//                                    }
//                                    
//                                    print(user!)
//                                    
//                                    print("USER VARIABLE")
//                                    print(userDetailResponse.userVariable)
//                                    
//                                                                        
//                                    UserDefaultHelper.shared.setupUser(user: user!)
//                                    completion(NetworkStatus.SUCCESS, user, nil)
//                                } catch  (let error){
//                                    print(error)
//                                    completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
//                                }
//                                break
//                            case .failure(let error):
//                                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
//                            }
//                        }
                    }else{
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                    }
                } catch (let error)  {
                    if let value = response.value{
                        let json = JSON(value)
                        if let message = json["data"].string{
                            completion(NetworkStatus.FAILURE, nil, message)
                        }else{
                            completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                        }
                    }
                }
            case .failure (let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }
    
    func register(email:String, password:String, firstName:String, lastName:String, gender:String, birthdate:String, phone:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        var fcmToken = ""
        
        if let fcmTokenGet = Messaging.messaging().fcmToken{
            fcmToken = fcmTokenGet
            print("FCM TOKEN: \(fcmTokenGet)")
        }
        
        /* Setup Gender for Request */
        var gender = gender
        if gender == GenderConstant.PRIA{
            gender = "L"
        }else{
            gender = "P"
        }
        /*-----*/
        
        /* Setup Birthdate for Request */
        let birthdate = Formatter.shared.birthDateFormatter(getDateFormat: "dd MMMM yyyy", printDateFormat: "yyyy-MM-dd",dateStr: birthdate)
        /*-----*/
        
        let parameters:[String:Any] = [
            "username":email,
            "password":password,
            "firstname":firstName,
            "lastname":lastName,
            "gender":gender,
            "birthday":birthdate,
            "phone":phone,
            "device_name":DeviceNameConstant.IPHONE,
            "fcm_id":fcmToken
        ]
        print(phone)
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/register", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    let accessToken = json["data"]["accessToken"].stringValue
                    UserDefaultHelper.shared.setToken(accessToken: accessToken)
                    
                    ProfileController.shared.getProfileDetail { (status, user, error) in
                        if let error = error{
                            completion(NetworkStatus.FAILURE, error)
                        }else{
                            completion(NetworkStatus.SUCCESS, nil)
                        }
                    }
                    
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func logout(completion:@escaping(_ status:String, _ error:String?)->Void){
        let token = UserDefaultHelper.shared.getToken()
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", token)]
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/logout", method: .post, parameters: [:], encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    completion(NetworkStatus.SUCCESS, nil)
                }else{
                    completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func loginBySocialMedia(accessToken:String, userString:String?, provider:String, completion:@escaping(_ status:String, _ user:User?, _ error:String?)->Void){
    
        var parameters:[String:Any] = [
            "provider":provider,
            "accessToken":accessToken,
            "device_name":DeviceNameConstant.IPHONE,
        ]
        
        print(parameters)
        
        if let userString = userString{
            parameters["userString"] = userString
        }
        
        if let fcmToken = Messaging.messaging().fcmToken{
            parameters["fcm_id"] = fcmToken
            print("FCM TOKEN: \(fcmToken)")
        }else{
            parameters["fcm_id"] = ""
        }
        
        print("\(Network.shared.getBaseURLAPI())/user/loginSocialMedia")
        AF.request("\(Network.shared.getBaseURLAPI())/user/loginSocialMedia", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    if json["data"].stringValue != "register"{
                        if json["result"].stringValue == ResponseStatus.SUCCESS{
                            let accessToken = json["data"]["accessToken"].stringValue
                            print("ACCESS TOKEN AUTH CONTROLLER \(accessToken)")
                            UserDefaultHelper.shared.setToken(accessToken: accessToken)
                            
                            ProfileController.shared.getProfileDetail { (status, user, error) in
                                if let error = error{
                                    completion(NetworkStatus.FAILURE, nil, error)
                                }else{
                                    completion(NetworkStatus.SUCCESS, nil, nil)
                                }
                            }
                            
                        }else{
                            let error = json["data"].stringValue
                            completion(NetworkStatus.FAILURE, nil, error)
                        }
                    }else{
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.GOOGLE_NOT_REGISTERED)
                    }
                }else{
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }
    
    func registerByGoogle(email:String, password:String, firstName:String, lastName:String, gender:String, birthdate:String, phone:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        
        print("REGISTER BY GOOGLE")
        
        var fcmToken = ""
                
        if let fcmTokenGet = Messaging.messaging().fcmToken{
            fcmToken = fcmTokenGet
            print("FCM TOKEN: \(fcmTokenGet)")
        }
        
        /* Setup Gender for Request */
        var gender = gender
        if gender == GenderConstant.PRIA{
            gender = "L"
        }else{
            gender = "P"
        }
        /*-----*/
        
        /* Setup Birthdate for Request */
        let birthdate = Formatter.shared.birthDateFormatter(getDateFormat: "dd MMMM yyyy", printDateFormat: "yyyy-MM-dd",dateStr: birthdate)
        /*-----*/
        
        var parameters:[String:Any] = [
            "username":email,
            "password":password,
            "firstname":firstName,
            "lastname":lastName,
            "gender":gender,
            "birthday":birthdate,
            "phone":phone,
            "device_name":DeviceNameConstant.IPHONE,
            "fcm_id":fcmToken,
        ]
        
        if let accessToken = SocialMediaConstant.accessToken{
            parameters["accessToken"] = accessToken
        }
        
        if let userString = SocialMediaConstant.userString{
            parameters["userString"] = userString
        }
        
        if let provider = SocialMediaConstant.provider{
            parameters["provider"] = provider
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/registerSocialMedia", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    let accessToken = json["data"]["accessToken"].stringValue
                    UserDefaultHelper.shared.setToken(accessToken: accessToken)
                    
                    ProfileController.shared.getProfileDetail { (status, user, error) in
                        if let error = error{
                            completion(NetworkStatus.FAILURE, error)
                        }else{
                            completion(NetworkStatus.SUCCESS, nil)
                        }
                    }
                    
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
}
