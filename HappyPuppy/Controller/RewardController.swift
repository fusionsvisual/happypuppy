//
//  RewardController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

class RewardController {
    static let shared = RewardController()
    
    private init(){}
    
    func createRedeem(rewardCode:String?, outlet:String?, completion:@escaping(_ status:String, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let rewardCode = rewardCode{
            parameters["reward_code"] = rewardCode
        }
        
        if let outlet = outlet{
            parameters["outlet"] = outlet
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/itemreward/redeem/doRedeemItem", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success (let value):
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    completion(NetworkStatus.SUCCESS, nil)
                }else{
                    completion(NetworkStatus.FAILURE, json["data"].stringValue)
                }
                
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func getRewards(length:Int?, start:Int?, completion:@escaping(_ status:String, _ rewardList: [Reward]?, _ error:String?)->Void){
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/itemreward/distinct/list", method: .get, parameters: parameters, headers: nil).validate().responseJSON { (response) in
            switch response.result{
             case .success:
                print(response)
                do {
                    let rewardResponse = try JSONDecoder().decode(RewardList.self, from: response.data!)
                    let rewardList = rewardResponse.rewardList
                    completion(NetworkStatus.SUCCESS, rewardList, nil)
                } catch(let error)  {
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                }

             case .failure:
                completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
            }
            print(response)
        }
    }
    
    func getRewardDetail(rewardId:String){
        let urlString = String.init(format: "\(Network.shared.getBaseURLAPI())/itemreward/view/%@", rewardId)
        AF.request(urlString, method: .get, headers: nil).responseDecodable(of: SingleRewardResponse.self) { (response) in
            print(response)
        }
    }
    
    func getMyRewardList(length:Int?, startDate:Int?, completion:@escaping(_ status:String, _ prizeList:[MyReward]?, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let startDate = startDate{
            parameters["start"] = startDate
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/itemreward/redeem/list", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let rewardResponse = try JSONDecoder().decode(MyRewardResponse.self, from: response.data!)
                    let rewardList = rewardResponse.data.data
                                        
                    completion(NetworkStatus.SUCCESS, rewardList, nil)
                } catch  (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getMyFinishedRewardList(length:Int?, startDate:Int?, completion:@escaping(_ status:String, _ prizeList:[MyReward]?, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let startDate = startDate{
            parameters["start"] = startDate
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/itemreward/redeem/listFinished", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let rewardResponse = try JSONDecoder().decode(MyRewardResponse.self, from: response.data!)
                    let rewardList = rewardResponse.data.data
                                        
                    completion(NetworkStatus.SUCCESS, rewardList, nil)
                } catch  (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getAvailableOutletList(prizeCode:String, city:String, completion:@escaping(_ status:String, _ outletList:[
        AvailableOutlet]?, _ error:String?)->Void){
        
        var outletList:[AvailableOutlet] = []
        
        var parameters:[String:Any] = [:]
        parameters["code"]  = prizeCode
        parameters["city"] = city
        
        
        AF.request("\(Network.shared.getBaseURLAPI())/itemreward/getOutletsByItem", method: .get, parameters: parameters, headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for outletResponse in json.arrayValue {
                    var outlet = AvailableOutlet()
                    outlet.name = outletResponse["OutletName"].stringValue
                    outlet.code = outletResponse["Code"].stringValue
                    outlet.outletCode = outletResponse["Outlet"].stringValue
                    outletList.append(outlet)
                }
                
                completion(NetworkStatus.SUCCESS, outletList, nil)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
}
