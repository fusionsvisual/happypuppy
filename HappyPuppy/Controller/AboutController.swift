//
//  AboutController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AboutController{
    static let shared = AboutController()
    
    func getContact(completion:@escaping(_ status:String, _ response:Contact?, _ error:String?)->Void){
        AF.request("\(Network.shared.getBaseURLAPI())/about", method: .get, parameters: [:], headers: nil).validate().responseJSON { (response) in
             switch response.result{
             case .success:
                print(response)
                do {
                    let aboutResponse = try JSONDecoder().decode(AboutResponse.self, from: response.data!)
                    let contact = aboutResponse.data.contact
                    completion(NetworkStatus.SUCCESS, contact, nil)
                } catch  {
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }

             case .failure:
                completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func getAbout(completion:@escaping(_ status:String, _ response:About?, _ error:String?)->Void){
        AF.request("\(Network.shared.getBaseURLAPI())/about", method: .get, parameters: [:], headers: nil).validate().responseDecodable(of: AboutResponse.self) { (response) in
            switch response.result{
               case .success(let value):
                completion(NetworkStatus.SUCCESS, value.data.data, nil)

            case .failure:
                completion(NetworkStatus.SUCCESS, nil, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func getTermsCondition(completion:@escaping(_ status:String, _ content:String?, _ error:String?)->Void){
        AF.request("\(Network.shared.getBaseURLAPI())/termsconditions", method: .get, parameters: [:], headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    let content = json["data"]["Description"].stringValue
                    completion(NetworkStatus.SUCCESS, content, nil)
                }else{
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func getPrivacyPolicy(completion:@escaping(_ status:String, _ content:String?, _ error:String?)->Void){
        AF.request("\(Network.shared.getBaseURLAPI())/privacypolicy", method: .get, parameters: [:], headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    let content = json["data"]["Description"].stringValue
                    completion(NetworkStatus.SUCCESS, content, nil)
                }else{
                    completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                }
                break
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func getFaqs(completion:@escaping(_ status:String, _ faqsList:[Faqs]?, _ error:String?)->Void){
        print("GET FAQS")
        AF.request("\(Network.shared.getBaseURLAPI())/faqs", method: .get, parameters: [:], headers: nil).validate().responseJSON { (response) in
            switch response.result{
            case .success (let value):
                print(response)
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    do {
                        let response = try JSONDecoder().decode(FaqsResponse.self, from: response.data!)
                        let faqsList = response.data.faqsList
                        completion(NetworkStatus.SUCCESS, faqsList, nil)
                    } catch (let error) {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
            }
        }
    }
    
    func sendEmail(subject:String, message:String, completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        print("Send Question Email")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "subject":subject,
            "pesan":message
        ]
        AF.request("\(Network.shared.getBaseURLAPI())/sendEmail", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    completion(NetworkStatus.SUCCESS, "Pesan Telah Terkirim!", nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
}



