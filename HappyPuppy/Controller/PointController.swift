//
//  PointController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire

class PointController{
    static let shared = PointController()
    
    func getPointList(year:String?, month:String?, length:String?, startDate:String?, completion:@escaping(_ status:String, _ pointList:[Point]?, _ error:String?)->Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let year = year{
            parameters["year"] = year
        }
        
        if let month = month{
            parameters["month"] = month
        }
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let startDate = startDate{
            parameters["start"] = startDate
        }
        
        
        AF.request("\(Network.shared.getBaseURLAPI())/points/view", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            
            print(response)
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(PointResponse.self, from: response.data!)
                    completion(NetworkStatus.SUCCESS ,response.data.data, nil)
                } catch let error {
                    print(error)
                }
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, [], ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func getPointExpireList(month:String?, completion:@escaping(_ status:String?, _ pointExpire:[PointExpire]?, _ error:String?)->Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let month = month{
            parameters["month"] = month
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/points/getPointsExpiredIn", method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            
            print(response)
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(PointExpireResponse.self, from: response.data!)
                    completion(NetworkStatus.SUCCESS,response.data.data, nil)
                } catch let error {
                    print(error)
                }
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, [], ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
}
