//
//  VoucherController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class VoucherController{
    static let shared = VoucherController()
    
    func getMyVoucherList(length:Int?, start:Int?, completion:@escaping(_ status:String, _ voucherList:[Voucher]?, _ totalCount:Int, _ error:String?)->Void){
        print("GET MY VOUCHER LIST")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/vouchers/view", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            print(response)
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                
                if result == ResponseStatus.SUCCESS{
                    do {
                        let response = try JSONDecoder().decode(VoucherResponse.self, from: response.data!)
                        let voucherList = response.data.data
                        completion(NetworkStatus.SUCCESS, voucherList, response.data.totalCount, nil)
                    } catch let error {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, 0, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, 0, error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, 0, error.errorDescription)
            }
        }
    }
    
    func getVoucherList(length:Int?, start:Int?, completion:@escaping(_ status:String, _ voucherList:[Voucher]?, _ totalCount:Int, _ error:String?)->Void){
//        print("GET VOUCHER LIST")
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/vouchers/getredeemable", method: .get, parameters: parameters, headers: nil).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(VoucherResponse.self, from: response.data!)
                    let voucherList = response.data.data
                    completion(NetworkStatus.SUCCESS, voucherList, response.data.totalCount, nil)
                } catch (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, 0, error.localizedDescription)
                }
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, 0, error.localizedDescription)
            }
        }
    }
    
    func createVoucherRedeem(voucherId:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        print("CREATE VOUCHER REDEEM")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "voucher_id":voucherId
        ]
        
        AF.request("\(Network.shared.getBaseURLAPI())/vouchers/doRedeemVoucher", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
               switch response.result{
               case .success(let value):
//                   print(response)
                   let json = JSON(value)
                   let result = json["result"].stringValue
                   if result == ResponseStatus.SUCCESS{
                       completion(NetworkStatus.SUCCESS, nil)
                   }else{
                       let error = json["data"].stringValue
                       completion(NetworkStatus.FAILURE, error)
                   }
               case .failure:
                   completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
               }
           }
    }
}
