//
//  BannerController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class HomeController{
    static let shared = HomeController()
    
    func getBannerList(completion:@escaping(_ bannerList: [Banner]?,_ error: String?)->Void) {
        let urlString = "\(Network.shared.getBaseURLAPI())/banners"
        print(urlString)
        AF.request(urlString, method: .get).responseJSON{ (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(BannerResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    print(error)
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                print(error)
                completion([], error.errorDescription)
            }
        }
    }
    
    func getCities(completion:@escaping(_ cityList: [City]?,_ error: String?)->Void) {
        AF.request("\(Network.shared.getBaseURLAPI())/city/list", method: .get).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(CityResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                completion([], error.errorDescription)
            }
        }
    }
    
    func getOutletList(date:String?, length:Int?, start: Int?, city: City?, latitude: Double?, longitude: Double?, completion:@escaping(_ total: Int?, _ outletList: [Outlet]?,_ error: String?)->Void){
        var parameters:[String:Any] = [:]
        
        if let date = date{
            parameters["date"] = date
        }
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        if let city = city{
            if city.id != nil{
                parameters["city"] = city.name
            }
        }
        
        if let latitude = latitude{
            parameters["latitude"] = latitude
        }
        
        if let longitude = longitude{
            parameters["longitude"] = longitude
        }
        
        print("PARAMS")
        print(parameters)
        
        AF.request("\(Network.shared.getBaseURLAPI())/outlet/list", method: .get, parameters: parameters).responseJSON{ (response) in
            print(response)
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                let data = json["data"]
                do {
                    let response = try JSONDecoder().decode(OutletResponse.self, from: response.data!)
                    completion(data["total"].intValue, response.data.outletList, nil)
                } catch (let error) {
                    print(error)
                    completion(nil, [], error.localizedDescription)
                }
            case.failure(let error):
                print(error)
                completion(nil, [], error.errorDescription)
            }
        }
    }
    
    func getOutletDetail(outletId: String, completion:@escaping(_ outlet: OutletDetail?,_ error: String?) -> Void) {
        let urlString = String.init(format: "\(Network.shared.getBaseURLAPI())/outlet/view/%@", outletId)
        print(urlString)
        
        AF.request(urlString, method: .get).responseJSON { (response) in
            print(response)
            
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(OutletDetailResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch (let error) {
                    print(error)
                    
                    completion(nil , error.localizedDescription)
                }
            case .failure(let error):
                print(error)
                completion(nil, error.errorDescription)
            }
        }
    }
    
    func getOutletPromo(outletId: String?, completion:@escaping(_ outletPromo: [OutletPromo]?, _ error: String?) -> Void) {
        var parameters:[String:Any] = [:]
        
        if let outletId = outletId{
            parameters["outlet"] = outletId
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/promo/findByOutlet", method: .get, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(OutletPromoListResponse.self, from: response.data!)
                    completion(response.data.promoList, nil)
                } catch(let error) {
                    print(error)
                    completion([], error.localizedDescription)
                }
            case .failure(let error):
                print(error)
                completion([], error.localizedDescription)
            }
        }
    }
    
    func getRoomTypePrice(_ code: String?, _ date: String?, _ time: String?, completion:@escaping(_ roomTypePrice: [RoomTypePrice]?, _ error: String?) -> Void) {
        var parameters:[String:Any] = [:]
        
        if let code = code{
            parameters["code"] = code
        }
        
        if let date = date{
            parameters["tanggal"] = date
        }
        
        if let time = time{
            parameters["hour"] = time
        }
        print("\(Network.shared.getBaseURLAPI())/outlet/roomtypeprice")
        print(parameters)
        
        AF.request("\(Network.shared.getBaseURLAPI())/outlet/roomtypeprice", method: .get, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(RoomTypePriceResponse.self, from: response.data!)
                    completion(response.data, nil)
                } catch {
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                print("ERROR: \(error)")
                completion([], error.errorDescription)
            }
        }
    }
    
    func getNews(length: Int?, start: String?, completion:@escaping(_ newsList: [News]?,_ error: String?)->Void){
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        if let start = start {
            parameters["start"] = start
        }
        
        let urlString = "\(Network.shared.getBaseURLAPI())/news/list"
        print(urlString)
        AF.request(urlString, method: .get, parameters: parameters).responseJSON{ (response) in
            print(response)
            switch response.result{
            case .success:
                do {
                    let response = try JSONDecoder().decode(NewsResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    print(error)
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                print(error)
                completion([], error.errorDescription)
            }
        }
    }
}
