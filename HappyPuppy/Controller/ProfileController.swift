//
//  ProfileController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProfileController {
    static let shared = ProfileController()
    
    func updateProfile(firstName:String, lastName:String, gender:String, birthdate:String, phone:String, profileImage:UIImage?, imageName:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        
        print("UPDATE PROFILE DETAIL")
        
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        /* Setup Gender for Request */
        var gender = gender
        if gender == GenderConstant.PRIA{
            gender = "L"
        }else{
            gender = "P"
        }
        /*-----*/
        
        /* Setup Birthdate for Request */
        let birthdate = Formatter.shared.birthDateFormatter(getDateFormat: "dd MMMM yyyy", printDateFormat: "yyyy-MM-dd",dateStr: birthdate)
        /*-----*/
                
        let parameters:[String:Any] = [
            "first_name":firstName,
            "last_name":lastName,
            "gender":gender,
            "birthday":birthdate,
            "phone":phone
        ]
        
        AF.upload(multipartFormData: { (multipartFromData) in
            for (key,value) in parameters{
                multipartFromData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            if let profileImage = profileImage {
                multipartFromData.append(profileImage.jpegData(compressionQuality: 0.25)!, withName: "image", fileName: "\(imageName).jpeg",mimeType: "image/jpeg")
            }
        }, to: "\(Network.shared.getBaseURLAPI())/user/editProfile",
            usingThreshold: UInt64.init(),
            method: .post,
            headers: headers).response{ response in
                switch response.result{
                case .success(let value):
                    let json = JSON(value!)
                    let result = json["result"].stringValue
                    if result == ResponseStatus.SUCCESS{
                        completion(NetworkStatus.SUCCESS, nil)
                    }else{
                        let error = json["data"].stringValue
                        completion(NetworkStatus.FAILURE, error)
                    }
                case .failure(let error):
                    completion(NetworkStatus.FAILURE, error.localizedDescription)
                }
        }
    }
    
    func updatePassword(oldPassword:String, newPassword:String, completion:@escaping(_ status:String, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters:[String:Any] = [
            "old_password":oldPassword,
            "new_password":newPassword
        ]
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/changePassword", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result{
            case .success(let value):
                print(response)
                let json = JSON(value)
                let result = json["result"].stringValue
                if result == ResponseStatus.SUCCESS{
                    completion(NetworkStatus.SUCCESS, nil)
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, error)
                }
            case .failure:
                completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
            }
        }
    }
    
    func getProfileDetail(completion:@escaping(_ status:String, _ user:User?, _ error:String?)->Void){
        
        print("GET PROFILE DETAIL")
        
        print("TOKEN \(UserDefaultHelper.shared.getToken())")
        
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        print("ACCESS TOKEN PROFILE CONTROLLER \(UserDefaultHelper.shared.getToken())")

                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/getProfile", method: .get, parameters: [:], headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let userResponse = try JSONDecoder().decode(UserDetailResponse.self, from: response.data!)
                                        
                    //* Save current user to userDefault
                    UserDefaultHelper.shared.setCurrentUser(data: try JSON(response.data!)["data"]["user"].rawData())
                    
                    var user = userResponse.userVariable.userDetail
                    
                    switch user?.memberType {
                    case "1":
                        user?.memberType = MemberType.BLUE
                    case "2":
                        user?.memberType = MemberType.SILVER
                    default:
                        user?.memberType = MemberType.GOLD
                    }
                    
                    // Handle for First Time Login and After Login
                    if let user = user{
                        UserDefaultHelper.shared.updateUser(user: user)
                        if let userSetting = userResponse.userVariable.settings{
                            UserDefaultHelper.shared.setRequireIDCard(isRequireIdCard: userSetting.requireKtp!)
                        }
                    }
                    completion(NetworkStatus.SUCCESS, user, nil)
                } catch (let error)  {
                    print(error)
                    
                    completion(NetworkStatus.SUCCESS, nil, error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                if let errorCode = error.responseCode{
                    switch errorCode {
                    case 403:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.UNAUTHORIZED)
                    default:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                    }
                }
            }
        }
    }
    
    func getProfileSettingsDetail(completion:@escaping(_ status:String, _ data:UserSettings?, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/getProfile", method: .get, parameters: [:], headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                do {
                    let userResponse = try JSONDecoder().decode(UserDetailResponse.self, from: response.data!)
                    let data = userResponse.userVariable.settings
                    
                    completion(NetworkStatus.SUCCESS, data, nil)
                } catch (let error)  {
                    completion(NetworkStatus.SUCCESS, nil, error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                if let errorCode = error.responseCode{
                    switch errorCode {
                    case 403:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.UNAUTHORIZED)
                    default:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                    }
                }
            }
        }
    }
    
    func otpVerification(otpNumber:String, completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        print("OTP VERIFICATION")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters:[String:Any] = [
            "otp":otpNumber,
        ]
                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/verifyOTP", method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func checkNewNumberOTP(phone:String, otpNumber:String, completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters:[String:Any] = [
            "new_phone": phone,
            "otp":otpNumber
        ]
                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/doVerifyNewPhone", method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func resendEmailVerification(completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        print("RESEND EMAIL VERIFICATION")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/sendVerifyEmail", method: .post, parameters: [:],encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func resendOTP(completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        print("RESEND OTP")
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                        
        AF.request("\(Network.shared.getBaseURLAPI())/user/resendOTP", method: .post, parameters: [:], encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func resendNewPhoneOTP(completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        let urlString = "\(Network.shared.getBaseURLAPI())/user/resendNewPhoneOTP"
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                    
        print(urlString)

        AF.request(urlString, method: .post, parameters: [:], encoding: JSONEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"]["result"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func forgetPassword(email:String, completion:@escaping(_ status:String, _ message:String?, _ error:String?)->Void){
        
        let parameters:[String:Any] = [
            "email":email
        ]
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/resetPassword", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let result = json["result"].stringValue
                switch result {
                case ResponseStatus.SUCCESS:
                    let message = json["data"].stringValue
                    completion(NetworkStatus.SUCCESS, message, nil)
                default:
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, "", error)
                }
                
            case .failure(let error):
                print(error)
//                completion(NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func getMemberList(completion:@escaping(_ status:String, _ memberList:[Member]?, _ error:String?)->Void){
        print("GET MEMBER LIST")
        
        AF.request("\(Network.shared.getBaseURLAPI())/user/getBenefits", method: .get, parameters: [:], headers: nil).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                if json["result"].stringValue == ResponseStatus.SUCCESS{
                    do {
                        let memberResponse = try JSONDecoder().decode(MemberResponse.self, from: response.data!)
                        let member = memberResponse.data?.memberList
                        completion(NetworkStatus.SUCCESS, member, nil)
                    } catch (let error) {
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }else{
                    let error = json["data"].stringValue
                    completion(NetworkStatus.FAILURE, nil, error)
                }
                
                break
            case .failure(let error):
                print(error)
                if let errorCode = error.responseCode{
                    switch errorCode {
                    case 403:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.UNAUTHORIZED)
                    default:
                        completion(NetworkStatus.FAILURE, nil, ErrorMessage.SOMETHING_WRONG)
                    }
                }
            }
        }
    }
    
    func uploadIdentityCard(identityCardImage:UIImage?, completion:@escaping(_ status:String,_ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let imageData = identityCardImage!.jpegData(compressionQuality: 0.5)!
        
        let urlString = "\(Network.shared.getBaseURLAPI())/user/uploadKTP"
        print("URL STRING: \(urlString)")
        AF.upload(multipartFormData: { (multipartFromData) in
                multipartFromData.append(imageData, withName: "image", fileName: "file.jpeg",mimeType: "image/jpeg")
//
        }, to: urlString,
            usingThreshold: UInt64.init(),
            method: .post,
            headers: headers).response{ response in
                do{
                    let parsedData = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print(parsedData)
                }
                catch{

                }
                switch response.result{
                case .success(let value):
                    let json = JSON(value!)
                    let result = json["result"].stringValue
                    if result == ResponseStatus.SUCCESS{
                        completion(NetworkStatus.SUCCESS, nil)
                    }else{
                        let error = json["data"].stringValue
                        completion(NetworkStatus.FAILURE, error)
                    }
                case .failure:
                    completion(NetworkStatus.FAILURE, ErrorMessage.SOMETHING_WRONG)
                }
        }
    }
}
