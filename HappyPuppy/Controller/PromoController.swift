//
//  PromoController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire

class PromoController{
    static let shared = PromoController()
    
    private init(){}
    
    func getPromoList(length:Int?, startDate:String?, latitude:Double?, longitude:Double?){
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let startDate = startDate{
            parameters["start"] = startDate
        }
        
        if let latitude = latitude{
            parameters["latitude"] = latitude
        }
        
        if let longitude = longitude{
            parameters["longitude"] = longitude
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/promo/list", method: .get, parameters: parameters, headers: nil).responseDecodable(of: PromoListResponse.self) { (response) in
            
            print(response)
        }
    }
    
    func getPromoDetail(promoId:String){
        let urlString = String.init(format: "\(Network.shared.getBaseURLAPI())/promo/view/%@", promoId)
        AF.request(urlString, method: .get, parameters: [:], headers: nil).responseDecodable(of: PromoResponse.self) { (response) in
            print(response.value)
            print(response)
        }
    }
    
    func getRoomType(outlet: String?, completion:@escaping(_ roomTypeList: [RoomTypeByOutlet]?,_ error: String?)->Void) {
        var parameters: [String:Any] = [:]
        
        if let outlet = outlet {
            parameters["outlet"] = outlet
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/roomtype/findByOutlet", method: .get, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(RoomTypeByOtletResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                completion([], error.errorDescription)
            }
        }
    }
    
    func getSingleReservationDetail(reservation_id: String?, completion:@escaping(_ reservation: SingleReservationDetail?,_ error: String?)->Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters: [String:Any] = [:]
        
        if let reservation_id = reservation_id {
            parameters["reservation_id"] = reservation_id
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/reservation/detail", method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(SingleReservationDetailResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    completion(nil , ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                completion(nil, error.errorDescription)
            }
        }
    }
}

