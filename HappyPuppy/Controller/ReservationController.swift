//
//  ReservationController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ReservationController {
    static let shared = ReservationController()
    
    private init(){}
    
    func createReservation(outletId: String, roomType: String, reservationDate: String, reservationTime: String, duration: String, currentDate: String, description: String, completion:@escaping(_ result: String?, _ message: String?, _ data: String?, _ error: String?) -> Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        let parameters: [String:Any] = [
            "outlet_id": outletId,
            "roomType": roomType,
            "reservationDate": reservationDate,
            "reservationTime": reservationTime,
            "duration": duration,
            "current_date": currentDate,
            "description": description
        ]
        
        AF.request("\(Network.shared.getBaseURLAPI())/reservations/booking", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                        
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let data = json["data"]
                completion(json["result"].stringValue, json["data"].stringValue, data["result"].stringValue, nil)
            case .failure(let error):
                completion("failure", NetworkStatus.FAILURE, nil, error.errorDescription)
            }
        }
    }
    
    func getReservations(length: Int?, start: Int?, completion:@escaping(_ bannerList: [Reservation]?, _ error: String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters: [String:Any] = [:]
        
        if let length = length {
            parameters["length"] = length
        }
        
        if let start = start {
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/reservation/view", method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            
            print(response.request!)
            print(response)
            
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(ReservationResponse.self, from: response.data!)
                    completion(response.data.reservationList, nil)
                } catch (let error) {
                    print(error)
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                print(error)
                completion([], error.errorDescription)
            }
        }
    }
    
    func getSingleReservationDetail(reservation_id: String?, completion:@escaping(_ reservation: Reservation?, _ error: String?)->Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters: [String:Any] = [:]
        
        if let reservation_id = reservation_id {
            parameters["reservation_id"] = reservation_id
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/reservation/detail", method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response.request!)
            print(response)
            switch response.result {
            case .success:
                do {
                    let reservation = try JSONDecoder().decode(Reservation.self, from: JSON(response.data!)["data"]["data"].rawData())
//                    let response = try JSONDecoder().decode(SingleReservationDetailResponse.self, from: response.data!)
                    completion(reservation, nil)
                } catch(let error) {
                    print(error)
                    
                    completion(nil , error.localizedDescription)
                }
            case .failure(let error):
                completion(nil, error.errorDescription)
            }
        }
    }
    
    func cancelReservation(reservation_id: String?, completion:@escaping(_ result: String?, _ message: String?, _ error: String?) -> Void) {
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters: [String:Any] = [:]
        
        if let reservation_id = reservation_id {
            parameters["reservation_id"] = reservation_id
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/reservations/cancel", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(json["result"].stringValue, json["data"].stringValue, nil)
            case .failure(let error):
                completion("failed", NetworkStatus.FAILURE, error.errorDescription)
            }
        }
    }
    
    func uploadReservationPayment(outletCode: String, reservationCode: String, uploadFile: UIImage?, senderBank: String?, senderName: String?, completion:@escaping(_ result: String?,_ message: String?, _ error: String?) -> Void ){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                
        var parameters:[String:Any] = [
            "outlet_code":outletCode,
            "reservation_code":reservationCode,
        ]
        
        if !senderBank!.isEmpty{
            parameters["bank_penyetor"] = senderBank
        }
        
        if !senderBank!.isEmpty{
            parameters["nama_penyetor"] = senderBank
        }
        
        print("UPLOAD PARAMETERS: \(parameters)")
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }

            if let image = uploadFile {
                multipartFormData.append(image.jpegData(compressionQuality: 0.25)!, withName: "upload_file", fileName: "image.jpeg", mimeType: "image/jpeg")
            }   
        }, to: "\(Network.shared.getBaseURLAPI())/reservations/uploadbukti",
            usingThreshold: UInt64.init(),
            method: .post,
            headers: headers).response { response in
                print(response.request!)
                print(response)

                do {
                    let parsedData = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                    print(parsedData)
                } catch {

                }
                switch response.result {
                case .success(let value):
                    print(response)
                    let json = JSON(value!)
                    completion(json["result"].stringValue, json["data"].stringValue, nil)
                case .failure(let error):
                    print(response)
                    completion(nil, nil, error.errorDescription)
                }
        }
    }
    
    func getRoomType(outlet: String?, completion:@escaping(_ roomTypeList: [RoomTypeByOutlet]?, _ error: String?)->Void) {
        var parameters: [String:Any] = [:]
        
        if let outlet = outlet {
            parameters["outlet"] = outlet
        }
        let stringURL = "\(Network.shared.getBaseURLAPI())/roomtype/findByOutlet"
        AF.request(stringURL, method: .get, parameters: parameters).responseJSON { (response) in
            print(response.request!)
            print(response)
            switch response.result {
            case .success:
                do {
                    let response = try JSONDecoder().decode(RoomTypeByOtletResponse.self, from: response.data!)
                    completion(response.data.data, nil)
                } catch {
                    print(error)
                    completion(nil, ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                completion([], error.errorDescription)
            }
        }
    }
    
    func getEstimatedPrices(outletCode:String, roomTypeId:Int, date:String, time:String, duration:String, completion:@escaping(_ roomPrices: [RoomPrice], _ error: String?)->Void) {
        
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters: [String:Any] = [
            "outlet_id": outletCode,
            "roomType": roomTypeId,
            "reservationDate": date,
            "reservationTime": time,
            "duration": duration
        ]
        
        let stringURL = "\(Network.shared.getBaseURLAPI())/reservations/getEstimatedPrice"
        AF.request(stringURL, method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response.request!)
            print(response)
            switch response.result {
            case .success:
                do {
                    let json = JSON(response.data!)
                    if json["result"].stringValue == "failed"{
                        completion([], json["data"].stringValue)
                    }else{
                        let response = try JSONDecoder().decode(RoomPriceResp.self, from: response.data!)
                        completion(response.roomPrices, nil)
                    }
                } catch {
                    print(error)
                    completion([], ErrorMessage.SOMETHING_WRONG)
                }
            case .failure(let error):
                completion([], error.errorDescription)
            }
        }
    }
    
    func getDateRange(completion:@escaping(_ dateRange: Int?, _ error: String?)->Void) {
        
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters: [String:Any] = [:]
        
        let stringURL = "\(Network.shared.getBaseURLAPI())/reservations/getRangeDayReservation"
        AF.request(stringURL, method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response.request!)
            print(response)
            switch response.result {
            case .success:
                let json = JSON(response.data!)
                if json["result"].stringValue == "failed"{
                    completion(nil, json["data"].stringValue)
                }else{
                    completion(json["data"]["Value_"].intValue, nil)
                }
            case .failure(let error):
                completion(nil, error.errorDescription)
            }
        }
    }
    
    func getReservationPolicy(completion:@escaping(_ policy: String?, _ error: String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters: [String:Any] = [:]
        
        let stringURL = "\(Network.shared.getBaseURLAPI())/reservations/getPolicy"
        AF.request(stringURL, method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response.request!)
            print(response)
            switch response.result {
            case .success:
                let json = JSON(response.data!)
                if json["result"].stringValue == "failed"{
                    completion(nil, json["data"].stringValue)
                }else{
                    completion(json["data"]["AgreementRsv"].stringValue, nil)
                }
            case .failure(let error):
                completion(nil, error.errorDescription)
            }
        }
    }
}
