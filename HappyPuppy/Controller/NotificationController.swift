//
//  NotificationController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import GoogleSignIn

class NotificationController {
    static let shared = NotificationController()
    
    func getNotifList(length:Int?, start:Int?, completion:@escaping(_ status:String, _ notifList:[Notification]?, _ totalValue:Int, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        var parameters:[String:Any] = [:]
        
        if let length = length{
            parameters["length"] = length
        }
        
        if let start = start{
            parameters["start"] = start
        }
        
        AF.request("\(Network.shared.getBaseURLAPI())/notification/view", method: .get, parameters: parameters, headers: headers).validate().responseJSON { (response) in
            print(response)
            switch response.result {
            case .success(let value):
                do {
                    let json = JSON(value)
                    let totalValue = json["data"]["totalValue"].intValue
                    let notificationResponse = try JSONDecoder().decode(NotificationResponse.self, from: response.data!)
                    let notifList = notificationResponse.data.data
                                        
                    completion(NetworkStatus.SUCCESS, notifList, totalValue, nil)
                } catch  (let error){
                    print(error)
                    completion(NetworkStatus.FAILURE, nil, 0, error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func updateNotifToRead(notifId:Int, completion:@escaping(_ status:String, _ error:String?)->Void){
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
        
        let parameters:[String:Any] = [
            "id" : notifId
        ]
        
        AF.request("\(Network.shared.getBaseURLAPI())/notification/setRead", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON(completionHandler: { (response) in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completion(NetworkStatus.SUCCESS, nil)
                case .failure(let error):
                    print(error)
                    completion(NetworkStatus.FAILURE, error.localizedDescription)
                }
            })
    }
    
    func getUnreadNotifCount(completion:@escaping(_ status: String,_ message:String?,_ count:Int?)->Void){
        let urlString = "\(Network.shared.getBaseURLAPI())/notification/checkUnread"
        let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", UserDefaultHelper.shared.getToken())]
                
        AF.request(urlString, method: .get, parameters: [:], headers: headers)
            .validate()
            .responseData(completionHandler: { (response) in
                guard let request = response.request else {return}
                print(request)
                
                print("RESPONSE ")
                print(response.error)
                print(response.error?.asAFError?.responseCode)
                
                if let error = response.error, let AFError = error.asAFError, let responseCode = AFError.responseCode{
                    if responseCode == 403{
                        completion(NetworkStatus.FAILURE, ErrorMessage.UNAUTHORIZED, nil)
                    }else{
                        completion(NetworkStatus.FAILURE, error.localizedDescription, nil)
                    }
                }else{
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        let totalCount = json["data"]["totalCount"].intValue
                        completion(NetworkStatus.SUCCESS, nil, totalCount)
                    case .failure(let error):
                        print(error)
                        completion(NetworkStatus.FAILURE, error.localizedDescription, nil)
                    }
                }
            })
    }
}
