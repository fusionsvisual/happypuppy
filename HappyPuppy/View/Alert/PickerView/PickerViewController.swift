//
//  PickerViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 23/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

protocol CustomPickerDelegate {
    func didPickerSelect(item:String)
}

class PickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let list = ["abc", "bca"]
    
    var delegate:CustomPickerDelegate?
    @IBOutlet var containerView: UIView!
    @IBOutlet var pickerViewContainer: UIView!
    
    @IBOutlet var pickerView: UIPickerView!
    var selectedItem:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        containerView.addGestureRecognizer(gesture)
    }
    
    @objc private func dismissView(){
        dismiss(animated: false)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[row]
    }
    
    @IBAction func doneAction(_ sender: Any) {
        dismissView()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.didPickerSelect(item: list[row])
    }
}
