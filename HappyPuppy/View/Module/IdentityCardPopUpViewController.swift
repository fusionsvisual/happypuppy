//
//  IdentityCardPopUpViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 15/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

protocol UploadIdentityCardDelegate {
    func didUploadClicked()
}

class IdentityCardPopUpViewController: UIViewController {

    var delegate:UploadIdentityCardDelegate?
    
    @IBOutlet var overlayView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var containerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var descLabel: UILabel!

    @IBOutlet var identityCardButton: UIButton!
    @IBOutlet var laterButton: UIButton!
    
    var IDCardStatus:String?
    var IDCardNote:String?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        overlayView.addGestureRecognizer(tapGesture)
        
        identityCardButton.layer.cornerRadius = 8
         laterButton.layer.cornerRadius = 8
        
        //MARK: Hide ContainerView
        self.containerBottomConstraint.constant = -self.containerView.frame.height
        
        //MARK: Setup Description Label
        if self.IDCardStatus == "0"{
            descLabel.text = "Untuk melengkapi proses verifikasi data, mohon kesediaanya untuk upload foto KTP anda (jika di bawah 17 tahun bisa upload kartu pelajar). Proses verifikasi dibutuhkan untuk melakukan reservasi di outlet kami."
        }else{
            if let IDCardNote  = self.IDCardNote{
                descLabel.text = "KTP anda telah ditolak oleh admin untuk alasan: \(IDCardNote). Mohon mengupload foto KTP lagi."
            }
        }
    }
    
    func showModal(){
        //MARK: Show ContainerView with Animation
        UIView.animate(withDuration: 0.15, animations: {
            self.containerBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }) { (isDone) in
            
        }
    }
    
    @objc private func dismissVC(){
        UIView.animate(withDuration: 0.25, animations: {
            self.containerBottomConstraint.constant = -self.containerView.frame.height
            self.view.layoutIfNeeded()
        }) { (isDone) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.didUploadClicked()
        }
//        self.present(uploadIdentityCardVC, animated: true, completion: nil)
        
    }
    
    @IBAction func laterAction(_ sender: Any) {
        self.dismissVC()
    }
    
}
