//
//  UploadIdentityCardViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

class UploadIdentityCardViewController: UIViewController,AVCapturePhotoCaptureDelegate{
    
    @IBOutlet var imagePreviewImageView: UIImageView!
    @IBOutlet var captureButton: UIButton!
    
    var imagePickers:UIImagePickerController?
    var previewLayer:AVCaptureVideoPreviewLayer?
    
    @IBOutlet var faceFrameView: UIView!
    @IBOutlet var bottomFrameView: UIView!
    @IBOutlet var cameraView: UIView!
    @IBOutlet var imagePreviewView: UIView!
    @IBOutlet var captureView: UIView!
    @IBOutlet var optionView: UIView!
    
    @IBOutlet var uploadButton: UIButton!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imagePreviewViewBottom: NSLayoutConstraint!
    
    private let photoOutput = AVCapturePhotoOutput()
    private var captureSession:AVCaptureSession?
    private var uploadedImage:UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.captureSession = AVCaptureSession()
        self.imagePreviewViewBottom.constant = -self.imagePreviewView.frame.height
        
        //MARK: Setup Dashed Line
        self.setupDashedLine(view: bottomFrameView, shape: "SQUARE")
        self.faceFrameView.backgroundColor = .clear
        
        self.bottomFrameView.backgroundColor = .clear
        self.setupDashedLine(view: self.faceFrameView, shape: "OVAL")
                
        self.captureView.layer.cornerRadius =  self.captureView.frame.height/2
        self.captureButton.layer.cornerRadius =  self.captureButton.frame.height/2
            
        self.imagePreviewView.layer.cornerRadius = 20
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:
                self.setupCaptureSession()
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.setupCaptureSession()
                    }
                }
            case .restricted, .denied:
                SVProgressHUD.showError(withStatus: "Camera not available! change the permission on settings")
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG) {
                    self.navigationController?.popViewController(animated: true)
                }
            default:
                return
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupCaptureSession(){
        DispatchQueue.main.async {
            guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back) else { return }
            guard let input = try? AVCaptureDeviceInput(device: captureDevice) else { return }
            guard let captureSession = self.captureSession else { return }
            captureSession.addInput(input)
            captureSession.addOutput(self.photoOutput)
            captureSession.startRunning()
            
            self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            if let previewLayer =  self.previewLayer{
                previewLayer.videoGravity = .resizeAspectFill
                self.cameraView.layer.addSublayer(previewLayer)
                previewLayer.frame = self.cameraView.bounds
            }
        }
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let sampleBuffer = photoSampleBuffer,
            let imageData =  AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer:  sampleBuffer, previewPhotoSampleBuffer: nil) {
            
            UIView.animate(withDuration: 0.15, animations: {
                self.imagePreviewViewBottom.constant = 0
                self.view.layoutIfNeeded()
            }) { (isDone) in
                if let image = UIImage(data: imageData){
                    
                    var finalImage = UIImage.init(cgImage: image.cgImage!, scale: image.scale, orientation: .right)
                    // MARK: Check current camera position
                    if let session = self.captureSession {
                        guard let currentCameraInput: AVCaptureInput = session.inputs.first else {return}
                        if let input = currentCameraInput as? AVCaptureDeviceInput {
                            if (input.device.position == .front) {
                                finalImage = UIImage.init(cgImage: image.cgImage!, scale: image.scale, orientation: .leftMirrored)
                            }
                            finalImage = finalImage.fixOrientation()
                        }
                    }
                    
                    self.imagePreviewImageView.image = finalImage
                    self.uploadedImage = finalImage
                }
            }
            
            if let captureSession = self.captureSession{
                captureSession.stopRunning()
            }
        } else {
            print("some error here")
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        
        //MARK: Photo Capture Animation
        cameraView.layer.opacity = 0
        UIView.animate(withDuration: 0.25) {
            self.cameraView.layer.opacity = 1
        }
    }
    
    private func setupDashedLine(view:UIView, shape:String){
        let dashedBorder = CAShapeLayer()
        if shape == "SQUARE"{
            dashedBorder.path = UIBezierPath(rect: view.bounds).cgPath
        }else{
            dashedBorder.path = UIBezierPath(ovalIn: view.bounds).cgPath
        }
        dashedBorder.strokeColor = UIColor.white.cgColor
        dashedBorder.lineWidth = 4
        dashedBorder.lineDashPattern = [10, 10]
        dashedBorder.fillColor = nil
        dashedBorder.lineJoin = .round
        dashedBorder.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.25)
        view.layer.addSublayer(dashedBorder)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        
        self.uploadButton.setTitle("", for: .normal)
        self.activityIndicator.isHidden = false
        SVProgressHUD.show(withStatus: "Uploading KTP")
        
        if let uploadedImage = self.uploadedImage{
            ProfileController.shared.uploadIdentityCard(identityCardImage: uploadedImage) { (status, error) in
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                }else{
                    SVProgressHUD.dismiss(withDelay: 1) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func captureAction(_ sender: Any) {
        self.photoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
    }
    
    @IBAction func switchCameraAction(_ sender: Any) {
        if let session = captureSession {
            guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
                return
            }

            session.beginConfiguration()
            session.removeInput(currentCameraInput)

            var newCamera: AVCaptureDevice! = nil
            if let input = currentCameraInput as? AVCaptureDeviceInput {
                if (input.device.position == .back) {
                    newCamera = cameraWithPosition(position: .front)
                } else {
                    newCamera = cameraWithPosition(position: .back)
                }
            }
                        
            var err: NSError?
            var newVideoInput: AVCaptureDeviceInput!
            do {
                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
            } catch let err1 as NSError {
                err = err1
                newVideoInput = nil
            }

            if newVideoInput == nil || err != nil {
                print("Error creating capture device input: \(err?.localizedDescription)")
            } else {
                session.addInput(newVideoInput)
            }

            session.commitConfiguration()
        }

    }
    
    // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
        for device in discoverySession.devices {
            if device.position == position {
                return device
            }
        }

        return nil
    }
}
