//
//  PointDateCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PointDateCollectionViewCell: UICollectionViewCell {

    /* UILabel */
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    /*-----*/
    
    /* UIView */
    @IBOutlet var containerView: UIView!
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
    }

}
