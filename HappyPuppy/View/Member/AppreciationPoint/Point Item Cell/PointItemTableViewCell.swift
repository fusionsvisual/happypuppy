//
//  PointItemTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 02/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PointItemTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
