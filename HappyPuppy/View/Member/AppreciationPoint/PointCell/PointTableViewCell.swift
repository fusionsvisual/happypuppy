//
//  PointTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 26/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PointTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    /* UIView */
    @IBOutlet var containerView: UIView!
    /*-----*/
    
    /* UIImageView */
    @IBOutlet var outletImageView: UIImageView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var pointLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var outletLabel: UILabel!
    /*-----*/
    
    @IBOutlet var prizeTableView: UITableView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    
    var pointDetails:[PointPrizeDetail] = []
    /* NSLayoutConstraint */
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        
        prizeTableView.delegate = self
        prizeTableView.dataSource = self
        prizeTableView.register(UINib(nibName: "PointItemTableViewCell", bundle: nil), forCellReuseIdentifier: "PointItemCell")
        prizeTableView.estimatedRowHeight = 44.0 ;
        prizeTableView.rowHeight = UITableView.automaticDimension;
    }
    
    private func setupViews(){
        /* Setup containerView UIView */
        containerView.layer.shadowColor  = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.25
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 2
        /*-----*/
    }
    
    override func prepareForReuse() {
        tableViewHeight.constant = CGFloat(16 * pointDetails.count)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointItemCell", for: indexPath) as! PointItemTableViewCell
        
        let pointDetail = pointDetails[indexPath.item]
        cell.nameLabel.text = pointDetail.name
        cell.pointLabel.text = String(pointDetail.point!)
        return cell
    }
}
