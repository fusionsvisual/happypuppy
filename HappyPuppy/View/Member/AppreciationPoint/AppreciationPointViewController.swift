//
//  AppreciationPointViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class AppreciationPointViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /* UITableView */
    @IBOutlet var pointTableView: UITableView!
    /*-----*/
    
    /* UITextField */
    @IBOutlet var monthTextField: UITextField!
    @IBOutlet var yearTextField: UITextField!
    /*-----*/
    
    /* UICollectionView */
    @IBOutlet var pointDataCollectionView: UICollectionView!
    /*-----*/
    
    /* UIView */
    @IBOutlet var monthViewContainer: UIView!
    @IBOutlet var yearViewContainer: UIView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var noDataLabel: UILabel!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* Variable */
    let monthList = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
    var yearList:[String] = []
    var monthIdx = 0
    var selectedYear = "Januari"
    var pointList:[Point] = []
    var pointExpireList:[PointExpire] = []
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func setupViews(){
        hideKeyboardWhenTappedAround()
        setupNavigationController()
        
        navigationController?.isNavigationBarHidden = false
        self.title = "Poin Apresiasi"
        
        /* Setup Point Data Collection View */
        pointDataCollectionView.delegate = self
        pointDataCollectionView.dataSource = self
        pointDataCollectionView.register(UINib(nibName: "PointDateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "pointDataCell")
        pointDataCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        let pointDataViewLayout = UICollectionViewFlowLayout()
        pointDataViewLayout.scrollDirection = .horizontal
        pointDataViewLayout.minimumLineSpacing = 16
        pointDataCollectionView.collectionViewLayout = pointDataViewLayout
        
        /* Setup Month UIView */
        monthViewContainer.layer.cornerRadius = 10
        monthViewContainer.layer.borderWidth = 1
        monthViewContainer.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        /*-----*/
        
         /* Setup UIToolbar for UIPickerView */
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pickerDone))
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.items = [spaceButton, btnDone]
        /*-----*/
        
        /* Get Current Date */
        let date = Date()
        let monthString = date.month
        
        let currentYear = Int(date.year)!
        for i in 0...9{
            yearList.append(String(currentYear - i))
        }
        yearList.sort(by: >)
        
        let yearString = date.year
        /*-----*/
        
        /* Setup monthTextField PickerView */
        print(monthString)
        self.monthIdx = monthList.firstIndex(of: monthString)! + 1
        let monthPickerView = UIPickerView()
        monthPickerView.delegate = self
        monthPickerView.dataSource = self
        monthPickerView.selectRow((self.monthIdx-1), inComponent: 0, animated: false)
        monthTextField.inputView = monthPickerView
        monthTextField.inputAccessoryView = toolbar
        monthTextField.tintColor = .clear
        monthTextField.text = monthList.filter{$0==monthString}.first
        /*-----*/
        
        /* Setup yearTextField PickerView */
        let yearIdx = yearList.firstIndex(of: yearString)!
        let yearPickerView = UIPickerView()
        yearPickerView.delegate = self
        yearPickerView.dataSource = self
        yearPickerView.selectRow(yearIdx, inComponent: 0, animated: false)
        yearTextField.inputView = yearPickerView
        yearTextField.inputAccessoryView = toolbar
        yearTextField.tintColor = .clear
        yearTextField.text = yearList.filter{$0==yearString}.first
        self.selectedYear = yearString
        /*-----*/
        
        /* Setup Year UIView */
        yearViewContainer.layer.cornerRadius = 10
        yearViewContainer.layer.borderWidth = 1
        yearViewContainer.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        /*-----*/
        
        /* Setup Table View */
        pointTableView.delegate = self
        pointTableView.dataSource = self
        pointTableView.register(UINib(nibName: "PointTableViewCell", bundle: nil), forCellReuseIdentifier: "PointCell")
        pointTableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 8, right: 0)
        pointTableView.rowHeight = UITableView.automaticDimension
        /*-----*/
    }
    
    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        self.activityIndicator.isHidden = false
        
        /* Clear pointExpireList  */
        self.pointExpireList.removeAll()
        self.pointDataCollectionView.reloadData()
        /*-----*/
        
        clearOutletData()
        setupData()
    }
    
    private func clearOutletData(){
        /* Clear pointList  */
        self.pointList.removeAll()
        self.pointTableView.reloadData()
        /*-----*/
    }
    
    
    private func setupData(){
        PointController.shared.getPointExpireList(month: String(monthIdx)) { (status, pointExpireList, error) in
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }else{
                if let pointExpireList = pointExpireList{
                    self.pointExpireList = pointExpireList
                    self.pointDataCollectionView.reloadData()
                }
                self.setupOutletData()
            }
        }
    }
    
    private func setupOutletData(){
        PointController.shared.getPointList(year: selectedYear, month: String(monthIdx), length: nil, startDate: nil) { (status, pointList, error) in
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }else{
                if pointList?.count == 0{
                    /* Setup noDataLabel UILabel */
                    self.noDataLabel.isHidden = false
                    /*-----*/
                }else{
                    if let pointList = pointList{
                        self.noDataLabel.isHidden = true
                        self.pointList = pointList
                        self.pointTableView.reloadData()
                    }
                }
                self.activityIndicator.isHidden = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 125, height: 75 )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pointExpireList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let pointExpire = pointExpireList[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pointDataCell", for: indexPath) as! PointDateCollectionViewCell
        
        /* Setup monthLabel UILabel */
        if let month = pointExpire.month{
            if let year = pointExpire.year{
                cell.monthLabel.text = "\(month) \(year)"
            }
        }
        /*-----*/
        
        /* Setup pointLabel UILabel */
        if let point = pointExpire.total{
            cell.pointLabel.text = "\(point) PTS"
        }
        /*-----*/
        
        return cell
    }
    
    
    /* Tableview Handler */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let pointObj = pointList[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointCell", for: indexPath) as! PointTableViewCell
        
        /* Setup Reward ImageView */
        if let brandData = pointObj.brand{
            if let photoUrl = brandData.photoUrl{
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    cell.outletImageView.af.setImage(withURL: url, filter: nil)
                    cell.outletImageView.contentMode = .scaleAspectFit
                }
            }
        }
        /*-----*/
        
        /* Setup pointLabel UILabel */
        if let point = pointObj.point{
            if pointObj.type == PointTypeConstant.ADD{
                cell.pointLabel.text = "+\(point)"
                 cell.pointLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.5725490196, blue: 0.1254901961, alpha: 1)
            }else{
                cell.pointLabel.text = "-\(point)"
                cell.pointLabel.textColor = .red
            }
        }
        /*-----*/
        
        /* Setup dateLabel UILabel */
        if let date = pointObj.date{
            cell.dateLabel.text = date
        }
        /*-----*/
        
        /* Setup outletLabel UILabel */
         cell.outletLabel.text =  pointObj.outlet.name
        /*-----*/
        
        
        if let prizeDetail = pointObj.detail{
            cell.pointDetails = prizeDetail
            cell.prepareForReuse()
            cell.prizeTableView.reloadData()
        }
        
        return cell
    }
    /*-----*/
    
    /* UIPickerView Action */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if monthTextField.isFirstResponder{
            return monthList.count
        }else{
            return yearList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if monthTextField.isFirstResponder{
            return monthList[row]
        }else{
            return yearList[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if monthTextField.isFirstResponder{
            monthTextField.text = monthList[row]
            monthIdx = monthList.firstIndex(of: monthList[row])! + 1
            
        }else{
            yearTextField.text = yearList[row]
            selectedYear = yearList[row]
        }
        clearOutletData()
        self.noDataLabel.isHidden = true
        self.activityIndicator.isHidden = false
        setupOutletData()
    }
    
    @objc private func pickerDone(){
        dismissKeyboard()
    }
    /*-----*/
    
    /* UIButton Handler */
    @IBAction func monthAction(_ sender: Any) {
        monthTextField.becomeFirstResponder()
    }
    
    @IBAction func yearAction(_ sender: Any) {
        yearTextField.becomeFirstResponder()
    }
    /*-----*/
}
