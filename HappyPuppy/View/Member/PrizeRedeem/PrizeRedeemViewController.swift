//
//  PrizeRedeemViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage

class PrizeRedeemViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    /* UIButton */
    @IBOutlet var notYetRedeemButton: UIButton!
    @IBOutlet var hasBeenRedeemButton: UIButton!
    /*-----*/
    
    /* UITableView */
    @IBOutlet var prizeRedeemTableView: UITableView!
    /*-----*/
    
    /* Variable */
    var rewardList:[MyReward] = []
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var prizeActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        self.title = "Pengambilan Hadiah"
        setupNavigationController()
        
        hasBeenRedeemButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        hasBeenRedeemButton.layer.borderWidth = 1
        
        /* Setup prizeRedeemTableView UITableView  */
        prizeRedeemTableView.delegate = self
        prizeRedeemTableView.dataSource = self
        prizeRedeemTableView.register(UINib(nibName: "PrizeRedeemTableViewCell", bundle: nil), forCellReuseIdentifier: "PrizeRedeemCell")
        prizeRedeemTableView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 24
            , right: 0)
        /*-----*/
        
    }
    
    private func setupData(){
        prizeActivityIndicator.isHidden = false
        if notYetRedeemButton.isSelected{
            RewardController.shared.getMyRewardList(length: nil, startDate: nil) { (status, rewardList, error) in
                
                self.prizeActivityIndicator.isHidden = true
                
                if let error = error{
                    print(error)
                }else{
                    if let rewardList = rewardList{
                        self.rewardList = rewardList
                        self.prizeRedeemTableView.reloadData()
                    }
                }
            }
        }else{
            RewardController.shared.getMyFinishedRewardList(length: nil, startDate: nil) { (status, rewardList, error) in
                
                self.prizeActivityIndicator.isHidden = true
                
                if let error = error{
                    print(error)
                }else{
                    if let rewardList = rewardList{
                        self.rewardList = rewardList
                        self.prizeRedeemTableView.reloadData()
                    }
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
       }
    
    @objc private func refresh(){
        self.clearData()
        setupData()
    }
    
    private func clearData(){
        prizeActivityIndicator.isHidden = false
        rewardList.removeAll()
        prizeRedeemTableView.reloadData()
    }
    
    
    
    @IBAction func notYetRedeemAction(_ sender: Any) {
        notYetRedeemButton.isSelected = true
        notYetRedeemButton.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        notYetRedeemButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        hasBeenRedeemButton.isSelected = false
        hasBeenRedeemButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        hasBeenRedeemButton.setTitleColor(#colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1), for: .normal)
        hasBeenRedeemButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        hasBeenRedeemButton.layer.borderWidth = 1
        self.clearData()
        setupData()
    }
    
    @IBAction func hasBeenRedeemAction(_ sender: Any) {
        hasBeenRedeemButton.isSelected = true
        hasBeenRedeemButton.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        hasBeenRedeemButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        notYetRedeemButton.isSelected = false
        notYetRedeemButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        notYetRedeemButton.setTitleColor(#colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1), for: .normal)
        notYetRedeemButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        notYetRedeemButton.layer.borderWidth = 1
        self.clearData()
        setupData()
    }
    
    /* UITableView Handler */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rewardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reward = self.rewardList[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrizeRedeemCell", for: indexPath) as! PrizeRedeemTableViewCell
        
        
        if let invoiceCode = reward.invoiceCode{
            cell.invoiceCodeLabel.text = invoiceCode
            /* Setup qrCodeImageView UIImageView */
            Global.shared.generateQRCode(from: invoiceCode) { (image) in
                cell.qrCodeActivityIndicator.isHidden = true
                cell.qrCodeImageView.image = image
            }
            /*-----*/
        }
        
        
        if let rewardData = reward.rewardData{
            /* Setup prizeNameLabel UILabel */
            cell.prizeNameLabel.text = rewardData.name
            /*-----*/
            
            /* Setup prizeImageView UIImageView */
            if let photoUrl = rewardData.photoUrl{
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)"){
                    cell.prizeImageView.af.setImage(withURL: url) { (image) in
                        
                    }
                }
            }
            /*-----*/
        }
        
        /* Setup quantityLabel UILabel */
        if let quantity = reward.quantity{
            cell.quantityLabel.text = String(quantity)
        }else{
            cell.quantityLabel.text = String(-1)
        }
        /*-----*/
        
        /* Setup transactionDateLabel UILabel */
        cell.transactionDateLabel.text = reward.transactionDate
        /*-----*/
        
        if let outletData = reward.outletData{
            cell.outletAddressLabel.text = outletData.address
        }
        
        return cell
    }
    /*-----*/
    
}
