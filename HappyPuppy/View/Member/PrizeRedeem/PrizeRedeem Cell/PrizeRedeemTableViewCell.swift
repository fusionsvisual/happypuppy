//
//  PrizeRedeemTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PrizeRedeemTableViewCell: UITableViewCell {
    
    /* UIImageView */
    @IBOutlet var prizeImageView: UIImageView!
    @IBOutlet var qrCodeImageView: UIImageView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var prizeNameLabel: UILabel!
    @IBOutlet var invoiceCodeLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var outletNameLabel: UILabel!
    @IBOutlet var outletAddressLabel: UILabel!
    @IBOutlet var transactionDateLabel: UILabel!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var qrCodeActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
