//
//  MemberContentTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class MemberContentTableViewCell: UITableViewCell {

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
