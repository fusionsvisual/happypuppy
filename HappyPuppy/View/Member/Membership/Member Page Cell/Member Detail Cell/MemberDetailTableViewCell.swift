//
//  MemberDetailTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 04/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class MemberDetailTableViewCell: UITableViewCell {

    @IBOutlet var containerView: UIView!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.shadowColor  = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.25
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
