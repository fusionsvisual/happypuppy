//
//  MembershipViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

struct MemberData{
    var isOpened:Bool = false
    var name:String = ""
    var patternURL:String?
    var iconURL:String?
    var benefitList:[Benefit] = []
}

class MembershipViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {    

    @IBOutlet var membershipTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var memberMenuList = ["BASIC", "SILVER", "GOLD"]
    var memberList:[MemberData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        setupNavigationController()
        
        membershipTableView.delegate = self
        membershipTableView.dataSource = self
        membershipTableView.register(UINib(nibName: "MemberHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberHeaderCell")
        membershipTableView.register(UINib(nibName: "MemberContentTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberContentCell")
        membershipTableView.estimatedRowHeight = UITableView.automaticDimension
        membershipTableView.rowHeight =  UITableView.automaticDimension
        membershipTableView.register(UINib(nibName: "MemberHeaderViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "MemberHeaderView")
    }
    
    private func setupData(){
        self.activityIndicator.isHidden = false
        self.membershipTableView.isHidden = true
        
        DispatchQueue.main.async {
            ProfileController.shared.getMemberList { (status, memberList, error) in
                self.activityIndicator.isHidden = true
                self.membershipTableView.isHidden = false

                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let memberList = memberList{
                        for member in memberList{
                            let name = member.name
                            let iconURL = member.iconImageUrl
                            let patternURL = member.patternURL
                            let benefitList = member.benefitList
                            let memberData = MemberData(isOpened: false, name: name, patternURL: patternURL, iconURL: iconURL, benefitList: benefitList)
                            self.memberList.append(memberData)
                        }
                        self.membershipTableView.reloadData()
                    }
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.title = "Keanggotaan"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
       }
    
    @objc private func refresh(){
        self.memberList.removeAll()
        self.setupData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MemberHeaderView") as! MemberHeaderViewCell
            if let lowMember = memberList.first, let highMember = memberList.last{
                headerView.descLabel.text = "Tingkatkan level keanggotaan Puppy Club! anda dari \(lowMember.name) menuju \(highMember.name) dengan mengumpulkan poin dari setiap transaksi, dan raih keuntungan di tiap levelnya."
            }
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
             return 130
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return memberList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if memberList[section].isOpened == true{
            return memberList[section].benefitList.count + 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let member = memberList[indexPath.section]
        if indexPath.item == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberHeaderCell", for: indexPath) as! MemberHeaderTableViewCell
            cell.nameLabel.text = member.name
            if let patternURL = URL(string: "https://adm.happypuppy.id/\(member.patternURL!)"){
                cell.headerImageView.af.setImage(withURL: patternURL)
            }
            if let iconURL = URL(string: "https://adm.happypuppy.id/\(member.iconURL!)"){
                cell.iconImageView.af.setImage(withURL: iconURL)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberContentCell", for: indexPath) as! MemberContentTableViewCell
            cell.descriptionLabel.text = member.benefitList[indexPath.item-1].description
//            if (member.benefitList.count-1) == (indexPath.item-1){
//                cell.bottomConstraint.constant = 16
//            }
//            cell.nameLabel.text = member.name
//            if let patternURL = URL(string: "https://adm.happypuppy.id/\(member.patternURL)"){
//                cell.headerImageView.af.setImage(withURL: patternURL)
//            }
//            if let iconURL = URL(string: "https://adm.happypuppy.id/\(member.iconURL)"){
//                cell.iconImageView.af.setImage(withURL: iconURL)
//            }
            return cell
        }
        
        
        
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if memberList[indexPath.section].isOpened == true{
            memberList[indexPath.section].isOpened = false
            let sections = IndexSet.init(integer: indexPath.section)
            membershipTableView.reloadSections(sections, with: .none)
        }else{
            memberList[indexPath.section].isOpened = true
            let sections = IndexSet.init(integer: indexPath.section)
            membershipTableView.reloadSections(sections, with: .none)
        }
    }
}
