//
//  MemberHeaderViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class MemberHeaderViewCell: UITableViewHeaderFooterView {
    @IBOutlet var descLabel: UILabel!
}
