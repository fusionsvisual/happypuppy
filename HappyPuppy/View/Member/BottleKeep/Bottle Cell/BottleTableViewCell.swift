//
//  BottleTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 01/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class BottleTableViewCell: UITableViewCell {

    /* UILabel */
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var volumeLabel: UILabel!
    @IBOutlet var saveDateLabel: UILabel!
    @IBOutlet var redeemerNameLabel: UILabel!
    @IBOutlet var takeDateLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    /*-----*/
    
    /* UIView */
    @IBOutlet var redeemerNameContainerView: UIView!
    @IBOutlet var takeDateContainerView: UIView!
    /*-----*/
    
    /* UIImageView */
    @IBOutlet var bottleImageView: UIImageView!
    @IBOutlet var qrCodeImageView: UIImageView!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var qrCodeActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
