//
//  BottleKeepViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class BottleKeepViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /* UILabel */
    @IBOutlet var noBottleLabel: UILabel!
    /*----*/
    
    /* UIButton */
    @IBOutlet var activeButton: UIButton!
    @IBOutlet var doneButton: UIButton!
    /*----*/
    
    /* UITableView */
    @IBOutlet var bottleTableView: UITableView!
    /*----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*----*/
    
    /* Variable */
    private var bottleList:[Bottle] = []
    /*----*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        self.title = "Simpanan Botol"
        setupNavigationController()
        
        /* Setup bottleTableView UITableView */
        bottleTableView.delegate = self
        bottleTableView.dataSource = self
        bottleTableView.register(UINib(nibName: "BottleTableViewCell", bundle: nil), forCellReuseIdentifier: "BottleCell")
        bottleTableView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 24, right: 0)
        /*----*/
        
        /* Setup activeButton UIButton */
        activeButton.layer.borderColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        activeButton.layer.borderWidth = 1
        /*----*/
        
        /* Setup doneButton UIButton */
        doneButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        doneButton.layer.borderWidth = 1
        /*----*/
    }
    
    private func setupData(){
        self.activityIndicator.isHidden = false
        self.noBottleLabel.isHidden = true
        bottleList.removeAll()
        bottleTableView.reloadData()
        
        if activeButton.isSelected{
            BottleController.shared.getActiveBottle(length: nil, start: nil) { (status, bottleList, error) in
                
                self.activityIndicator.isHidden = true
                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let bottleList = bottleList{
                        if bottleList.count != 0{
                            self.bottleList = bottleList
                            self.bottleTableView.reloadData()
                        }else{
                            self.noBottleLabel.isHidden = false
                        }
                    }
                }
            }
        }else{
            BottleController.shared.getDoneBottle(length: nil, start: nil) { (status, bottleList, error) in
                
                self.activityIndicator.isHidden = true
                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let bottleList = bottleList{
                        if bottleList.count != 0{
                            self.bottleList = bottleList
                            self.bottleTableView.reloadData()
                        }else{
                            self.noBottleLabel.isHidden = false
                        }
                    }
                }
                
            }
        }
    }
    
    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bottleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bottle = bottleList[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier:"BottleCell", for: indexPath) as! BottleTableViewCell
        cell.brandLabel.text = bottle.brandDisplayText
        cell.sizeLabel.text = bottle.size
        cell.volumeLabel.text = bottle.volume
        cell.saveDateLabel.text = bottle.saveDate
        cell.statusLabel.text = bottle.status
        
        if activeButton.isSelected{
            cell.redeemerNameContainerView.isHidden = true
            cell.takeDateContainerView.isHidden = true
        }else{
            cell.redeemerNameLabel.text = bottle.redeemerName
            cell.takeDateLabel.text = bottle.takeDate
            cell.redeemerNameContainerView.isHidden = false
            cell.takeDateContainerView.isHidden = false
        }
        
        DispatchQueue.main.async {
            Global.shared.generateQRCode(from: bottle.qrCode, completion: { (image) in
                cell.qrCodeImageView.image = image
            })
        }
        
        if let imageURL = URL(string: "https://adm.happypuppy.id/\(bottle.photoUrl)"){
            cell.bottleImageView.af.setImage(withURL: imageURL)
            cell.qrCodeActivityIndicator.isHidden = true
        }
        
        return cell
    }
    
    @IBAction func activeAction(_ sender: Any) {
        activeButton.isSelected = true
        activeButton.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        activeButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        activeButton.layer.borderColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        
        doneButton.isSelected = false
        doneButton.backgroundColor = .clear
        doneButton.setTitleColor(#colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1), for: .normal)
        doneButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        
        setupData()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        doneButton.isSelected = true
        doneButton.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        doneButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        doneButton.layer.borderColor = #colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1)
        
        activeButton.isSelected = false
        activeButton.backgroundColor = .clear
        activeButton.setTitleColor(#colorLiteral(red: 0.2392156863, green: 0.3568627451, blue: 0.6705882353, alpha: 1), for: .normal)
        activeButton.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        
        setupData()
    }
    
    @objc private func refresh(){
        setupData()
    }
}
