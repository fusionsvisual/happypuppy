//
//  BottleVerificationViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class BottleVerificationViewController: UIViewController {

    /* UIImageView */
    @IBOutlet var bottleImageView: UIImageView!
    /*-----*/
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var volumeLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var takeByLabel: UILabel!
    @IBOutlet var redeemerNameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!

    /* UITextField */
    @IBOutlet var otpTextField: UITextField!
    /*-----*/

    @IBOutlet var takeByContainerView: UIView!
    @IBOutlet var redeemerNameContainerView: UIView!
    @IBOutlet var phoneContainerView: UIView!
    
    var bottle:Bottle?

    init(bottle: Bottle) {
        super.init(nibName: nil, bundle: nil)
        self.bottle = bottle
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    private func setupViews(){
        hideKeyboardWhenTappedAround()
        self.title = "Simpan Botol OTP"
        
        if let bottle = self.bottle{
            /* Setup ImageView */
            if let url = URL(string: "https://adm.happypuppy.id/\(bottle.photoUrl)") {
                bottleImageView.af.setImage(withURL: url, filter: nil)
            }
            /*-----*/
            
            /* Setup UILabel */
            brandLabel.text = bottle.brandDisplayText
            volumeLabel.text = bottle.volume
            sizeLabel.text = bottle.size
            /*-----*/
        }
        
        /* Setup otpTextField UITextField Placeholder */
        otpTextField.attributedPlaceholder = NSAttributedString(string: "OTP",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        /*-----*/
        
        setupFormInputs()
    }

    private func setupFormInputs() {
        guard let bottle = bottle,
              let takeStatus = bottle.takeStatus else {
            // Setup form inputs for save state
            takeByContainerView.isHidden = true
            redeemerNameContainerView.isHidden = true
            phoneContainerView.isHidden = true
            return
        }
        // Setup form inputs for take state
        takeByLabel.text = TakeStatus(rawValue: takeStatus)?.displayTitle
        redeemerNameLabel.text = bottle.redeemerName
        phoneLabel.text = bottle.takePhone
    }

    private func textFieldValidator()->Bool{
        
        if otpTextField.text!.isEmpty{
            return false
        }
        
        return true
    }

    private func startSaveOtpVerification(
        bottleId: Int,
        otpNumber: String
    ) {
        BottleController.shared.saveVerification(bottleId: bottleId, otpNumber: otpNumber) { (status, message, error) in
            if let error = error{
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
            }else{
                SVProgressHUD.dismiss()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    private func startTakeOtpVerification(
        bottleId: Int,
        otpNumber: String
    ) {
        BottleController.shared.verifyTakeOtp(
            bottleId: bottleId,
            otpNumber: otpNumber,
            onSuccess: { [weak self] successMessage in
                SVProgressHUD.dismiss()
                self?.navigationController?.popViewController(animated: true)
            },
            onFailed: { errorMessage in
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: errorMessage)
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
            }
        )
    }

    @IBAction func sendOTPAction(_ sender: Any) {
        if textFieldValidator(){
            SVProgressHUD.show(withStatus: "Mengecek OTP")
            guard let bottle = bottle,
                  let otpNumber = otpTextField.text else {
                return
            }
            if bottle.takeStatus == nil {
                startSaveOtpVerification(
                    bottleId: bottle.id,
                    otpNumber: otpNumber
                )
                return
            }
            startTakeOtpVerification(
                bottleId: bottle.id,
                otpNumber: otpNumber
            )
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
}
