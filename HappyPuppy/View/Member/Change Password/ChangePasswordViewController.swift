//
//  ChangePasswordViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    /* UITextField */
    @IBOutlet var oldPasswordTextField: UITextField!
    @IBOutlet var newPasswordTextField: UITextField!
    @IBOutlet var confirmNewPasswordTextField: UITextField!
    /*-----*/
    
    /* UIButton */
    @IBOutlet var updatePasswordButton: UIButton!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        setupNavigationController()
        hideKeyboardWhenTappedAround()
        
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        confirmNewPasswordTextField.delegate = self
        
        /* Setup UITextField Placeholder */
        oldPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Kata Sandi Lama",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        newPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Kata Sandi Baru",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        confirmNewPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Konfirmasi Kata Sandi",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        /*-----*/
    }
    
    private func setupNavigationController(){
        self.title = "Ubah Kata Sandi"
    }
    
    private func textFieldValidator()->Bool{
        
        if oldPasswordTextField.text!.isEmpty{
            return false
        }
        
        if newPasswordTextField.text!.isEmpty{
            return false
        }
        
        if confirmNewPasswordTextField.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case oldPasswordTextField:
            newPasswordTextField.becomeFirstResponder()
        case newPasswordTextField:
            confirmNewPasswordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func updatePasswordAction(_ sender: Any) {
        let oldPassword = oldPasswordTextField.text!
        let newPassword = newPasswordTextField.text!
        let confimNewPassword = confirmNewPasswordTextField.text!
        
        if textFieldValidator(){
            if newPassword == confimNewPassword{
                
                self.activityIndicator.isHidden = false
                self.updatePasswordButton.setTitle("", for: .normal)
                
                ProfileController.shared.updatePassword(oldPassword: oldPassword, newPassword: newPassword) { (status, error) in
                    if let error = error{
                        SVProgressHUD.showError(withStatus: error)
                        
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                        self.activityIndicator.isHidden = true
                        self.updatePasswordButton.setTitle("UBAH KATA SANDI", for: .normal)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                SVProgressHUD.showError(withStatus: "Password Not Match!")
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
}
