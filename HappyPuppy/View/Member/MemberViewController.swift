//
//  MemberViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleSignIn
import AlamofireImage
import Alamofire

class MemberViewController: UIViewController {
        
    /* UILabel */
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var myPointLabel: UILabel!
    @IBOutlet var memberTypeLabel: UILabel!
    @IBOutlet var memberTypeDescLabel: UILabel!
    @IBOutlet var memberCodeLabel: UILabel!
    @IBOutlet var maxPointLabel: UILabel!
    @IBOutlet var progressCurrentPointLabel: UILabel!
    @IBOutlet var pointTargetLabel: UILabel!
    @IBOutlet var currentMemberLabel: UILabel!
    @IBOutlet var nextMemberLabel: UILabel!
    
    /* UIView */
    @IBOutlet var profileImageViewContainer: UIView!
    @IBOutlet var qrCodeViewContainer: UIView!
    @IBOutlet var pointProgressViewContainer: UIView!
    @IBOutlet var pointBarViewContainer: UIView!
    @IBOutlet var memberView: UIView!
    @IBOutlet var pointBarMeterView: UIView!
    @IBOutlet var changeProfileContainerView: UIView!
    @IBOutlet var changePasswordContainerView: UIView!
    
    /* NSLayoutConstraint */
    @IBOutlet var memberMenuTopConstraint: NSLayoutConstraint!
    @IBOutlet var pointProgressViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var pointBarMeterViewWidthConstraint: NSLayoutConstraint!
    
    /* UIImageView */
    @IBOutlet var qrCodeImageView: UIImageView!
    @IBOutlet var profileImageView: UIImageView!

    /* UIButton */
    @IBOutlet var settingButton: UIButton!
    
    /* UIActivityIndicatorView */
    @IBOutlet var QrCodeActivityIndicator: UIActivityIndicatorView!
    
    /* Variable */
    var user:User?
    var firstName = ""
    var lastName = ""
    var email = ""
    var point = ""
    var memberType = ""
    var memberCode = ""
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupData()
        
    }
    
    private func setupViews(){
        
        /* Setup Profile ImageView */
        profileImageViewContainer.layer.cornerRadius = profileImageViewContainer.frame.height/2
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        /*-----*/
        
        /* Setup qrCodeViewContainer */
        qrCodeViewContainer.layer.masksToBounds = false
        qrCodeViewContainer.layer.shadowColor  = UIColor.black.cgColor
        qrCodeViewContainer.layer.shadowOpacity = 0.25
        qrCodeViewContainer.layer.shadowOffset = .zero
        qrCodeViewContainer.layer.shadowRadius = 2
        /*-----*/
        
        /* Setup memberTypeLabel UILabel */
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToMembershipView))
        memberView.addGestureRecognizer(gesture)
        /*-----*/
        
        /* Setup User Profile */
        self.reloadUserView()
        /*-----*/
    }
    
    private func setupData(){
        DispatchQueue.main.async {
            ProfileController.shared.getProfileDetail { (status, user, error) in
                if let error = error {
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG) {
                        self.logoutProcess()
                    }
                }else{
                    if let user = user {
                        self.user = user
                    }
                    /*-----*/
                    self.reloadUserView()
                }
            }
        }
    }
    
    private func reloadUserView(){
        /* Setup User Profile */
        if let imageURL = URL(string: "\(Network.shared.getBaseURL())/user_uploads/\(UserDefaultHelper.shared.getPhotoUrl())"){
            self.profileImageView.clearCache(urlRequest: URLRequest(url: imageURL))
            self.profileImageView.af.setImage(withURL: imageURL)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMemberView"), object: nil)
        }
        
        firstName = UserDefaultHelper.shared.getFirstName()
        lastName = UserDefaultHelper.shared.getLastName()
        email = UserDefaultHelper.shared.getEmail()
        point = UserDefaultHelper.shared.getPoint()
        memberType = UserDefaultHelper.shared.getMemberType()
        memberCode = UserDefaultHelper.shared.getMemberCode()
        
        profileImageViewContainer.backgroundColor = UIColor(hexString: UserDefaultHelper.shared.getMemberColor())
        nameLabel.text = "\(firstName) \(lastName)"
        emailLabel.text = email
        myPointLabel.text = point
        memberTypeLabel.text = memberType
        memberTypeLabel.textColor = UIColor(hexString: UserDefaultHelper.shared.getMemberColor())
        memberTypeDescLabel.textColor = UIColor(hexString: UserDefaultHelper.shared.getMemberColor())
        memberCodeLabel.text = memberCode
        setupQrCode()
        
        if memberType != MemberType.GOLD{
            var pointProgress:CGFloat = 0
            if let user = self.user{
                let nextMemberName = user.nextMember.name
                let pointFloat = (NumberFormatter().number(from:point) as! CGFloat)
                let maxPoint = CGFloat(user.currentMember.maxPoint)
                
                
                /* Setup pointTargetLabel UILabel */
                if nextMemberName == "" || user.currentMember.maxPoint == 0{
                    self.pointProgressViewHeightConstraint.constant = 0
                    self.pointProgressViewContainer.isHidden = true
                }else{
                    var pointDifferent = Int(maxPoint - pointFloat)
                    if pointDifferent<0{
                        pointDifferent = 0
                    }
                    pointTargetLabel.text = "Kumpulkan \(Int(pointDifferent)) poin untuk naik ke \(nextMemberName)"
                    /*-----*/
                    
                    maxPointLabel.text = "/ \(Int(maxPoint)) Pts"
                    currentMemberLabel.text = user.currentMember.name
                    nextMemberLabel.text = nextMemberName
                    
                    /* Setup Point Bar */
                    progressCurrentPointLabel.text = "Status \(point) pts"
                    pointBarViewContainer.layer.cornerRadius = pointBarViewContainer.frame.height/2
                    pointBarMeterView.layer.cornerRadius = pointBarMeterView.frame.height/2
                    
                    pointProgress = (pointFloat/maxPoint)
                    if pointProgress > 1{
                        pointProgress = 1
                    }
                    pointBarMeterViewWidthConstraint.constant = CGFloat(pointProgress) * pointBarViewContainer.frame.width
                }
                /*-----*/
            }
        }else{
            pointProgressViewContainer.isHidden = true
            pointProgressViewHeightConstraint.constant = 0
            memberMenuTopConstraint.constant = 0
        }
        
        
        /*-----*/
    }
    
    private func setupQrCode(){
//        UserDefaultHelper.shared.setQrImage(qrData: nil)

        if let imageData = UserDefaultHelper.shared.getQrImage(){
            self.QrCodeActivityIndicator.isHidden = true
            self.qrCodeImageView.isHidden = false
            self.qrCodeImageView.image = UIImage(data: imageData)
        }else{
            DispatchQueue.main.async {
                Global.shared.generateQRCode(from: self.memberCode, completion: { (image) in
                    self.QrCodeActivityIndicator.isHidden = true
                    self.qrCodeImageView.isHidden = false
                    self.qrCodeImageView.image = image
                    if let imageData = image.pngData(){
                        UserDefaultHelper.shared.setQrImage(qrData: imageData)
                    }
                })
            }
        }
    }
    
    private func logoutProcess(){
        SVProgressHUD.show()
        AuthenticationContoller.shared.logout { (status, error) in
            SVProgressHUD.dismiss()
            
            GIDSignIn.sharedInstance().disconnect()
            GIDSignIn.sharedInstance().signOut()
            UserDefaultHelper.shared.clearUser()
            
            let loginVC = UINavigationController(rootViewController: LoginViewController())
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: true, completion: nil)
            
        }
    }
    
    @objc private func goToMembershipView(){
        let membershipVC = MembershipViewController()
        navigationController?.pushViewController(membershipVC, animated: true)
    }
    
    @IBAction func didVerifStatusClicked(_ sender: Any) {
        guard let user = self.user else {return}
        let accountVerificationVC = AccountVerificationViewController()
        accountVerificationVC.isEmailVerified = user.isVerified
        accountVerificationVC.isPhoneVerified = user.isPhoneVerified
        navigationController?.pushViewController(accountVerificationVC, animated: true)
    }
    
    @IBAction func editProfileAction(_ sender: Any) {
        let changeProfileVC = ChangeProfileViewController()
        navigationController?.pushViewController(changeProfileVC, animated: true)
    }
    
    @IBAction func appreciationPointAction(_ sender: Any) {
        let appreciationPointVC = AppreciationPointViewController()
        navigationController?.pushViewController(appreciationPointVC, animated: true)
    }
    
    @IBAction func prizeRedeemAction(_ sender: Any) {
        let prizeRedeemVC = PrizeRedeemViewController()
        navigationController?.pushViewController(prizeRedeemVC, animated: true)
    }
    
    @IBAction func membershipAction(_ sender: Any) {
        let membershipVC = MembershipViewController()
        navigationController?.pushViewController(membershipVC, animated: true)
    }
    
    @IBAction func transactionActivityAction(_ sender: Any) {
        let transactionActivityVC = TransactionActivityViewController()
        navigationController?.pushViewController(transactionActivityVC, animated: true)
    }
    
    @IBAction func bottleKeepAction(_ sender: Any) {
        let bottleKeepVC = BottleKeepViewController()
        navigationController?.pushViewController(bottleKeepVC, animated: true)
    }
    
    @IBAction func settingAction(_ sender: Any) {
        
        if (changeProfileContainerView.isHidden){
            changeProfileContainerView.isHidden = false
            changePasswordContainerView.isHidden = false
            settingButton.setImage(UIImage(named: "DownIconOutline"), for: .normal)
        }else{
            changeProfileContainerView.isHidden = true
            changePasswordContainerView.isHidden = true
            settingButton.setImage(UIImage(named: "NextIcon"), for: .normal)
        }
    }
    @IBAction func changeProfileAction(_ sender: Any) {
        let changeProfileVC = ChangeProfileViewController()
        navigationController?.pushViewController(changeProfileVC, animated: true)
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        let changePasswordVC = ChangePasswordViewController()
        navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    @IBAction func accountVerificationAction(_ sender: Any) {
        guard let user = self.user else {return}
        let accountVerificationVC = AccountVerificationViewController()
        accountVerificationVC.isEmailVerified = user.isVerified
        accountVerificationVC.isPhoneVerified = user.isPhoneVerified
        navigationController?.pushViewController(accountVerificationVC, animated: true)
    }
    
    @IBAction func contactUsAction(_ sender: Any) {
        let contactUsVC = ContactUsViewController()
        navigationController?.pushViewController(contactUsVC, animated: true)
    }
    
    @IBAction func aboutUsAction(_ sender: Any) {
        let aboutUsVC = AboutUsViewController()
        navigationController?.pushViewController(aboutUsVC, animated: true)
    }
    
    @IBAction func termsConditionAction(_ sender: Any) {
        let termsConditionVC = TermsConditionViewController()
        navigationController?.pushViewController(termsConditionVC, animated: true)
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        let privacyPolicyVC = PrivacyPolicyViewController()
        navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }
    
    @IBAction func openVODLink(_ sender: Any) {
        guard let url = URL(string: "https://apps.apple.com/id/app/happup/id1495005838") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        self.logoutProcess()
    }
}
