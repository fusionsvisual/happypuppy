//
//  AccountVerificationViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class AccountVerificationViewController: UIViewController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var phoneVerificationDescViewContainer: UIView!
    
    /* UILabel */
    @IBOutlet var phoneLabel: UILabel!
    /*-----*/
    
    /* UITextField */
    @IBOutlet var otpTextField: UITextField!
    /*-----*/
    
    @IBOutlet var sendOTPButton: UIButton!
    @IBOutlet var sendEmailVerificationButton: UIButton!
    @IBOutlet var phoneVerificationButton: UIButton!
    @IBOutlet var identityCardVerifBtn: UIButton!
    
    @IBOutlet var emailVerificationImageView: UIImageView!
    @IBOutlet var phoneVerificationImageView: UIImageView!
    @IBOutlet var identityCardVerifIV: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var sendEmailVerificationButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var emailVerifV: UIView!
    @IBOutlet var otpVerifV: UIView!
    
    
    // MARK: Update phone number from edit profile
    var newPhoneNumber:String?
    var changeProfileDelegate: ChangeProfileDelegate?
    /*-----------------------*/
    
    var isPhoneVerified = false
    var isEmailVerified = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func setupViews(){
        hideKeyboardWhenTappedAround()
        setupNavigationController()
        
        //MARK: newPhoneNumber from edit profile, if phone number edited
        let phoneNumber = self.newPhoneNumber ?? UserDefaultHelper.shared.getPhone()
        phoneLabel.text = "Kami telah mengirim SMS ke nomer HP " + phoneNumber
        /*-----------------------*/
        
        /* Setup otpTextField UITextField Placeholder */
        otpTextField.attributedPlaceholder = NSAttributedString(string: "OTP",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        /*-----*/
        
        self.updateViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupData()
    }
    
    private func setupData(){
        self.activityIndicator.isHidden = false
        self.containerView.isHidden = true
        DispatchQueue.main.async {
            ProfileController.shared.getProfileDetail { (status, user, error) in
                if let error = error {
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG) {
                    }
                }else{
                    guard let user = user else {return}
                    self.containerView.isHidden = false
                    self.activityIndicator.isHidden = true
                    self.isEmailVerified = user.isVerified
                    self.isPhoneVerified = user.isPhoneVerified
                    
                    if user.idCardStatus == IdentityCardStatus.APPROVED{
                        self.identityCardVerifBtn.isHidden = true
                        self.identityCardVerifIV.image = UIImage(named: "VerifiedIcon")
                    }else if user.idCardStatus == IdentityCardStatus.WAITING {
                        self.identityCardVerifBtn.backgroundColor = Color.DISABLED
                        self.identityCardVerifBtn.isEnabled = false
                        UIView.performWithoutAnimation {
                            self.identityCardVerifBtn.setTitle("MENUNGGU VERIFIKASI DARI ADMIN", for: .normal)
                            self.identityCardVerifBtn.layoutIfNeeded()
                        }
                    }else if user.idCardStatus == IdentityCardStatus.NOT_VERIFIED || user.idCardStatus == IdentityCardStatus.REJECTED{
                        self.identityCardVerifBtn.backgroundColor = Color.PRIMARY
                        self.identityCardVerifBtn.isEnabled = true
                        UIView.performWithoutAnimation {
                            self.identityCardVerifBtn.setTitle("UPLOAD KTP / KARTU PELAJAR", for: .normal)
                            self.identityCardVerifBtn.layoutIfNeeded()
                        }
                    }
                    self.setupViews()
                }
            }
        }
        
        if self.newPhoneNumber != nil{
            ProfileController.shared.resendNewPhoneOTP { (status, message, error) in
                if let error = error{
                    SVProgressHUD.dismiss()
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.title = "Verifikasi Akun"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        self.setupData()
    }
    
    private func textFieldValidator()->Bool{
        
        if otpTextField.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    private func updateViews(){
        
        if isEmailVerified{
            self.hideEmailSection()
        }
        
        if self.isPhoneVerified, self.newPhoneNumber == nil{
            self.hidePhoneSection()
        }
    }
    
    private func hideEmailSection(){
        self.emailVerificationImageView.image = UIImage(named: "VerifiedIcon")
        self.emailVerifV.isHidden = true
    }
    
    private func hidePhoneSection(){
        self.phoneVerificationImageView.image = UIImage(named: "VerifiedIcon")
        self.otpVerifV.isHidden = true
    }
    
    @IBAction func resendEmailVerificationAction(_ sender: Any) {
        SVProgressHUD.show(withStatus: "Mengirim Ulang E-mail")
        ProfileController.shared.resendEmailVerification { (status, message, error) in
            if let error = error{
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
            }else{
                if let message = message{
                    SVProgressHUD.showInfo(withStatus: message)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }
            }
        }
    }
    
    @IBAction func resendOTPAction(_ sender: Any) {
        SVProgressHUD.show(withStatus: "Mengirim Ulang OTP")
        if self.newPhoneNumber == nil{
            // MARK: Resend OTP number without update number
            ProfileController.shared.resendOTP { (status, message, error) in
                if let error = error{
                    SVProgressHUD.dismiss()
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let message = message{
                        SVProgressHUD.showInfo(withStatus: message)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }
            }
        }else{
            // MARK: Resend OTP number after update number
            ProfileController.shared.resendNewPhoneOTP { (status, message, error) in
                if let error = error{
                    SVProgressHUD.dismiss()
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let message = message{
                        SVProgressHUD.showInfo(withStatus: message)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }
            }
        }
    }
    
    @IBAction func otpVerificationAction(_ sender: Any) {
        if textFieldValidator(){
            SVProgressHUD.show(withStatus: "Mengecek OTP")
            let otpNumber = otpTextField.text!
            
            if let phoneNumber = self.newPhoneNumber{
                // MARK: Check OTP number after change number
                ProfileController.shared.checkNewNumberOTP(phone: phoneNumber, otpNumber: otpNumber) { (status, message, error) in
                    if let error = error{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }else{
                        SVProgressHUD.showSuccess(withStatus: "Phone number verified.")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT) {
                            self.changeProfileDelegate?.didPhoneNumberVerified()
                        }
                    }
                }
            }else{
                // MARK: Check OTP number without change number
                ProfileController.shared.otpVerification(otpNumber: otpNumber) { (status, message, error) in
                    if let error = error{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }else{
                        SVProgressHUD.showSuccess(withStatus: "Phone number verified.")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
        
    }
    
    @IBAction func uploadIdentityCardAction(_ sender: Any) {
        let identityCardPopUpVC = IdentityCardPopUpViewController()
        identityCardPopUpVC.delegate = self
        identityCardPopUpVC.IDCardStatus = UserDefaultHelper.shared.getIdCardStatus()
        identityCardPopUpVC.IDCardNote = UserDefaultHelper.shared.getidCardNote()
        self.present(identityCardPopUpVC, animated: true) {
            identityCardPopUpVC.showModal()
        }
    }
}

extension AccountVerificationViewController: UploadIdentityCardDelegate{
    func didUploadClicked() {
        let uploadIdentityCardVC = UploadIdentityCardViewController()
        self.navigationController?.pushViewController(uploadIdentityCardVC, animated: true)
    }
}
