//
//  PrivacyPolicyViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 25/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    /* UILabel */
    @IBOutlet var descLabel: UILabel!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var descActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        self.title = "Kebijakan Privacy"
        setupNavigationController()
    }
    
    private func setupData(){
        self.descLabel.isHidden = true
        self.descActivityIndicator.isHidden = false
        
        AboutController.shared.getPrivacyPolicy { (status, desc, error) in
            if let error = error{
                print(error)
            }else{
                if let desc = desc{
                    self.descActivityIndicator.isHidden = true
                    self.descLabel.isHidden = false
                    self.descLabel.attributedText = desc.htmlToAttributedString
                }
            }
        }
    }
    
    private func setupNavigationController(){
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.isTranslucent = false
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        setupData()
    }
}
