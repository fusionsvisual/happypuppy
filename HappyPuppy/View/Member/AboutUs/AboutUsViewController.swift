//
//  AboutUsViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet var aboutUsContainerView: UIView!
    @IBOutlet var aboutUsLabel: UILabel!
    @IBOutlet var progressIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        setupNavigationController()
    }
    
    private func setupData(){
        aboutUsContainerView.isHidden = true
        self.progressIndicator.isHidden = false
        progressIndicator.startAnimating()
        
        AboutController.shared.getAbout { (status, response, error) in
            if let response = response{
                self.progressIndicator.stopAnimating()
                self.progressIndicator.isHidden = true
                
                self.aboutUsLabel.attributedText = response.description?.htmlToAttributedString
                self.aboutUsContainerView.isHidden = false
            }
        }
    }
    
    private func setupNavigationController(){
        self.title = "Tentang Kami"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
       }
    
    @objc private func refresh(){
        setupData()
    }
}
