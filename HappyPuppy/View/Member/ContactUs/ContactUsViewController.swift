//
//  ContactUsViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContactUsViewController: UIViewController {

    /* UIView */
    @IBOutlet var contentContainerView: UIView!
    @IBOutlet var contactContainerView: UIView!
    @IBOutlet var locationContainerView: UIView!
    /*----*/
    
    /* UILabel */
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var facebookLabel: UILabel!
    @IBOutlet var instagramLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var faxLabel: UILabel!
    @IBOutlet var companyLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    /*----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        setupNavigationController()
        
        contactContainerView.layer.shadowColor  = UIColor.black.cgColor
        contactContainerView.layer.shadowOpacity = 0.25
        contactContainerView.layer.shadowOffset = .zero
        contactContainerView.layer.shadowRadius = 2
        
        locationContainerView.layer.shadowColor  = UIColor.black.cgColor
        locationContainerView.layer.shadowOpacity = 0.25
        locationContainerView.layer.shadowOffset = .zero
        locationContainerView.layer.shadowRadius = 2
    }
    
    private func setupNavigationController(){
        self.title = "Hubungi Kami"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    private func setupData(){
        self.contentContainerView.isHidden = true
        self.activityIndicator.isHidden = false
        AboutController.shared.getContact { (status, contact, error) in
            self.activityIndicator.isHidden = true
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
            }else{
                if let contact = contact{
                    self.contentContainerView.isHidden = false
                    self.emailLabel.text = contact.email!
                    self.facebookLabel.text = contact.facebook!
                    self.instagramLabel.text = contact.facebook!
                    self.phoneLabel.text = contact.telp
                    self.faxLabel.text = contact.fax
                    
                    self.companyLabel.text = contact.companyName!
                    self.addressLabel.text = contact.address1!
                }
            }
        }
    }
    
    @objc private func refresh(){
        setupData()
    }
    
    @IBAction func faqAction(_ sender: Any) {
        let faqVC = FAQViewController()
        self.navigationController?.pushViewController(faqVC, animated: true)
        
    }
    
    @IBAction func sendEmailAction(_ sender: Any) {
        let sendQuestionVC = SendQuestionViewController()
        self.navigationController?.pushViewController(sendQuestionVC, animated: true)
    }
    
}
