//
//  SendQuestionViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 01/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class SendQuestionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /* UIButton */
    @IBOutlet var sendButton: UIButton!
    /*----*/
    
    /* UITextField */
    @IBOutlet var subjectTextField: UITextField!
    /*----*/
    
    /* UITextView */
    @IBOutlet var messageTextView: UITextView!
    /*----*/
    
    /* Variable */
    var subjectPickerView = UIPickerView()
    private var subjectList = ["Waralaba", "Keluhan", "Pujian", "Kritik & Saran", "Permintaan Lagu", "Lowongan Pekerjaan", "Lain"]
    /*----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        self.title = "Kirim Surel"
        hideKeyboardWhenTappedAround()
        
        /* Setup UIToolbar for UIPickerView */
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pickerDone))
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.items = [spaceButton, btnDone]
        /*-----*/
        
        messageTextView.backgroundColor = .white
        messageTextView.textContainerInset = UIEdgeInsets(top: 16, left: 8, bottom: 16, right: 8)
        
        /* Setup subjectPickerView UIPickerView */
        subjectPickerView.delegate = self
        subjectPickerView.dataSource = self
        subjectTextField.inputView = subjectPickerView
        subjectTextField.inputAccessoryView = toolbar
        subjectTextField.tintColor = .clear
        /*-----*/
    }
    private func textFieldValidator()->Bool{
        
        if subjectTextField.text!.isEmpty{
            return false
        }
        
        if messageTextView.text!.isEmpty{
            return false
        }
        
        
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjectList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.subjectTextField.text = subjectList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjectList[row]
    }
    
    @objc private func pickerDone(){
        dismissKeyboard()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        let subject = subjectTextField.text!
        let message = messageTextView.text!
        if textFieldValidator(){
            SVProgressHUD.show(withStatus: "Mengirim Pesan")
            AboutController.shared.sendEmail(subject: subject, message: message) { (status, message, error) in
                if let error = error{
                    SVProgressHUD.dismiss()
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let message = message{
                        SVProgressHUD.showInfo(withStatus: message)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }else{
            SVProgressHUD.showError(withStatus: "Silahkan Isi Pesan Terlebih Dahulu!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
        
    }
    
}
