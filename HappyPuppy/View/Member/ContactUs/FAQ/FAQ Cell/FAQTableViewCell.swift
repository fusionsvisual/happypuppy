//
//  FAQTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 01/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {
    
    /* UILabel */
    @IBOutlet var categoryLabel: UILabel!
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
