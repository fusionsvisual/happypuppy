//
//  FAQContentTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 01/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class FAQContentTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        descriptionLabel.attributedText = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
