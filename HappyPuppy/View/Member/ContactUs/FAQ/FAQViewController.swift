//
//  FAQViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

struct FAQData {
    var isOpened:Bool = false
    var title:String = ""
    var contentList:[FAQDataContent] = []
}

struct FAQDataContent{
    var question:String
    var answer:String
}

class FAQViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /* UITableView */
    @IBOutlet var faqTableView: UITableView!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* Variable */
    var faqList:[FAQData] = []
    /*-----*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        setupNavigationController()
        
        faqTableView.delegate = self
        faqTableView.dataSource = self
        faqTableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQCell")
        faqTableView.register(UINib(nibName: "FAQContentTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQContentCell")
        faqTableView.rowHeight = UITableView.automaticDimension
    }
    
    private func setupNavigationController(){
        self.title = "FAQs"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    private func setupData(){
        faqList.removeAll()
        faqTableView.reloadData()
        activityIndicator.isHidden = false
        
        AboutController.shared.getFaqs { (status, faqList, error) in
            self.activityIndicator.isHidden = true
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
            }else{
                if let faqList = faqList{
                    for faq in faqList{
                        let title = faq.question!
                        let description = faq.answer!
                        let faqDataContent = FAQDataContent(question: title, answer: description)
                        
                        if let index = self.faqList.firstIndex(where: {$0.title == faq.category}){
                            self.faqList[index].contentList.append(faqDataContent)
                        }else{
                            var faqData = FAQData(isOpened: false, title: faq.category!, contentList: [])
                            faqData.contentList.append(faqDataContent)
                            self.faqList.append(faqData)
                        }
                    }
                    self.faqTableView.reloadData()
                }
            }
        }
    }
    
    @objc private func refresh(){
        setupData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return faqList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if faqList[section].isOpened == true{
            return faqList[section].contentList.count + 1
        }else{
            return 1
        }
    }

    func tableView(
        _ tableView: UITableView,
        heightForRowAt indexPath: IndexPath
    ) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQTableViewCell
            cell.categoryLabel.text = faqList[indexPath.section].title
            return cell
        }else{
            let content = faqList[indexPath.section].contentList[indexPath.row-1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "FAQContentCell", for: indexPath) as! FAQContentTableViewCell
            cell.titleLabel.text = content.question
            DispatchQueue.main.async {
                cell.descriptionLabel.attributedText = content
                    .answer
                    .htmlToAttributedString
            }
            return cell
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if faqList[indexPath.section].isOpened == true{
            faqList[indexPath.section].isOpened = false
            let sections = IndexSet.init(integer: indexPath.section)
            faqTableView.reloadSections(sections, with: .automatic)
        }else{
            faqList[indexPath.section].isOpened = true
            let sections = IndexSet.init(integer: indexPath.section)
            faqTableView.reloadSections(sections, with: .automatic)
        }
    }
}
