//
//  ChangeProfileViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol ChangeProfileDelegate {
    func didPhoneNumberVerified()
}

class ChangeProfileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    /* UIImageView */
    @IBOutlet var profileImageView: UIImageView!
    /*-----*/
    
    /* UITextField */
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var birthdateTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* UIButton */
    @IBOutlet var updateProfileButton: UIButton!
    /*-----*/
    
    /* Variable */
    var profileImage: UIImage?
    let genderList = ["Pria", "Wanita"]
    let pickerView = UIPickerView()
    let birthdateDatePicker = UIDatePicker()
    var imagePicker = UIImagePickerController()
    var imageName = ""
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        
        hideKeyboardWhenTappedAround()
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        genderTextField.delegate = self
        birthdateTextField.delegate = self
        phoneTextField.delegate = self
        
        /* Setup UIToolbar for UIPickerView */
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pickerDone))
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.items = [spaceButton, btnDone]
        /*-----*/
        
        setupNavigationController()
        
        /* Setup profileImageView UIImageView */
        let profileImageGesture = UITapGestureRecognizer(target: self, action: #selector(openGallery))
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        
        profileImageView.addGestureRecognizer(profileImageGesture)
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        /*-----*/
        
        /* Setup UITextField Placeholder */
        firstNameTextField.attributedPlaceholder = NSAttributedString(string: "Nama Depan",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Nama Belakang",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        genderTextField.attributedPlaceholder = NSAttributedString(string: "Gender",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        birthdateTextField.attributedPlaceholder = NSAttributedString(string: "Tanggal Lahir",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        phoneTextField.attributedPlaceholder = NSAttributedString(string: "+62",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        /*-----*/
        
        /* Setup genderTextField UIPickerView() */
        genderTextField.inputView = pickerView
        genderTextField.inputAccessoryView = toolbar
        genderTextField.tintColor = .clear
        /*-----*/
        
        /* Setup genderTextField UITextField */
        pickerView.delegate = self
        pickerView.dataSource = self
        /*-----*/
        
        /* Setup birthdateTextField UITextField */
        birthdateDatePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            self.birthdateDatePicker.preferredDatePickerStyle = .wheels
        }
        birthdateDatePicker.locale = Locale(identifier: "id_ID")
        birthdateDatePicker.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
        birthdateTextField.inputView = birthdateDatePicker
        birthdateTextField.inputAccessoryView = toolbar
        birthdateTextField.tintColor = .clear
        /*-----*/
        
        self.updateViews()
    }
    
    private func setupData(){
        self.updateProfileButton.isEnabled = false
        ProfileController.shared.getProfileDetail { (status, user, error) in
            if let error = error{
                print(error)
            }else{
                /* Change changeProfileButton UIButton */
                self.updateProfileButton.isEnabled = true
                self.updateProfileButton.backgroundColor = .black
                self.updateProfileButton.setTitleColor(.white, for: .normal)
                /*-----*/
                self.updateViews()
            }
        }
    }
    
    private func updateViews(){
        
        if let imageURL = URL(string: "\(Network.shared.getBaseURL())/user_uploads/\(UserDefaultHelper.shared.getPhotoUrl())"){
            profileImageView.af.setImage(withURL: imageURL)
        }
        
        /* Setup firstNameTextField UITextField */
        firstNameTextField.text = UserDefaultHelper.shared.getFirstName()
        /*-----*/
        
        /* Setup lastNameTextField UITextField */
        lastNameTextField.text = UserDefaultHelper.shared.getLastName()
        /*-----*/
        
        /* Setup genderTextField UITextField */
        let gender = UserDefaultHelper.shared.getGender()
        if gender == "L"{
            genderTextField.text = genderList[0]
        }else{
            genderTextField.text = genderList[1]
        }
        /*-----*/
        
        /* Setup birthdateTextField UITextField */
        let birthdate = UserDefaultHelper.shared.getBirthdate()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "id_ID")
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let date = dateFormatterGet.date(from: birthdate)!
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "id_ID")
        dateFormatterPrint.dateFormat = "dd MMMM yyyy"
        birthdateTextField.text = dateFormatterPrint.string(from: date)
        birthdateTextField.addTarget(self, action: #selector(self.didDateSelected(_:)), for: .editingDidEnd)
        /*-----*/
        
        /* Setup selected date on UIDatePicker */
        birthdateDatePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -90, to: Date())
        birthdateDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -15, to: Date())
        birthdateDatePicker.setDate(date, animated: false)
        birthdateDatePicker.locale = Locale(identifier: "id_ID")
       /*-----*/
        
        /* Setup phoneTextField UITextField */
        let phone = UserDefaultHelper.shared.getPhone()
        phoneTextField.text = phone
        /*-----*/
    }
    
    private func setupNavigationController(){
        self.title = "Ubah Profil"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        
        /* Change changeProfileButton UIButton */
        updateProfileButton.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
        updateProfileButton.setTitleColor(#colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1), for: .normal)
        /*-----*/
        
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        genderTextField.text = ""
        birthdateTextField.text = ""
        phoneTextField.text = ""
        
        setupData()
    }
    
    /* UIPickerView Handler */
    @objc private func pickerDone(){
        if genderTextField.isEditing{
            dismissKeyboard()
            birthdateTextField.becomeFirstResponder()
        }else{
            dismissKeyboard()
            phoneTextField.becomeFirstResponder()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderTextField.text = genderList[row]
    }
    /*-----*/
    
    /* UIDatePicker Handler */
    @objc private func didSelectDate(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "id_ID")
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let dateString = dateFormatter.string(from: sender.date)
        
        birthdateTextField.text = dateString
    }
    /*-----*/
    
    private func textFieldValidator()->Bool{
        
        if firstNameTextField.text!.isEmpty{
            return false
        }
        
        if lastNameTextField.text!.isEmpty{
            return false
        }
        
        if genderTextField.text!.isEmpty{
            return false
        }
        
        if birthdateTextField.text!.isEmpty{
            return false
        }
        
        if phoneTextField.text!.isEmpty{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
           genderTextField.becomeFirstResponder()
        default:
            self.resignFirstResponder()
            updateProfile()
        }
        return false
    }
    
    private func showUpdateBtn(){
        self.activityIndicator.isHidden = true
        self.updateProfileButton.setTitle("UBAH PROFIL", for: .normal)
    }
    
    private func hideUpdateBtn(){
        self.activityIndicator.isHidden = false
        self.updateProfileButton.setTitle("", for: .normal)
    }
    
    private func updateProfile(){
        if textFieldValidator(){
            
            self.hideUpdateBtn()
            
            let firstName = firstNameTextField.text!
            let lastName = lastNameTextField.text!
            let gender = genderTextField.text!
            let birthdate = birthdateTextField.text!
            let phone = phoneTextField.text!
            
            ProfileController.shared.updateProfile(firstName: firstName, lastName: lastName, gender: gender, birthdate: birthdate, phone: phone, profileImage: self.profileImage, imageName: self.imageName) { (status, error) in
                self.showUpdateBtn()
                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if UserDefaultHelper.shared.getPhone() != self.phoneTextField.text{
                        let accountVerificationVC = AccountVerificationViewController()
                        accountVerificationVC.changeProfileDelegate = self
                        accountVerificationVC.newPhoneNumber = self.phoneTextField.text
                        self.navigationController?.pushViewController(accountVerificationVC, animated: true)
                    }else{
                        SVProgressHUD.showSuccess(withStatus: "Update profile successful")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @objc private func didDateSelected(_ sender: UITextField){
        // MARK: Handle when date datepicker never moved and click done
        let selectedDate = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: self.birthdateDatePicker.date, timezone: .current, locale: Locale(identifier: "id_ID"))
        self.birthdateTextField.text = selectedDate
    }
    
    @objc private func openGallery(){
        let alert = UIAlertController(title: "Upload Image", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.presentImagePickerController(sourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.presentImagePickerController(sourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //* Handler action sheet for iPad
        alert.popoverPresentationController?.sourceView = profileImageView
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func presentImagePickerController(sourceType: UIImagePickerController.SourceType){
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.sourceType = sourceType
        imagePickerVC.delegate = self
        imagePickerVC.modalPresentationStyle = .fullScreen
        
        self.present(imagePickerVC, animated: true)
    }
    
    @IBAction func updateProfileAction(_ sender: Any) {
        updateProfile()
    }
}

extension ChangeProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            
            if #available(iOS 11.0, *) {
                if let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                    imageName = imageURL.lastPathComponent
                }
            } else {
                if let imageURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                    imageName = imageURL.lastPathComponent
                }
            }
            
            self.profileImage = image
            profileImageView.image = image
        }
    }
}

extension ChangeProfileViewController: ChangeProfileDelegate{
    func didPhoneNumberVerified() {
        SVProgressHUD.showSuccess(withStatus: "Update profile successful")
        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        self.navigationController?.popViewController(animated: true)
    }
}
