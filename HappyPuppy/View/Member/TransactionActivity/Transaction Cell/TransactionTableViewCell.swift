//
//  TransactionTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    /* UIView */
    @IBOutlet var containerView: UIView!
    /*-----*/
    
    /* UIImageView */
    @IBOutlet var outletImageView: UIImageView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var outletLabel: UILabel!
    /*-----*/
    
    /* Variable */
    var delegate:ReceiptDelegate?
    var index:Int = 0
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews(){
        containerView.layer.shadowColor  = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.25
        containerView.layer.shadowOffset = .zero
        containerView.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    @IBAction func cellAction(_ sender: Any) {
        delegate?.cellClicked(index: self.index)
    }
    
    @IBAction func removeAction(_ sender: Any) {
        delegate?.removeItem(index: self.index)
    }
    
}
