//
//  TransactionActivityViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

protocol ReceiptDelegate {
    func cellClicked(index:Int)
    func removeItem(index:Int)
}

class TransactionActivityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ReceiptDelegate{

    /* UITableView */
    @IBOutlet var transactionTableView: UITableView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var noDataLabel: UILabel!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var receiptActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* Variable */
    var receiptList:[Receipt] = []
    /*-----*/
    private let spinner = UIActivityIndicatorView(style: .gray)
    private var length: Int = 10
    private var startIndex: Int = .zero
    private var isPaginationEnabled: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        receiptActivityIndicator.isHidden = false
        setupData()
    }
    
    private func setupViews(){
        self.title = "Receipt"
        setupNavigationController()
        
        /* Setup transactionTableView UITableView*/
        transactionTableView.delegate = self
        transactionTableView.dataSource = self
        transactionTableView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionCell")
        /*-----*/
    }

    private func setupData(){
        ReceiptController.shared.getReceipt(
            length: length,
            startIndex: startIndex
        ) { [weak self] (status, response, error) in
            guard let self = self else { return }
            self.receiptActivityIndicator.isHidden = true
            guard let response = response,
                  let data = response.data,
                  let receipts = data.receiptList else {
                return
            }
            if let error = error {
                print(error)
                return
            }
            if receipts.count == .zero
                && self.startIndex == .zero {
                self.noDataLabel.isHidden = false
                return
            }
            self.isPaginationEnabled = !receipts.isEmpty
            self.receiptList.append(
                contentsOf: receipts
            )
            self.transactionTableView.reloadData()
            self.transactionTableView.tableFooterView = nil
        }
    }

    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
   }
    
    @objc private func refresh(){
        startIndex = .zero
        receiptList.removeAll()
        transactionTableView.reloadData()
        setupData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receiptList.count
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let receipt = receiptList[indexPath.item]
//        let receiptDetailVC = TransactionDetailViewController()
//        if let outletData = receipt.outletData{
//            if let outletCode = outletData.code{
//                receiptDetailVC.outletCode = outletCode
//                if let invoiceCode = receipt.invoiceCode{
//                    receiptDetailVC.invoiceCode = invoiceCode
//                    navigationController?.pushViewController(receiptDetailVC, animated: true)
//                }else{
//                    print("DATA TIDAK DITEMUKAN")
//                }
//            }else{
//                print("DATA TIDAK DITEMUKAN")
//            }
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let receipt = receiptList[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionTableViewCell
        
        cell.delegate = self
        cell.index = indexPath.item
        cell.dateLabel.text = receipt.date
        if let outletData = receipt.outletData{
            cell.outletLabel.text = "\(outletData.name)"
            if let brandData = outletData.brandData{
                /* Setup prizeImageView UIImageView */
                if let photoUrl = brandData.photoUrl{
                    if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)"){
                        cell.outletImageView.af.setImage(withURL: url) { (image) in
                            
                        }
                    }
                }
                /*-----*/
            }
        }
        cell.prepareForReuse()
        
        return cell
    }
    
    func cellClicked(index:Int) {
        let receipt = receiptList[index]
        let receiptDetailVC = TransactionDetailViewController()
        if let outletData = receipt.outletData{
            if let outletCode = outletData.code{
                receiptDetailVC.outletCode = outletCode
                if let invoiceCode = receipt.invoiceCode{
                    receiptDetailVC.invoiceCode = invoiceCode
                    navigationController?.pushViewController(receiptDetailVC, animated: true)
                }else{
                    print("DATA TIDAK DITEMUKAN")
                }
            }else{
                print("DATA TIDAK DITEMUKAN")
            }
        }
    }
    
    func removeItem(index: Int) {
        
        let receipt = receiptList[index]
        if let outletData = receipt.outletData{
            if let outletCode = outletData.code{
                if let invoiceCode = receipt.invoiceCode{
                    if let summaryCode = receipt.summaryCode{
                        print(outletCode)
                        print(summaryCode)
                        let alert = UIAlertController(title: "Hapus Receipt \(invoiceCode)", message: "Apakah kamu yakin?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            ReceiptController.shared.deleteReceipt(outletCode: outletCode, receiptCode: summaryCode) { (status, error) in
                                if let error = error{
                                    print(error)
                                }else{
                                    SVProgressHUD.showInfo(withStatus: "Invoice Telah Dihapus!")
                                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                                    self.refresh()
                                    return
                                }
                            }
                        }))
                        
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            }
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != transactionTableView { return }
        let contentHeight = transactionTableView.contentSize.height
        let scrollTreshold = contentHeight - transactionTableView.frame.size.height
        if scrollView.contentOffset.y >= scrollTreshold
            && transactionTableView.tableFooterView == nil
            && isPaginationEnabled {
            startIndex += length
            spinner.startAnimating()
            spinner.frame = CGRect(
                origin: .zero,
                size: CGSize(
                    width: transactionTableView.bounds.width,
                    height: 35
                )
            )
            transactionTableView.tableFooterView = spinner
            transactionTableView.tableFooterView?.isHidden = false
            setupData()
        }
    }
}
