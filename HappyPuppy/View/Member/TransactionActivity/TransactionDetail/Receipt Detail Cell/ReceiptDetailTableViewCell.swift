//
//  ReceiptTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class ReceiptDetailTableViewCell: UITableViewCell {

    /* UILabel */
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    /*-----*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
