//
//  TransactionDetailViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* UIView */
    @IBOutlet var topContainerView: UIView!
    @IBOutlet var bottomContainerView: UIView!
    /*-----*/
    
    /* UITableView */
    @IBOutlet var receiptDetailTableView: UITableView!
    /*-----*/
    
    /* NSLayoutConstraint */
    @IBOutlet var receiptDetailHeightConstraint: NSLayoutConstraint!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var customerNameLabel: UILabel!
//    @IBOutlet var invoiceCodeLabel: UILabel!
    @IBOutlet var roomNumberLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var roomPriceLabel: UILabel!
    @IBOutlet var roomPriceSubLabel: UILabel!
    @IBOutlet var otherPriceLabel: UILabel!
    @IBOutlet var discountLabel: UILabel!
    @IBOutlet var subtotalPriceLabel: UILabel!
    @IBOutlet var fnbServiceLabel: UILabel!
    @IBOutlet var roomServiceLabel: UILabel!
    @IBOutlet var fnbTaxLabel: UILabel!
    @IBOutlet var roomTaxLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var paymentMethodLabel: UILabel!
    @IBOutlet var paymentTotalLabel: UILabel!
    /*-----*/
    
    /* Variable */
    var receiptDetail:ReceiptionInvoice?
    var outletCode:String = ""
    var invoiceCode:String = ""
    /* */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        
        self.title = "Detail Receipt"
        setupNavigationController()
        
        /* Setup topContainerView UIView */
        topContainerView.layer.shadowColor  = UIColor.black.cgColor
        topContainerView.layer.shadowOpacity = 0.25
        topContainerView.layer.shadowOffset = .zero
        topContainerView.layer.shadowRadius = 2
        /*-----*/
        
        /* Setup bottomContainerView UIView */
        bottomContainerView.layer.shadowColor  = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.25
        bottomContainerView.layer.shadowOffset = .zero
        bottomContainerView.layer.shadowRadius = 2
        /*-----*/
        
        /* Setup receiptDetailTableView UITableView */
        receiptDetailTableView.delegate = self
        receiptDetailTableView.dataSource = self
        receiptDetailTableView.register(UINib(nibName: "ReceiptDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiptDetailCell")
        /*-----*/
    }
    
    private func setupData(){
        ReceiptController.shared.getReceiptDetail(outletCode: outletCode, invoiceCode: invoiceCode) { (status, receiptDetail, error) in
            if let error = error{
                print(error)
            }else{
                if let receiptDetail = receiptDetail{
                    
                    self.receiptDetail = receiptDetail
                    
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "id_ID")
                    formatter.numberStyle = .currency
                    
                    self.roomNumberLabel.text = receiptDetail.roomNumber
                    self.customerNameLabel.text = receiptDetail.customerName
                    self.dateLabel.text = receiptDetail.date
                    
                    if let roomPrice = formatter.string(for: receiptDetail.roomPrice){
                        self.roomPriceLabel.text = roomPrice.replacingOccurrences(of: "Rp", with: "")
                        self.roomPriceSubLabel.text = roomPrice.replacingOccurrences(of: "Rp", with: "")
                    }
                    
                    if let otherPrice = formatter.string(for: receiptDetail.otherPrice){
                        self.otherPriceLabel.text = otherPrice.replacingOccurrences(of: "Rp", with: "")
                    }
                    
                    
                    if let otherPrice = formatter.string(for: receiptDetail.otherPrice){
                        self.otherPriceLabel.text = otherPrice.replacingOccurrences(of: "Rp", with: "")
                    }
                    
                    if let discount = formatter.string(for: receiptDetail.discount){
                         self.discountLabel.text = discount.replacingOccurrences(of: "Rp", with: "")
                    }
                    
                    /* Setup subtotalPriceLabel UILabel */
                    if let roomPrice = receiptDetail.roomPrice{
                        if let otherPrice = receiptDetail.otherPrice{
                            if let discount = receiptDetail.discount{
                                let subtotalInt = (roomPrice + otherPrice) - discount
                                if let subtotalStr = formatter.string(for: subtotalInt){
                                     self.subtotalPriceLabel.text = subtotalStr.replacingOccurrences(of: "Rp", with: "")
                                }
                            }
                        }
                    }
                    /*-----*/
                    
                    /* Setup fnbServiceLabel UILabel */
                    if let fnbService = formatter.string(for: receiptDetail.fnbServiceCharge){
                         self.fnbServiceLabel.text = fnbService.replacingOccurrences(of: "Rp", with: "")
                    }
                    /*-----*/
                    
                    /* Setup roomServiceLabel UILabel */
                    if let roomService = formatter.string(for: receiptDetail.roomServiceCharge){
                         self.roomServiceLabel.text = roomService.replacingOccurrences(of: "Rp", with: "")
                    }
                    /*-----*/
                    
                    /* Setup fnbTaxLabel UILabel */
                    if let fnbTax = formatter.string(for: receiptDetail.fnbTax){
                         self.fnbTaxLabel.text = fnbTax.replacingOccurrences(of: "Rp", with: "")
                    }
                    /*-----*/
                    
                    /* Setup roomTaxLabel UILabel */
                    if let roomTax = formatter.string(for: receiptDetail.roomServiceTax){
                         self.roomTaxLabel.text = roomTax.replacingOccurrences(of: "Rp", with: "")
                    }
                    /*-----*/
                    
                    if let totalPayment = receiptDetail.totalPayment{
                        self.totalPriceLabel.text = Formatter.shared.priceFormatter(totalPayment)
                        self.paymentTotalLabel.text = Formatter.shared.priceFormatter(totalPayment)
                    }
                    
                    if let paymentMethod = receiptDetail.paymentMethod{
                        self.paymentMethodLabel.text = paymentMethod
                    }
                    
                    
                    /* Reload TableView */
                    self.receiptDetailHeightConstraint.constant = CGFloat(receiptDetail.receiptItem.count) * self.receiptDetailTableView.rowHeight
                    self.receiptDetailTableView.reloadData()
                    /*-----*/
                    
                    self.activityIndicator.isHidden = true
                    self.topContainerView.isHidden = false
                    self.bottomContainerView.isHidden = false
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        self.receiptDetail = nil
        self.receiptDetailTableView.reloadData()
        self.activityIndicator.isHidden = false
        self.topContainerView.isHidden = true
        self.bottomContainerView.isHidden = true
        setupData()
    }
    
    /* UITableView Handler */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let receiptDetail = receiptDetail{
            return receiptDetail.receiptItem.count
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptDetailCell", for: indexPath) as! ReceiptDetailTableViewCell
        if let receiptDetail = self.receiptDetail{
            let receiptItem = receiptDetail.receiptItem[indexPath.item]
            cell.nameLabel.text = receiptItem.nama
            cell.quantityLabel.text = "\(receiptItem.quantity) x \(receiptItem.price)"
            cell.totalPriceLabel.text = Formatter.shared.priceFormatter(receiptItem.quantity * receiptItem.price)
        }
        
        return cell
    }
    /*-----*/
}
