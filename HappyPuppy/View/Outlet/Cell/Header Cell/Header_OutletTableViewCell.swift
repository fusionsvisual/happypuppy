//
//  Header_OutletTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Header_OutletTableViewCell: UITableViewCell {

    @IBOutlet var cityTF: UITextField!
    
    var delegate: OutletHeaderDelegate?
    var cities: [City] = [] {
        didSet{
            if let firstItem = self.cities.first, self.selectedCity == nil{
                if let selectedCity = self.selectedCity{
                    self.cityTF.text = selectedCity.name
                }else{
                    self.cityTF.text = firstItem.name
                }
                self.citiesPV.reloadAllComponents()
            }
        }
    }
    var selectedCity: City? = nil{
        didSet{
            guard let selectedCity = self.selectedCity else {return}
            self.cityTF.text = selectedCity.name
            if let index = self.cities.firstIndex(where: {$0.id == selectedCity.id}){
                self.citiesPV.selectRow(index, inComponent: 0, animated: false)
            }
        }
    }
    private var citiesPV: UIPickerView = UIPickerView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.cityTF.delegate = self
        self.cityTF.inputView = self.citiesPV
        self.cityTF.tintColor = .clear
        
        self.citiesPV.delegate = self
        self.citiesPV.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func selectCityAction(_ sender: Any) {
        self.cityTF.becomeFirstResponder()
    }
    
}

extension Header_OutletTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.cityTF.text = self.cities[row].name
        self.selectedCity = self.cities[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.cities[row].name
    }
}

extension Header_OutletTableViewCell: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let selectedCity = self.selectedCity else {return}
        self.delegate?.didCitySelected(city: selectedCity)
    }
}
