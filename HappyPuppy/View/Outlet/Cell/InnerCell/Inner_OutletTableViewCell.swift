//
//  Content_OutletTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 28/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Inner_OutletTableViewCell: UITableViewCell {

    @IBOutlet var operationalTimeLs: [UILabel]!
    var schedules: [Schedule] = []{
        didSet{
            for schedule in self.schedules {
                if let dayIdentifier = Int(schedule.day!), dayIdentifier < 8 {
                    let openTime = DateFormatterHelper.shared.formatString(oldDateFormat: "HH:mm:ss", newDateFormat: "HH:mm", dateString: schedule.timeStart!, timezone: TimeZone(abbreviation: "UTC"))
                    let closeTime = DateFormatterHelper.shared.formatString(oldDateFormat: "HH:mm:ss", newDateFormat: "HH:mm", dateString: schedule.timeFinish!, timezone: TimeZone(abbreviation: "UTC"))
                    self.operationalTimeLs[dayIdentifier-1].text = String(format: "%@ - %@", openTime, closeTime)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
