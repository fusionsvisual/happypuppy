//
//  Shimmer_OutletTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 27/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Shimmer_OutletTableViewCell: UITableViewCell {

    @IBOutlet var shimmerContainerV: PuppyClubShimmeringView!
    @IBOutlet var shimmerContentV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shimmerContainerV.contentView = self.shimmerContentV
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
