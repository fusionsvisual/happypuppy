//
//  OutletTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SVProgressHUD

class OutletTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var outletDetailButton: UIButton!
    @IBOutlet weak var outletImage: UIImageView!
    @IBOutlet weak var outletName: UILabel!
    @IBOutlet weak var outletAddress: UILabel!
    @IBOutlet weak var outletCity: UILabel!
    @IBOutlet weak var isOpen: UILabel!
    @IBOutlet weak var closeTime: UILabel!
    @IBOutlet weak var outletDistance: UILabel!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var directionView: UIView!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var reservationView: UIView!
    @IBOutlet weak var reservationButton: UIButton!
    @IBOutlet var chevronDownIV: UIImageView!
    
    var delegate: OutletDelegate?
    var section:Int = 0
    var outletItem:OutletItem? = nil{
        didSet{
            self.setupValue()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews() {
        self.phoneView.layer.cornerRadius = 10
        self.directionView.layer.cornerRadius = 10
        self.reservationView.layer.cornerRadius = 10
        self.reservationView.layer.borderColor = UIColor(cgColor: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)).cgColor
        self.reservationView.layer.borderWidth = 3
    }
    
    private func setupValue(){
        guard let outletItem = self.outletItem, let outlet = outletItem.outlet else {return}
        
        // MARK: Setup outlet logo
        self.outletImage.image = UIImage()
        if let imageUrlStr = outlet.brandData?.photoUrl{
            if let imageUrl = URL(string: "https://adm.happypuppy.id/" + imageUrlStr){
                self.outletImage.af.setImage(withURL: imageUrl)
            }
        }
        
        // MARK: Setup outlet name
        self.outletName.text = outlet.name
        
        // MARK: Setup outlet distance from user location
        if let latitude = outlet.outletLat, let longitude = outlet.outletLong{
            let outletCoordinate = CLLocation(latitude: Double(latitude) ?? 0.0, longitude: Double(longitude) ?? 0.0)
            let userCoordinate = CLLocation(latitude: UserDefaults.standard.double(forKey: "currentLatitude"), longitude: UserDefaults.standard.double(forKey: "currentLongitude"))

            let distanceStr = String(format: "%.2f", outletCoordinate.distance(from: userCoordinate).convert(from: .meters, to: .kilometers))
            self.outletDistance.text = "\(distanceStr) KM"
        }else{
            outletDistance.text = "0.00 KM"
        }
        
        // MARK: Setup outlet address
        self.outletAddress.text = outlet.address
        
        // MARK: Setup outlet city
        self.outletCity.text = outlet.city
        
        // MARK: Setup outlet status
        OutletHelper().checkOutletDateTime(outlet: outlet, openStatusL: self.isOpen, openDescL: self.closeTime, reservationV: self.reservationView, reservationBtn: self.reservationButton)
        
        // MARK: Setup dropdown button
        if outletItem.isOpened{
            self.chevronDownIV.transform = CGAffineTransform.identity.rotated(by: .pi)
        }else{
            self.chevronDownIV.transform = CGAffineTransform.identity
        }
        self.layoutIfNeeded()
    }
    
//    private func setupOutletAsOpen(operational: Operational?){
//        if let operational = operational, let openDate = operational.date, let closeTime = operational.timeEnd{
//            let intervalInSecond = Global.shared.willCloseTime(date: openDate, time: closeTime)
//            if intervalInSecond <= 3600 {
//                // MARK: Outlet close Soon
//                self.isOpen.textColor = UIColor(hexString: "#FFA724")
//                self.isOpen.text = "Segera tutup"
//                self.closeTime.text = "Jam \(closeTime)"
//            }else{
//                // MARK: Outlet open
//                self.isOpen.text = "Buka"
//                self.isOpen.textColor = UIColor(hexString: "#008000")
//                self.closeTime.text = "Tutup jam \(closeTime)"
//            }
//            // MARK: Disable resercation button
//            self.reservationButton.isEnabled = true
//            self.reservationView.layer.borderColor = Color.BUTTON_PRIMARY.cgColor
//            self.reservationButton.setTitleColor(Color.BUTTON_PRIMARY, for: .normal)
//        }else{
//            self.isOpen.text = "nil"
//            self.closeTime.text = "nil"
//
//            self.disableReservationBtn()
//        }
//    }
    
//    private func setupOutletAsClose(operational: Operational?){
//        if let operational = operational, let status = operational.isOpen{
//            if status == OutletStatus.OPEN{
//                // MARK: Outlet will open soon
//                self.isOpen.text = "Tutup"
//                self.isOpen.textColor = UIColor(red: 233/255, green: 26/255, blue: 0/255, alpha: 1)
//                if let openTime = operational.timeStart{
//                    self.closeTime.text = String(format: "Buka jam %@", openTime)
//                }else{
//                    self.closeTime.text = "nil"
//                }
//            }else{
//                // MARK: Outlet close
//                self.isOpen.text = "Tutup"
//                self.isOpen.textColor = UIColor(red: 233/255, green: 26/255, blue: 0/255, alpha: 1)
//                self.closeTime.text = "Libur"
//            }
//        }
//
//        self.disableReservationBtn()
//    }
    
    private func disableReservationBtn(){
        // MARK: Disable resercation button
        self.reservationButton.isEnabled = false
        self.reservationView.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25).cgColor
        self.reservationButton.setTitleColor(UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25), for: .normal)
    }
    
    @IBAction func outletDetailAction(_ sender: Any) {
        if let outletItem = self.outletItem, let outlet = outletItem.outlet{
            self.delegate?.goToOutletDetail(outlet: outlet)
        }else{
            SVProgressHUD.showError(withStatus: "Outlet is missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @IBAction func seeScheduleAction(_ sender: Any) {
        guard let outletItem = self.outletItem else {return }
        UIView.animate(withDuration: 0.25) {
            if outletItem.isOpened{
                self.chevronDownIV.transform = CGAffineTransform.identity
            }else{
                self.chevronDownIV.transform = CGAffineTransform.identity.rotated(by: .pi)
            }
            self.layoutIfNeeded()
        } completion: { (isDone) in
            self.delegate?.didSeeSchedule(section: self.section)
        }
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let outletItem = self.outletItem, let outlet = outletItem.outlet{
            if let phoneNumber = outlet.telp{
                let final = phoneNumber.replacingOccurrences(of: "[ |()]", with: "", options: .regularExpression)
                if let url = URL(string: "tel://\(final)"), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url as URL)
                }
            }else{
                SVProgressHUD.showError(withStatus: "Phone number is missing")
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }
        }else{
            SVProgressHUD.showError(withStatus: "Outlet is missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @IBAction func navigateAction(_ sender: Any) {
        if let outletItem = self.outletItem, let outlet = outletItem.outlet, let latitudeStr = outlet.outletLat, let longitudeStr = outlet.outletLong, let latitude = Double(latitudeStr), let longitude = Double(longitudeStr){
            let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = outlet.name
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }else{
            SVProgressHUD.showError(withStatus: "Unable to navigate")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @IBAction func reservationAction(_ sender: Any) {
        if let outletItem = self.outletItem, let outlet = outletItem.outlet{
            self.delegate?.goToReservation(outlet: outlet)
        }else{
            SVProgressHUD.showError(withStatus: "Outlet is missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
}
