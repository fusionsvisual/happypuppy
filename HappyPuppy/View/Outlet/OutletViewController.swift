//
//  OutletViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SVProgressHUD

struct OutletItem{
    var outlet:Outlet?
    var isOpened:Bool = false
}

protocol OutletDelegate {
    func goToReservation(outlet: Outlet)
    func goToOutletDetail(outlet: Outlet)
    func didSeeSchedule(section: Int)
}

protocol OutletHeaderDelegate {
    func didCitySelected(city: City)
}

class OutletViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var outletTV: UITableView!
        
    var locationManager = CLLocationManager()
    let dateFormatter = DateFormatter()
    
    var cityList:[City] = []
    var allCity = [String]()

    var isOutletLoaded = false
    var idCardStatus:String? = "0"
    var isVerified:String = "0"
    var requireKtp:String? = "0"
    
    private var cities:[City] = [
        City(id: nil, name: "Semua Kota")
    ] {
        didSet{
            self.outletTV.reloadData()
        }
    }
    private var outletItems:[OutletItem] = []
    
    // MARK: Variable for infinity scrolling
    private var spinner = UIActivityIndicatorView(style: .gray)
    private var limit:Int = 3; private var offset:Int = 0; private var totalCount:Int = 0; private var isLoading = false
    
    private var searchQuery = ""
    private var selectedCityName:String?
    private var selectedCity: City?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNotifObserver()
        setupPickerAndTableView()
        
        self.cities.removeSubrange(1..<self.cities.count)
        self.clearOutletList()
        self.setupData()
    }
    
    private func setupNotifObserver(){
        // MARK: Setup selected city, after "see all" button tapped from HomeVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCurrentCity), name: ObserverName.SET_OUTLET_CITY, object: nil)
    }
    
    func setupData() {
        HomeController.shared.getCities { (cityList, error) in
            guard let cityList = cityList else{return}
            self.cities.append(contentsOf: cityList)
            self.setupOutletData()
        }
        
        ProfileController.shared.getProfileDetail { (status, user, error) in
            self.idCardStatus = user?.idCardStatus
            if let user = user {
                if user.phone?.prefix(3) == "+62" {
                    if user.isVerified && user.isPhoneVerified{
                        self.isVerified = "1"
                    } else {
                        self.isVerified = "0"
                    }
                } else {
                    if user.isVerified {
                        self.isVerified = "1"
                    } else {
                        self.isVerified = "0"
                    }
                }
                
            }
            self.outletTV.reloadData()
        }
        
        ProfileController.shared.getProfileSettingsDetail { (status, settings, error) in
            self.requireKtp = settings?.requireKtp
            self.outletTV.reloadData()
        }
    }
    
    private func setupOutletData(){
        HomeController.shared.getOutletList(date: nil, length: self.limit, start: self.offset, city: self.selectedCity, latitude: UserDefaults.standard.double(forKey: "currentLatitude"), longitude: UserDefaults.standard.double(forKey: "currentLongitude")) { (total, outletList, error) in
        
            self.isOutletLoaded = true
            self.totalCount = total ?? 0
            
            if let outletList = outletList{
                if self.offset > 0{
                    // MARK: Pagination handler
                    self.isLoading = false
                    self.hideToggleSpinner()
                }
                
                for outlet in outletList {
                    // MARK: Create outlet item for outlet schedule cell
                    let outletItem = OutletItem(outlet: outlet, isOpened: false)
                    self.outletItems.append(outletItem)
                }
            }
            
            self.outletTV.reloadData()
        }
    }
    
    func setupPickerAndTableView() {
        self.outletTV.delegate = self
        self.outletTV.dataSource = self
        
        self.outletTV.register(UINib(nibName: "Header_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        let outletNibCell = UINib(nibName: "OutletTableViewCell", bundle: nil)
        self.outletTV.register(outletNibCell, forCellReuseIdentifier: "outletTableViewCell")
        self.outletTV.register(UINib(nibName: "Inner_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "InnerCell")
        self.outletTV.register(UINib(nibName: "Shimmer_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        
        let loadingNibCell = UINib(nibName: "LoadingTableViewCell", bundle: nil)
        outletTV.register(loadingNibCell, forCellReuseIdentifier: "loadingTableViewCell")
    }
    
    private func clearOutletList(){
        self.offset = 0
        self.isOutletLoaded = false
        self.outletItems.removeAll()
        self.outletTV.reloadData()
    }
    
    @objc private func updateCurrentCity(notification: NSNotification){
        if let userInfo = notification.userInfo{
            if let selectedCity = userInfo["selected_city"] as? City{
                self.outletTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                self.clearOutletList()
                self.selectedCity = selectedCity
                self.setupOutletData()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = self.outletTV.contentSize.height

        if contentHeight > 0, offsetY >= (contentHeight - self.outletTV.frame.size.height), !self.isLoading{
            if (self.offset + self.limit) < self.totalCount{
                self.offset += self.limit
                self.showToggleSpinner()
                self.isLoading = true
                self.setupOutletData()
            }
        }
    }
    
    func showToggleSpinner(){
        //MARK: Show TableView Spinner Footer
        self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.outletTV.bounds.width, height: CGFloat(50))
        self.outletTV.tableFooterView = self.spinner
        self.spinner.startAnimating()
    }

    func hideToggleSpinner(){
        //MARK: Hide TableView Spinner Footer
        self.outletTV.tableFooterView = nil
    }
}

extension OutletViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1{
            return UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 16))
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 16
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isOutletLoaded {
            return self.outletItems.count + 1
        }else{
            return 10 + 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            if self.isOutletLoaded{
                if self.outletItems[section-1].isOpened{
                    return 2
                }
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! Header_OutletTableViewCell
            cell.delegate = self
            cell.cities = self.cities
            cell.selectedCity = self.selectedCity
            return cell
        }else{
            if self.isOutletLoaded{
                if indexPath.item == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "outletTableViewCell", for: indexPath) as! OutletTableViewCell
                    cell.delegate = self
                    cell.section = indexPath.section
                    cell.outletItem = self.outletItems[indexPath.section - 1]
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCell", for: indexPath) as! Inner_OutletTableViewCell
                    if let outlet = outletItems[indexPath.section - 1].outlet{
                        cell.schedules = outlet.schedule ?? []
                    }
                    return cell
                }
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath)
                return cell
            }
        }
        
    }
}

extension OutletViewController: OutletDelegate{
    func goToReservation(outlet: Outlet) {
        if ReservationHelper.shared.checkVerification(outlet: outlet){
            //* Move to reservationVC if all the requirement passed
            let reservationVC = ReservationViewController()
            reservationVC.outlet = outlet
            self.navigationController?.pushViewController(reservationVC, animated: true)
        }
    }
    
    func didSeeSchedule(section: Int) {
        self.outletItems[section - 1].isOpened = !self.outletItems[section - 1].isOpened
        let sections = IndexSet.init(integer: section)
        self.outletTV.reloadSections(sections, with: .automatic)
    }
    
    func goToOutletDetail(outlet: Outlet) {
//        let vc = DetailOutletViewController()
//        vc.outlet = outlet
//        vc.outletId = outlet.id!
//        vc.outletCode = outlet.code!
//        vc.open = outlet.operational?.isOpen
//        vc.openTime = outlet.operational?.timeStart
//        vc.closeTime = outlet.operational?.timeEnd
//        self.navigationController?.pushViewController(vc, animated: true)
                
        let outletDetailVC = DetailOutletViewController()
        outletDetailVC.outlet = outlet
        outletDetailVC.operational = outlet.operational
        self.navigationController?.pushViewController(outletDetailVC, animated: true)
    }
}

extension OutletViewController: OutletHeaderDelegate{
    func didCitySelected(city: City) {
        self.selectedCity = city
        self.clearOutletList()
        self.setupOutletData()
    }
}
