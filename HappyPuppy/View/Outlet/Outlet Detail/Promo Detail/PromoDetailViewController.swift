//
//  PromoDetailViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 08/10/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PromoDetailViewController: UIViewController {
    
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var promoImageHeight: NSLayoutConstraint!
    @IBOutlet weak var promoTitle: UILabel!
    @IBOutlet weak var promoDescription: UILabel!
    var promo: OutletPromo?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Detail Promo"
        promoImageHeight.constant = UIScreen.main.bounds.height/2
        
        if let photoUrl = promo?.photo {
            if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                promoImage.af.setImage(withURL: url)
            }
        }
        
        promoTitle.text = promo?.name
        promoDescription.text = promo?.description
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "back"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }
}
