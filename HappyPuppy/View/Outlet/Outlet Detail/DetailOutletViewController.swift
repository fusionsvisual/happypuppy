//
//  DetailOutletV2ViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa
import CoreLocation
import SVProgressHUD

protocol OutletInfoDelegate {
    func didReservationButtonTapped()
    func didCallButtonTapped()
    func didNavButtonTapped()
    func didShareButtonTapped()
}

class DetailOutletViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var outlet: Outlet?
    var operational: Operational?
    var promos: [OutletPromo] = []
    var roomTypes: [RoomTypePrice] = []
    var operationalData: OperationalData?
    
    private var isDataLoaded:Bool = false
    private var isScheduleOpened:Bool = false
    private var selectedDate: String = ""
    private var selectedTime: String = ""
    private var disposeBag:DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.observeViewModel()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.disposeBag = DisposeBag()
    }
    
    private func setupViews(){
        self.title = "Detail Outlet"
        
        //* Setup Right Bar Button Item
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(self.refresh))
        
        //* Setup Table View
        self.tableView.register(UINib(nibName: "OutletTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "TitleCell")
        self.tableView.register(UINib(nibName: "OutletEmptyStateTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyStateCell")

        self.tableView.register(UINib(nibName: "OutletDetailShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.tableView.register(UINib(nibName: "OutletInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletInfoCell")
        self.tableView.register(UINib(nibName: "OutletScheduleHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletScheduleHeaderCell")
        self.tableView.register(UINib(nibName: "Inner_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "ScheduleContentCell")
        self.tableView.register(UINib(nibName: "OutletDetailContactTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletContactCell")
        
        self.tableView.register(UINib(nibName: "FacilityTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletFacilityCell")
        self.tableView.register(UINib(nibName: "OutletGalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletGalleryCell")
        self.tableView.register(UINib(nibName: "OutletPromoTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletPromoCell")
        
        self.tableView.register(UINib(nibName: "OutletRoomTypePickerTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletRoomTypeCell")
        self.tableView.register(UINib(nibName: "HeaderRoomTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderRoomTypeCell")
        self.tableView.register(UINib(nibName: "RoomTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomTypeCell")
        
        self.tableView.register(UINib(nibName: "OutletFooterTableViewCell", bundle: nil), forCellReuseIdentifier: "FooterCell")
    }
    
    private func setupData(){
        guard let outlet = self.outlet else {
            SVProgressHUD.showError(withStatus: "Outlet not found")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            
            self.navigationController?.popViewController(animated: true)
            return
        }
        OutletVM.shared.getOutletDetail(outletId: outlet.id!)
    }
    
    private func observeViewModel(){
        OutletVM.shared.outlet.bind { (outlet) in
            self.outlet = outlet
            
            OutletVM.shared.getAllOutletPromos(outletCode: outlet.code!)
        }.disposed(by: self.disposeBag)
        
        OutletVM.shared.promos.bind { (promos) in
            self.promos = promos
                        
            self.selectedDate = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: Date(), timezone: nil, locale: nil)
            let dateQuery = DateFormatterHelper.shared.formatDate(dateFormat: "yyyy-MM-dd", date: Date(), timezone: nil, locale: nil)
            self.selectedTime = DateFormatterHelper.shared.getCurrentDate(dateFormat: "HH:mm", timezone: nil)
            
            if let outlet = self.outlet, outlet.reservationPaymentType{
                self.selectedTime = "18:00"
            }
            let timeQuery = Int(self.selectedTime.split(separator: ":").first!)!
            
            OutletVM.shared.getAllOutletRoomTypes(outletCode: self.outlet!.code, date: dateQuery, hour: String(timeQuery))
        }.disposed(by: self.disposeBag)
        
        OutletVM.shared.roomTypes.bind { (roomTypes) in
            self.isDataLoaded = true
                        
            self.outlet!.operational = self.operational
            self.operationalData = OutletHelper().checkOutletDateTime1(outlet: self.outlet!)
            
            self.roomTypes = roomTypes
            
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
        }.disposed(by: self.disposeBag)
        
        OutletVM.shared.errMsg.bind { (errMsg) in
            SVProgressHUD.showError(withStatus: errMsg)
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: self.disposeBag)
    }
    
    @objc private func refresh(){
        self.isDataLoaded = false
        self.tableView.reloadData()
        
        self.setupData()
    }
}

extension DetailOutletViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 1){
            if indexPath.row == 0 {
                self.isScheduleOpened = !self.isScheduleOpened
                self.tableView.reloadData()
                self.tableView.layoutIfNeeded()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isDataLoaded{
            return 1
        }
        
        if self.outlet!.reservationPaymentType{
            return 7
        }
        
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !self.isDataLoaded{
            return 1
        }
        
        if section == 1{
            if self.isScheduleOpened{
                return 2
            }
            return 1
        }
        
        if section == 4 || section == 5{
            return 2
        }
        
        if (section == 3){
            if let outlet = self.outlet, outlet.facilities.count > 0{
                return outlet.facilities.count + 1
            }
            return 2
        }
        
        if (section == 6){
//            if let outlet = self.outlet, outlet.reservationPaymentType{
//                return self.roomTypes.count + 1
//            }
            return self.roomTypes.count + 2
        }
        
        if (section == 7){
            if let outlet = self.outlet, !outlet.reservationPaymentType {
                return 1
            }
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !self.isDataLoaded{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
        
        if (indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutletInfoCell", for: indexPath) as! OutletInfoTableViewCell
            cell.delegate = self
            cell.outlet = self.outlet
            cell.operationalData = self.operationalData
            return cell
        }
        
        if (indexPath.section == 1){
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "OutletScheduleHeaderCell", for: indexPath) as! OutletScheduleHeaderTableViewCell
                cell.operationalData = self.operationalData
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleContentCell", for: indexPath) as! Inner_OutletTableViewCell
            if let outlet = self.outlet, let schedule = outlet.schedule{
                cell.schedules = schedule
            }
            return cell
        }
        
        if (indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutletContactCell", for: indexPath) as! OutletDetailContactTableViewCell
            cell.outlet = self.outlet
            return cell
        }
        
        if (indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5){
            if (indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! OutletTitleTableViewCell
                if (indexPath.section == 3){
                    cell.titleL.text = "Fasilitas Outlet"
                }else if (indexPath.section == 4){
                    cell.titleL.text = "Galeri"
                }else if (indexPath.section == 5){
                    cell.titleL.text = "Promo"
                }
                return cell
            }
        }
        
        if indexPath.section == 3{
            if let outlet = self.outlet, outlet.facilities.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyStateCell", for: indexPath) as! OutletEmptyStateTableViewCell
                cell.emptyStateTitleL.text = "No facilities data"
                return cell
            }
            let facility = self.outlet!.facilities[indexPath.row - 1]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutletFacilityCell", for: indexPath) as! FacilityTableViewCell
            if let imageUrl = URL(string: String(format: "%@/%@", Network.ASSET_URL, (facility.iconUrl ?? ""))){
                cell.icon.af.setImage(withURL: imageUrl)
            }
            cell.facilityLabel.text = facility.description
            if indexPath.row == self.outlet!.facilities.count{
                cell.bottomC.constant = 24
            }
            return cell
        }
        
        if indexPath.section == 4{
            if let outlet = self.outlet, outlet.galleries.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyStateCell", for: indexPath) as! OutletEmptyStateTableViewCell
                cell.emptyStateTitleL.text = "No Galleries data"
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutletGalleryCell", for: indexPath) as! OutletGalleryTableViewCell
            cell.outlet = self.outlet
            return cell
        }
        
        if indexPath.section == 5{
            if self.promos.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyStateCell", for: indexPath) as! OutletEmptyStateTableViewCell
                cell.emptyStateTitleL.text = "No Promos data"
                return cell
            }
                        
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutletPromoCell", for: indexPath) as! OutletPromoTableViewCell
            cell.delegate = self
            cell.promos = self.promos
            return cell
        }
        
        if indexPath.section == 6{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "OutletRoomTypeCell", for: indexPath) as! OutletRoomTypePickerTableViewCell
                cell.selectedDate = self.selectedDate
                cell.selectedTime = self.selectedTime
                cell.delegate = self
                
                if (self.outlet!.reservationPaymentType){
                    cell.pickerSV.isHidden = true
                }
                
                return cell
            }
            
            if (indexPath.row == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderRoomTypeCell", for: indexPath) as! HeaderRoomTypeTableViewCell
                
                guard let outlet = self.outlet else {return cell}
                
                cell.facilityHeaderL.isHidden = outlet.reservationPaymentType
                cell.minimumHourHeaderL.isHidden = !outlet.reservationPaymentType
                cell.priceVHeaderWidth.constant = ((self.view.frame.width - 48) - 16) * 0.3
                
                if outlet.reservationPaymentType{
                    cell.priceVHeaderWidth.constant = ((self.view.frame.width - 48) - 16) * 0.35
                }
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoomTypeCell", for: indexPath) as! RoomTypeTableViewCell
            guard let outlet = self.outlet else {return cell}
            
            let roomType = self.roomTypes[indexPath.row - 2]
            cell.roomtype.text = roomType.room?.roomType
            cell.facilities = roomType.facility
            cell.pax.text = roomType.room?.pax
            
            cell.facilityV.isHidden = outlet.reservationPaymentType
            cell.minimumHourV.isHidden = !outlet.reservationPaymentType
            cell.priceVWidth.constant = ((view.frame.width - 48) - 16) * 0.3
            
            var price = roomType.price!

            if outlet.reservationPaymentType{
                price = roomType.room!.minimumCharge
                cell.minimumHourL.text = String(roomType.room!.minimumHour)
                cell.priceVWidth.constant = ((view.frame.width - 48) - 16) * 0.35
            }
            
            cell.price.text = "Rp. \(Formatter.shared.priceFormatter(price))"
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell", for: indexPath) as! OutletFooterTableViewCell
        cell.selectedDate = self.selectedDate
        cell.selectedTime = self.selectedTime
        
        return cell
    }
    
    private func createTitleV(title:String) -> UIView{
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 56))
        let titleL = UILabel(frame: CGRect(x: 24, y: view.frame.height/2 - 21/2, width: self.view.frame.width-48, height: 21))
        titleL.font = UIFont.systemFont(ofSize: 21, weight: .bold)
        titleL.text = title
        view.addSubview(titleL)
        
        return view
    }
}


extension DetailOutletViewController: OutletInfoDelegate{
    func didReservationButtonTapped() {
        guard let outlet = self.outlet else {
            SVProgressHUD.showError(withStatus: "Outlet data is missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return
        }
                
        if ReservationHelper.shared.checkVerification(outlet: outlet){
            let reservationVC = ReservationViewController()
            reservationVC.outlet = self.outlet
            self.navigationController?.pushViewController(reservationVC, animated: true)
        }
    }
    
    func didCallButtonTapped() {
        guard let outlet = self.outlet, let phoneNumber = outlet.telp else {
            SVProgressHUD.showError(withStatus: "Phone number not found")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return
        }
        
        let parsedPhoneNumber = phoneNumber.replacingOccurrences(of: "[ |()]", with: "", options: .regularExpression)
        if let url = URL(string: "tel://\(parsedPhoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url as URL)
        }
    }
    
    func didNavButtonTapped() {
        guard let outlet = self.outlet, let outletLat = outlet.outletLat, let outletLong = outlet.outletLong else {
            SVProgressHUD.showError(withStatus: "Location not found")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return
        }
        
        let latitude = Double(outletLat)!
        let longitude = Double(outletLong)!
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = outlet.name
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }

    func didShareButtonTapped() {
        guard let outlet = self.outlet, let outletCode = outlet.code else {
            SVProgressHUD.showError(withStatus: "Outlet not found")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return
        }
        
        let url = URL(string: "\(Network.shared.getBaseURL())/outlet/detail/\(outletCode)")!
        let ac = UIActivityViewController(activityItems: [url] as [URL], applicationActivities: nil)
        self.present(ac, animated: true)
    }
}

extension DetailOutletViewController: OutletPromoDelegate{
    func didPromoSelected(promo: OutletPromo) {
        let promoDetailVC = PromoDetailViewController()
        promoDetailVC.promo = promo
        self.navigationController?.pushViewController(promoDetailVC, animated: true)
    }
}

extension DetailOutletViewController: OutletRoomTypeDelegate{
    func didDateSelected(selectedDate: String) {
        self.selectedDate = selectedDate
        self.refreshRoomType()
    }
    
    func didTimeSelected(selectedTime: String) {
        self.selectedTime = selectedTime
        self.refreshRoomType()
    }
    
    private func refreshRoomType(){
        self.roomTypes.removeAll()
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
        
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 7), at: .bottom, animated: false)
        
        let dateQuery = DateFormatterHelper.shared.formatString(oldDateFormat: "dd MMMM yyyy", newDateFormat: "yyyy-MM-dd", dateString: self.selectedDate, timezone: nil)
        let timeQuery = Int(self.selectedTime.split(separator: ":").first!)!
        OutletVM.shared.getAllOutletRoomTypes(outletCode: self.outlet?.code!, date: dateQuery, hour: String(timeQuery))
    }
}
