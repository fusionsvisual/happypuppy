//
//  GalleryCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 07/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
