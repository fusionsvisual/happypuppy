//
//  OutletGalleryTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletGalleryTableViewCell: UITableViewCell {

    @IBOutlet var galleryColV: UICollectionView!
    
    var outlet: Outlet? = nil {
        didSet{
            self.galleryColV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.galleryColV.delegate = self
        self.galleryColV.dataSource = self
        self.galleryColV.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
        self.galleryColV.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
    }
}

extension OutletGalleryTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 200)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let outlet = self.outlet else {
            return 0
        }
        return outlet.galleries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCollectionViewCell
        let photoString = String(format: "%@/%@", Network.ASSET_URL, self.outlet!.galleries[indexPath.row].photo!).replacingOccurrences(of: " ", with: "%20")
        if let photoUrl = URL(string: photoString){
            cell.image.af.setImage(withURL: photoUrl)
        }
        return cell
    }
}
