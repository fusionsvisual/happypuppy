//
//  FacilityTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 07/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class FacilityTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var facilityLabel: UILabel!
    @IBOutlet var bottomC: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
