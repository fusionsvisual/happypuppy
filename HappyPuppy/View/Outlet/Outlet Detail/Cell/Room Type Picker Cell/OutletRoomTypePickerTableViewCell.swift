//
//  OutletRoomTypePickerTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

protocol OutletRoomTypeDelegate {
    func didDateSelected(selectedDate: String)
    func didTimeSelected(selectedTime: String)
}

class OutletRoomTypePickerTableViewCell: UITableViewCell {

    @IBOutlet var dateTF: UITextField!
    @IBOutlet var timeTF: UITextField!
    @IBOutlet var pickerSV: UIStackView!
    
    var delegate: OutletRoomTypeDelegate?
    var selectedDate: String? = nil{
        didSet{
            guard let selectedDate = self.selectedDate, let formattedDate = DateFormatterHelper.shared.stringToDate(dateString: selectedDate, dateFormat: "dd MMMM yyyy", timezone: TimeZone.init(abbreviation: "UTC")) else {return}
            self.dateTF.text = selectedDate;
            self.dateDP.date = formattedDate
        }
    }
    var selectedTime: String? = nil {
        didSet{
            guard let selectedTime = self.selectedTime else {return}
            let timeIndex = Int(selectedTime.split(separator: ":").first!)!
            self.timeTF.text = self.timeTitles[timeIndex]
            self.timePV.selectRow(timeIndex, inComponent: 0, animated: false)
        }
    }
    
    private var dateDP = UIDatePicker()
    private var timePV = UIPickerView()
    
    private let timeTitles = ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        if let selectedDate = self.selectedDate{
            self.dateTF.text = selectedDate
        }
        if #available(iOS 13.4, *) {
            self.dateDP.preferredDatePickerStyle = .wheels
        }
        self.dateDP.datePickerMode = .date
        self.dateTF.text = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: Date(), timezone: nil, locale: nil)
        self.dateTF.tintColor = .clear
        self.dateTF.inputView = self.dateDP
        self.dateTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.didDoneClicked))
        
        self.timePV.delegate = self
        self.timePV.selectRow(Global.shared.getCurrentTimeOnlyHour(), inComponent: 0, animated: true)
        
        if let selectedTime = self.selectedTime{
            self.timeTF.text = selectedTime
        }
        self.timeTF.text = self.timeTitles[self.timePV.selectedRow(inComponent: 0)]
        self.timeTF.tintColor = .clear
        self.timeTF.inputView = self.timePV
        self.timeTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.didDoneClicked))
    }
    
    @objc private func didDoneClicked(textField: UITextField){
        if textField == self.dateTF{
            self.dateTF.text = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: self.dateDP.date, timezone: nil, locale: nil)
            self.delegate?.didDateSelected(selectedDate: self.dateTF.text!)
            return
        }
        self.timeTF.text = self.timeTitles[self.timePV.selectedRow(inComponent: 0)]
        self.delegate?.didTimeSelected(selectedTime: self.timeTF.text!)
    }
}

extension OutletRoomTypePickerTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.timeTitles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.timeTitles[row]
    }
}

//extension OutletRoomTypePickerTableViewCell: UITextFieldDelegate{
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//    }
//}
