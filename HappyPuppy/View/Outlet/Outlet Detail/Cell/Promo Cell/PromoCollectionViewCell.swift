//
//  PromoCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 07/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var promoDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
    }

}
