//
//  OutletPromoTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

protocol OutletPromoDelegate {
    func didPromoSelected(promo: OutletPromo)
}

class OutletPromoTableViewCell: UITableViewCell {

    @IBOutlet var promoColV: UICollectionView!
    
    var delegate: OutletPromoDelegate?
    var promos: [OutletPromo] = []{
        didSet{
            self.promoColV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.promoColV.delegate = self
        self.promoColV.dataSource = self
        self.promoColV.register(UINib(nibName: "PromoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PromoCell")
        self.promoColV.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
    }
}

extension OutletPromoTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didPromoSelected(promo: self.promos[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 225)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.promos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromoCell", for: indexPath) as! PromoCollectionViewCell
        let promo = self.promos[indexPath.row]
        if let promoUrl = URL(string: String(format: "%@/%@", Network.ASSET_URL, promo.photo!)){
            cell.image.af.setImage(withURL: promoUrl)
        }
        cell.promoDescription.text = promo.name
        return cell
    }
}
