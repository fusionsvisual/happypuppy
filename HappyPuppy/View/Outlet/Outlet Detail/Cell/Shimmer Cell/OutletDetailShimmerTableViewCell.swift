//
//  OutletDetailShimmerTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletDetailShimmerTableViewCell: UITableViewCell {

    @IBOutlet var shimmerV: PuppyClubShimmeringView!
    @IBOutlet var shimmerContentV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }

    private func setupViews(){
        self.shimmerV.contentView = self.shimmerContentV
    }
}
