//
//  HeaderRoomTypeTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 08/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class HeaderRoomTypeTableViewCell: UITableViewCell {

    @IBOutlet var containerV: UIView!
    
    @IBOutlet var facilityHeaderL: UILabel!
    @IBOutlet var minimumHourHeaderL: UILabel!
    
    @IBOutlet var priceVHeaderWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
