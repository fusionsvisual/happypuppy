//
//  OutletInfoTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class OutletInfoTableViewCell: UITableViewCell {
    
    @IBOutlet var bannerIV: UIImageView!
    
    @IBOutlet var outletNameL: UILabel!
    @IBOutlet var outletLocL: UILabel!
    @IBOutlet var outletAddressL: UILabel!
    
    @IBOutlet var reservationBtn: UIButton!
    
    var delegate: OutletInfoDelegate?
    var outlet: Outlet? = nil {
        didSet{
            self.setupViews()
        }
    }
    var operationalData: OperationalData? = nil {
        didSet{
            if let operationalData = self.operationalData {
                self.reservationBtn.isEnabled = operationalData.isBtnEnabled
                if self.reservationBtn.isEnabled {
                    self.reservationBtn.layer.borderColor = operationalData.reservBorderColor
                    self.reservationBtn.setTitleColor(operationalData.reservTitleColor, for: .normal)
                }else{
                    self.reservationBtn.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25).cgColor
                    self.reservationBtn.setTitleColor(UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25), for: .normal)
                }
            }
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.reservationBtn.backgroundColor = .clear
        
        guard let outlet = self.outlet else {return}
        if let photoUrlStr = outlet.photo{
            if let photoUrl = URL(string: String(format: "%@/%@", Network.ASSET_URL, photoUrlStr)){
                self.bannerIV.af.setImage(withURL: photoUrl)
            }
        }
        self.outletNameL.text = outlet.brandData?.name
        self.outletLocL.text = outlet.name
        self.outletAddressL.text = outlet.address
        
        //* Setup reservation button
        self.reservationBtn.layer.borderWidth = 3
        self.reservationBtn.layer.cornerRadius = 10
        
//        OutletHelper.shared.checkOutletDateTime(outlet: self.outlet!, openStatusL: <#T##UILabel#>, openDescL: <#T##UILabel#>, reservationV: <#T##UIView#>, reservationBtn: self.reservationBtn)
    }

    @IBAction func reservationAction(_ sender: Any) {
        self.delegate?.didReservationButtonTapped()
    }
    
    @IBAction func callAction(_ sender: Any) {
        self.delegate?.didCallButtonTapped()
    }
    
    @IBAction func navAction(_ sender: Any) {
        self.delegate?.didNavButtonTapped()
    }
    
    @IBAction func shareAction(_ sender: Any) {        
        self.delegate?.didShareButtonTapped()
    }
}
