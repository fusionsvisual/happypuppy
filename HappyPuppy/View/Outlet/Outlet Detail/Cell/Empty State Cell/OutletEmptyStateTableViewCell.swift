//
//  OutletEmptyStateTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletEmptyStateTableViewCell: UITableViewCell {

    @IBOutlet var emptyStateTitleL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
