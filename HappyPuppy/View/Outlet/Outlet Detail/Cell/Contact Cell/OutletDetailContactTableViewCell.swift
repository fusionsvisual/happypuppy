//
//  OutletDetailContactTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletDetailContactTableViewCell: UITableViewCell {
    
    @IBOutlet var outletPhoneL: UILabel!
    @IBOutlet var outletInstaL: UILabel!
    
    var outlet: Outlet? = nil {
        didSet{
            self.setupViews()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    private func setupViews(){
        guard let outlet = self.outlet else {return}
        
        self.outletPhoneL.text = outlet.telp
        self.outletInstaL.text = outlet.instagram
    }
    
}
