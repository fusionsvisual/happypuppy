//
//  FacilityRoomTypeTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 16/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class FacilityRoomTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var facilityLabel: UILabel!
    @IBOutlet weak var facilityLabelHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
