//
//  OutletFooterTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletFooterTableViewCell: UITableViewCell {

    @IBOutlet var footerDescL: UILabel!
    
    var selectedDate: String?
    var selectedTime: String? = nil {
        didSet{
            guard let selectedDate = self.selectedDate, let selectedTime = self.selectedTime else {return}
            let parsedTime = String(format: "%@:00", String(selectedTime.split(separator: ":").first!))
            self.footerDescL.text = "Harga diatas harga sewa ruangan per jam untuk tanggal \(selectedDate) dan pukul \(parsedTime). Harga di atas sudah termasuk pajak dan service."
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
