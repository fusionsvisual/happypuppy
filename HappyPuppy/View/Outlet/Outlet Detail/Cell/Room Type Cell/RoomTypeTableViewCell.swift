//
//  ScheduleTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 07/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class RoomTypeTableViewCell: UITableViewCell {
    
    @IBOutlet var containerV: UIView!
    
    @IBOutlet var facilityV: UIView!
    @IBOutlet var minimumHourV: UIView!
    
    @IBOutlet weak var roomtype: UILabel!
    @IBOutlet weak var facilityTableView: UITableView!
    @IBOutlet weak var facilityTablewViewHeight: NSLayoutConstraint!
    @IBOutlet weak var price: UILabel!
    @IBOutlet var minimumHourL: UILabel!
    @IBOutlet weak var pax: UILabel!
    
    @IBOutlet var priceVWidth: NSLayoutConstraint!
    
    var facilities: [Facility]? = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupTablewView()
        
        facilityTableView.estimatedRowHeight = 26
        facilityTableView.rowHeight = UITableView.automaticDimension
    }

    func setupTablewView() {
        facilityTableView.delegate = self
        facilityTableView.dataSource = self
        
        let facilityRoomTypeNibCell = UINib(nibName: "FacilityRoomTypeTableViewCell", bundle: nil)
        facilityTableView.register(facilityRoomTypeNibCell, forCellReuseIdentifier: "facilityRoomTypeTableViewCell")
    }
}

extension RoomTypeTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facilities!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "facilityRoomTypeTableViewCell", for: indexPath) as! FacilityRoomTypeTableViewCell
        
        if let quantity = facilities![indexPath.row].quantity, let description = facilities![indexPath.row].description {
            cell.facilityLabel.text = "\(quantity) x \(description)"
        }
        
        return cell
    }
}
