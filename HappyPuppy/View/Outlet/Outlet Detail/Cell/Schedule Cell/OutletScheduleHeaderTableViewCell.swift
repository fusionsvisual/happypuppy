//
//  ScheduleHeaderTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 18/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class OutletScheduleHeaderTableViewCell: UITableViewCell {

    @IBOutlet var openStatusL: UILabel!
    @IBOutlet var openTimeL: UILabel!
    
    var operationalData: OperationalData? = nil {
        didSet{
            if let operationalData = self.operationalData {
                self.openStatusL.text = operationalData.openStatus
                self.openTimeL.text = operationalData.openDesc
                
                self.openStatusL.textColor = operationalData.statusColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    private func setupViews(){
//        self.openStatusL.text = ""
//    }
    
}
