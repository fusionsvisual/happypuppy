//
//  VoucherDetailViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class VoucherDetailViewController: UIViewController {
    
    /* UIImageView */
    @IBOutlet var voucherImageView: UIImageView!
    @IBOutlet var qrCodeImageView: UIImageView!
    /*-----*/
    
    /* UILabel */
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var expiredDateTitleL: UILabel!
    @IBOutlet var expiredDateL: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var qrCodeLabel: UILabel!
    /*-----*/
    
    /* UIView */
    @IBOutlet var qrCodeViewContainer: UIView!
    /*-----*/
    
    /* UIButton */
    @IBOutlet var redeemButton: UIButton!
    /*-----*/
    
    var voucher:Voucher?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        
        /* Setup Navigation Controller Title */
        self.title = "Voucher"
        
        /* Setup Button Corner Radius */
        redeemButton.layer.cornerRadius = 10
        
        if let voucher = voucher{
            
            /* Setup Title Label */
            titleLabel.text = voucher.name
            
            /* Setup Expired Date Label */
            if voucher.expiredDate != ""{
                self.expiredDateL.text = voucher.expiredDate
            }else{
                self.expiredDateTitleL.text = "POIN:"
                self.expiredDateL.text = voucher.redeemPoint
            }
            
            /* Setup Description Label */
            if let description = voucher.description{
                descriptionLabel.attributedText = description.htmlToAttributedString
            }
            
            /* Setup ImageView */
            if let photoUrl = voucher.photoUrl{
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    voucherImageView.af.setImage(withURL: url, filter: nil)
                }
            }
            
            /* Setup redeemButton UIButton */
            if voucher.code == ""{
                self.redeemButton.isHidden = false
                self.qrCodeViewContainer.isHidden = true
            }else{
                self.redeemButton.isHidden = true
                self.qrCodeViewContainer.isHidden = false
                self.qrCodeLabel.text = voucher.code
                DispatchQueue.main.async {
                    Global.shared.generateQRCode(from: voucher.code) { (image) in
                        self.qrCodeImageView.image = image
                    }
                }
            }
        }
    }
    
    
    @IBAction func redeemAction(_ sender: Any) {
        SVProgressHUD.show()
        
        if let voucher = voucher{
            if let voucherId = voucher.id{
                VoucherController.shared.createVoucherRedeem(voucherId: voucherId) { (status, error) in
                    SVProgressHUD.dismiss()
                    
                    if let error = error{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }else{
                        ProfileController.shared.getProfileDetail { (status, user, error) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
}
