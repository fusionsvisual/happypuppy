//
//  ShimmerTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import Shimmer

class ShimmerVoucherTableViewCell: UITableViewCell {

    @IBOutlet var shimmerContainerV: FBShimmeringView!
    @IBOutlet var shimmerContentV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shimmerContainerV.shimmeringSpeed = 1000
        self.shimmerContainerV.isShimmering = true
        self.shimmerContainerV.contentView = shimmerContentV
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
