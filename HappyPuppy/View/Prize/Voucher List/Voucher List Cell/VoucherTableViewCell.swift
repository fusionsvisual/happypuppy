//
//  VoucherTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class VoucherTableViewCell: UITableViewCell {

    @IBOutlet var voucherImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
