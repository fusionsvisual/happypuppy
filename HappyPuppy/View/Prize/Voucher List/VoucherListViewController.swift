//
//  VoucherListViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

enum VoucherType {
    case MY_VOUCHER, AVAILABLE_VOUCHER
}

class VoucherListViewController: UIViewController{
    
    @IBOutlet var voucherTableView: UITableView!
    
    var voucherType: VoucherType = .MY_VOUCHER
    
    // MARK: Pagination Variable
    private var spinner = UIActivityIndicatorView(style: .gray)
    private var totalCount:Int = 0
    private var offset = 0
    private var limit = 10
    
    private var voucherList:[Voucher] = []
    private var isDataLoaded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
    }
    
    private func setupViews(){
        self.title = "Voucher"
        
        self.voucherTableView.register(UINib(nibName: "VoucherTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherListCell")
        self.voucherTableView.register(UINib(nibName: "ShimmerVoucherTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        voucherTableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 0, right: 0)
        
    }
    
    private func setupData(){
        if self.voucherType == .MY_VOUCHER{
            VoucherController.shared.getMyVoucherList(length: self.limit, start: self.offset, completion: { (status, voucherList, totalCount, error) in
                self.setupValue(status: status, voucherList: voucherList, totalCount: totalCount, error: error)
            })
        }else{
            VoucherController.shared.getVoucherList(length: self.limit, start: self.offset) { (status, voucherList, totalCount, error)  in
                self.setupValue(status: status, voucherList: voucherList, totalCount: totalCount, error: error)
            }
        }
    }
    
    private func setupValue(status:String, voucherList:[Voucher]?, totalCount:Int, error:String?){
        self.isDataLoaded = true
        
        if let error = error{
            SVProgressHUD.showError(withStatus: error)
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }else{
            if let voucherList = voucherList{
                if self.offset == 0{
                    self.totalCount = totalCount
                    self.voucherList = voucherList
                    self.voucherTableView.reloadData()
                }else{
                    self.voucherList.append(contentsOf: voucherList)
                    self.voucherTableView.reloadData()
                    self.toggleSpinner(show: false)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = self.voucherTableView.contentSize.height
        
        if contentHeight > 0, offsetY >= (contentHeight - self.voucherTableView.frame.size.height){
            if (self.offset + self.limit) < self.totalCount{
                self.offset += self.limit
                self.toggleSpinner(show: true)
                self.setupData()
            }
        }
    }
    
    // MARK: Toggle Spinner for Infinity Scrolling
    func toggleSpinner(show:Bool){
        if show{
            //MARK: Show TableView Spinner Footer
            self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.voucherTableView.bounds.width, height: CGFloat(50))
            self.voucherTableView.tableFooterView = spinner
            self.spinner.startAnimating()
        }else{
            //MARK: Hide TableView Spinner Footer
            self.voucherTableView.tableFooterView = nil
        }
    }
    
}

extension VoucherListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isDataLoaded{
            return voucherList.count
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isDataLoaded{
            let voucher = voucherList[indexPath.item]
            let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherListCell", for: indexPath) as! VoucherTableViewCell
            cell.titleLabel.text = voucher.name
            cell.descriptionLabel.text = voucher.description?.htmlToString
            
            /* Setup Voucher ImageView */
            cell.voucherImageView.image = UIImage(named: "NoImageRectangle")
            if let photoUrl = voucher.photoUrl{
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    let filter = AspectScaledToFillSizeFilter(size: cell.voucherImageView.frame.size)
                    cell.voucherImageView.af.setImage(withURL: url, filter: filter)
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isDataLoaded{
            let voucherDetailVC = VoucherDetailViewController()
            voucherDetailVC.voucher = voucherList[indexPath.item]
            navigationController?.pushViewController(voucherDetailVC, animated: true)
        }
    }
}
