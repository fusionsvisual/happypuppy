//
//  PrizeViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class PrizeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    @IBOutlet var myVoucherCollectionView: UICollectionView!
    @IBOutlet var prizeCollectionView: UICollectionView!
    @IBOutlet var voucherCollectionView: UICollectionView!
    
    @IBOutlet var myVoucherListReadMoreButton: UIButton!
    @IBOutlet var prizeReadMoreButton: UIButton!
    @IBOutlet var voucherReadMoreButton: UIButton!
    
    @IBOutlet var notHaveMyVoucherListLabel: UILabel!
    
    var rewardList:[Reward] = []
    var myVoucherList:[Voucher] = []
    var voucherList:[Voucher] = []
    
    var isRewardLoaded = false
    var isMyVoucherLoaded = false
    var isVoucherLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupData()
    }
    
    private func setupViews(){
        setupCollectionView()
    }
    
    private func setupData(){
//        DispatchQueue.main.async {
            VoucherController.shared.getMyVoucherList(length: nil, start: nil, completion: { (status, voucherList, totalCount, error) in
                
                self.isMyVoucherLoaded = true
                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let myVoucherList = voucherList{
                        self.myVoucherList = myVoucherList
                        if self.myVoucherList.count == 0{
                            self.notHaveMyVoucherListLabel.isHidden = false
                        }
                    }else{
                        self.notHaveMyVoucherListLabel.isHidden = false
                    }
                    self.myVoucherListReadMoreButton.isEnabled = true
                    self.myVoucherCollectionView.reloadData()
                }
            })
//        }
        
        
        DispatchQueue.main.async {
            RewardController.shared.getRewards(length: nil, start: nil) { (status, rewardList, error) in
                self.isRewardLoaded = true
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let rewardList = rewardList{
                        self.rewardList = rewardList
                    }
                    self.prizeReadMoreButton.isEnabled = true
                    self.prizeCollectionView.reloadData()
                }
            }
        }
        
        DispatchQueue.main.async {
            VoucherController.shared.getVoucherList(length: nil, start: nil) { (status, voucherList, totalCount, error)  in
                self.isVoucherLoaded = true
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let voucherList = voucherList{
                        self.voucherList = voucherList
                    }
                    self.voucherReadMoreButton.isEnabled = true
                    self.voucherCollectionView.reloadData()
                }
            }
        }
    }
    
    private func setupCollectionView(){
        let colViewLayout = UICollectionViewFlowLayout()
        colViewLayout.scrollDirection = .horizontal
        colViewLayout.minimumInteritemSpacing = 16
        
        let myVoucherViewLayout = UICollectionViewFlowLayout()
        myVoucherViewLayout.scrollDirection = .horizontal
        myVoucherViewLayout.minimumInteritemSpacing = 16
        /* Setup Voucher CollectionView */
        myVoucherCollectionView.collectionViewLayout = myVoucherViewLayout
        myVoucherCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        myVoucherCollectionView.delegate = self
        myVoucherCollectionView.dataSource = self
        myVoucherCollectionView.register(UINib(nibName: "VoucherCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "voucherCell")
        
        /* Setup Prize CollectionView */
        prizeCollectionView.collectionViewLayout = colViewLayout
        prizeCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        prizeCollectionView.delegate = self
        prizeCollectionView.dataSource = self
        prizeCollectionView.register(UINib(nibName: "PrizeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "prizeCell")
        
        let voucherViewLayout = UICollectionViewFlowLayout()
        voucherViewLayout.scrollDirection = .horizontal
        voucherViewLayout.minimumInteritemSpacing = 16
        /* Setup Voucher CollectionView */
        voucherCollectionView.collectionViewLayout = voucherViewLayout
        voucherCollectionView.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        voucherCollectionView.delegate = self
        voucherCollectionView.dataSource = self
        voucherCollectionView.register(UINib(nibName: "VoucherCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "voucherCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case prizeCollectionView:
            return CGSize(width: 175, height: 229)
        case myVoucherCollectionView:
            return CGSize(width: 300, height: 174   )
        default:
            return CGSize(width: 300, height: 188)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case prizeCollectionView:
            if isRewardLoaded{
                 return rewardList.count
            }else{
                return ShimmerConstant.TOTAL_CELL
            }
        case myVoucherCollectionView:
            if isMyVoucherLoaded{
                return myVoucherList.count
            }else{
                return ShimmerConstant.TOTAL_CELL
            }
        default:
            if isVoucherLoaded{
                 return voucherList.count
            }else{
                return ShimmerConstant.TOTAL_CELL
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case prizeCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "prizeCell", for: indexPath) as! PrizeCollectionViewCell
            if rewardList.count != 0{
                let reward = rewardList[indexPath.item]
                /* Setup Reward ImageView */
                if let photoUrl = reward.photoUrl{
                    if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                        let filter = AspectScaledToFillSizeFilter(size: cell.prizeImageView.frame.size)
                        cell.prizeImageView.af.setImage(withURL: url, filter: filter)
                    }
                }
                cell.turnOffShimmer()
                cell.nameLabel.text = reward.name
                cell.pointsLabel.text = "\(reward.point ?? "0") pts"
            }
            return cell
        case myVoucherCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "voucherCell", for: indexPath) as! VoucherCollectionViewCell
            /* Setup Voucher ImageView */
            if myVoucherList.count != 0{
                let voucher = myVoucherList[indexPath.item]
                if let photoUrl = voucher.photoUrl{
                    if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                        let filter = AspectScaledToFillSizeFilter(size: cell.voucherImageView.frame.size)
                        cell.voucherImageView.af.setImage(withURL: url, filter: filter)
                    }
                }
                cell.turnOffShimmer()
                cell.nameLabel.text = voucher.name
                cell.pointsLabel.isHidden = true
            }
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "voucherCell", for: indexPath) as! VoucherCollectionViewCell
            
            if voucherList.count != 0{
                let voucher = voucherList[indexPath.item]
                /* Setup Voucher ImageView */
                if let photoUrl = voucher.photoUrl{
                    if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                        let filter = AspectScaledToFillSizeFilter(size: cell.voucherImageView.frame.size)
                        cell.voucherImageView.af.setImage(withURL: url, filter: filter)
                    }
                }
                cell.turnOffShimmer()
                cell.nameLabel.text = voucher.name
                cell.pointsLabel.text = "\(voucher.redeemPoint) POIN"
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case prizeCollectionView:
            if self.isRewardLoaded{
                let prizeDetailVC = PrizeDetailViewController()
                prizeDetailVC.prize = rewardList[indexPath.item]
                navigationController?.pushViewController(prizeDetailVC, animated: true)
            }
        case myVoucherCollectionView:
            if self.isMyVoucherLoaded{
                let voucherDetailVC = VoucherDetailViewController()
                voucherDetailVC.voucher = myVoucherList[indexPath.item]
                navigationController?.pushViewController(voucherDetailVC, animated: true)
            }
        default:
            if self.isVoucherLoaded{
                let voucherDetailVC = VoucherDetailViewController()
                voucherDetailVC.voucher = voucherList[indexPath.item]
                navigationController?.pushViewController(voucherDetailVC, animated: true)
            }
        }
    }
    
    @IBAction func myVoucherrReadMoreAction(_ sender: Any) {
        let voucherListVC = VoucherListViewController()
        voucherListVC.voucherType = .MY_VOUCHER
        navigationController?.pushViewController(voucherListVC, animated: true)
    }
    
    @IBAction func prizeReadMoreAction(_ sender: Any) {
        let prizeListVC = PrizeListViewController()
        prizeListVC.prizeList = self.rewardList
        navigationController?.pushViewController(prizeListVC, animated: true)
    }
    
    @IBAction func voucherReadMoreAction(_ sender: Any) {
        let voucherListVC = VoucherListViewController()
        voucherListVC.voucherType = .AVAILABLE_VOUCHER
        navigationController?.pushViewController(voucherListVC, animated: true)
    }
}
