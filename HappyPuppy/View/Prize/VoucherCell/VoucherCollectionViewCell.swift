//
//  VoucherCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class VoucherCollectionViewCell: UICollectionViewCell {

    @IBOutlet var containerView: UIView!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    @IBOutlet var voucherImageView: UIImageView!
    
    @IBOutlet var imageShimmerView: FBShimmeringView!
    @IBOutlet var nameShimmerView: FBShimmeringView!
    @IBOutlet var pointShimmerView: FBShimmeringView!
    
    @IBOutlet var imageLoading: UIView!
    @IBOutlet var nameLoading: UIView!
    @IBOutlet var pointLoading: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         setupViews()
    }
    
    private func setupViews(){
        voucherImageView.layer.cornerRadius = 10
        imageShimmerView.isShimmering = true
        nameShimmerView.isShimmering = true
        pointShimmerView.isShimmering = true
        
        imageShimmerView.contentView = imageLoading
        nameShimmerView.contentView = nameLoading
        pointShimmerView.contentView = pointLoading
    }
    
    func turnOffShimmer(){
        containerView.backgroundColor = .white
        
        imageShimmerView.isHidden = true
        imageShimmerView.isShimmering = false
        
        nameShimmerView.isHidden = true
        nameShimmerView.isShimmering = false
        
        pointShimmerView.isHidden = true
        pointShimmerView.isShimmering = false
    }

}
