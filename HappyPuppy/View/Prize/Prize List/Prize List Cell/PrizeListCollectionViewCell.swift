//
//  PrizeListCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PrizeListCollectionViewCell: UICollectionViewCell {

    @IBOutlet var shadowView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var prizeImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
        
        shadowView.layer.shadowColor  = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.25
        shadowView.layer.shadowOffset = .zero
        shadowView.layer.shadowRadius = 2
        
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true

    }

}
