//
//  PrizeListViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import AlamofireImage

class PrizeListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var prizeCollectionView: UICollectionView!
    
    var prizeList:[Reward] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        self.title = "Daftar Hadiah"
        
        /* Setup Prize Collection View */
        prizeCollectionView.delegate = self
        prizeCollectionView.dataSource = self
        prizeCollectionView.register(UINib(nibName: "PrizeListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PrizeListCell")
        prizeCollectionView.contentInset = UIEdgeInsets(top: 32, left: 24, bottom: 24, right: 24)
        /*-----*/
    }
    
    /* Collection View Handler */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((view.frame.width/2)-30), height: ((view.frame.width/2)-30)*1.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prizeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let prize = prizeList[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PrizeListCell", for: indexPath) as! PrizeListCollectionViewCell
        cell.nameLabel.text = prize.name
        
        /* Setup Prize PointLabel */
        if let point = prize.point{
            cell.pointLabel.text = "\(point) pts"
        }
        /* Setup Reward ImageView */
        if let photoUrl = prize.photoUrl{
            if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                let filter = AspectScaledToFillSizeFilter(size: cell.prizeImageView.frame.size)
                cell.prizeImageView.af.setImage(withURL: url, filter: filter)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let prizeDetailVC = PrizeDetailViewController()
        prizeDetailVC.prize = prizeList[indexPath.item]
        navigationController?.pushViewController(prizeDetailVC, animated: true)
    }
    /*-----*/
}
