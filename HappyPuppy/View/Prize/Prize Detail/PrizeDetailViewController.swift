//
//  PrizeDetailViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 23/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class PrizeDetailViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /* UILabel */
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    
    /* UIImageView */
    @IBOutlet var prizeImageView: UIImageView!
    
    /* UIView */
    @IBOutlet var cityTextFieldContainer: UIView!
    @IBOutlet var outletTextFieldContainer: UIView!
    
    /* UITextField */
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var outletTextField: UITextField!
    
    /* UIButton */
    @IBOutlet var exchangeButton: UIButton!
    
    /* UIActivityIndicator */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var redeemActivityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    let pickerView = UIPickerView()
    
    var prize:Reward?
    //    var prizeImage:UIImage?
    
    var cityList:[City] = []
    var outletList:[AvailableOutlet] = []
    var selectedCity:City?
    var selectedOutlet:AvailableOutlet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
    }
    
    private func setupViews(){
        
        setupNavigationController()
        hideKeyboardWhenTappedAround()
        
        // Setup ImageView
        prizeImageView.layer.cornerRadius = 10
        let filter = AspectScaledToFillSizeFilter(size: prizeImageView.frame.size)
        if let prize = self.prize{
            if let photoUrl = prize.photoUrl{
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)"){
                    prizeImageView.af.setImage(withURL: url, filter: filter)
                }
            }
            
            /* Setup PointLabel */
            nameLabel.text = prize.name
            
            /* Setup PointLabel */
            pointLabel.text = String(format: "%@ pts", arguments: [prize.point!])
        }
 
        cityTextFieldContainer.layer.borderWidth = 1
        cityTextFieldContainer.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        
        outletTextFieldContainer.layer.borderWidth = 1
        outletTextFieldContainer.layer.borderColor = #colorLiteral(red: 0.9022639394, green: 0.9022851586, blue: 0.9022737145, alpha: 1)
        
        outletTextField.delegate = self
        cityTextField.delegate = self
        
        setupPickerViews()
    }
    
    private func setupData(){
        self.activityIndicator.isHidden = false

        if let selectedCity = self.selectedCity{
            self.outletTextField.isEnabled = false
            self.exchangeButton.isEnabled = false

            if let prize = prize{
                if let prizeCode = prize.code{
                    RewardController.shared.getAvailableOutletList(prizeCode: prizeCode, city: selectedCity.name!) { (status, outletList, error) in
                        self.activityIndicator.isHidden = true
                        
                        if let outletList = outletList{
                            if outletList.count != 0{
                                self.outletList = outletList
                                
                                self.exchangeButton.isEnabled = true
                                self.exchangeButton.backgroundColor = #colorLiteral(red: 0.9299855828, green: 0.4350600839, blue: 0.5511230826, alpha: 1)
                                self.exchangeButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                                self.outletTextField.isEnabled = true
                                self.outletTextField.text = outletList[0].name
                                self.selectedOutlet = outletList[0]
                            }else{
                                self.exchangeButton.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
                                self.exchangeButton.setTitleColor(#colorLiteral(red: 0.6588235294, green: 0.6588235294, blue: 0.6588235294, alpha: 1), for: .normal)
                                self.outletTextField.text = "Hadiah tidak tersedia"
                            }
                            
                        }else{
                            if let error = error{
                                print(error)
                            }
                        }
                    }
                }
            }
        }else{
            cityTextField.isEnabled = false
            
            UtilsController.shared.getCityList { (status, cityList, error) in
                self.activityIndicator.isHidden = true
                
                if let cityList = cityList{
                    self.cityList = cityList
                    self.cityTextField.isEnabled = true
                    let firstIndex = cityList.firstIndex(where: { ($0.name?.contains("Surabaya"))!})
                    self.cityTextField.text = cityList[firstIndex!].name
                    self.selectedCity = cityList[firstIndex!]
                    self.setupData()
                }else{
                    if let error = error{
                        print(error)
                    }
                }
            }
        }
        
    }
    
    /* Setup Navigation Controller */
    private func setupNavigationController(){
        self.title = "Detail Hadiah"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }
    
    @objc private func refresh(){
        self.selectedCity = nil
        self.selectedOutlet = nil
        
        self.cityTextField.text = ""
        self.outletTextField.text = ""
        
        setupData()
    }
    /*-----*/
    
    /* UITextField Handler */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
        if textField == cityTextField{
            pickerView.selectRow(cityList.firstIndex(where: {$0.name == selectedCity?.name})!, inComponent: 0, animated: false)
        }else{
            pickerView.selectRow(outletList.firstIndex(where: {$0.name == selectedOutlet?.name})!, inComponent: 0, animated: false)
        }
    }
    /*-----*/
    
    /* UIPickerView Handler */
    private func setupPickerViews(){
        pickerView.delegate = self
        pickerView.dataSource = self
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pickerDone))
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.items = [spaceButton, btnDone]
        
        cityTextField.inputView = pickerView
        cityTextField.inputAccessoryView = toolbar
        
        outletTextField.inputView = pickerView
        outletTextField.inputAccessoryView = toolbar
    }
    
    @objc private func pickerDone(){
        dismissKeyboard()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if cityTextField.isEditing{
            return cityList.count
        }else{
            return outletList.count
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if cityTextField.isEditing{
            return cityList[row].name
        }else{
            return outletList[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if cityTextField.isFirstResponder{
            cityTextField.text = cityList[row].name
            self.selectedCity = cityList[row]
            setupData()
        }else if (outletTextField.isFirstResponder){
            if outletList.count != 0{
                print(outletList.count)
                outletTextField.text = outletList[row].name
                self.selectedOutlet = outletList[row]
            }
        }
    }
    /*-----*/
    
    /* UIButton Handler */
    @IBAction func redeemAction(_ sender: Any) {
        if let prize = self.prize{
            if let selectedOutlet = self.selectedOutlet{
                exchangeButton.setTitle("", for: .normal)
                redeemActivityIndicator.isHidden = false
                RewardController.shared.createRedeem(rewardCode: prize.code, outlet: selectedOutlet.outletCode) { (status, error) in
                    if status == NetworkStatus.SUCCESS{
                        ProfileController.shared.getProfileDetail { (status, user, error) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        self.exchangeButton.setTitle("TUKAR HADIAH", for: .normal)
                        self.redeemActivityIndicator.isHidden = true
                        
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }
                }
            }
        }
        
        
        
    }
    /*-----*/
}
