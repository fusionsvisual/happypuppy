//
//  PrizeCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PrizeCollectionViewCell: UICollectionViewCell {

    /* ShimmerView */
    @IBOutlet var imageShimmeringView: FBShimmeringView!
    @IBOutlet var titleLoadingShimmerView: FBShimmeringView!
    @IBOutlet var pointShimmerView: FBShimmeringView!
    
    @IBOutlet var imageLoading: UIView!
    @IBOutlet var titleLoading: UIView!
    @IBOutlet var pointLoading: UIView!
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var prizeImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews(){
        containerView.backgroundColor = .clear
        imageShimmeringView.isShimmering = true
        titleLoadingShimmerView.isShimmering = true
        pointShimmerView.isShimmering = true
        
        imageShimmeringView.contentView = imageLoading
        titleLoadingShimmerView.contentView = titleLoading
        pointShimmerView.contentView = pointLoading
        
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
    }
    
    func turnOffShimmer(){
        containerView.backgroundColor = .white
        
        imageShimmeringView.isHidden = true
        imageShimmeringView.isShimmering = false
        
        titleLoadingShimmerView.isHidden = true
        titleLoadingShimmerView.isShimmering = false
        
        pointShimmerView.isHidden = true
        titleLoadingShimmerView.isShimmering = false
    }
}
