//
//  NotificationsViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var notifTableView: UITableView!
    
    private var isDataLoaded: Bool = false
    
    let spinner = UIActivityIndicatorView(style: .gray)
    var notifList:[Notification] = []
    var totalValue = 0
    var length = 15
    var start = 0
    
    /* Change status bar color */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate() 
        self.notifTableView.delegate = self
        self.notifTableView.dataSource = self
        self.notifTableView.register(UINib(nibName: "ShimmerNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        self.notifTableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "notifCell")
        
        self.setupViews()
        self.setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.notifTableView.reloadData()
    }
    
    private func setupViews(){
        self.title = "Notifikasi"
        setupNavigationController()
    }
    
    private func setupData(){                
        NotificationController.shared.getNotifList(length: self.length, start: self.start) { (status, notifList, totalValue, error) in
            
            self.isDataLoaded = true
            
            self.totalValue = totalValue
                        
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }else{
                if let notifList = notifList{
                    if self.notifList.count == 0{
                        self.notifList = notifList
                        self.notifTableView.isHidden = false
                    }else{
                        self.spinner.stopAnimating()
                        self.notifTableView.tableFooterView?.isHidden = true
                        self.notifList.append(contentsOf: notifList)
                    }
                    self.notifTableView.reloadData()
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isDataLoaded{
            return notifList.count
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isDataLoaded{
            // MARK: Set status as read
            self.notifList[indexPath.item].status = "1"
            
            let notificationDetailVC = NotificationDetailViewController()
            notificationDetailVC.notification = self.notifList[indexPath.item]
            navigationController?.pushViewController(notificationDetailVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isDataLoaded{
            let cell = tableView.dequeueReusableCell(withIdentifier: "notifCell") as! NotificationTableViewCell
            let notif = notifList[indexPath.item]
            cell.titleLabel.text = notif.title
            cell.bodyLabel.text = notif.description
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM yyyy"

            let createdDate: Date = dateFormatterGet.date(from: notif.createdDate)!
            cell.dateLabel.text = dateFormatterPrint.string(from: createdDate)
            
            // Setup unreadIcon
            if notif.status == "1"{
                cell.setRead()
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.isDataLoaded{
            let offsetY = scrollView.contentOffset.y
            let contentHeight = self.notifTableView.contentSize.height
            
            if contentHeight > 0, offsetY >= (contentHeight - self.notifTableView.frame.size.height){
                if (self.start + self.length) < self.totalValue{
                    self.start += self.length
                    
                    self.spinner.startAnimating()
                    self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: notifTableView.bounds.width, height: CGFloat(35))
                    self.notifTableView.tableFooterView = spinner
                    self.notifTableView.tableFooterView?.isHidden = false
                    
                    self.setupData()
                }
            }
        }
    }
    
    @objc private func refresh(){
        self.isDataLoaded = false
        
        self.totalValue = 0
        self.length = 15
        self.start = 0
                
        self.notifList.removeAll()
        self.notifTableView.reloadData()
        
        self.setupData()
    }
}
