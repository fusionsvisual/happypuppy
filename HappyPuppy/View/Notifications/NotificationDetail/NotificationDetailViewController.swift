//
//  NotificationDetailViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 31/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class NotificationDetailViewController: UIViewController {

    /* UILabel */
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    /*-----*/
    
    /* UIButton */
    @IBOutlet var notifButton: UIButton!
    /*-----*/
    
    /* UIActivityIndicatorView */
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /*-----*/
    
    /* Variable */
    var notification:Notification?
    /*-----*/
    
    
    
    var linkName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        self.hideKeyboardWhenTappedAround()
        self.setupNavigationController()

        if let notification = notification{
            self.titleLabel.text = notification.title
            self.dateLabel.text = Formatter.shared.birthDateFormatter(getDateFormat: "yyyy-MM-dd HH:mm:ss", printDateFormat: "dd MMMM yyyy", dateStr: notification.createdDate)
            self.descriptionLabel.text = notification.description
                        
            if let linkName = notification.linkName{
                self.linkName = linkName
                notifButton.isHidden = false
                notifButton.layer.cornerRadius = 8
                notifButton.setTitle(linkName, for: .normal)
            }else{
                notifButton.isHidden = true
            }
            
            NotificationController.shared.updateNotifToRead(notifId: notification.id) { (status, error) in
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }else{
                    // MARK: Setup Unread Count Locally
                    UserDefaultHelper.shared.updateUnreadCount()
                    // END
                }
            }
        }
    }
    
    private func setupNavigationController(){
        self.title = "Notifikasi"
    }

    private func navigateToBottleVerification(bottle: Bottle) {
        self.navigationController?.pushViewController(
            BottleVerificationViewController(
                bottle: bottle
            ),
            animated: true
        )
    }

    private func enableButton() {
        activityIndicator.isHidden = true
        notifButton.isEnabled = true
        notifButton.setTitleColor(.white, for: .normal)
    }

    @IBAction func notifAction(_ sender: Any) {
        switch self.linkName {
            case NotificationURLConstant.ACCOUNT_VERIFICATION:
                navigationController?.pushViewController(AccountVerificationViewController(), animated: true)
            /*
                Reservation Detail for Payment
            */
            case NotificationURLConstant.PAYMENT:
                let reservationDetailVC = MyReservationDetailViewController()
                guard let notification = notification else {return}
                if let range = notification.url!.range(of: "/") {
                    let reservationId = notification.url![range.upperBound...]
                    reservationDetailVC.reservationId = String(reservationId)
                }
                self.navigationController?.pushViewController(reservationDetailVC, animated: true)
            default:
                if let notification = notification{
                    self.activityIndicator.isHidden = false
                    self.notifButton.isEnabled = false
                    self.notifButton.setTitleColor(#colorLiteral(red: 0.2686035633, green: 0.4428007901, blue: 0.7515776753, alpha: 1), for: .normal)
                    if let actionUrl = notification.url,
                       let requestId = actionUrl.split(separator: "/").last,
                       actionUrl.contains("takeOTP") {
                        BottleController.shared.requestTakeItemOtp(
                            requestId: Int(requestId) ?? 0,
                            onSuccess: { [weak self] response in
                                self?.enableButton()
                                self?.navigateToBottleVerification(
                                    bottle: response.data
                                )
                            },
                            onFailed: { [weak self] errorMessage in
                                self?.enableButton()
                                SVProgressHUD.showError(withStatus: errorMessage)
                                SVProgressHUD.dismiss(withDelay: 0.5)
                            }
                        )
                        return
                    }
                    if let notifUrl = notification.url{
                        let bottleId = notifUrl.replacingOccurrences(of: "https://club.happypuppy.id/keepingbottle/saveOTP/", with: "")
                        print(bottleId)
                        BottleController.shared.requestSaveItemOtp(id: bottleId) { (status, bottle,error)  in
                            self.activityIndicator.isHidden = true
                            self.notifButton.isEnabled = true
                            self.notifButton.setTitleColor(.white, for: .normal)
                            
                            if let error = error {
                                SVProgressHUD.showError(withStatus: error)
                                SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                            }else{
                                let bottleVerificationVC = BottleVerificationViewController()
                                bottleVerificationVC.bottle = bottle
                                self.navigationController?.pushViewController(bottleVerificationVC, animated: true)
                            }
                        }
                    }
                }
        }
    }
}
