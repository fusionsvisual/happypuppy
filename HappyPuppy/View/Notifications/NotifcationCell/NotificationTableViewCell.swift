//
//  NotificationTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var unreadMarkLabel: UILabel!
    
    var status = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRead(){
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        bodyLabel.font = UIFont.systemFont(ofSize: 14)
        dateLabel.font = UIFont.systemFont(ofSize: 14)
        unreadMarkLabel.isHidden = true
    }
    
    
    override func prepareForReuse() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        bodyLabel.font = UIFont.boldSystemFont(ofSize: 14)
        dateLabel.font = UIFont.boldSystemFont(ofSize: 14)

        unreadMarkLabel.isHidden = false
    }
}
