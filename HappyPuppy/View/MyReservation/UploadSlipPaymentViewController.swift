//
//  UploadSlipPaymentViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 28/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class UploadSlipPaymentViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var slipImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var downPayment: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var senderName: UITextField!
    @IBOutlet weak var senderBank: UITextField!
    
    var imagePicker = UIImagePickerController()
    let dateFormatter = DateFormatter()
    
    var reservation: Reservation?
    var outletCode: String? = ""
    var reservationId: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setupView()
        setupData()
    }

    func setupView() {
        self.title = "Upload Bukti Pembayaran"
    }

    func setupData() {
        ReservationController.shared.getSingleReservationDetail(reservation_id: reservationId) { (data, error) in
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }else{
                guard let data = data else{return}
                self.reservation = data
                self.name.text = data.name
                if let downPayment = data.downPayment {
                    self.downPayment.text = "Rp. \(Formatter.shared.priceFormatter(downPayment))"
                }
                self.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let toDate = self.dateFormatter.date(from: data.transactionDate!)
                self.dateFormatter.dateFormat = "dd LLLL yyyy HH:mm"
                self.date.text = self.dateFormatter.string(from: toDate!)
            }
        }
    }
    
    @IBAction func uploadButtonTapped(_ sender: Any) {
        self.showAlert()
    }
    
    @IBAction func uploadTapped(_ sender: Any) {
        SVProgressHUD.show(withStatus: "Sedang mengunggah bukti pembayaran")
        if senderBank.text == "" && senderName.text == ""{
            SVProgressHUD.showError(withStatus: "Please fill the blanks.")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }else{
            ReservationController.shared.uploadReservationPayment(outletCode: outletCode!, reservationCode: reservationId!, uploadFile: slipImage!.image!, senderBank: senderBank.text!, senderName: senderName.text!) { (result, message, error) in
                if let error = error {
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                } else {
                    SVProgressHUD.dismiss()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
            self?.slipImage.image = image
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
