//
//  ReservationViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 25/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class ReservationViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var reservationAddress: UILabel!
    @IBOutlet var totalPriceL: UILabel!
    @IBOutlet var minimumChargeL: UILabel!
    
    @IBOutlet weak var reservationImage: UIImageView!
    @IBOutlet weak var facilityCollectionView: UICollectionView!
    @IBOutlet weak var roomTypeTF: UITextField! {
        didSet {
            roomTypeTF.tintColor = UIColor.lightGray
            roomTypeTF.setIcon(#imageLiteral(resourceName: "DownIcon"))
        }
    }
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var durationTextField: UITextField! {
        didSet {
            durationTextField.tintColor = UIColor.lightGray
            durationTextField.setIcon(#imageLiteral(resourceName: "DownIcon"))
        }
    }
    @IBOutlet var noteTextV: UITextView!
    
    @IBOutlet var priceSummaryTV: UITableView!
    @IBOutlet var priceSummaryTVHeightC: NSLayoutConstraint!
    
    @IBOutlet var durationBtn: UIButton!
    
    @IBOutlet var durationChevIV: UIImageView!
    
    //* Views inside stack view for pricing summary
    @IBOutlet var priceSummaryV: UIView!
    @IBOutlet var minimumPriceV: UIView!
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let roomTypePickerView = UIPickerView()
    let durationPickerView = UIPickerView()
    let toolBar = UIToolbar()
    
    var selectedTextFieldForPicker:UITextField?
    
    var itemSelected = ""
//    var outletName: String? = ""
    let roomType: String? = ""
    let duration = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    
//    var outletCode: String? = ""
    var outlet: Outlet?
    var roomTypeByOutletList: [RoomTypeByOutlet] = []
    var facilities: [Facility]? = []
    var roomPrices: [RoomPrice] = []
    
    var roomTypeID = ""
    
    private var selectedRoomType:RoomTypeByOutlet?
    private var selectedDate:String?
    private var selectedTime:String?
    private var selectedDuration:String?
    
    private var dateRange:Int?

    init(outlet:Outlet?) {
        self.outlet = outlet
        
        super.init(nibName: nil, bundle: nil)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
        setupCollectionView()
        setupView()
        setupRoomType()
        setupReservationDate()
        setupReservationTime()
        setupDurationPicker()
        
        hideKeyboardWhenTappedAround()
    }
    
    private func setupCollectionView(){
        self.facilityCollectionView.delegate = self
        self.facilityCollectionView.dataSource = self
        
        let facilityNibCell = UINib(nibName: "FacilityCollectionViewCell", bundle: nil)
        facilityCollectionView.register(facilityNibCell, forCellWithReuseIdentifier: "facilityCollectionViewCell")
    }
    
    @IBAction func roomReservationTapped(_ sender: Any) {
        if dateTextField.text != "" || timeTextField.text != "" {
            SVProgressHUD.show()
            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let formattedDate = format.string(from: date)
            let reservationDate = Global.shared.fromddLLLLyyyyToyyyyMMdd(dateTextField.text!)
            
            var duration = self.durationTextField.text!
            
            if self.outlet!.reservationPaymentType{
                //* Set duration to 1 if reservationPaymentType is "true"
                duration = "1"
            }
            
            ReservationController.shared.createReservation(outletId: self.outlet!.code!, roomType: roomTypeID, reservationDate: reservationDate, reservationTime: timeTextField.text!, duration: duration, currentDate: formattedDate, description: self.noteTextV.text!) { (result, message, data, error) in
                if result == "success" {
                    if let reservationId = data{
                        ReservationController.shared.getSingleReservationDetail(reservation_id: reservationId) { (detail, error) in
                            SVProgressHUD.dismiss()
                            if let response = detail{
                                let vc = MyReservationDetailViewController()
                                var reservation = Reservation()
                                reservation.status = response.status
                                reservation.transactionDate = response.transactionDate
                                
                                vc.reservationId = reservationId
                                vc.reservation = reservation
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }else{
                        SVProgressHUD.showInfo(withStatus: "Reservation ID is missing")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }
                } else if result == "failed"{
                    SVProgressHUD.showInfo(withStatus: message!)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                } else {
                    SVProgressHUD.showError(withStatus: message!)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }
            }
        } else {
            SVProgressHUD.showInfo(withStatus: "Ada form yang masih kosong!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    func setupView() {
        self.title = "Reservasi"
        
        self.roomTypeTF.tintColor = .clear
        
        durationTextField.text = duration[0]
        self.durationTextField.tintColor = .clear
        
        reservationImage.clipsToBounds = true
        reservationImage.layer.cornerRadius = 10
        
        self.priceSummaryTVHeightC.constant = 0
        
        self.priceSummaryTV.register(UINib(nibName: "PriceSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "SummaryCell")
        
        if (self.outlet!.reservationPaymentType){
            //* Select default date to today if reservationPaymentType == true
            self.datePicker.setDate(Date(), animated: true)
        }else{
            //* Select default date to tommorow if reservationPaymentType == false
            self.datePicker.setDate(self.oneDayfromNow, animated: true)
        }
        
        self.dateTextField.tintColor = .clear
        self.dateTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.didDateSelected(_:)))
        
        self.timeTextField.tintColor = .clear
        self.timeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.didTimeSelected(_:)))
        
        self.noteTextV.layer.cornerRadius = 4
        self.noteTextV.layer.borderWidth = 1
        self.noteTextV.layer.borderColor = UIColor(red: 228/255, green: 228/255, blue: 228/255, alpha: 1).cgColor
        self.noteTextV.textContainerInset = UIEdgeInsets(top: 8, left: 4, bottom: 8, right: 4)
        
        //* Hide minimumPriceV
        self.minimumPriceV.isHidden = true
        
        //* Handle reservationPaymentType is "true"
        guard let outlet = self.outlet else {return}
        if (outlet.reservationPaymentType){
            guard let roomType = outlet.roomType, let firstRoomType = roomType.first else {
                SVProgressHUD.showError(withStatus: "No room type found")
                SVProgressHUD.dismiss()
                
                self.navigationController?.popViewController(animated: true)
                return
            }
            
            //* Inactive durationTextField
            self.durationTextField.text = String(firstRoomType.minimumHour)
            self.durationTextField.isEnabled = false
            self.durationChevIV.isHidden = true
            
            //* Hide priceSummaryV
            self.priceSummaryV.isHidden = true
            
            //* Show minimumPriceV
            self.minimumPriceV.isHidden = false
        }
    }
    
    func setupData() {
        ReservationController.shared.getRoomType(outlet: self.outlet?.code) { (roomTypeList, String) in
            guard let roomTypeList = roomTypeList else{return}
            self.roomTypeByOutletList = roomTypeList
            
            self.roomTypeID = "\(self.roomTypeByOutletList[0].id!)"
            self.facilities = roomTypeList[0].facilities
            self.facilityCollectionView.reloadData()
            
            self.reservationAddress.text = self.outlet?.name
            if let photoUrl = self.roomTypeByOutletList[0].photo {
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    self.reservationImage.af.setImage(withURL: url)
                }
            }
            
            self.selectedRoomType = self.roomTypeByOutletList.first
            self.selectedDuration = self.duration.first
            
            self.roomTypeTF.text = "\(self.roomTypeByOutletList[0].roomType!)"
            
            ReservationController.shared.getDateRange { (dateRange, error) in
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }else{
                    let calendar = Calendar.current
                    
                    if let maximumDate = calendar.date(byAdding: .day, value: dateRange!, to: Date()){
                        self.datePicker.maximumDate = maximumDate
                    }
                }
            }
        }
    }
    
    private func getRoomPriceData(){
        if let outlet = self.outlet, let roomType = self.selectedRoomType, let selectedDate = self.selectedDate, let selectedTime = self.selectedTime, let selectedDuration = self.selectedDuration{
            
            SVProgressHUD.show()
            
            ReservationController.shared.getEstimatedPrices(outletCode: outlet.code!, roomTypeId: roomType.id!, date: selectedDate, time: selectedTime, duration: selectedDuration) { (roomPrices, error) in
                
                SVProgressHUD.dismiss()
                
                if let error = error{
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }else{
                    self.roomPrices = roomPrices
                    self.priceSummaryTVHeightC.constant = CGFloat(self.roomPrices.count * (17 + 16))
                    
                    var totalPrice = 0
                    for roomPrice in self.roomPrices{
                        totalPrice+=roomPrice.totalPrice
                    }
                    
                    self.totalPriceL.text = "Rp." + Formatter.shared.priceFormatter(totalPrice)
                    self.priceSummaryTV.reloadData()
                }
            }
        }
    }
    
    func setupRoomType() {
        self.roomTypeTF.inputView = self.roomTypePickerView
        self.roomTypePickerView.delegate = self
    }
    
    func setupReservationDate() {
        addDatePicker(dateTextField, mode: .date)
        dateTextField.delegate = self
    }
    
    var oneDayfromNow: Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current
        return calendar.date(byAdding: .day, value: 1, to: Date())!
    }
    
    func addDatePicker(_ textField:UITextField, mode: UIDatePicker.Mode) {
        // Setup inputView
        datePicker.datePickerMode = mode
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
        
        if (self.outlet!.reservationPaymentType){
            //* Maximum Booking is 3 hours before if reservationPaymentType == true
            self.datePicker.minimumDate = Date()
        }else{
            //* Maximum Booking is 24 hours before if reservationPaymentType == false
            self.datePicker.minimumDate = oneDayfromNow
        }
        
        self.datePicker.addTarget(self, action: #selector(self.datePickerAction(_:)), for: .valueChanged)
        
        textField.inputView = datePicker
    }
    
    func setupReservationTime() {
        addTimePicker(timeTextField, mode: .time)
        timeTextField.delegate = self
    }
    
    private func addTimePicker(_ textField:UITextField, mode: UIDatePicker.Mode){
        // Setup inputView
        timePicker.datePickerMode = mode
        if #available(iOS 13.4, *) {
            self.timePicker.preferredDatePickerStyle = .wheels
        }
        timePicker.minuteInterval = 15
        self.timePicker.date = self.convertTimeByInterval(timeInterval: 15, date: Date())

        self.timeTextField.inputView = self.timePicker
        
        if (self.outlet!.reservationPaymentType){
            //* Decrement current datetime to 3 hour before if reservationPaymentType == true
            var caledar = Calendar.current
            caledar.timeZone = TimeZone.current
            let minimumDate = caledar.date(byAdding: .hour, value: -3, to: self.convertTimeByInterval(timeInterval: 15, date: Date()))
            
            //* Set minimum booking time to 3 hour before if reservationPaymentType == true
            self.timePicker.minimumDate = minimumDate
        }
    }
    
    func setupDurationPicker() {
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton,doneButton], animated: false)

        durationTextField.inputAccessoryView = toolBar
        durationTextField.inputView = durationPickerView
        durationPickerView.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        roomTypePickerView.reloadAllComponents()
        durationPickerView.reloadAllComponents()
        selectedTextFieldForPicker = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.roomTypeTF{
            let selectedRow = self.roomTypePickerView.selectedRow(inComponent: 0)
            let selected = roomTypeByOutletList[selectedRow].roomType
            self.roomTypeTF.text = "\(selected!)"
            self.roomTypeID = String(roomTypeByOutletList[selectedRow].id!)
            if let photoUrl = roomTypeByOutletList[selectedRow].photo {
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    reservationImage.af.setImage(withURL: url)
                }
            }
            self.facilities = self.roomTypeByOutletList[selectedRow].facilities
            self.facilityCollectionView.reloadData()

            self.selectedRoomType = self.roomTypeByOutletList[selectedRow]
        }else if textField == self.timeTextField{
            let formatter = DateFormatter()
            guard let selectedTextFieldForPicker = selectedTextFieldForPicker else{return}
            formatter.dateFormat = "HH:mm"
            selectedTextFieldForPicker.text = formatter.string(from: timePicker.date)
            self.selectedTime = DateFormatterHelper.shared.formatDate(dateFormat: "HH:mm", date: self.timePicker.date, timezone: nil, locale: nil)
            self.getRoomPriceData()
        }else if textField == self.durationTextField{
            let selectedRow = self.durationPickerView.selectedRow(inComponent: 0)
            self.durationTextField.text = self.duration[selectedRow]
            self.selectedDuration = self.duration[selectedRow]
        }
        
        if self.outlet!.reservationPaymentType{
            //* Handle if reservation payment type is "true"
            guard let selectedRoomType = self.selectedRoomType else {return}
            self.durationTextField.text = String(selectedRoomType.minimumHour)
            self.minimumChargeL.text = String(format: "Harga: Rp. %@", Formatter.shared.priceFormatter(selectedRoomType.minimumCharge))
        }else{
            self.getRoomPriceData()
        }
    }
    
    func convertTimeByInterval(timeInterval: Int, date: Date)->Date{
        // MARK: Setup selected time based on interval
        var calendar = Calendar.current
        calendar.timeZone = .current
        let timeInterval = timeInterval
        let nextDiff = timeInterval - calendar.component(.minute, from: date) % timeInterval
        return calendar.date(byAdding: .minute, value: nextDiff, to: date) ?? Date()
    }
    
    @IBAction func selectRoomTypeAction(_ sender: Any) {
        self.roomTypeTF.becomeFirstResponder()
    }
    
    @IBAction func selectDurationAction(_ sender: Any) {
        self.durationTextField.becomeFirstResponder()
    }
    
    @objc private func didDateSelected(_ sender: UITextField){
        // MARK: Handle when date datepicker never moved
        if self.selectedDate == nil{
            self.selectedDate = DateFormatterHelper.shared.formatDate(dateFormat: "yyyy-MM-dd", date: self.datePicker.date, timezone: nil, locale: nil)
            self.dateTextField.text = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: self.datePicker.date, timezone: .current, locale: nil)
            self.getRoomPriceData()
        }
    }
    
    @objc private func didTimeSelected(_ sender: UITextField){
        // MARK: Handle when time datepicker never moved
//        if self.selectedTime == nil{
//            let finalDate = self.convertTimeByInterval(timeInterval: 15, date: Date())
//            print("FINAL DATE: \(finalDate)")
//
//            self.selectedTime = DateFormatterHelper.shared.formatDate(dateFormat: "HH:mm", date: finalDate, timezone: nil, locale: nil)
//            self.timeTextField.text = self.selectedTime
//            self.getRoomPriceData()
//        }
    }
    
    @objc private func datePickerAction(_ sender:UIDatePicker){
        
        switch sender {
        case self.datePicker:
            let formatter = DateFormatter()
            guard let selectedTextFieldForPicker = selectedTextFieldForPicker else{return}
            
            formatter.dateFormat = "dd LLLL yyyy"
            selectedTextFieldForPicker.text = formatter.string(from: datePicker.date)
            
            self.selectedDate = DateFormatterHelper.shared.formatDate(dateFormat: "yyyy-MM-dd", date: self.datePicker.date, timezone: nil, locale: nil)
            self.getRoomPriceData()
//        case self.timePicker:
//            let formatter = DateFormatter()
//            guard let selectedTextFieldForPicker = selectedTextFieldForPicker else{return}
//
//            formatter.dateFormat = "HH:mm"
//            selectedTextFieldForPicker.text = formatter.string(from: timePicker.date)
//
//            self.selectedTime = DateFormatterHelper.shared.formatDate(dateFormat: "HH:mm", date: self.timePicker.date, timezone: nil, locale: nil)
//
//            self.getRoomPriceData()
        default:
            break
        }
    }

    @objc func cancelPicker(){
        self.view.endEditing(true)
    }
    
    @objc func donePicker(_ sender:Any){
        self.view.endEditing(true)
    }
}

extension ReservationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if roomTypeTF.isFirstResponder {
            return roomTypeByOutletList.count
        } else if durationTextField.isFirstResponder {
            return duration.count
        }
        return 0
    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if roomTypeTF.isFirstResponder {
//            let selected = roomTypeByOutletList[row].roomType
//            roomTypeTF.text = "\(selected!)"
//            self.roomTypeID = String(roomTypeByOutletList[row].id!)
//            if let photoUrl = roomTypeByOutletList[row].photo {
//                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
//                    reservationImage.af.setImage(withURL: url)
//                }
//            }
//            self.facilities = self.roomTypeByOutletList[row].facilities
//            facilityCollectionView.reloadData()
//
//            self.selectedRoomType = self.roomTypeByOutletList[row]
//        } else if durationTextField.isFirstResponder {
//            durationTextField.text = duration[row]
//
//            self.selectedDuration = self.duration[row]
//        }
//
//        if self.outlet!.reservationPaymentType{
//            //* Handle if reservation payment type is "true"
//            guard let selectedRoomType = self.selectedRoomType else {return}
//            self.durationTextField.text = String(selectedRoomType.minimumHour)
//            self.minimumChargeL.text = String(format: "Harga: Rp. %@", Formatter.shared.priceFormatter(selectedRoomType.minimumCharge))
//        }else{
//            self.getRoomPriceData()
//        }
//    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if roomTypeTF.isFirstResponder {
            return "\(roomTypeByOutletList[row].roomType!)"
        } else if durationTextField.isFirstResponder {
            return duration[row]
        }
        return nil
    }
}

extension ReservationViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return facilities!.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "facilityCollectionViewCell", for: indexPath) as! FacilityCollectionViewCell
        
        if let icon = facilities![indexPath.row].type?.icon {
            if let url = URL(string: "https://adm.happypuppy.id/\(icon)") {
                cell.icon.af.setImage(withURL: url)
            }
        }
        
        if let description = facilities![indexPath.row].description, let pax = facilities![indexPath.row].quantity {
            cell.label.text = "\(description) x \(pax)"
        }
        
        return cell
    }
}

extension ReservationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.self.roomPrices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! PriceSummaryTableViewCell
        let startTime =
            DateFormatterHelper.shared.formatString(oldDateFormat: "yyyy-MM-dd HH:mm:ss", newDateFormat: "HH:mm", dateString: self.roomPrices[indexPath.item].startTime, timezone: TimeZone(abbreviation: "UTC"))
        let endTime = DateFormatterHelper.shared.formatString(oldDateFormat: "yyyy-MM-dd HH:mm:ss", newDateFormat: "HH:mm", dateString: self.roomPrices[indexPath.item].endTime, timezone: TimeZone(abbreviation: "UTC"))
        cell.timeL.text = startTime + " - " + endTime
        cell.priceL.text = "Rp." + Formatter.shared.priceFormatter(self.roomPrices[indexPath.item].totalPrice)
        return cell
    }
}
