//
//  UploadPaymentViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

enum PaymentMethod {
    case QRIS, BANK_TRANSFER
}

class UploadPaymentViewController: UIViewController {
    
    
    @IBOutlet var reservationNumberL: UILabel!
    @IBOutlet var customerNameL: UILabel!
    @IBOutlet var paymentDateL: UILabel!
    @IBOutlet var totalPaymentL: UILabel!
    
    @IBOutlet var accountNameV: UIView!
    @IBOutlet var bankNameV: UIView!
    
    @IBOutlet var accountNameTF: UITextField!
    @IBOutlet var bankNameTF: UITextField!
    
    @IBOutlet var paymentReceiptIV: UIImageView!
    
    var paymentMethod: PaymentMethod = .QRIS
    var reservation:Reservation?
    private var receiptImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupDefaultValue()
    }
    
    private func setupViews(){
        switch paymentMethod {
        case .QRIS:
            self.accountNameV.isHidden = true
            self.bankNameV.isHidden = true
            break
        default:
            self.accountNameV.isHidden = false
            self.bankNameV.isHidden = false
            break
        }
    }
    
    private func setupDefaultValue(){
        guard let reservation = self.reservation else {return}
        self.reservationNumberL.text = reservation.reservationId
        self.customerNameL.text = reservation.name
        self.totalPaymentL.text = "Rp. " + Formatter.shared.priceFormatter(reservation.downPayment!)
        self.paymentDateL.text = DateFormatterHelper.shared.getCurrentDate(dateFormat: "dd MMM yy HH:mm", timezone: nil)
    }
    
    private func presentImagePickerController(sourceType: UIImagePickerController.SourceType){
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.sourceType = sourceType
        imagePickerVC.delegate = self
        imagePickerVC.modalPresentationStyle = .fullScreen
        
        self.present(imagePickerVC, animated: true)
    }
    
    private func isValid()->Bool{
        
        if self.paymentMethod == .BANK_TRANSFER{
            if self.bankNameTF.text! == ""{
                return false
            }
            
            if self.accountNameTF.text! == ""{
                return false
            }
        }
        
        return true
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        let alert = UIAlertController(title: "Upload Image", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.presentImagePickerController(sourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.presentImagePickerController(sourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        if self.isValid(){
            if let receiptImage = self.receiptImage{
                if let reservation = self.reservation{
                    SVProgressHUD.show()
                    ReservationController.shared.uploadReservationPayment(outletCode: reservation.outletData!.code!, reservationCode: reservation.reservationId!, uploadFile: receiptImage, senderBank: self.bankNameTF.text, senderName: self.accountNameTF.text) { (result, message, error) in
                        
                        SVProgressHUD.dismiss()
                        
                        if let error = error{
                            SVProgressHUD.showError(withStatus: error)
                            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                        }else{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }else{
                SVProgressHUD.showError(withStatus: "Mohon upload foto")
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }
        }else{
            SVProgressHUD.showError(withStatus: "Please fill the blanks")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
}

extension UploadPaymentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        
//        SVProgressHUD.show()
        self.paymentReceiptIV.image = image
        self.receiptImage = image
//        ProductVM.shared.uploadProductImage(image: image)

        self.view.layoutIfNeeded()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)
    }
}
