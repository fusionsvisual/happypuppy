//
//  MyReservationViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 14/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class MyReservationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let dateFormatter = DateFormatter()
    var count = 0
    weak var timer: Timer?
    var timerArray:[Int] = []
    
    private var isDataLoaded = false
    var reservationList:[Reservation] = []
    var timerList:[Int] = []
    
    /*
     Rerfresh control for tableview pull to refresh
     */
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.tableView?.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupData()
    }
    
    func setupData() {
        ReservationController.shared.getReservations(length: 50, start: 0) { (reservationList, error) in
            guard let reservationList = reservationList else{return}
            
            /*
             Update isDataLoaded to remove shimmer effect.
             */
            self.isDataLoaded = true
            
            /*
             End refreshing
             */
            if self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }
            
            self.reservationList = reservationList
            if !self.timerList.isEmpty {
                self.timerList.removeAll()
            }
            for i in reservationList {
                let addOneHour = Global.shared.addOneHourFromNow(i.transactionDate!)
                let interval = Global.shared.getTimeIntervalFromNow(addOneHour)
                self.timerList.append(interval)
            }
            self.tableView.reloadData()
        }
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let reservationNibCell = UINib(nibName: "MyReservationTableViewCell", bundle: nil)
        self.tableView.register(reservationNibCell, forCellReuseIdentifier: "myReservationTableViewCell")
        self.tableView.register(UINib(nibName: "Shimmer_MyReservationTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        
        /*
         Setup refesh control
         */
        self.refreshControl.addTarget(self, action: #selector(self.refreshTV(_:)), for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
    }
    
    /*
     Refresh control Handler for TableView
     */
    @objc private func refreshTV(_ sender: Any){
        self.isDataLoaded = false
        self.reservationList.removeAll()
        self.tableView.reloadData()
        
        self.setupData()
    }
}

extension MyReservationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isDataLoaded{
            return reservationList.count
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isDataLoaded{
            let vc = MyReservationDetailViewController()
            vc.reservation = reservationList[indexPath.item]
            vc.reservationId = reservationList[indexPath.item].reservationId
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isDataLoaded{
            let reservation = reservationList[indexPath.item]
            let cell = tableView.dequeueReusableCell(withIdentifier: "myReservationTableViewCell", for: indexPath) as! MyReservationTableViewCell
            
            cell.selectionStyle = .none
            cell.view.clipsToBounds = true
            cell.view.roundCornersView([.topLeft, .topRight], radius: 16)
            if let photoUrl = reservation.outletImage {
                if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                    cell.photo.clipsToBounds = true
                    cell.photo.af.setImage(withURL: url)
                }
            }
            cell.name.text = reservation.outletName
            let date = reservation.reservationDate!
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let finalDate = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd LLLL yyyy HH:mm"
            cell.date.text = dateFormatter.string(from: finalDate!)
            
            cell.time.text = "00:00-00:00"
            let reservationTime = reservation.reservationDate!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let resTimeFinal = dateFormatter.date(from: reservationTime)
            dateFormatter.dateFormat = "HH:mm"
            let leftTime = dateFormatter.string(from: resTimeFinal!)
            let checkoutTime = reservation.checkoutDate!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let checkTimeFinal = dateFormatter.date(from: checkoutTime)
            dateFormatter.dateFormat = "HH:mm"
            let rightTime = dateFormatter.string(from: checkTimeFinal!)
            cell.time.text = "\(leftTime) - \(rightTime)"
            
            if let roomType = reservation.roomDetail?.roomType, let pax = reservation.roomDetail?.pax {
                cell.kamar.text = "\(roomType)"
                cell.kapasitas.text = "\(pax)"
            }
            
            let reservationStatus = reservation.status
            switch reservationStatus {
            case "1":
                cell.status.textColor = UIColor(hexString: "#0c7b2a")
                cell.status.text = "Berhasil"
            case "2":
                cell.status.textColor = UIColor(hexString: "#0c7b2a")
                cell.status.text = "Selesai"
            case "3":
                cell.status.textColor = UIColor(hexString: "#000000")
                cell.status.text = "Cancel"
            case "4":
                cell.status.textColor = Color.reservation.REJECTED
                cell.status.text = "Mohon Hubungi Outlet"
            case "5":
                cell.status.textColor = UIColor(hexString: "#0c7b2a")
                cell.status.text = "Menunggu Konfirmasi"
            case "6":
                cell.status.textColor = UIColor(hexString: "#000000")
                cell.status.text = "Kadaluarsa"
            case "7":
                cell.status.textColor = UIColor(hexString: "#000000")
                cell.status.text = "Hangus"
            default:
                cell.status.textColor = UIColor(hexString: "#c11021")
                
                let addOneHour = Global.shared.addOneHourFromNow(reservation.transactionDate!)
                let interval = Global.shared.getTimeIntervalFromNow(addOneHour)
                print(interval)
                    if interval > 0 {  //Jika interval lebih dari 0
                        if interval/60 < 10 {  //Jika menit lebih kecil dari 10
                            if interval % 60 < 10 {    //Jika detik lebih kecil dari 10
                                let minutes = String(format: "%02d", interval / 60)
                                let seconds = String(format: "%02d", interval % 60)
                                cell.status.text = "Menunggu Pembayaran \n(00:" + minutes + ":" + seconds + ")"
                            } else {    //Jika detik lebih besar dari 10
                                let minutes = String(format: "%02d", interval / 60)
                                let seconds = String(interval % 60)
                                cell.status.text = "Menunggu Pembayaran \n(00:" + minutes + ":" + seconds + ")"
                            }
                        } else {    //Jika menit lebih besar dari 10
                            if interval % 60 < 10 { //Jika detik lebih kecil dari 10
                                let minutes = String(interval / 60)
                                let seconds = String(format: "%02d", interval % 60)
                                cell.status.text = "Menunggu Pembayaran \n(00:" + minutes + ":" + seconds + ")"
                            } else { //Jika detik lebih besar dari 10
                                let minutes = String(interval / 60)
                                let seconds = String(interval % 60)
                                cell.status.text = "Menunggu Pembayaran \n(00:" + minutes + ":" + seconds + ")"
                            }
                        }
                    } else {    //Jika interval sama dengan 0
                        //                    cell.status.textColor = UIColor(hexString: "#c11021")
                        cell.status.text = "Menunggu Pembayaran \n(00:00:00)"
                    }
    //            }
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath) as! Shimmer_MyReservationTableViewCell
            return cell
        }
    }
}
