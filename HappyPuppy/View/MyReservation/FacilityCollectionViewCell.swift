//
//  FacilityCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 17/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class FacilityCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
