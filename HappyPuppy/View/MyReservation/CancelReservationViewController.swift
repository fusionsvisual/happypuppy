//
//  CancelReservationViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 30/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class CancelReservationViewController: UIViewController {
    
    @IBOutlet weak var reservationCode: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var noView: UIView!
    var reservationId: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    func setupView() {
        self.title = "Pembatalan Booking"
        
        self.reservationCode.text = reservationId!
        
        self.noView.layer.borderColor = #colorLiteral(red: 0.9202741385, green: 0.4620800614, blue: 0.6524695754, alpha: 1)
        self.noView.layer.borderWidth = 3
        self.noView.layer.cornerRadius = 10
    }
    
    @IBAction func yesTapped(_ sender: Any) {
        SVProgressHUD.show()
        ReservationController.shared.cancelReservation(reservation_id: reservationId) { (result, message, error) in
            self.navigationController?.popViewController(animated: true)
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func noTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
