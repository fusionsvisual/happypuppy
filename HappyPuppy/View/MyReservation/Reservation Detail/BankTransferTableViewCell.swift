//
//  BankTransferTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class BankTransferTableViewCell: UITableViewCell {
    
    var delegate:MyReservationDetailProtocol?

    @IBOutlet var bankL: UILabel!
    @IBOutlet var accountNameL: UILabel!
    @IBOutlet var totalPriceL: UILabel!
    @IBOutlet var uploadPaymentProofBtn: UIButton!
    
    var bankAccountNumber:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func copyBankNumberAction(_ sender: Any) {
        if let bankAccountNumber = self.bankAccountNumber{
            UIPasteboard.general.string = bankAccountNumber
            
            SVProgressHUD.showSuccess(withStatus: "Copied to clipboard")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }else{
            SVProgressHUD.showError(withStatus: "Bank account number missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    @IBAction func uploadPaymentAction(_ sender: Any) {
        self.delegate?.didUploadBankPaymentTapped()
    }
    
}
