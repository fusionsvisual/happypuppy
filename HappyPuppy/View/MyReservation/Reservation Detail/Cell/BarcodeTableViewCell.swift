//
//  BarcodeTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class BarcodeTableViewCell: UITableViewCell {

    @IBOutlet var warningL: UILabel!
    @IBOutlet var qrisBarcodeIV: UIImageView!
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var uploadProofBtn: UIButton!
    
    var delegate: MyReservationDetailProtocol?
    var qrisImageUrl:URL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.saveBtn.layer.cornerRadius = self.saveBtn.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc private func didImageSaved(image:UIImage, didFinishSavingWithError error:NSError?,contextInfo:UnsafeMutableRawPointer?){
        if let error = error{
            SVProgressHUD.showError(withStatus: error.localizedDescription)
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }else{
            SVProgressHUD.showSuccess(withStatus: "Tersimpan di gallery")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @IBAction func saveImageAction(_ sender: Any) {
        SVProgressHUD.show()
        if let qrisImageUrl = self.qrisImageUrl{
            AF.download(qrisImageUrl).responseData { (response) in
                if let data = response.value, let image = UIImage(data: data) {
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.didImageSaved(image:didFinishSavingWithError:contextInfo:)), nil)
                }else{
                    SVProgressHUD.showError(withStatus: "Gagal menyimpan gambar")
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }
                
            }
            DispatchQueue.main.async {
                
                
            }
        }else{
            SVProgressHUD.showError(withStatus: "Qris barcode missing")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
    
    @IBAction func uploadProofAction(_ sender: Any) {
        self.delegate?.didUploadQRISPaymentTapped()
    }
    
}
