 //
//  MyReservationDetailViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 27/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD

struct PaymentMethodItem {
    var title:String = ""
    var isOpened:Bool = false
}

protocol MyReservationDetailProtocol {
    func didUploadQRISPaymentTapped()
    func didUploadBankPaymentTapped()
}

class MyReservationDetailViewController: UIViewController {
    
    @IBOutlet var reservationInfoSV: UIStackView!
    
    @IBOutlet weak var status: UILabel!
//    @IBOutlet weak var qrCode: UIImageView!
    @IBOutlet weak var reservationNumber: UILabel!
//    @IBOutlet weak var transferTo: UILabel!
//    @IBOutlet weak var bankAndBankNumber: UILabel!
//    @IBOutlet weak var bankName: UILabel!
//    @IBOutlet weak var downPayment: UILabel!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet var paymentContainerV: UIView!
    
    @IBOutlet var qrCodeIV: UIImageView!
    @IBOutlet var qrCodeIVHeigthC: NSLayoutConstraint!
    
    @IBOutlet weak var timer: UILabel!
    @IBOutlet weak var billDate: UILabel!
    @IBOutlet weak var outletImage: UIImageView!
    @IBOutlet weak var outletName: UILabel!
    @IBOutlet weak var outletAddress: UILabel!
    @IBOutlet weak var outletPhoneNumber: UILabel!
    @IBOutlet weak var reservationDate: UILabel!
    @IBOutlet weak var reservationCheckInTime: UILabel!
    @IBOutlet weak var reservationCheckOutTime: UILabel!
    @IBOutlet weak var reservationRoomTypeName: UILabel!
    @IBOutlet weak var reservationRoomPax: UILabel!
    @IBOutlet weak var reservationNote: UILabel!
    @IBOutlet weak var roomTypeName: UILabel!
    @IBOutlet weak var paymentDetailTableView: UITableView!
    @IBOutlet weak var paymentDetailTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalDownPayment: UILabel!
    @IBOutlet weak var custName: UILabel!
    @IBOutlet weak var custPhone: UILabel!
    @IBOutlet weak var custEmail: UILabel!
    @IBOutlet weak var termsAndCondition: UILabel!
    @IBOutlet weak var cancelButtonView: UIView!
    @IBOutlet weak var cancelReservationButton: UIButton!
    
    @IBOutlet var paymentMethodTV: UITableView!
    @IBOutlet var paymentMethodTVHeightC: NSLayoutConstraint!
    
    
    @IBOutlet var reservationInfoContainerTopFirstC: NSLayoutConstraint!
    @IBOutlet var reservationInfoContainerTopSecondC: NSLayoutConstraint!
    
    private var paymentTimer:Timer?
    var reservation: Reservation?
    
    let qrisBarcodeRowHeight: CGFloat = 455
    let paymentMethodRowHeight:CGFloat = 184
    let currentDate = Date()
    let dateFormatter = DateFormatter()
    var reservationId: String?
    var count = 0
    
    var paymentMethodItems:[PaymentMethodItem] = [
        PaymentMethodItem(title: "Barcode QRIS", isOpened: false),
        PaymentMethodItem(title: "Transfer Bank", isOpened: false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.paymentTimer?.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupTimer()
        self.setupData()
    }
    
    func setupView(){
        self.title = "Detail Reservasi"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Refresh"), style: .plain, target: self, action: #selector(refresh))
        
        self.paymentMethodTV.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodCell")
        self.paymentMethodTV.register(UINib(nibName: "BankTransferTableViewCell", bundle: nil), forCellReuseIdentifier: "BankTransferCell")
        self.paymentMethodTV.register(UINib(nibName: "BarcodeTableViewCell", bundle: nil), forCellReuseIdentifier: "BarcodeCell")
        
        self.paymentMethodTVHeightC.constant = CGFloat(self.paymentMethodItems.count * 46)
        
        self.timerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.timerView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.timerView.layer.borderWidth = 0.2
        self.timerView.layer.cornerRadius = 5
        
        self.outletImage.clipsToBounds = true
        self.outletImage.layer.cornerRadius = 10
        
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        style.headIndent = 12
        let title = NSMutableAttributedString(string: "\u{2022} Harap datang 15 menit sebelum jam reservasi. Reservasi dibatalkan apabila tidak datang 10 menit dari jam reservasi (berdasarkan jam komputer outlet)", attributes: [NSAttributedString.Key.paragraphStyle: style])
        let poin1 = NSMutableAttributedString(string: "\n\u{2022} Pengembalian dana atas pembatalan reservasi melalui web keanggotaan mengikuti syarat dan ketentuan berlaku", attributes: [NSAttributedString.Key.paragraphStyle: style])
        let poin2 = NSMutableAttributedString(string: "\n\u{2022} Pengembalian dana atas pembatalan reservasi melalui web keanggotaan mengikuti syarat dan ketentuan berlaku", attributes: [NSAttributedString.Key.paragraphStyle: style])
        let poin3 = NSMutableAttributedString(string: "\n\u{2022} Silahkan melakukan transfer bank dalam jangka waktu yang ditentukan. Jika melebihi batas waktu, maka reservasi Anda akan dibatalkan secara otomatis.", attributes: [NSAttributedString.Key.paragraphStyle: style])
        let poin4 = NSMutableAttributedString(string: "\n\u{2022} Pembayaran Reservasi yang sudah dilakukan sebelum Jam 18.00, akan dikonfirmasi oleh Outlet maksimum lamanya 3 Jam dari berhasilnya upload bukti transfer.", attributes: [NSAttributedString.Key.paragraphStyle: style])
        let poin5 = NSMutableAttributedString(string: "\n\u{2022} Pembayaran Reservasi yang sudah dilakukan diatas Jam 18.00, akan dikonfirmasi oleh Outlet keesokan harinya maksimum jam 12.00", attributes: [NSAttributedString.Key.paragraphStyle: style])
        title.append(poin1)
        title.append(poin2)
        title.append(poin3)
        title.append(poin4)
        title.append(poin5)
        termsAndCondition.attributedText = title
        
        self.cancelButtonView.layer.borderWidth = 3
        self.cancelButtonView.layer.borderColor = #colorLiteral(red: 0.9202741385, green: 0.4620800614, blue: 0.6524695754, alpha: 1)
        self.cancelButtonView.layer.cornerRadius = 10
    }
    
    private func setupTableView(){
        self.paymentDetailTableView.delegate = self
        self.paymentDetailTableView.dataSource = self
        
        let paymentDetailNibCell = UINib(nibName: "PaymentDetailTableViewCell", bundle: nil)
        paymentDetailTableView.register(paymentDetailNibCell, forCellReuseIdentifier: "PaymentDetailCell")
    }
    
    @objc func refresh() {
        self.setupData()
    }
    
    func setupData() {
        SVProgressHUD.show()
        
//        for x in 0..<self.paymentMethodItems.count{
//            self.paymentMethodItems[x].isOpened = false
//        }
//        UIView.animate(withDuration: 0.25) {
//            self.paymentMethodTVHeightC.constant = CGFloat(self.paymentMethodItems.count * 46)
//            self.view.layoutIfNeeded()
//        }
        
        DispatchQueue.main.async {
            ReservationController.shared.getSingleReservationDetail(reservation_id: self.reservationId!) { (reservation, error) in
                SVProgressHUD.dismiss()
                
                guard let reservation = reservation else{return}
                self.reservation = reservation
                
                self.paymentDetailTableViewHeight.constant = CGFloat(44 * reservation.priceDetails.count)
                self.paymentDetailTableView.reloadData()

                if reservation.status! != "0" {
                    switch reservation.status {
                        case "1", "2", "3", "4", "6", "7":
                            self.paymentMethodTVHeightC.constant = 0
                            self.paymentContainerV.isHidden = true
                            self.timerView.isHidden = true
                            self.billDate.isHidden = true
                            
                            self.reservationInfoContainerTopFirstC.priority = UILayoutPriority(rawValue: 250)
                            self.reservationInfoContainerTopSecondC.priority = UILayoutPriority(rawValue: 1000)
                        default:
                            break
                    }
                    
                    // MARK: Setup Cancel Button
                    switch reservation.status {
                        case "1", "2", "3", "4", "5", "6", "7":
                            self.cancelButtonView.isHidden = true
                        default:
                            self.cancelButtonView.isHidden = false
                    }
                    
                    if reservation.status == "1"{
                        self.qrCodeIVHeigthC.constant = 125
                        Global.shared.generateQRCode(from: reservation.reservationId!) { (qrCodeImage) in
                            self.qrCodeIV.image = qrCodeImage
                        }
                    }else{
                        self.qrCodeIVHeigthC.constant = self.reservationInfoSV.frame.height
                    }
                }
                
                switch reservation.status {
                case "1":
                    self.status.textColor = UIColor(hexString: "#0c7b2a")
                    self.status.text = "Berhasil"
                case "2":
                    self.status.textColor = UIColor(hexString: "#0c7b2a")
                    self.status.text = "Selesai"
                case "3":
                    self.status.textColor = UIColor(hexString: "#000000")
                    self.status.text = "Cancel"
                case "4":
                    self.status.textColor = Color.reservation.REJECTED
                    self.status.text = "Mohon Hubungi Outlet"
                case "5":
                    self.status.textColor = UIColor(hexString: "#0c7b2a")
                    self.status.text = "Menunggu Konfirmasi"
                case "6":
                    self.status.textColor = UIColor(hexString: "#000000")
                    self.status.text = "Kadaluarsa"
                case "7":
                    self.status.textColor = UIColor(hexString: "#000000")
                    self.status.text = "Hangus"
                default:
                    self.status.textColor = UIColor(hexString: "#c11021")
                    self.status.text = "Menunggu Pembayaran"
                }
                self.reservationNumber.text = reservation.reservationId
                                
                // MARK: Setup payment status description
                if var paymentProofUploadDate = reservation.paymentDate {
                    // MARK: Payment succeed, waiting for confirmation
                    if let outletData = reservation.outletData{
                        switch outletData.timeZoneName! {
                            case "WITA":
                                paymentProofUploadDate = Global.shared.incrementDate(paymentProofUploadDate, 1)
                            case "WIT":
                                paymentProofUploadDate = Global.shared.incrementDate(paymentProofUploadDate, 2)
                            default:
                                break
                        }
                        let paymentDate = DateFormatterHelper.shared.formatString(oldDateFormat: "yyyy-MM-dd HH:mm:ss", newDateFormat: "dd MMM yy HH:mm", dateString: paymentProofUploadDate, timezone: TimeZone(abbreviation: "UTC"))
                        self.billDate.text = String(format: "Terima kasih, bukti pembayaran telah diterima tanggal %@ %@. Mohon menunggu proses verifikasi pembayaran.", paymentDate, outletData.timeZoneName!)
                        self.paymentMethodTV.reloadData()
                    }else{
                        SVProgressHUD.showError(withStatus: "Outlet data missing")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }
                    
                    //MARK: Hide cancel reservation button
                    self.cancelReservationButton.isHidden = true
                }else{
                    // MARK: Payment unpaid
                    if let transactionDate = reservation.transactionDate{
                        // MARK: Setup Payment Cuntdown
                        /*
                         Increment 1 hour from current date, not based on Timezone
                         */
                        let paymentDate = DateFormatterHelper.shared.formatString(oldDateFormat: "yyyy-MM-dd HH:mm:ss", newDateFormat: "dd MMM yy HH:mm", dateString: Global.shared.incrementDate(transactionDate, 1), timezone: TimeZone(abbreviation: "UTC"))
                        self.billDate.text = String(format: "*Pembayaran dilakukan sebelum tanggal %@.", paymentDate)
                    }
                    
                    if let outletImage = reservation.outletData?.photo {
                        if let url = URL(string: "https://adm.happypuppy.id/\(outletImage)") {
                            self.outletImage.af.setImage(withURL: url)
                        }
                    }
                }
                if let transactionDate = reservation.transactionDate{
                    self.count = Global.shared.getTimeIntervalFromNow(Global.shared.incrementDate(transactionDate, 1))
                }
                
                self.outletName.text = reservation.outletData?.name
                self.outletAddress.text = reservation.outletData?.address
                self.outletPhoneNumber.text = reservation.outletData?.telp
                self.reservationDate.text = Global.shared.fromDateTimeToDate(reservation.reservationDate!)
                self.reservationCheckInTime.text = Global.shared.fromDateTimeToTime(reservation.reservationDate!)
                self.reservationCheckOutTime.text = Global.shared.fromDateTimeToTime(reservation.checkoutDate!)
                self.reservationRoomTypeName.text = reservation.roomTypeName
                if let pax = reservation.roomDetail?.pax {
                    self.reservationRoomPax.text = "\(pax)"
                }
                self.reservationNote.text = reservation.note
                self.roomTypeName.text = reservation.roomTypeName
                
                if let downPayment = reservation.downPayment {
                    self.totalDownPayment.text = "Rp. \(Formatter.shared.priceFormatter(downPayment))"
                }
                
                self.custName.text = reservation.name
                self.custPhone.text = reservation.phoneNumber
                self.custEmail.text = UserDefaultHelper.shared.getEmail()
                
                ReservationController.shared.getReservationPolicy { (policy, error) in
                    if let error = error{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }else{
                        self.termsAndCondition.attributedText = policy!.htmlToAttributedString
                    }
                }
                
                if self.paymentTimer == nil{
                    self.setupTimer()
                }
            }
        }
    }
    
    private func setupTimer(){
        if let reservation = self.reservation, let status = reservation.status, let transactionDate = reservation.transactionDate{
            if status == "0" || status == "5" || status == "8"{
                self.count = Global.shared.getTimeIntervalFromNow(Global.shared.incrementDate(transactionDate, 1))
                self.paymentTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            }
        }
    }
    
    func minutesBetweenDates(_ oldDate: Date, _ newDate: Date) -> Double {

        //get both times sinces refrenced date and divide by 60 to get minutes
        let newDateMinutes = newDate.timeIntervalSinceReferenceDate/60
        let oldDateMinutes = oldDate.timeIntervalSinceReferenceDate/60

        //then return the difference
        return Double(newDateMinutes - oldDateMinutes)
    }
    
    @IBAction func cancelReservationTapped(_ sender: Any) {
        let vc = CancelReservationViewController()
        vc.reservationId = reservationId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func updateTimer() {
            if(count > 0){  //Jika interval lebih dari 0
                timer.textColor = .black
                if count/60 < 10 {  //Jika menit lebih kecil dari 10
                    if count % 60 < 10 {    //Jika detik lebih kecil dari 10
                        let minutes = String(format: "%02d", count / 60)
                        let seconds = String(format: "%02d", count % 60)
                        timer.text = "00:" + minutes + ":" + seconds
                        count-=1
                    } else {    //Jika detik lebih besar dari 10
                        let minutes = String(format: "%02d", count / 60)
                        let seconds = String(count % 60)
                        timer.text = "00:" + minutes + ":" + seconds
                        count-=1
                    }
                } else {    //Jika menit lebih besar dari 10
                    if count % 60 < 10 { //Jika detik lebih kecil dari 10
                        let minutes = String(count / 60)
                        let seconds = String(format: "%02d", count % 60)
                        timer.text = "00:" + minutes + ":" + seconds
                        count-=1
                    } else { //Jika detik lebih besar dari 10
                        let minutes = String(count / 60)
                        let seconds = String(count % 60)
                        timer.text = "00:" + minutes + ":" + seconds
                        count-=1
                    }
                }
            } else {    //Jika interval sama dengan 0
                timer.textColor = .red
                timer.text = "00:00:00"
            }
    }
}

extension MyReservationDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case self.paymentDetailTableView:
            return 1
        default:
            switch self.reservation?.status {
            case "1", "2", "3", "6", "7":
                return 0
            default:
                return self.paymentMethodItems.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
            case self.paymentDetailTableView:
                guard let reservation = self.reservation else {return 0}
                return reservation.priceDetails.count                
            default:
                if self.paymentMethodItems[section].isOpened{
                    return 2
                }else{
                    return 1
                }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
            case self.paymentDetailTableView:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailCell", for: indexPath) as! PaymentDetailTableViewCell
                if let reservation =  self.reservation{
                    cell.time.text = reservation.priceDetails[indexPath.row].timeDisplay
                    if let downPayment = reservation.priceDetails[indexPath.row].totalPrice {
                        cell.price.text = "Rp. \(Formatter.shared.priceFormatter(downPayment))"
                    }
                }
                return cell
            default:
                if indexPath.item == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodCell", for: indexPath) as! PaymentMethodTableViewCell
                    cell.titleL.text = self.paymentMethodItems[indexPath.section].title
                    cell.isSelect = self.paymentMethodItems[indexPath.section].isOpened
                    
                    return cell
                }else{
                    if indexPath.section == 0{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "BarcodeCell", for: indexPath) as! BarcodeTableViewCell
                        cell.delegate = self
                        if let reservation = self.reservation, let outletData = reservation.outletData{
                            if let qrisImageUrlStr = outletData.qrisImageUrl{
                                if let qrisImageUrl = URL(string: "https://adm.happypuppy.id/" + qrisImageUrlStr){
                                    cell.qrisImageUrl = qrisImageUrl
                                    cell.qrisBarcodeIV.af.setImage(withURL: qrisImageUrl)
                                }
                            }else{
                                cell.warningL.isHidden = false
                                cell.saveBtn.isHidden = true
                            }
                            
                            if reservation.paymentProofUrl != ""{
                                cell.uploadProofBtn.setTitle("KIRIM ULANG PEMBAYARAN", for: .normal)
                            }
                        }                        
                        return cell
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "BankTransferCell", for: indexPath) as! BankTransferTableViewCell
                        cell.delegate = self
                        if let reservationDetail = self.reservation{
                            if let outletData = reservationDetail.outletData{
                                cell.bankAccountNumber = outletData.bankNumber
                                cell.bankL.text = (outletData.bank ?? "Missing Bank Name") + " " + (reservationDetail.outletData?.bankNumber ?? "")
                                cell.accountNameL.text = outletData.bankAccountName
                                cell.totalPriceL.text = "Rp. " + Formatter.shared.priceFormatter(reservationDetail.downPayment!)
                            }
                            
                            if reservationDetail.paymentProofUrl != ""{
                                cell.uploadPaymentProofBtn.setTitle("KIRIM ULANG PEMBAYARAN", for: .normal)
                            }
                        }
                        
                        return cell
                    }
                   
                }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.paymentMethodTV{
            if indexPath.row == 0{
                self.paymentMethodItems[indexPath.section].isOpened = !self.paymentMethodItems[indexPath.section].isOpened
                if self.paymentMethodItems[indexPath.section].isOpened{
                    UIView.animate(withDuration: 0.25) {
                        if indexPath.section == 0{
                            self.paymentMethodTVHeightC.constant += self.qrisBarcodeRowHeight
                        }else{
                            self.paymentMethodTVHeightC.constant += self.paymentMethodRowHeight
                        }
                        self.view.layoutIfNeeded()
                    }
                }else{
                    UIView.animate(withDuration: 0.25) {
                        if indexPath.section == 0{
                            self.paymentMethodTVHeightC.constant -= self.qrisBarcodeRowHeight
                        }else{
                            self.paymentMethodTVHeightC.constant -= self.paymentMethodRowHeight
                        }
                        self.view.layoutIfNeeded()
                    }
                }
                let sections = IndexSet.init(integer: indexPath.section)
                self.paymentMethodTV.reloadSections(sections, with: .automatic)
            }
        }
    }
}

extension MyReservationDetailViewController: MyReservationDetailProtocol{
    func didUploadQRISPaymentTapped() {
        let uploadPaymentVC = UploadPaymentViewController()
        uploadPaymentVC.paymentMethod = .QRIS
        uploadPaymentVC.reservation = self.reservation
        self.navigationController?.pushViewController(uploadPaymentVC, animated: true)
    }
    
    func didUploadBankPaymentTapped() {
        let uploadPaymentVC = UploadPaymentViewController()
        uploadPaymentVC.paymentMethod = .BANK_TRANSFER
        uploadPaymentVC.reservation = self.reservation
        self.navigationController?.pushViewController(uploadPaymentVC, animated: true)
    }
}
