//
//  MyReservationTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 05/07/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class MyReservationTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var kamar: UILabel!
    @IBOutlet weak var kapasitas: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Border
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        view.layer.cornerRadius = 20
        
        //Shadow
        view.backgroundColor = .white
        view.layer.masksToBounds = true
        view.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }    
}
