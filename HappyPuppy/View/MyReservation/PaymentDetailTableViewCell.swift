//
//  PaymentDetailTableViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 11/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class PaymentDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
