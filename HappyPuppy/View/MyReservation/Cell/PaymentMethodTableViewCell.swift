//
//  PaymentMethodTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 12/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet var titleL: UILabel!
    @IBOutlet var chevronDownIV: UIImageView!
    
    var isSelect:Bool = false{
        didSet{
            self.chevronDownIV.transform = CGAffineTransform.identity
            
            if self.isSelect{
                self.chevronDownIV.transform = self.chevronDownIV.transform.rotated(by: .pi)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
