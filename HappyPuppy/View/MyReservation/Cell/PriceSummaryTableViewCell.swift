//
//  PriceSummaryTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 12/02/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class PriceSummaryTableViewCell: UITableViewCell {

    @IBOutlet var timeL: UILabel!
    @IBOutlet var priceL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
