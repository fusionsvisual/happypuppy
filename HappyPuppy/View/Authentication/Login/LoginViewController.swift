//
//  LoginViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 21/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleSignIn
import FontAwesome_swift
import AuthenticationServices

class LoginViewController: UIViewController, UITextFieldDelegate, ASAuthorizationControllerDelegate {

    

    @IBOutlet var loginGoogleLabel: UILabel!
    @IBOutlet var passwordToggleLabel: UILabel!

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var loginLoadIndiciator: UIActivityIndicatorView!
    
    @IBOutlet var loginAppleContainer: UIStackView!
    @IBOutlet var passwordViewContainer: UIView!

    var isPasswordSeen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func setupViews(){
        
        hideKeyboardWhenTappedAround()
        
        if view.frame.width <= 414{
            loginGoogleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        }
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
                
        let emailPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        
        /* Setup Email TextField */
        emailTextField.delegate = self
        emailTextField.layer.cornerRadius = 8
        emailTextField.attributedPlaceholder = NSAttributedString(string: "E-mail",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailTextField.leftView = emailPaddingView
        emailTextField.leftViewMode = .always
        emailTextField.attributedPlaceholder = NSAttributedString(string: "E-mail",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        
        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        
        /* Setup Password TextField */
        passwordTextField.delegate = self
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordTextField.leftView = passwordPaddingView
        passwordTextField.leftViewMode = .always
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        
        /* Setup Login Button */
        loginButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        loginButton.layer.borderWidth = 1
        
        // Setup Password Toggle
        let passwordToggleGesture = UITapGestureRecognizer(target: self, action: #selector(passwordToggle))
        passwordToggleLabel.addGestureRecognizer(passwordToggleGesture)
        passwordViewContainer.layer.cornerRadius = 8
        
        // Setup Apple Id Sign InButton
        if #available(iOS 13.0, *) {
            self.setUpSignInAppleButton()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /* Notification Listener for Google Login from App Delegate */
        NotificationCenter.default.addObserver(self, selector: #selector(googleLogin(_:)), name: NotificationConstant.GOOGLE_LOGIN, object: nil)
        /*-----*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NotificationConstant.GOOGLE_LOGIN, object: nil)
    }
    
    @available(iOS 13.0, *)
    func setUpSignInAppleButton() {
        
        let appleLoginButton = ASAuthorizationAppleIDButton(type: .default, style: .white)
        appleLoginButton.addTarget(self, action: #selector(loginAppleAction(_:)), for: .touchUpInside)
        //Add button on some view or stack
        self.loginAppleContainer.addArrangedSubview(appleLoginButton)
    }
    
    @objc func passwordToggle(sender:UITapGestureRecognizer){
        if isPasswordSeen{
            isPasswordSeen = !isPasswordSeen
            passwordTextField.isSecureTextEntry = true
            passwordToggleLabel.text = String.fontAwesomeIcon(name: .eye)
        }else{
            isPasswordSeen = !isPasswordSeen
            passwordTextField.isSecureTextEntry = false
            passwordToggleLabel.text = String.fontAwesomeIcon(name: .eyeSlash)
        }
        
    }
    
    @objc private func googleLogin(_ notification:NSNotification){
        if let socialMediaToken = notification.userInfo?["access_token"] as? String{
            print(socialMediaToken)
            AuthenticationContoller.shared.loginBySocialMedia(accessToken: socialMediaToken, userString: nil, provider: "google") { (status, message, error) in
                if let error = error{
                    if error == ErrorMessage.GOOGLE_NOT_REGISTERED{
                        
                        let secondRegisterVC = SecondRegisterViewController()
                        SocialMediaConstant.accessToken = socialMediaToken
                        SocialMediaConstant.provider = "google"
                        if let userInfo = notification.userInfo{
                            if let firstName = userInfo["full_name"] as? String, let lastName = userInfo["last_name"] as? String, let email = userInfo["email"] as? String{
                                UserDefaultHelper.shared.setSMUserInfo(firstName: firstName, lastName: lastName, email: email)
                            }
                        }
                        
                        self.navigationController?.pushViewController(secondRegisterVC, animated: true)
                    }else{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }else{
                    let tabBarVC = TabBarViewController()
                    tabBarVC.modalPresentationStyle = .fullScreen
                    self.present(tabBarVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    // Handle Apple ID Sign In
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        print("APPLE SIGN IN SUCCESS!")
        
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            var identityToken:String?
            if let token = appleIDCredential.identityToken {
                identityToken = String(bytes: token, encoding: .utf8)
            }
            
            AuthenticationContoller.shared.loginBySocialMedia(accessToken: identityToken!, userString: userIdentifier, provider: "apple") { (status, message, error) in
                if let error = error{
                    if error == ErrorMessage.GOOGLE_NOT_REGISTERED{
                        let secondRegisterVC = SecondRegisterViewController()
                        secondRegisterVC.socialMediaToken = identityToken
                        SocialMediaConstant.accessToken = identityToken
                        SocialMediaConstant.userString = userIdentifier
                        SocialMediaConstant.provider = SocialMediaProvider.APPLE
                        
                        if let fullName = fullName, let email = email{
                            if let firstName = fullName.givenName, let lastName = fullName.familyName{
                                UserDefaultHelper.shared.setAppleIDUserInfo(firstName: firstName, lastName: lastName, email: email)
                            }
                        }
                        
                        self.navigationController?.pushViewController(secondRegisterVC, animated: true)
                    }else{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }else{
                    let tabBarVC = TabBarViewController()
                    tabBarVC.modalPresentationStyle = .fullScreen
                    self.present(tabBarVC, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    // MARK: User never Sign In with Apple ID
    @available(iOS 13.0, *)
    @objc func loginAppleAction(_ button:Any){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    // END
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
            return true
        default:
            dismissKeyboard()
            loginProcess()
            return true
        }
    }
    
    private func textFieldValidator()->Bool{
        if emailTextField.text!.isEmpty{
            return false
        }
        if passwordTextField.text!.isEmpty{
            return false
        }
        return true
    }
    
    private func loginProcess(){
        if textFieldValidator(){
                        
            /* Show loading indicator */
            loginLoadIndiciator.isHidden = false
            loginButton.setTitle("", for: .normal)
            
            let email = emailTextField.text!
            let password = passwordTextField.text!
            AuthenticationContoller.shared.login(email: email, password: password) { (status, user, error) in
                if let error = error {
                    
                    self.loginLoadIndiciator.isHidden = true
                    
                    self.loginButton.setTitle("LOGIN", for: .normal)
                    self.loginLoadIndiciator.isHidden = true
                    self.loginLoadIndiciator.stopAnimating()
                    
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                }else{
                    ProfileController.shared.getProfileDetail { (status, user, error) in
                        if let error = error {
                            SVProgressHUD.showError(withStatus: error)
                            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                            return
                        }
                        
                        let tabBarVC = UINavigationController(rootViewController: TabBarViewController())
                        tabBarVC.modalPresentationStyle = .fullScreen
                        self.present(tabBarVC, animated: true, completion: nil)
                    }
                }
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: 0.5)
        }
    }
    
    @IBAction func forgetPasswordAction(_ sender: Any) {
        let forgetPasswordVC = ForgetPasswordViewController()
        navigationController?.pushViewController(forgetPasswordVC, animated: true)
    }
    
    @IBAction func signGoogleAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let firstRegisterVC = FirstRegisterViewController()
        navigationController?.pushViewController(firstRegisterVC, animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        loginProcess()
    }
    
    
}
