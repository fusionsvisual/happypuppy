//
//  ForgetPasswordViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 04/06/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgetPasswordViewController: UIViewController {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var forgetPasswordButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        
        hideKeyboardWhenTappedAround()
        
        /* Setup TextField Placeholder */
        let emailPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        emailTextField.attributedPlaceholder = NSAttributedString(string: "E-mail",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        emailTextField.leftView = emailPaddingView
        emailTextField.leftViewMode = .always
        /*-----*/
        
        /* Setup forgetPasswordButton UIButton */
        forgetPasswordButton.layer.borderWidth = 1
        forgetPasswordButton.layer.borderColor = #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 0.25)
        /*-----*/
        
        UINavigationBar.appearance().barTintColor = .black
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    private func textFieldValidator()->Bool{
        if emailTextField.text!.isEmpty{
            return false
        }
        return true
    }
    
    @IBAction func forgetPasswordAction(_ sender: Any) {
        if textFieldValidator(){
            activityIndicator.isHidden = false
            forgetPasswordButton.setTitle("", for: .normal)
            
            let email = emailTextField.text!
            ProfileController.shared.forgetPassword(email: email) { (status, message, error) in
                if let error = error{
                    self.activityIndicator.isHidden = true
                    self.forgetPasswordButton.setTitle("LUPA KATA SANDI", for: .normal)
                    
                    SVProgressHUD.showError(withStatus: error)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }else{
                    if let message = message{
                        SVProgressHUD.showInfo(withStatus: message)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
        }
    }
}
