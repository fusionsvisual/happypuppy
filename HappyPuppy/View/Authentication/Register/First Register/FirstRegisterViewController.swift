//
//  RegisterViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 29/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleSignIn
import AuthenticationServices

class FirstRegisterViewController: UIViewController, ASAuthorizationControllerDelegate {

    @IBOutlet var loginAppleContainer: UIStackView!
    @IBOutlet var loginGoogleLabel: UILabel!
    @IBOutlet var registerUsingEmailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
                
        /* Notification Listener for Google Login from App Delegate */
        NotificationCenter.default.addObserver(self, selector: #selector(googleLogin(_:)), name: NotificationConstant.GOOGLE_LOGIN, object: nil)
        /*-----*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NotificationConstant.GOOGLE_LOGIN, object: nil)
    }
    
    private func setupViews(){
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        /* Setup Login Button */
        registerUsingEmailButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        registerUsingEmailButton.layer.borderWidth = 1
        /*-----*/
        
        if view.frame.width <= 414{
            loginGoogleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        }
        
        // Setup Apple Id Sign InButton
        if #available(iOS 13.0, *) {
            self.setUpSignInAppleButton()
        }
    }
    
    @available(iOS 13.0, *)
    func setUpSignInAppleButton() {
        let appleLoginButton = ASAuthorizationAppleIDButton(type: .default, style: .white)
        appleLoginButton.addTarget(self, action: #selector(loginAppleAction(_:)), for: .touchUpInside)
        //Add button on some view or stack
        self.loginAppleContainer.addArrangedSubview(appleLoginButton)
    }
    
    @available(iOS 13.0, *)
    @objc func loginAppleAction(_ button:Any){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
        
    }
    
    // MARK: User already Sign In with Apple ID
    @available(iOS 13.0, *)
    func performExistingAccountSetupFlows(){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let requests = [request,
                        ASAuthorizationPasswordProvider().createRequest()]
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    // END
    
    // Handle Apple ID Sign In
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        print("APPLE SIGN IN SUCCESS!")
        
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            var identityToken:String?
            if let token = appleIDCredential.identityToken {
                identityToken = String(bytes: token, encoding: .utf8)
            }
            
            AuthenticationContoller.shared.loginBySocialMedia(accessToken: identityToken!, userString: userIdentifier, provider: "apple") { (status, message, error) in
                if let error = error{
                    if error == ErrorMessage.GOOGLE_NOT_REGISTERED{
                        let secondRegisterVC = SecondRegisterViewController()
                        secondRegisterVC.socialMediaToken = identityToken
                        SocialMediaConstant.accessToken = identityToken
                        SocialMediaConstant.userString = userIdentifier
                        SocialMediaConstant.provider = "apple"
                        if let fullName = fullName, let email = email{
                            if let firstName = fullName.givenName, let lastName = fullName.familyName{
                                UserDefaultHelper.shared.setSMUserInfo(firstName: firstName, lastName: lastName, email: email)
                            }
                        }
                        
                        self.navigationController?.pushViewController(secondRegisterVC, animated: true)
                    }else{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }else{
                    self.navigationController?.pushViewController(TabBarViewController(), animated: true)
                }
            }
            
        }
    }
    
    @objc private func googleLogin(_ notification:NSNotification){
        if let socialMediaToken = notification.userInfo?["access_token"] as? String{
            print(socialMediaToken)
            AuthenticationContoller.shared.loginBySocialMedia(accessToken: socialMediaToken, userString: nil, provider: "google") { (status, message, error) in
                if let error = error{
                    if error == ErrorMessage.GOOGLE_NOT_REGISTERED{
                        
                        let secondRegisterVC = SecondRegisterViewController()
                        secondRegisterVC.socialMediaToken = socialMediaToken
                        if let userInfo = notification.userInfo{
                            if let firstName = userInfo["full_name"] as? String, let lastName = userInfo["last_name"] as? String, let email = userInfo["email"] as? String{
                                UserDefaultHelper.shared.setSMUserInfo(firstName: firstName, lastName: lastName, email: email)
                            }
                        }
                        
                        self.navigationController?.pushViewController(secondRegisterVC, animated: true)
                    }else{
                        SVProgressHUD.showError(withStatus: error)
                        SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                    }
                }else{
                    self.navigationController?.pushViewController(TabBarViewController(), animated: true)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func googleRegisterAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func registerUsingEmailAction(_ sender: Any) {
       let secondRegisterVC = SecondRegisterViewController()
        navigationController?.pushViewController(secondRegisterVC, animated: true)
    }
}
