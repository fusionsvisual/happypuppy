//
//  SecondRegisterViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 29/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class SecondRegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var passwordToggleLabel: UILabel!
    @IBOutlet var confirmPasswordToggleLabel: UILabel!
    
    @IBOutlet var passwordContainer: UIView!
    @IBOutlet var cPasswordContainer: UIView!
    
    /* UITextField */
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var confirmPasswordTextField: UITextField!
    /*-----*/
    
    /* Variable */
    var socialMediaToken:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        
        hideKeyboardWhenTappedAround()
        
        let firstNamePaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        firstNameTextField.delegate = self
        firstNameTextField.layer.cornerRadius = 8
        firstNameTextField.attributedPlaceholder = NSAttributedString(string: "Nama Depan",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        firstNameTextField.leftView = firstNamePaddingView
        firstNameTextField.leftViewMode = .always
        
        let lastNamePaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        lastNameTextField.delegate = self
        lastNameTextField.layer.cornerRadius = 8
        lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Nama Belakang",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        lastNameTextField.leftView = lastNamePaddingView
        lastNameTextField.leftViewMode = .always
        
        let emailPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        emailTextField.delegate = self
        emailTextField.layer.cornerRadius = 8
        emailTextField.attributedPlaceholder = NSAttributedString(string: "E-mail",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        emailTextField.leftView = emailPaddingView
        emailTextField.leftViewMode = .always
        
        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        passwordTextField.delegate = self
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        passwordTextField.leftView = passwordPaddingView
        passwordTextField.leftViewMode = .always
        passwordContainer.layer.cornerRadius = 8

        let confirmPasswordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        confirmPasswordTextField.leftView = confirmPasswordPaddingView
        confirmPasswordTextField.leftViewMode = .always
        cPasswordContainer.layer.cornerRadius = 8

        
        /* Setup Login Button */
        nextButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        nextButton.layer.borderWidth = 1
        /*-----*/
        
        /* Fill UITextField for Social Login */
        
        let SMUserInfo = UserDefaultHelper.shared.getSMUserInfo()
        
        if let firstName = SMUserInfo["firstName"]{
            firstNameTextField.text = firstName
        }
        
        if let lastName = SMUserInfo["lastName"]{
            lastNameTextField.text = lastName
        }
        
        if let email =  SMUserInfo["email"]{
            emailTextField.text = email
        }
        
        // Setup Password Toggle
        let passwordToggleGesture = UITapGestureRecognizer(target: self, action: #selector(passwordToggleAction))
        passwordToggleLabel.addGestureRecognizer(passwordToggleGesture)
        
        // Setup Confirm Password Toggle
        let confirmPasswordToggleGesture = UITapGestureRecognizer(target: self, action: #selector(passwordToggleAction))
        confirmPasswordToggleLabel.addGestureRecognizer(confirmPasswordToggleGesture)
        
        // MARK: Handle Apple ID Sign In
        /*
            - Set First Name, Last Name, Email TextField Value
            - Disable First Name & Last Name TextField
            - Disable Email Text Field
         */
        if (SocialMediaConstant.provider == SocialMediaProvider.APPLE){
            firstNameTextField.isEnabled = false
            firstNameTextField.alpha = 0.5
            
            lastNameTextField.isEnabled = false
            lastNameTextField.alpha = 0.5
            
            emailTextField.isEnabled = false
            emailTextField.alpha = 0.5
            
            let appleIdUserInfo = UserDefaultHelper.shared.getAppleIDInfo()
            if let firstName = appleIdUserInfo["firstName"] as? String, let lastName = appleIdUserInfo["lastName"] as? String, let email = appleIdUserInfo["email"] as? String{
                firstNameTextField.text = firstName
                lastNameTextField.text = lastName
                emailTextField.text = email
            }else{
                SVProgressHUD.showError(withStatus: "Please Revoke Your Apple Sign In Credentials.")
                SVProgressHUD.dismiss(withDelay: 2) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        // END
    }
    
    @objc private func passwordToggleAction(){
        if passwordTextField.isSecureTextEntry{
            passwordToggleLabel.text = String.fontAwesomeIcon(name: .eyeSlash)
            confirmPasswordToggleLabel.text = String.fontAwesomeIcon(name: .eyeSlash)
        }else{
            passwordToggleLabel.text = String.fontAwesomeIcon(name: .eye)
            confirmPasswordToggleLabel.text = String.fontAwesomeIcon(name: .eye)
        }
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        confirmPasswordTextField.isSecureTextEntry = !confirmPasswordTextField.isSecureTextEntry
    }
    
    @objc private func confirmPasswordToggleAction(){
        
    }
    
    private func textFieldValidator()->Bool{
        
        if firstNameTextField.text!.isEmpty{
            return false
        }
        
        if lastNameTextField.text!.isEmpty{
            return false
        }
        
        if emailTextField.text!.isEmpty{
            return false
        }
        
        if passwordTextField.text!.isEmpty{
            return false
        }
        
        if confirmPasswordTextField.text!.isEmpty{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(textField)
        print(textField == firstNameTextField)
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            confirmPasswordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func backAction(_ sender: Any) {
        UserDefaultHelper.shared.clearSMUserInfo()
        SocialMediaConstant.clear()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if textFieldValidator(){
            if Global.shared.isValidEmail(emailTextField.text!){
                if passwordTextField.text == confirmPasswordTextField.text{
                    if (passwordTextField.text!.count >= 6){
                            let thirdRegisterVC = ThirdRegisterViewController()
                            thirdRegisterVC.firstName = firstNameTextField.text!
                            thirdRegisterVC.lastName = lastNameTextField.text!
                            thirdRegisterVC.email = emailTextField.text!
                            thirdRegisterVC.password = passwordTextField.text!
                            navigationController?.pushViewController(thirdRegisterVC, animated: true)
                    }else{
                        SVProgressHUD.showError(withStatus: "The password must be at least 6 characters.")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }
                        
                    }else{
                        SVProgressHUD.showError(withStatus: "Password Not Match!")
                        SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                    }
                }else{
                    SVProgressHUD.showError(withStatus: "You have entered an invalid e-mail address")
                    SVProgressHUD.dismiss(withDelay: DelayConstant.LONG)
                }
            }else{
                SVProgressHUD.showError(withStatus: "Fill The Blanks!")
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }
            
    }
}
