//
//  ThirdRegisterViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD

class ThirdRegisterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    /* UITextField */
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var birthdateTextField: UITextField!
    @IBOutlet var countryCodeTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    /*-----*/
    
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var phoneViewContainer: UIView!
    @IBOutlet var termsPolicyLabel: UILabel!
    
    /* Variable */
//    var socialMediaToken:String?
    var firstName = ""
    var lastName = ""
    var email = ""
    var password = ""
    var selectedPhoneCode = "62"
    
    let genderPickerView = UIPickerView()
    let birthdateDatePicker = UIDatePicker()
    let countryCodePickerView = UIPickerView()
    let genderList = ["Pria", "Wanita"]
    /*-----*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    private func setupViews(){
        hideKeyboardWhenTappedAround()
        
        /* Setup UIToolbar for UIPickerView */
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(pickerDone))
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.items = [spaceButton, btnDone]
        /*-----*/
        
        /* Setup genderTextField UITextField */
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        let genderPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        genderTextField.layer.cornerRadius = 8
        genderTextField.text = genderList[0]
        genderTextField.leftView = genderPaddingView
        genderTextField.leftViewMode = .always
        genderTextField.inputView = genderPickerView
        genderTextField.inputAccessoryView = toolbar
        genderTextField.tintColor = .clear
        /*-----*/
        
        /* Setup birthdateTextField UITextField */
        birthdateDatePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            self.birthdateDatePicker.preferredDatePickerStyle = .wheels
        }
        birthdateDatePicker.locale = Locale(identifier: "id_ID")
        birthdateDatePicker.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
        birthdateDatePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -90, to: Date())
        birthdateDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -15, to: Date())
        let birthdatePaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 50))
        
        birthdateTextField.layer.cornerRadius = 8
        birthdateTextField.attributedPlaceholder = NSAttributedString(string: "Tanggal Lahir",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        birthdateTextField.leftView = birthdatePaddingView
        birthdateTextField.leftViewMode = .always
        birthdateTextField.inputView = birthdateDatePicker
        birthdateTextField.inputAccessoryView = toolbar
        birthdateTextField.tintColor = .clear
        self.birthdateTextField.addTarget(self, action: #selector(self.didDateSelected(_:)), for: .editingDidEnd)
        /*-----*/
        
        /* Setup phoneTextField UITextField */
        phoneViewContainer.layer.cornerRadius = 8
        
        phoneTextField.delegate = self
        phoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6470588235, alpha: 1)])
        /*-----*/
        
        // Setup Country Code TextField
        countryCodePickerView.delegate = self
        countryCodePickerView.dataSource = self
        countryCodeTextField.inputView = countryCodePickerView
        countryCodeTextField.inputAccessoryView = toolbar
        countryCodeTextField.tintColor = .clear
        
        /* Setup Login Button */
        nextButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        nextButton.layer.borderWidth = 1
        /*-----*/
        
        // Setup Terms and Condition Label
        let text = termsPolicyLabel.text!
        let attributedString = NSMutableAttributedString(string: text)
        
        // Setup "Syarat Kondisi" Text Style
        let termsRange = (text as NSString).range(of: "Syarat Kondisi")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow, range: termsRange)
        
        // Setup "Kebijakan Privacy" Text Style
        let privacyRange = (text as NSString).range(of: "Kebijakan Privacy")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.yellow, range: privacyRange)
        
        termsPolicyLabel.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(termsPolicyAction(_:)))
        tapGesture.numberOfTouchesRequired = 1

        termsPolicyLabel.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//    }
    
    @objc private func didDateSelected(_ sender: UITextField){
        // MARK: Handle when date datepicker never moved and click done
        let selectedDate = DateFormatterHelper.shared.formatDate(dateFormat: "dd MMMM yyyy", date: self.birthdateDatePicker.date, timezone: .current, locale: Locale(identifier: "id_ID"))
        self.birthdateTextField.text = selectedDate
    }
    
    
    @objc func termsPolicyAction(_ gesture:UITapGestureRecognizer){
        let text = termsPolicyLabel.text!
        let termsRange = (text as NSString).range(of: "Syarat Kondisi")
        let privacyRange = (text as NSString).range(of: "Kebijakan Privacy")
        
        if gesture.didTapAttributedTextInLabel(label: termsPolicyLabel, inRange: termsRange) {
            navigationController?.pushViewController(TermsConditionViewController(), animated: true)
          } else if gesture.didTapAttributedTextInLabel(label: termsPolicyLabel, inRange: privacyRange) {
              navigationController?.pushViewController(PrivacyPolicyViewController(), animated: true)
          }
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @objc private func didSelectDate(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "id_ID")
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let dateString = dateFormatter.string(from: sender.date)
        
        birthdateTextField.text = dateString
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case genderPickerView:
            return genderList.count
        default:
            return CountryCodes.list.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case genderPickerView:
            self.genderTextField.text = genderList[row]
        default:
            let countryCode = CountryCodes.list[row]
            self.selectedPhoneCode = countryCode.codeNumber
            self.countryCodeTextField.text = "\(countryCode.flag) \(countryCode.codeId) +\(countryCode.codeNumber)"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case genderPickerView:
             return genderList[row]
        default:
            let countryCode = CountryCodes.list[row]
            return "\(countryCode.flag) \(countryCode.name)(\(countryCode.codeId)) +\(countryCode.codeNumber)"
        }
       
    }
    
    private func textFieldValidator()->Bool{
        
        if genderTextField.text!.isEmpty{
            return false
        }
        
        if birthdateTextField.text!.isEmpty{
            return false
        }
        
        if phoneTextField.text!.isEmpty{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneTextField{
            resignFirstResponder()
        }
        return false
    }
    
    @IBAction func checkAction(_ sender: Any) {
        if !checkButton.isSelected{
            checkButton.isSelected = true;
            checkButton.setImage(#imageLiteral(resourceName: "CheckIcon"), for: .normal)
        }else{
            checkButton.isSelected = false;
            checkButton.setImage(#imageLiteral(resourceName: "UncheckIcon"), for: .normal)
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if textFieldValidator(){
            if checkButton.isSelected{
                
                SVProgressHUD.show()
                
                let gender = genderTextField.text!
                let birthDate = birthdateTextField.text!
                let phone = "+\(selectedPhoneCode)\(phoneTextField.text!)"
                
                if SocialMediaConstant.accessToken != nil{
                    AuthenticationContoller.shared.registerByGoogle(email: email, password: password, firstName: firstName, lastName: lastName, gender: gender, birthdate: birthDate, phone: phone) { (status, error) in
                        if let error = error{
                            SVProgressHUD.showError(withStatus: error)
                            SVProgressHUD.dismiss(withDelay: 1)
                        }else{
                            SocialMediaConstant.accessToken = nil
                            UserDefaultHelper.shared.clearSMUserInfo()
                            SVProgressHUD.dismiss()
                            self.navigationController?.pushViewController(TabBarViewController(), animated: true)
                        }
                    }
                }else{
                    AuthenticationContoller.shared.register(email: email, password: password, firstName: firstName, lastName: lastName, gender: gender, birthdate: birthDate, phone: phone) { (status, error) in
                        
                        if let error = error{
                            SVProgressHUD.showError(withStatus: error)
                            SVProgressHUD.dismiss(withDelay: 1)
                        }else{
                            SVProgressHUD.dismiss()
                            self.navigationController?.pushViewController(TabBarViewController(), animated: true)
                        }
                    }
                }
            }else{
                SVProgressHUD.showError(withStatus: "Mohon Tick Checkbox Terlebih Dahulu!")
                SVProgressHUD.dismiss(withDelay: 0.5)
            }
        }else{
            SVProgressHUD.showError(withStatus: "Fill The Blanks!")
            SVProgressHUD.dismiss(withDelay: 0.5)
        }
    }
    
    @objc private func pickerDone(){
        if genderTextField.isEditing{
            dismissKeyboard()
            birthdateTextField.becomeFirstResponder()
        }else{
            dismissKeyboard()
            phoneTextField.becomeFirstResponder()
        }
    }
}
