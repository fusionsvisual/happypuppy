//
//  AuthenticationMenuViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class AuthenticationMenuViewController: UIViewController {

    /* UIButton */
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    /*-----*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        registerButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        registerButton.layer.borderWidth = 1
        
        loginButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        loginButton.layer.borderWidth = 1
    }
    
    @IBAction func registerButton(_ sender: Any) {
        let registerVC = FirstRegisterViewController()
        navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }
}
