//
//  Header_HomeTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Header_HomeTableViewCell: UITableViewCell {

    @IBOutlet var bannerCV: UICollectionView!
    @IBOutlet var cityTF: UITextField!
        
    var delegate: HomeDelegate?
    var cities:[City] = [] {
        didSet{
            if let selectedCity = self.selectedCity{
                self.cityTF.text = selectedCity.name
            }else{
                if let firstCity = self.cities.first{
                    self.cityTF.text = firstCity.name
                }
            }
            self.citiesPV.reloadAllComponents()
        }
    }
    
    var banners: [Banner] = [] {
        didSet{
            self.bannerCV.reloadData()
        }
    }
    
    var selectedCity:City? = nil{
        didSet{
            guard let selectedCity = self.selectedCity else {return}
            self.cityTF.text = selectedCity.name
        }
    }
    
    var isBannerLoaded = false
    
    private var citiesPV: UIPickerView = UIPickerView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.cityTF.delegate = self
        self.cityTF.tintColor = .clear
        self.cityTF.inputView = self.citiesPV
        
        self.citiesPV.delegate = self
        self.citiesPV.dataSource = self
        
        self.bannerCV.delegate = self
        self.bannerCV.dataSource = self
        self.bannerCV.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCell")
        self.bannerCV.register(UINib(nibName: "Shimmer_BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShimmerCell")
    }
    
    @IBAction func selectCityAction(_ sender: Any) {
        self.cityTF.becomeFirstResponder()
    }
}

extension Header_HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isBannerLoaded{
            return self.banners.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.isBannerLoaded{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCollectionViewCell
            if let bannerUrlStr = banners[indexPath.item].photo_url {
                if let bannerUrl = URL(string: String(format: "https://adm.happypuppy.id/%@", bannerUrlStr)) {
                    cell.bannerIV.af.setImage(withURL: bannerUrl)
                }
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
    }
}

extension Header_HomeTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.cities[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.cityTF.text = self.cities[row].name
        self.selectedCity = self.cities[row]
    }
}

extension Header_HomeTableViewCell: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let selectedCity = self.selectedCity else {return}
        self.delegate?.didCitySelected(city: selectedCity)
    }
}
