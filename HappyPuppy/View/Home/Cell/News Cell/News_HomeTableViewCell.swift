//
//  News_HomeTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class News_HomeTableViewCell: UITableViewCell {

    @IBOutlet var thumbnailV: UIView!
    @IBOutlet var shimmerContainerV: PuppyClubShimmeringView!
    @IBOutlet var shimmerContentV: UIView!
    
    @IBOutlet var newsCV: UICollectionView!
    
    @IBOutlet var thumbnailNewsIV: UIImageView!
    @IBOutlet var thumbnailNewsDateL: UILabel!
    @IBOutlet var thumbnailNewsTitleL: UILabel!
    @IBOutlet var thumbnailNewsDescL: UILabel!
    
    var delegate:HomeDelegate?
    var isNewsLoaded = false
    var news:[News] = []{
        didSet{
            if self.isNewsLoaded{
                self.thumbnailV.isHidden = false
                self.shimmerContainerV.isHidden = true
            }else{
                self.thumbnailV.isHidden = true
                self.shimmerContainerV.isHidden = false
            }
            
            if self.news.count > 0{
                self.firstNews = news.removeFirst()
                if let newsUrlStr = self.firstNews!.photo {
                    if let url = URL(string: String(format: "https://adm.happypuppy.id/%@", newsUrlStr)) {
                        self.thumbnailNewsIV.af.setImage(withURL: url)
                    }
                }
                self.thumbnailNewsTitleL.text = self.firstNews!.title
                self.thumbnailNewsDescL.attributedText = self.firstNews!.description?.htmlToAttributedString
                self.thumbnailNewsDateL.text = Global.shared.fromDateTimeToDate(self.firstNews!.eventDate!)
            }
            self.newsCV.reloadData()
        }
    }
    
    private var firstNews:News?
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.shimmerContainerV.contentView = self.shimmerContentV
        
        self.thumbnailNewsIV.layer.cornerRadius = 10
        
        self.newsCV.delegate = self
        self.newsCV.dataSource = self
        self.newsCV.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        self.newsCV.register(UINib(nibName: "Shimmer_News_HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShimmerCell")
        self.newsCV.register(UINib(nibName: "News_HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewsCell")
    }
    
    @IBAction func moveToDetailAction(_ sender: Any) {
        if let firstNews = self.firstNews{
            self.delegate?.didNewsSelected(news: firstNews)
        }
    }
}

extension News_HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isNewsLoaded{
            return self.news.count
        }else{
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 208)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isNewsLoaded{
            self.delegate?.didNewsSelected(news: self.news[indexPath.item])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.isNewsLoaded{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! News_HomeCollectionViewCell
            cell.news = self.news[indexPath.item]
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
    }
}
