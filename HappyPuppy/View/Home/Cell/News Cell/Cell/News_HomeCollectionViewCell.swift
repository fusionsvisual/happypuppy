//
//  News_HomeCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class News_HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var newsIV: UIImageView!
    @IBOutlet var newsDateL: UILabel!
    @IBOutlet var newsTitleL: UILabel!
    
    var news: News? = nil{
        didSet{
            guard let news = self.news else {return}
            self.newsIV.image = UIImage()
            if let newsUrlStr = news.photo {
                if let newsUrl = URL(string: String(format: "https://adm.happypuppy.id/%@", newsUrlStr)) {
                    self.newsIV.af.setImage(withURL: newsUrl)
                }
            }
            self.newsDateL.text = Global.shared.fromDateTimeToDate(news.eventDate!)
            self.newsTitleL.text = news.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.newsIV.layer.cornerRadius = 10
    }

}
