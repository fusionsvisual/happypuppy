//
//  Voucher_HomeViewTableViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Voucher_HomeTableViewCell: UITableViewCell {

    @IBOutlet var voucherCV: UICollectionView!
    
    var delegate: HomeDelegate?
    var isVoucherLoaded = false
    var vouchers:[Voucher] = [] {
        didSet{
            self.voucherCV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.voucherCV.delegate = self
        self.voucherCV.dataSource = self
        self.voucherCV.contentInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        self.voucherCV.register(UINib(nibName: "Voucher_HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VoucherCell")
        self.voucherCV.register(UINib(nibName: "Shimmer_Voucher_HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShimmerCell")

    }
}

extension Voucher_HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.isVoucherLoaded{
            return self.vouchers.count
        }else{
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isVoucherLoaded{
            self.delegate?.didVoucherSelected(voucher: self.vouchers[indexPath.item])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.isVoucherLoaded{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VoucherCell", for: indexPath) as! Voucher_HomeCollectionViewCell
            cell.voucher = self.vouchers[indexPath.item]
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShimmerCell", for: indexPath)
            return cell
        }
    }
}
