//
//  Voucher_HomeCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Voucher_HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var voucherIV: UIImageView!
    
    @IBOutlet var voucherTitleL: UILabel!
    @IBOutlet var voucherPointL: UILabel!
    
    var voucher: Voucher? = nil{
        didSet{
            guard let voucher = self.voucher else{return}
            self.voucherIV.image = UIImage()
            if let voucherUrlStr = voucher.photoUrl{
                if let voucherUrl = URL(string: String(format: "https://adm.happypuppy.id/%@", voucherUrlStr)) {
                    self.voucherIV.af.setImage(withURL: voucherUrl)
                }
            }
            self.voucherTitleL.text = voucher.name
            self.voucherPointL.text = String(format: "%@ POIN", voucher.redeemPoint)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
    
    private func setupViews(){
        self.voucherIV.layer.cornerRadius = 10
    }

}
