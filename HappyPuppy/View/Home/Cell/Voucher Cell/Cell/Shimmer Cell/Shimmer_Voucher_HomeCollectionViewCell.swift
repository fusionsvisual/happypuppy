//
//  Shimmer_Voucher_HomeCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit

class Shimmer_Voucher_HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var shimmerContainerV: PuppyClubShimmeringView!
    @IBOutlet var shimmerContentV: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shimmerContainerV.contentView = self.shimmerContentV
    }

}
