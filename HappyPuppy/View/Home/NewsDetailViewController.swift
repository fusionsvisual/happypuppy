//
//  NewsDetailViewController.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 24/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var newsDetailImage: UIImageView!
    @IBOutlet weak var newsDetailTitle: UILabel!
    @IBOutlet weak var newsDetailDate: UILabel!
    @IBOutlet weak var newsDetailDescription: UILabel!
    
    var news: News?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Berita"

        if let photoUrl = news?.photo {
            if let url = URL(string: "https://adm.happypuppy.id/\(photoUrl)") {
                newsDetailImage.af.setImage(withURL: url)
            }
        }
        newsDetailTitle.text = news?.title
        
        if let date = news?.eventDate! {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let finaleDate = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd LLLL yyyy"
            newsDetailDate.text = dateFormatter.string(from: finaleDate!)
        }
        
        newsDetailDescription.text = news?.description?.htmlToString
    }

}
