//
//  VoucherCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 20/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class HomeVoucherCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var redeemPoint: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        image.layer.masksToBounds = true
        image.layer.cornerRadius = 10
        
        name.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}
