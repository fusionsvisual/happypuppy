//
//  HeaderCollectionViewCell.swift
//  HappyPuppy
//
//  Created by Samuel Krisna on 19/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var headerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
