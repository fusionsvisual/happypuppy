//
//  Ver2_HomeViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 30/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation

protocol HomeDelegate{
    func didCitySelected(city: City)
    func didNewsSelected(news: News)
    func didVoucherSelected(voucher: Voucher)
}

class HomeViewController: UIViewController {

    @IBOutlet var contentTV: UITableView!
    
    private var isBannerLoaded = false
    private var banners: [Banner] = []
    
    private var cities:[City] = [
        City(id: nil, name: "Semua Kota")
    ] {
        didSet{
            self.contentTV.reloadSections(IndexSet.init(integer: 0), with: .none)
        }
    }
    private var selectedCity:City?
    
    private var isOutletLoaded = false
    private var outletItems:[OutletItem] = []
    
    private var isVoucherLoaded = false
    private var vouchers:[Voucher] = []
    
    private var isNewsLoaded = false
    private var news:[News] = []
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        self.clearData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.setupData()  
    }
    
    private func setupViews(){
        self.contentTV.register(UINib(nibName: "Header_HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        self.contentTV.register(UINib(nibName: "OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "OutletCell")
        self.contentTV.register(UINib(nibName: "Inner_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "InnerCell")
        self.contentTV.register(UINib(nibName: "Shimmer_OutletTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerCell")
        
        self.contentTV.register(UINib(nibName: "Voucher_HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "VoucherCell")
        self.contentTV.register(UINib(nibName: "News_HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
    }
    
    private func setupData(){
        HomeController.shared.getBannerList { (banners, error) in
            if let error = error{
                SVProgressHUD.showError(withStatus: error)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            }else{
                guard let banners = banners else{return}
                self.isBannerLoaded = true
                self.banners = banners
                self.contentTV.reloadSections(IndexSet.init(integer: 0), with: .none)
            }
        }
        
        HomeController.shared.getCities { (cityList, error) in
            guard let cityList = cityList else{return}
            self.cities.append(contentsOf: cityList)
            self.setupOutletData()
        }
        
        VoucherController.shared.getVoucherList(length: nil, start: nil) { (status, voucherList, totalCount, error) in
            guard let voucherList = voucherList else{return}
            self.isVoucherLoaded = true
            self.vouchers = voucherList
            self.contentTV.reloadData()
        }
        
        HomeController.shared.getNews(length: nil, start: nil) { (newsList, error) in
            guard let newsList = newsList else{return}
            self.isNewsLoaded = true
            self.news = newsList
            self.contentTV.reloadData()
        }
    }

    private func fetchOutletList() {
        HomeController.shared.getOutletList(date: nil, length: 3, start: 0, city: self.selectedCity, latitude: UserDefaults.standard.double(forKey: "currentLatitude"), longitude: UserDefaults.standard.double(forKey: "currentLongitude")) { (total, outletList, error) in
            
            self.isOutletLoaded = true
            self.outletItems.removeAll()
            
            if let outletList = outletList{
                for outlet in outletList {
                    // MARK: Create outlet item for outlet schedule cell
                    let outletItem = OutletItem(outlet: outlet, isOpened: false)
                    self.outletItems.append(outletItem)
                }
            }
            self.contentTV.reloadData()
        }
    }

    private func setupOutletData(){
        var locationPermission = CLLocationManager.authorizationStatus()
        if #available(iOS 14, *) {
            locationPermission = locationManager.authorizationStatus
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if locationPermission == .authorizedAlways
            || locationPermission == .authorizedWhenInUse {
            locationManager.requestLocation()
            return
        }
        locationManager.requestAlwaysAuthorization()
    }

    private func clearData(){
        self.isBannerLoaded = false
        self.banners.removeAll()
        
        self.cities.removeSubrange(1..<self.cities.count)
        
        self.clearOutletData()
        
        self.isVoucherLoaded = false
        self.vouchers.removeAll()
        
        self.isNewsLoaded = false
        self.news.removeAll()
                
        self.contentTV.reloadData()
    }
    
    private func clearOutletData(){
        self.isOutletLoaded = false
        self.outletItems.removeAll()
    }
    
    @objc func didSeeAllOutlet(){
        if let selectedCity = self.selectedCity{
            NotificationCenter.default.post(name: ObserverName.SEE_ALL_OUTLET, object: nil, userInfo: ["selected_city": selectedCity])
        }else{
            NotificationCenter.default.post(name: ObserverName.SEE_ALL_OUTLET, object: nil, userInfo: [:])
        }
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isOutletLoaded{
            return self.outletItems.count + 3
        }else{
            return 3 + 3
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.isOutletLoaded{
            if section > 0, section == self.outletItems.count{
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 35))
                button.setTitle("Selengkapnya", for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
                button.setTitleColor(Color.BUTTON_PRIMARY, for: .normal)
                button.addTarget(self, action: #selector(didSeeAllOutlet), for: .touchUpInside)
                return button
            }
        }else{
            return UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 24))
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.isOutletLoaded{
            if section > 0, section == self.outletItems.count{
               return 35 + 24
            }
        }else{
            if section == 3{
                return 24
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section > 0 && section <= self.outletItems.count{
            if self.isOutletLoaded{
                if self.outletItems[section-1].isOpened{
                    return 2
                }
            }
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! Header_HomeTableViewCell
            cell.delegate = self
            cell.isBannerLoaded = self.isBannerLoaded
            cell.banners = self.banners
            cell.cities = self.cities
            cell.selectedCity = self.selectedCity
            return cell
        }else{
            if self.isOutletLoaded{
                if indexPath.section == self.outletItems.count + 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherCell", for: indexPath) as! Voucher_HomeTableViewCell
                    cell.delegate = self
                    cell.isVoucherLoaded = self.isVoucherLoaded
                    cell.vouchers = self.vouchers
                    return cell
                }else if indexPath.section == self.outletItems.count + 2{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! News_HomeTableViewCell
                    cell.delegate = self
                    cell.isNewsLoaded = self.isNewsLoaded
                    cell.news = self.news
                    return cell
                }else{
                    if indexPath.item == 0{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "OutletCell", for: indexPath) as! OutletTableViewCell
                        cell.delegate = self
                        cell.section = indexPath.section
                        cell.outletItem = self.outletItems[indexPath.section - 1]
                        return cell
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCell", for: indexPath) as! Inner_OutletTableViewCell
                        if let outlet = outletItems[indexPath.section - 1].outlet{
                            cell.schedules = outlet.schedule ?? []
                        }
                        return cell
                    }
                }
            }else{
                if indexPath.section == 4{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherCell", for: indexPath) as! Voucher_HomeTableViewCell
                    cell.delegate = self
                    cell.isVoucherLoaded = self.isVoucherLoaded
                    cell.vouchers = self.vouchers
                    return cell
                }else if indexPath.section == 5{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! News_HomeTableViewCell
                    cell.delegate = self
                    cell.isNewsLoaded = self.isNewsLoaded
                    cell.news = self.news
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerCell", for: indexPath) as! Shimmer_OutletTableViewCell
                    return cell
                }
            }
        }
    }
}

extension HomeViewController: HomeDelegate{
    func didVoucherSelected(voucher: Voucher) {
        let voucherVC = VoucherDetailViewController()
        voucherVC.voucher = voucher
        self.navigationController?.pushViewController(voucherVC, animated: true)
    }
    
    func didNewsSelected(news: News) {
        let newsVC = NewsDetailViewController()
        newsVC.news = news
        self.navigationController?.pushViewController(newsVC, animated: true)
    }
    
    func didCitySelected(city: City) {
        self.selectedCity = city
        self.clearOutletData()
        self.contentTV.reloadData()
        self.setupOutletData()
    }
}

extension HomeViewController: OutletDelegate{
    func goToOutletDetail(outlet: Outlet) {
        let detailOutletVC = DetailOutletViewController()
        detailOutletVC.outlet = outlet
        detailOutletVC.operational = outlet.operational
        self.navigationController?.pushViewController(detailOutletVC, animated: true)
    }
    
    func goToReservation(outlet: Outlet){
        let reservationVC = ReservationViewController()
        reservationVC.outlet = outlet
        self.navigationController?.pushViewController(reservationVC, animated: true)
    }
    
    func didSeeSchedule(section: Int) {
        self.outletItems[section - 1].isOpened = !self.outletItems[section - 1].isOpened
        let sections = IndexSet.init(integer: section)
        self.contentTV.reloadSections(sections, with: .automatic)
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        var locationPermission = CLLocationManager.authorizationStatus()
        if #available(iOS 14, *) {
            locationPermission = manager.authorizationStatus
        }
        if locationPermission == .authorizedAlways
            || locationPermission == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }

    func locationManager(
        _ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation]
    ) {
        guard let location = locations.last else { return }
        UserDefaults.standard.set(
            location.coordinate.latitude,
            forKey: "currentLatitude"
        )
        UserDefaults.standard.set(
            location.coordinate.longitude,
            forKey: "currentLongitude"
        )
        fetchOutletList()
    }
    
    func locationManager(
        _ manager: CLLocationManager,
        didFailWithError error: Error
    ) {
        fetchOutletList()
    }
}
