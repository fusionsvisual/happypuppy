//
//  TabBarViewController.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 13/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleSignIn

class TabBarViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet var topBarTopConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet var badgeContainer: UIView!
    @IBOutlet var verifProgressContainerV: UIView!
    
    @IBOutlet var userAvatarViewContainer: UIView!
    @IBOutlet var userAvatarImageView: UIImageView!
    @IBOutlet var verifProgressVs: [UIView]!

    @IBOutlet var tabBarLabels: [UILabel]!
    @IBOutlet var tabBarButtons:[UIButton]!
    
    /* UILabel */
    @IBOutlet var verifProcessHeaderL: UILabel!
    @IBOutlet var verifProgressLs: [UILabel]!
    @IBOutlet var badgeCountLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    @IBOutlet var memberTypeLabel: UILabel!
    /*-----*/
    
    @IBOutlet var verifiedIV: UIImageView!
    
    private var verifiedItems:[String] = []
    
    var homeViewController:UIViewController = HomeViewController()
    var outletViewController:UIViewController = OutletViewController()
    var myReservationViewController:UIViewController = MyReservationViewController()
    var prizeViewController:UIViewController = PrizeViewController()
    var memberViewController:UIViewController = MemberViewController()
    
    var viewControllers: [UIViewController]!
    
    var selectedIndex: Int = 0
    
    /* Change status bar color */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerNotification()
        
        viewControllers = [homeViewController, outletViewController, myReservationViewController, prizeViewController, memberViewController]
        
        /* Setup default selected tab */
        tabBarButtons[selectedIndex].isSelected = true
        didPressTab(tabBarButtons[selectedIndex])
        setupViews()
        setupTopBar()
        self.setupObserver()
    }
    
    private func registerNotification(){
        /* Notification Listener for Update Profile from MemberVC */
        NotificationCenter.default.addObserver(self, selector: #selector(reloadView), name: NSNotification.Name(rawValue: "reloadMemberView"), object: nil)
        
        //* Refresh profile data on appBecomeActive
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshProfile), name: NotificationConstant.REFRESH_PROFILE, object: nil)
    }
    
    private func setupNavigationController(){
        navigationController?.isNavigationBarHidden = true
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupTopBar()
            
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        self.registerNotification()
        self.setupData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NotificationConstant.REFRESH_PROFILE, object: nil)
    }
    
    private func setupData(){
        self.setuoNotifCountData()
        ProfileController.shared.getProfileDetail { (status, user, error) in
            if let error = error{
                if error == ErrorMessage.UNAUTHORIZED{
                    SVProgressHUD.showError(withStatus: ErrorMessage.UNAUTHORIZED)
                    SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT) {
                        self.logoutProcess()
                    }
                }
            }else{
                self.setupTopBar()
            }
        }
    }
    
    @objc
    private func refreshProfile(){
        self.setupData()
    }
    
    private func setuoNotifCountData(){
        NotificationController.shared.getUnreadNotifCount { (status, message, count) in
            if status == NetworkStatus.FAILURE{
                SVProgressHUD.showError(withStatus: message)
                SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
                                    
                if message == ErrorMessage.UNAUTHORIZED{
                    GIDSignIn.sharedInstance().disconnect()
                    GIDSignIn.sharedInstance().signOut()
                    UserDefaultHelper.shared.clearUser()
                    
                    let loginVC = LoginViewController()
                    loginVC.modalPresentationStyle = .fullScreen
                    self.present(loginVC, animated: true, completion: nil)
                }
            }else{
                if let count = count{
                    if count > 0{
                        UserDefaultHelper.shared.setUnreadCount(count: count)
                        self.badgeContainer.isHidden = false
                        self.badgeCountLabel.text = String(count)
                    }
                }
            }
        }
    }
    
    @objc private func reloadView(){
        self.setupTopBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    
        self.setupUnreadBadge()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupViews(){
        setupNavigationController()
        
        userAvatarViewContainer.layer.cornerRadius = userAvatarViewContainer.frame.height/2
        userAvatarImageView.layer.cornerRadius = userAvatarImageView.frame.height/2
        
        /* Setup userAvatarViewContainer UITapGesture */
        let gesture = UITapGestureRecognizer(target: self, action: #selector(profileClicked))
        userAvatarViewContainer.addGestureRecognizer(gesture)
        /*-----*/
        
        badgeContainer.layer.cornerRadius = badgeContainer.frame.height/2
        
        // Handler for < iOS 13
        guard #available(iOS 13.0, *) else{
            self.topBarTopConstraint.constant = -24
            return
        }
        
        for verifProgressV in self.verifProgressVs {
            verifProgressV.layer.cornerRadius = 2
            verifProgressV.backgroundColor = Color.VERIFICATION.DEFAULT
        }
        
        for verifProgressL in self.verifProgressLs {
            verifProgressL.textColor = Color.VERIFICATION.DEFAULT
        }
    }
    
    private func setupObserver(){
        // MARK: Setup observer for see all outlet from HomeViewController
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSeeAllOutletTapped), name: ObserverName.SEE_ALL_OUTLET, object: nil)
    }
    
    private func setupUnreadBadge(){
        // MARK: Setup Notification Unread Count Locally
        let unreadCount = UserDefaultHelper.shared.getUnreadCount()
        if unreadCount > 0 {
            self.badgeContainer.isHidden = false
            self.badgeCountLabel.text = "\(unreadCount)"
        }else{
            self.badgeContainer.isHidden = true
        }
        // END
    }
    
    private func setupTopBar(){
        /* Setup UILabel */
        if let imageURL = URL(string: "\(Network.shared.getBaseURL())/user_uploads/\(UserDefaultHelper.shared.getPhotoUrl())"){
            userAvatarImageView.af.setImage(withURL: imageURL)
        }
        
        let firstName = UserDefaultHelper.shared.getFirstName()
        self.nameLabel.text = firstName
        self.pointLabel.text = "Anda memiliki \(UserDefaultHelper.shared.getPoint()) pts"
        
        self.memberTypeLabel.text = UserDefaultHelper.shared.getMemberType().capitalizingFirstLetter()
        self.memberTypeLabel.textColor = UIColor(hexString: UserDefaultHelper.shared.getMemberColor())
        self.userAvatarViewContainer.backgroundColor = UIColor(hexString: UserDefaultHelper.shared.getMemberColor())
                
        // MARK: Setup Verification Progress
        self.verifiedItems.removeAll()
        
        if UserDefaultHelper.shared.getEmailStatus(){
            self.verifiedItems.append("EMAIL")
        }
        
        if UserDefaultHelper.shared.getPhoneStatus(){
            self.verifiedItems.append("PHONE_NUMBER")
        }

        if UserDefaultHelper.shared.getIdCardStatus() == IdentityCardStatus.APPROVED || !UserDefaultHelper.shared.getIsRequireIdCard(){
            self.verifiedItems.append("ID_CARD")
        }
                        
        if self.verifiedItems.count < 3{
            for idx in 0..<self.verifiedItems.count {
                if idx == 0{
                    self.verifProgressVs[idx].backgroundColor = Color.VERIFICATION.EMAIL
                    self.verifProgressLs[idx].textColor = Color.VERIFICATION.EMAIL
                }else{
                    self.verifProgressVs[idx].backgroundColor = Color.VERIFICATION.PHONE_NUMBER
                    self.verifProgressLs[idx].textColor = Color.VERIFICATION.PHONE_NUMBER
                }
            }
            self.verifProcessHeaderL.text = String(self.verifiedItems.count) + "/3 Proses Verifikasi Data Anda"
        }else{
            self.verifiedIV.isHidden = false
            self.verifProgressContainerV.isHidden = true
        }
        
        /*-----*/
    }
    
    @objc func didSeeAllOutletTapped(notification: NSNotification) {
        self.moveView(currentIndex: 1)
        if let userInfo = notification.userInfo{
            // MARK: Update selected city on OutletVC
            NotificationCenter.default.post(name: ObserverName.SET_OUTLET_CITY, object: nil, userInfo: userInfo)
        }
    }
    
    @IBAction func didPressTab(_ sender: UIButton) {
        if sender.tag == 3{
            let idCardStatus = UserDefaultHelper.shared.getIdCardStatus()
            if idCardStatus == IdentityCardStatus.NOT_VERIFIED || idCardStatus == IdentityCardStatus.REJECTED {
                let identityCardPopUpVC = IdentityCardPopUpViewController()
                identityCardPopUpVC.delegate = self
                identityCardPopUpVC.IDCardStatus = UserDefaultHelper.shared.getIdCardStatus()
                identityCardPopUpVC.IDCardNote = UserDefaultHelper.shared.getidCardNote()
                self.present(identityCardPopUpVC, animated: true) {
                    identityCardPopUpVC.showModal()
                }
            }
        }
        self.moveView(currentIndex: sender.tag)
    }
    
    private func moveView(currentIndex:Int){
        let previousIndex = selectedIndex
        tabBarLabels[previousIndex].textColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.5)
        tabBarButtons[previousIndex].tintColor = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 0.5)
        tabBarButtons[previousIndex].isSelected = false
        
        /* Remove Previous View Controller */
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        selectedIndex = currentIndex
        tabBarLabels[selectedIndex].textColor = .black
        tabBarButtons[selectedIndex].tintColor = .black
        tabBarButtons[selectedIndex].isSelected = true
        
        /* Show Selected View Controller */
        let selectedVC = viewControllers[selectedIndex]
        addChild(selectedVC)
        selectedVC.view.frame = contentView.bounds
        contentView.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)
        
        self.setuoNotifCountData()
    }
    
    private func logoutProcess(){
        SVProgressHUD.show()
        AuthenticationContoller.shared.logout { (status, error) in
            SVProgressHUD.dismiss()
            
            GIDSignIn.sharedInstance().disconnect()
            GIDSignIn.sharedInstance().signOut()
            UserDefaultHelper.shared.clearUser()
            
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: true, completion: nil)
        }
    }
    
    @objc private func profileClicked(){
        self.moveView(currentIndex: 4)
    }
   
    @IBAction func accountVerifAction(_ sender: Any) {
        self.navigationController?.pushViewController(AccountVerificationViewController(), animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        navigationController?.pushViewController(NotificationsViewController(), animated: true)
    }
}

extension TabBarViewController: UploadIdentityCardDelegate{
    func didUploadClicked() {
        let uploadIdentityCardVC = UploadIdentityCardViewController()
        self.navigationController?.pushViewController(uploadIdentityCardVC, animated: true)
    }
}
