//
//  ReservationHelper.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation
import SVProgressHUD

class ReservationHelper{
    static let shared = ReservationHelper()
    
    func checkVerification(outlet: Outlet) -> Bool{
        //* Show error message if the reservation payment type is true && phone verification status is false
        if (outlet.reservationPaymentType && !UserDefaultHelper.shared.getPhoneStatus()){
            SVProgressHUD.showError(withStatus: ErrorMessage.PHONE_NOT_VERIFIED)
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return false
        }
        
        if (!outlet.reservationPaymentType){
            
            guard let user = UserDefaultHelper.shared.getCurrentUser() else {return false}
            
            if user.phone?.prefix(3) == "+62" {
                //* If reservation payment type is "false", for indonesian people phone and email must be verified
                if user.isVerified && user.isPhoneVerified{
                    return true
                }
            }else{
                //* If reservation payment type is "false", for foreign people phone and email must be verified
                if user.isVerified {
                    return true
                }
            }
            
            //* Show error message
            SVProgressHUD.showInfo(withStatus: ErrorMessage.EMAIL_AND_PHONE_NOT_VERIFIED)
            SVProgressHUD.dismiss(withDelay: DelayConstant.SHORT)
            return false
        }
        
        //* Return true if Reservation Payment Type is "true" && phone verification status is true
        return true
    }
}
