//
//  OutletHelper.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/05/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation

struct OperationalStatus {
    static let CLOSE = "Tutup"
    static let OPEN = "Open"
//    static let OPEN_STATUS = ""
}

struct OperationalData {
    var openStatus = ""
    var statusColor: UIColor?
    
    var openDesc = ""
    
    var reservTitleColor = UIColor(cgColor: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1))
    var reservBorderColor = UIColor(cgColor: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)).cgColor
    
    var isBtnEnabled = false
}

class OutletHelper{
    private var outletData: OperationalData?
    
    func checkOutletDateTime1(outlet: Outlet) -> OperationalData{
        self.outletData = OperationalData()

        if let operational = outlet.operational{
            if let status = outlet.operational!.isOpen{
                var isTodayOpen = false
                if status == OutletStatus.OPEN{
                    isTodayOpen = self.setupOutletOpenStatus1(date: operational.date!, timeStart: operational.timeStart!, timeEnd: operational.timeEnd!)
                }
                
                if !isTodayOpen{
                    let isYesterdayOpen = self.setupOutletOpenStatus1(date: operational.dateYesterday!, timeStart: operational.timeStartYesterday!, timeEnd: operational.timeEndYestarday!)

                    if !isYesterdayOpen {
                        self.outletData!.openStatus = OperationalStatus.CLOSE
                        self.outletData!.statusColor = UIColor(red: 233/255, green: 26/255, blue: 0/255, alpha: 1)

                        if operational.isOpen == OutletStatus.OPEN{
                            self.outletData!.openDesc = "Buka jam \(operational.timeStart!)"
                        }else{
                            self.outletData!.openDesc = "Libur"
                        }

                        //* Always enable reservation button if reservationPaymentType == True
                        if !outlet.reservationPaymentType{
                            self.outletData!.isBtnEnabled = false
                            return self.outletData!
                        }
                    }
                }
                
                self.outletData!.isBtnEnabled = true
            }
        }
        
        return self.outletData!
    }
    
    private func setupOutletOpenStatus1(date: String, timeStart: String, timeEnd: String) -> Bool{
        let currentDate = Date()
 
        //* Convert timeStart to date with UTC Timezone
        let openTimeStr = String(format: "%@ %@", date, timeStart)
        let openTimeDate = DateFormatterHelper.shared.stringToDate(dateString: openTimeStr, dateFormat: "yyyy-MM-dd HH:mm", timezone: nil)
        
        //* Convert timeEnd to date with UTC Timezone
        let closeTimeStr = String(format: "%@ %@", date, timeEnd)
        let closeTimeDate = DateFormatterHelper.shared.stringToDate(dateString: closeTimeStr, dateFormat: "yyyy-MM-dd HH:mm", timezone: nil)
        
        guard let openTimeDate = openTimeDate, var closeTimeDate = closeTimeDate else {return false}
        
        if openTimeDate > closeTimeDate{
            closeTimeDate.addTimeInterval(86400)
        }
        
        if (currentDate > openTimeDate && currentDate < closeTimeDate){
            let currentDate = Date()
                        
            if (closeTimeDate.timeIntervalSince(currentDate) < 3600){
                self.outletData!.openStatus = "Segera Tutup"
                self.outletData!.statusColor = UIColor(hexString: "#FFA724")

                self.outletData!.openDesc = "Tutup jam \(timeEnd)"
                return true
            }

            self.outletData!.openStatus = "Buka"
            self.outletData!.statusColor = UIColor(hexString: "#008000")

            self.outletData!.openDesc = "Tutup jam \(timeEnd)"
            return true
        }
        
        return false
    }
    
    func checkOutletDateTime(outlet: Outlet, openStatusL: UILabel, openDescL: UILabel, reservationV: UIView, reservationBtn: UIButton){
        if let operational = outlet.operational{
            if let status = outlet.operational!.isOpen{
                
                var isTodayOpen = false
                if status == OutletStatus.OPEN{
                    isTodayOpen = self.setupOutletOpenStatus(date: operational.date!, timeStart: operational.timeStart!, timeEnd: operational.timeEnd!, openStatusL: openStatusL, openDescL: openDescL)
                }
                
                if !isTodayOpen{
                    let isYesterdayOpen = self.setupOutletOpenStatus(date: operational.dateYesterday!, timeStart: operational.timeStartYesterday!, timeEnd: operational.timeEndYestarday!, openStatusL: openStatusL, openDescL: openDescL)
                    
                    if !isYesterdayOpen {
                        openStatusL.text = "Tutup"
                        openStatusL.textColor = UIColor(red: 233/255, green: 26/255, blue: 0/255, alpha: 1)

                        if operational.isOpen == OutletStatus.OPEN{
                            openDescL.text = "Buka jam \(operational.timeStart!)"
                        }else{
                            openDescL.text = "Libur"
                        }
                        
                        //* Always enable reservation button if reservationPaymentType == True
                        if !outlet.reservationPaymentType{
                            self.disableReservationBtn(reservationV: reservationV, reservationBtn: reservationBtn)
                            return
                        }
                    }
                }
                
                self.enableReservationBtn(reservationV: reservationV, reservationBtn: reservationBtn)
            }
        }
    }
    
    private func setupOutletOpenStatus(date: String, timeStart: String, timeEnd: String, openStatusL: UILabel, openDescL: UILabel) -> Bool{
        let currentDate = Date()
 
        //* Convert timeStart to date with UTC Timezone
        let openTimeStr = String(format: "%@ %@", date, timeStart)
        let openTimeDate = DateFormatterHelper.shared.stringToDate(dateString: openTimeStr, dateFormat: "yyyy-MM-dd HH:mm", timezone: nil)
        
        //* Convert timeEnd to date with UTC Timezone
        let closeTimeStr = String(format: "%@ %@", date, timeEnd)
        let closeTimeDate = DateFormatterHelper.shared.stringToDate(dateString: closeTimeStr, dateFormat: "yyyy-MM-dd HH:mm", timezone: nil)
        
        guard let openTimeDate = openTimeDate, var closeTimeDate = closeTimeDate else {return false}
        
        if openTimeDate > closeTimeDate{
            closeTimeDate.addTimeInterval(86400)
        }
        
        if (currentDate > openTimeDate && currentDate < closeTimeDate){
            let currentDate = Date()
                        
            //* Check if the outlet close soon
            if (closeTimeDate.timeIntervalSince(currentDate) < 3600){
                openStatusL.text = "Segera Tutup"
                openStatusL.textColor = UIColor(hexString: "#FFA724")
                openDescL.text = "Tutup jam \(timeEnd)"
                
                return true
            }

            //* Do if outlet open and not close soon
            openStatusL.text = "Buka"
            openStatusL.textColor = UIColor(hexString: "#008000")
            openDescL.text = "Tutup jam \(timeEnd)"
            
            return true
        }
        
        return false
    }
    
    private func disableReservationBtn(reservationV: UIView, reservationBtn: UIButton){
        reservationBtn.isEnabled = false
        reservationV.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25).cgColor
        reservationBtn.setTitleColor(UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25), for: .normal)
    }
    
    private func enableReservationBtn(reservationV: UIView, reservationBtn: UIButton){
        reservationBtn.isEnabled = true
        reservationV.layer.borderColor = UIColor(cgColor: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)).cgColor
        reservationBtn.setTitleColor(UIColor(cgColor: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)), for: .normal)
    }
}
