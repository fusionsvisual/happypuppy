//
//  AppDelegate.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 12/05/20.
//  Copyright © 2020 FusionsVisual. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import SVProgressHUD
import Firebase
import GoogleSignIn
import FirebaseMessaging

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate{

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        SVProgressHUD.setDefaultMaskType(.black)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        var rootViewContoller:UIViewController = AuthenticationMenuViewController()
        if UserDefaultHelper.shared.getToken() != "" && UserDefaultHelper.shared.getCurrentUser() != nil{
            rootViewContoller = TabBarViewController()
        }
        let rootNavigationController = UINavigationController.init(rootViewController: rootViewContoller)
        rootNavigationController.isNavigationBarHidden = true
        
        window?.rootViewController = rootNavigationController
        window?.makeKeyAndVisible()

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
//        IQKeyboardManager.shared.disabledToolbarClasses = [ReservationViewController.self]
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        GIDSignIn.sharedInstance().clientID = GIDSignInConstant.CLIENT_ID
        GIDSignIn.sharedInstance().delegate = self
    
        return true
    }
    
     func application(_ application: UIApplication,
                      open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
       return GIDSignIn.sharedInstance().handle(url)
     }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }else{
        let accessToken = user.authentication.idToken
        let fullName = user.profile.name
        let familyName = user.profile.familyName
        let email = user.profile.email
         let userInfo:[String: Any?] = [
            "access_token": accessToken,
            "full_name": fullName,
            "last_name": familyName,
            "email": email
        ]
        NotificationCenter.default.post(name: NotificationConstant.GOOGLE_LOGIN, object: nil, userInfo: userInfo as [AnyHashable : Any])
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print("USER DISCONNECTED")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print("FCM TOKEN \(deviceToken)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("MESSAGE RECEIVED")
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("MESSAGE RECEIVED")
        print(userInfo)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        //* Refresh profile after app go to background. For deeplink purposes
        NotificationCenter.default.post(name: NotificationConstant.REFRESH_PROFILE, object: nil)
    }
    
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool {
        
        // Determine who sent the URL.
        let sendingAppID = options[.sourceApplication]
        print("source application = \(sendingAppID ?? "Unknown")")
        
        // Process the URL.
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
            let albumPath = components.path,
            let params = components.queryItems else {
                print("Invalid URL or album path missing")
                return false
        }
        
        if let photoIndex = params.first(where: { $0.name == "index" })?.value {
            print("albumPath = \(albumPath)")
            print("photoIndex = \(photoIndex)")
            return true
        } else {
            print("Photo index missing")
            return false
        }
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo

    // Print full message.
    print(userInfo)

    completionHandler()
  }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
  // [START refresh_token]
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    print("Firebase registration token: \(fcmToken)")
    
//    let dataDict:[String: String] = ["token": fcmToken]
//    NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
  }
  // [END refresh_token]
}


