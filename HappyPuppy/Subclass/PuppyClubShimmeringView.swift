//
//  PuppyClubShimmeringView.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 28/03/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import UIKit
import Shimmer

class PuppyClubShimmeringView: FBShimmeringView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews(){
        self.shimmeringSpeed = 500
        self.isShimmering = true
    }
}
