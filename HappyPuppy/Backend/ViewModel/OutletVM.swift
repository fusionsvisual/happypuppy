//
//  OutletVM.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class OutletVM {
    static let shared = OutletVM()
    let outlet:PublishSubject<Outlet> = PublishSubject<Outlet>()
    
    let roomTypes:PublishSubject<[RoomTypePrice]> = PublishSubject<[RoomTypePrice]>()
    let promos:PublishSubject<[OutletPromo]> = PublishSubject<[OutletPromo]>()
    let errMsg:PublishSubject<String> = PublishSubject<String>()

    func getOutletDetail(outletId:String){
        let endpoint = String(format: "/outlet/view/%@", outletId)
        let params:[String: Any] = [:]
        APIService(isPrivate: false).get(url: endpoint, params: params) { (status, data, errMsg) in
            if let errMsg = errMsg{
                self.errMsg.onNext(errMsg)
            }
            do{
                let json = try JSON(data: data!)
                let outlet = try JSONDecoder().decode(Outlet.self, from: json["data"]["data"].rawData())
                self.outlet.onNext(outlet)
            }catch(let err){
                self.errMsg.onNext(err.localizedDescription)
            }
        }
    }
    
    func getAllOutletPromos(outletCode:String){
        let endpoint = String(format: "/promo/findByOutlet")
        let params:[String: Any] = [
            "outlet": outletCode
        ]
        APIService(isPrivate: false).get(url: endpoint, params: params) { (status, data, errMsg) in
            if let errMsg = errMsg{
                self.errMsg.onNext(errMsg)
            }
            do{
                let json = try JSON(data: data!)
                let promos = try JSONDecoder().decode([OutletPromo].self, from: json["data"]["data"].rawData())
                self.promos.onNext(promos)
            }catch(let err){
                self.errMsg.onNext(err.localizedDescription)
            }
        }
    }
    
    func getAllOutletRoomTypes(outletCode: String?, date:String?, hour:String?){
        let endpoint = String(format: "/outlet/roomtypeprice")
        var params:[String: Any] = [:]
        
        if let outletCode = outletCode{
            params["code"] = outletCode
        }
        
        if let date = date{
            params["tanggal"] = date
        }
        
        if let hour = hour{
            params["hour"] = hour
        }
        
        APIService(isPrivate: false).get(url: endpoint, params: params) { (status, data, errMsg) in
            if let errMsg = errMsg{
                self.errMsg.onNext(errMsg)
            }
            do{
                let json = try JSON(data: data!)
                let roomTypes = try JSONDecoder().decode([RoomTypePrice].self, from: json["data"].rawData())
                self.roomTypes.onNext(roomTypes)
            }catch(let err){
                self.errMsg.onNext(err.localizedDescription)
            }
        }
    }
}
