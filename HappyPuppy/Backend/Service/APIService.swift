//
//  APIService.swift
//  HappyPuppy
//
//  Created by Filbert Hartawan on 19/06/21.
//  Copyright © 2021 FusionsVisual. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import SwiftyJSON

struct NetworkStatus {
    static let SUCCESS = "SUCCESS"
    static let FAILURE = "FAILURE"
}

struct ErrMsg{
    static let UNAUTHORIZED = "UNAUTHORIZED"
    static let NO_INTERNET_CONNECTION = "NO_INTERNET_CONNECTION"
}

class APIService {
    private var isPrivate = false
    private var maxRetryCount = 10
    private var loadCount = 0
    
    init(isPrivate: Bool) {
        self.isPrivate = isPrivate
    }

    func get(url:String, params:[String:Any], completion:@escaping(_ status:String, _ data:Data?,_ error:String?)->Void){
        if NetworkReachabilityManager()?.isReachable ?? false{
            let accessToken = UserDefaultHelper.shared.getToken()
            if accessToken != ""{
                let headers: HTTPHeaders = ["Authorization": String.init(format: "Bearer %@", accessToken)]
                let stringURL = String(format: "%@%@", arguments: [Network.shared.getBaseURLAPI(), url])
                
                print(stringURL)
                
                AF.request(stringURL, method: .get, parameters: params ,headers: headers).responseData { (response) in
                    
                    do{
                        let parsedData = try JSONSerialization.jsonObject(with: response.data!) as! Dictionary<String, AnyObject>
                        
                        guard let request = response.request else {return}
                        
                        print(request)
                        
                        // MARK: PRETTY PRINT JSON RESPONSE
                        let jsonData = try JSONSerialization.data(withJSONObject: parsedData, options: .prettyPrinted)
                        print(String(decoding: jsonData, as: UTF8.self))
            
                        switch response.result{
                        case .success:
                            do {
                                let json = try JSON(data: response.data!)
                                if (json["result"].stringValue == "failed"){
                                    completion(NetworkStatus.FAILURE, nil, json["data"].stringValue)
                                    return
                                }
                                completion(NetworkStatus.SUCCESS, response.data, nil)
                            } catch (let err) {
                                completion(NetworkStatus.FAILURE, nil, err.localizedDescription)
                            }
                        case .failure(let error):
                            print(error)
                            if error.isSessionTaskError{
                                 self.loadCount = self.loadCount + 1
                                 if self.loadCount < 10{
                                     self.get(url: url, params: params, completion: completion)
                                 }else{
                                     completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                                 }
                             }else{
                                 completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                            }
                        }
                    }catch (let error){
                        print(error)
                        completion(NetworkStatus.FAILURE, nil, error.localizedDescription)
                    }
                }
            }else{
                // MARK: Logout Process
                UserDefaultHelper.shared.clearUser()
//                UserDefaultHelper.shared.deleteUser()
                
                let loginVC = LoginViewController()
                loginVC.modalPresentationStyle = .fullScreen
                UIApplication.shared.keyWindow?.rootViewController?.present(loginVC, animated: true, completion: nil)
                
                completion(NetworkStatus.FAILURE, nil, ErrMsg.UNAUTHORIZED)
            }
        }else {
            DispatchQueue.main.async {
                completion(NetworkStatus.FAILURE, nil, ErrMsg.NO_INTERNET_CONNECTION)
            }
        }
    }
}
